using UnityEditor;
using UnityEditor.AddressableAssets;

namespace Nighday {

public class AddressableSingletonScriptableAssetEditor {

    public static TType GetForEditor<TType>()
        where TType : AsyncLoadedScriptableObjectSingletonBase {
        var assetKey = typeof(TType).Name;
        var assetPath = GetAssetPathFromAddressablesByAssetKey(assetKey);
        TType asset = null;
        if (assetPath == null) {
            asset = GetAssetPathFromAddressableBundlesByAssetKey<TType>(assetPath);
        } else {
            asset = AssetDatabase.LoadAssetAtPath<TType>(assetPath);
        }

        if (asset == null) {
            UnityEngine.Debug.LogError("Не удается найти " + assetKey + " в Каталоге Addressables.");
        }

        return asset;
    }
    
    private static string GetAssetPathFromAddressablesByAssetKey(string assetKey) {
        var address = assetKey;
        var groups = AddressableAssetSettingsDefaultObject.Settings.groups;
        foreach (var group in groups) {
            foreach (var entry in group.entries) {
                if (address == entry.address) {
                    return entry.AssetPath;
                }
            }
        }
        return default;
    }
    private static TType GetAssetPathFromAddressableBundlesByAssetKey<TType>(string assetKey) {
        //TODO: надо доставать ассет из бандлов в редакторе
        return default;
    }

}
}