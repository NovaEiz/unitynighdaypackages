using System;
using UnityEngine;

[assembly: UnityEngine.Scripting.AlwaysLinkAssembly]
namespace Nighday.Addressables {

public class AddressablesMainThread : MonoBehaviour {

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static void Init() {
        OnUpdateOneTime = null;
        _instance = null;
        var instance = Instance;
    }

    private static AddressablesMainThread _instance;

    public static AddressablesMainThread Instance {
        get {
            if (_instance == null) {
                _instance = new GameObject("AddressablesMainThread").AddComponent<AddressablesMainThread>();
                DontDestroyOnLoad(_instance.gameObject);
            }
            return _instance;
        }
    }

    public static event Action OnUpdateOneTime;

    public void Update() {
        if (OnUpdateOneTime != null) {
            var action = OnUpdateOneTime;
            OnUpdateOneTime = null;
            action?.Invoke();
        }
    }

}

}