using System;
using Nighday.VisualScripting;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Nighday.Addressables {

[UnityEngine.Scripting.Preserve]
[UnitTitle("Addressables.Instantiate")]
[UnitCategory("_Nighday/Addressables")]
public class AssetReferenceInstantiateNode : MultipleCustomEventUnit<EmptyEventArgs> {

    [DoNotSerialize]
    private object _instance;

    [DoNotSerialize]
    public ValueInput assetReferenceInput;

    [DoNotSerialize]
    public ValueInput parentInput;

    [DoNotSerialize]
    public ValueInput instantiateInWorldSpaceInput;

    [DoNotSerialize]
    public ValueOutput instanceOutput;
    
    [DoNotSerialize]
    [PortLabel("Enter")]
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabel("Exit")]
    public ControlOutput exit { get; private set; }

    [DoNotSerialize]
    [PortLabel("OnComplete")]
    public ControlOutput onComplete { get; private set; }


    protected override bool register => true;

    protected override bool ShouldTrigger(Flow flow, EmptyEventArgs args) {
        return true;
    }

    private EventHook GetHook(GraphReference reference) {
        var hook = new EventHook("AssetReferenceInstantiateEvent_" + Guid.NewGuid().ToString(), reference.machine);
        return hook;
    }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        onComplete = ControlOutput(nameof(onComplete));
        
        assetReferenceInput = ValueInput<AssetReference>("assetReference", null);
        parentInput = ValueInput<Transform>("parent", null);
        instantiateInWorldSpaceInput = ValueInput<bool>("instantiateInWorldSpace", false);
        instanceOutput = ValueOutput<object>("instance",
            (flow) => {
                return _instance;
            });
        
        Succession(enter, exit);

        base.Definition();
    }

    private ControlOutput Trigger(Flow flow) {
        var assetReference = flow.GetValue<AssetReference>(assetReferenceInput);
        var instantiateInWorldSpace = flow.GetValue<bool>(instantiateInWorldSpaceInput);

        var reference = flow.stack.ToReference();
        var data = flow.stack.GetElementData<Data>(this);
        var hook = GetHook(reference);
        Data.EventPoint eventPoint = null;
        eventPoint = data.RegisterEventPoint(hook,
            (args) => {
                data.UnregisterEventPoint(eventPoint);
                TriggerOnComplete(reference, args);
            });
        
        Action action = async () => {
            Transform parent = null;
            if (parentInput.hasValidConnection) {
                parent = flow.GetValue<Transform>(parentInput);
            }

            _instance = await assetReference.InstantiateAsync(parent, instantiateInWorldSpace).Task;
            try {
                EventBus.Trigger(hook);
            } catch (Exception e) {
                this.SetException(flow.stack, e);
                UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
            }
        };

        action();

        return exit;
    }
    
    public void TriggerOnComplete(GraphReference reference,
                                  EmptyEventArgs args) {
        var flow = Flow.New(reference);

        if (!ShouldTrigger(flow, args)) {
            flow.Dispose();
            return;
        }

        AssignArguments(flow, args);

        RunOnElement(flow);
    }
    
    private void RunOnElement(Flow flow) {
        if (flow.enableDebug) {
            var editorData = flow.stack.GetElementDebugData<IUnitDebugData>(this);

            editorData.lastInvokeFrame = EditorTimeBinding.frame;
            editorData.lastInvokeTime = EditorTimeBinding.time;
        }

        if (coroutine) {
            flow.StartCoroutine(onComplete,
                flow.stack.GetElementData<Data>(this)
                    .activeCoroutines);
        } else {
            flow.Run(onComplete);
        }
    }

}

}