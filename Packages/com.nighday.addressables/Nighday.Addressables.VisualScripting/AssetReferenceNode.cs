using System;
using System.Collections;
using Nighday.VisualScripting;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Nighday.Addressables {

[UnityEngine.Scripting.Preserve]
[UnitTitle("Addressables.AssetReference")]
[UnitCategory("_Nighday/Addressables")]
public class AssetReferenceNode : MultipleCustomEventUnit<EmptyEventArgs> {

    [DoNotSerialize]
    private AssetReference _assetReference;
    
    [DoNotSerialize]
    public ValueInput assetKeyValueInput;

    [DoNotSerialize]
    public ValueOutput assetReferenceValueOutput;

    [DoNotSerialize]
    public ValueOutput assetValueOutput;

    [DoNotSerialize]
    public ValueOutput assetKeyValueOutput;

    [DoNotSerialize] // Mandatory attribute, to make sure we don’t serialize data that should never be serialized.
    [PortLabel("Enter")]
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabel("Exit")]
    public ControlOutput exit { get; private set; }

    [DoNotSerialize]
    [PortLabel("OnComplete")]
    public ControlOutput onComplete { get; private set; }
    
    public enum AssetType {
        GameObject,
        Texture2D,
        Sprite,
        ScriptableObject,
        UnityObject
    }
    
    [Serialize]
    [Inspectable, UnitHeaderInspectable("AssetType")]
    [InspectorWide]
    public AssetType assetType { get; set; }

    [Serialize]
    [Inspectable, UnitHeaderInspectable("Auto load")]
    [InspectorToggleLeft]
    public bool autoLoad { get; set; }

    protected override bool register => true;

    protected override bool ShouldTrigger(Flow flow, EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook_Complete;
    public EventHook GetHook_Complete(GraphReference reference) {
        _hook_Complete = new EventHook("AssetReferenceCompleted_" + Guid.NewGuid().ToString(), reference.machine);

        return _hook_Complete;
    }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);
        assetKeyValueInput = ValueInput<string>("assetKey", "");
        assetReferenceValueOutput = ValueOutput<AssetReference>("assetReference", (flow) => { return _assetReference; });
        assetValueOutput = ValueOutput<UnityEngine.Object>("asset", (flow) => { return _assetReference.Asset; });
        assetKeyValueOutput = ValueOutput<string>("assetKey_", (flow) => { return (string)_assetReference.RuntimeKey; });
        
        exit = ControlOutput(nameof(exit));
        onComplete = ControlOutput(nameof(onComplete));
        Succession(enter, exit);

        base.Definition();
    }

    public void TriggerOnComplete(GraphReference reference,
                                  EmptyEventArgs args) {
        var flow = Flow.New(reference);

        if (!ShouldTrigger(flow, args)) {
            flow.Dispose();
            return;
        }

        AssignArguments(flow, args);

        RunOnComplete(flow);
    }
    
    private void RunOnComplete(Flow flow) {
        if (flow.enableDebug) {
            var editorData = flow.stack.GetElementDebugData<IUnitDebugData>(this);

            editorData.lastInvokeFrame = EditorTimeBinding.frame;
            editorData.lastInvokeTime = EditorTimeBinding.time;
        }

        if (coroutine) {
            flow.StartCoroutine(onComplete,
                flow.stack.GetElementData<Data>(this)
                    .activeCoroutines);
        } else {
            flow.Run(onComplete);
        }
    }

    private ControlOutput Trigger(Flow flow) {
        var data = flow.stack.GetElementData<Data>(this);

        var reference = flow.stack.ToReference();
        var hook_onComplete = GetHook_Complete(reference);
        var eventPoint_Complete = data.RegisterEventPoint(hook_onComplete, (args) => { TriggerOnComplete(reference, args); });

        var assetKey = flow.GetValue<string>(assetKeyValueInput);

        var assetReference = new AssetReference(assetKey);
        _assetReference = assetReference;
        
        if (autoLoad) {
            Action action = () => {
                // AddressablesMainThread.OnUpdateOneTime += () => {
                    try {
                        EventBus.Trigger(hook_onComplete);
                        data.UnregisterEventPoint(eventPoint_Complete);
                    } catch (Exception e) {
                        this.SetException(flow.stack, e);
                        UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
                    }
                // };
            };

            try {

                switch (assetType) {
                case AssetType.GameObject:
                    assetReference.PerfectLoadAssetAsync<GameObject>((asyncOperationHandle) => {
                        _assetReference = assetReference;
                        action();
                    });
                    break;
                case AssetType.Texture2D:
                    assetReference.PerfectLoadAssetAsync<Texture2D>((asyncOperationHandle) => {
                        _assetReference = assetReference;
                        action();
                    });
                    break;
                case AssetType.Sprite:
                    assetReference.PerfectLoadAssetAsync<Sprite>((asyncOperationHandle) => {
                        _assetReference = assetReference;
                        action();
                    });
                    break;
                case AssetType.ScriptableObject:
                    assetReference.PerfectLoadAssetAsync<ScriptableObject>((asyncOperationHandle) => {
                        _assetReference = assetReference;
                        action();
                    });
                    break;
                case AssetType.UnityObject:
                    assetReference.PerfectLoadAssetAsync<UnityEngine.Object>((asyncOperationHandle) => {
                        _assetReference = assetReference;
                        action();
                    });
                    break;
                }
            } catch (Exception e) {
                this.SetException(flow.stack, e);
                UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
            }
        }

        return exit;
    }

}

}