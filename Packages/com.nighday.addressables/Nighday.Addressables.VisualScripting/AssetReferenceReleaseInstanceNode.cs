using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace Nighday.Addressables {

[UnityEngine.Scripting.Preserve]
[UnitTitle("AssetReference.ReleaseInstance")]
[UnitCategory("_Nighday/Addressables")]
public class AssetReferenceReleaseInstanceNode : Unit {
    
    [DoNotSerialize]
    public ValueInput assetReferenceInput;
    
    [DoNotSerialize]
    public ValueInput gameObjectInput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        assetReferenceInput = ValueInput<AssetReference>("assetReference", null);
        gameObjectInput = ValueInput<GameObject>("gameObject", null);

        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        if (assetReferenceInput.hasValidConnection) {
            var assetReference = flow.GetValue<AssetReference>(assetReferenceInput);
            assetReference.ReleaseAsset();
        }
        if (gameObjectInput.hasValidConnection) {
            var gameObject = flow.GetValue<GameObject>(gameObjectInput);
            UnityEngine.AddressableAssets.Addressables.ReleaseInstance(gameObject);
        }
        
        return exit;
    }


}

}