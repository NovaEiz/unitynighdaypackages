using System;
using System.Collections;
using Nighday.VisualScripting;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace Nighday.Addressables {

[UnityEngine.Scripting.Preserve]
[UnitTitle("Addressables.SceneAssetReference")]
[UnitCategory("_Nighday/Addressables")]
public class SceneAssetReferenceNode : CustomEventUnit<EmptyEventArgs> {

    [DoNotSerialize]
    private SceneInstance _sceneInstance;

    [DoNotSerialize]
    private AssetReference _assetReference;

    [DoNotSerialize]
    public ValueInput assetKeyValueInput;

    [DoNotSerialize]
    public ValueOutput assetReferenceValueOutput;

    [DoNotSerialize]
    public ValueOutput assetValueOutput;

    [DoNotSerialize] // Mandatory attribute, to make sure we don’t serialize data that should never be serialized.
    [PortLabel("Enter")]
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabel("Exit")]
    public ControlOutput exit { get; private set; }

    [Serialize]
    [Inspectable, UnitHeaderInspectable("Auto load")]
    [InspectorToggleLeft]
    public bool autoLoad { get; set; }

    [Serialize]
    [Inspectable, UnitHeaderInspectable("LoadSceneMode")]
    [InspectorWide]
    public LoadSceneMode loadSceneMode { get; set; }

    protected override bool register => true;

    protected override bool ShouldTrigger(Flow flow, EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;
    public override EventHook GetHook(GraphReference reference) {
        if (_hook.Equals(default)) {
            _hook = new EventHook("SceneAssetReferenceCompleted_" + Guid.NewGuid().ToString(), reference.machine);
        }
        return _hook;
    }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);
        assetKeyValueInput = ValueInput<string>("assetKey", "");
        assetReferenceValueOutput = ValueOutput<AssetReference>("assetReference", (flow) => { return _assetReference; });
        assetValueOutput = ValueOutput<SceneInstance>("asset",
            (flow) => {
                return _sceneInstance;
            });

        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);

        base.Definition();
    }

    private ControlOutput Trigger(Flow flow) {
        var assetKey = flow.GetValue<string>(assetKeyValueInput);

        _assetReference = new AssetReference(assetKey);

        if (autoLoad) {
            _assetReference.PerfectLoadSceneAsync(loadSceneMode, (sceneInstance_) => {
                AddressablesMainThread.OnUpdateOneTime += () => {
                    _sceneInstance = sceneInstance_;
                    try {
                        EventBus.Trigger(_hook);
                    } catch (Exception e) {
                        this.SetException(flow.stack, e);
                        UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
                    }
                };
            });
        }

        return exit;
    }

}

}