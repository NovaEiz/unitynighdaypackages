using System;
using Nighday.VisualScripting;
using Unity.VisualScripting;
using UnityEngine.ResourceManagement.ResourceProviders;

namespace Nighday.Addressables {

[UnityEngine.Scripting.Preserve]
[UnitTitle("Addressables.UnloadSceneAsync")]
[UnitCategory("_Nighday/Addressables")]
public class UnloadSceneAsyncNode : CustomEventUnit<EmptyEventArgs> {

    [DoNotSerialize]
    public ValueInput sceneInstanceInput;

    [DoNotSerialize]
    [PortLabel("Enter")]
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabel("Exit")]
    public ControlOutput exit { get; private set; }


    protected override bool register => true;

    protected override bool ShouldTrigger(Flow flow, EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;
    public override EventHook GetHook(GraphReference reference) {
        _hook = new EventHook("UnloadSceneAsyncEvent_" + Guid.NewGuid().ToString(), reference.machine);
        return _hook;
    }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        sceneInstanceInput = ValueInput<object>("sceneInstance", null);

        Succession(enter, exit);

        base.Definition();
    }

    private ControlOutput Trigger(Flow flow) {
        var sceneInstance = (SceneInstance)(flow.GetValue<object>(sceneInstanceInput));

        Action action = async () => {
            if (sceneInstance.Scene.isLoaded && sceneInstance.Scene.IsValid()) {
                await global::UnityEngine.AddressableAssets.Addressables.UnloadSceneAsync(sceneInstance).Task;
            }
            try {
                EventBus.Trigger(_hook);
            } catch (Exception e) {
                this.SetException(flow.stack, e);
                UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
            }
        };

        action();

        return exit;
    }

}

}