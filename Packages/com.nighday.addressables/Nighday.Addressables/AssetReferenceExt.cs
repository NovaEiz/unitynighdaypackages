using System.Threading.Tasks;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace Nighday {

public static class AssetReferenceExt {
    public static async Task<T> PerfectLoadAssetAsync<T>(this AssetReference assetReference)
        where T : UnityEngine.Object
    {
        if (assetReference.Asset == null) {
            if (assetReference.OperationHandle.IsValid() && !assetReference.OperationHandle.IsDone) {
                while (assetReference.Asset == null) {
                    await Task.Yield();
                }
            } else {
                await assetReference.LoadAssetAsync<T>().Task;
            }
        }

        return (T)assetReference.Asset;
    }
    public static void PerfectLoadAssetAsync<T>(this AssetReference assetReference, System.Action<T> callback)
        where T : UnityEngine.Object
    {
        if (assetReference.Asset == null) {
            if (assetReference.OperationHandle.IsValid() && !assetReference.OperationHandle.IsDone) {
                assetReference.OperationHandle.Completed += (_) => {
                    callback(_.Result as T);
                };
                // while (assetReference.Asset == null) {
                //     await Task.Yield();
                // }
            } else {
                assetReference.LoadAssetAsync<T>().Completed += (_) => {
                    callback(_.Result as T);
                };
            }
        } else {
            callback((T)assetReference.Asset);
        }
    }
    
    public static void PerfectLoadSceneAsync(this AssetReference assetReference, LoadSceneMode loadSceneMode, System.Action<SceneInstance> callback)
    {
        if (assetReference.Asset == null) {
            if (assetReference.OperationHandle.IsValid() && !assetReference.OperationHandle.IsDone) {
                assetReference.OperationHandle.Completed += (_) => {
                    callback((SceneInstance)_.Result);
                };
                // while (assetReference.Asset == null) {
                //     await Task.Yield();
                // }
            } else {
                assetReference.LoadSceneAsync(loadSceneMode).Completed += (_) => {
                    callback((SceneInstance)_.Result);
                };
            }
        } else {
            callback((SceneInstance)((object)assetReference.Asset));
        }
    }
}
}