using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Nighday {

    public class AsyncLoadedEnumBase<TType, TEnumItem> : AsyncLoadedScriptableObjectSingleton<TType> 
        where TType : AsyncLoadedScriptableObjectSingleton<TType>
        where TEnumItem : IEnumItem
    {

        public virtual TEnumItem[] Items => null;
	
        public TEnumItem GetOnce(int value) {
            foreach (var item in Items) {
                if (item == null) {
                    continue;
                }
                if ((value & item.Index) != 0) {
                    return item;
                }
            }

            return default;
        }
	
        public TEnumItem GetOnce(string key) {
            foreach (var item in Items) {
                if (item == null) {
                    continue;
                }
                if (item.Key == key) {
                    return item;
                }
            }

            return default;
        }

        public List<TEnumItem> GetListSortByIndex() {
            var enumItems = this.Items;
            var items = new List<TEnumItem>();
            if (enumItems.Length > 0) {
                items = enumItems.Where((i) => {
                    if (i == null || string.IsNullOrEmpty(i.Key)) {
                        return false;
                    }
                    return true;
                }).OrderBy(i => {
                    return i.Index;
                }).Select((i) => {
                    return i;
                }).ToList();
            }

            return items;
        }
	
    }

}