using UnityEditor;
using UnityEngine;

namespace Nighday {

public class AsyncLoadedScriptableObjectSingletonBase : ScriptableObject {
    public virtual void Constructor() {
    }
}

public partial class AsyncLoadedScriptableObjectSingleton<TType> : AsyncLoadedScriptableObjectSingletonBase
    where TType : AsyncLoadedScriptableObjectSingleton<TType> {
    public static TType _instance = null;

    // public static TType Instance {
    //     get {
    //         return _instance;
    //     }
    // }

    
    public static TType Instance {
        get {
            // if (_instance == null) {
            //     _instance = AddressableSingletonScriptableAsset.Get<TType>();
            // }
            //
            return _instance;
        }
    }

    public override void Constructor() {
        _instance = this as TType;
        OnCreate();
    }

    protected virtual void OnCreate() {
    }
}
}