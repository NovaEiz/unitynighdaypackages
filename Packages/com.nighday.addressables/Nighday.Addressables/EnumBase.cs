using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Nighday {

public interface IEnumItem {
	string                      Key   { get; set; }
	int                         Index { get; set; }
}

[Serializable]
public class EnumItem : IEnumItem {
	[SerializeField] private string _key;
	[SerializeField] private int    _index;

	public string Key {
		get => _key;
		set => _key = value;
	}
	public int Index {
		get => _index;
		set => _index = value;
	}
}

public class EnumBase<TType, TEnumItem> : ScriptableObjectSingleton<TType> 
	where TType : ScriptableObjectSingleton<TType>
	where TEnumItem : IEnumItem
{

	public virtual TEnumItem[] Items => null;
	
	public TEnumItem GetOnce(int value) {
		foreach (var item in Items) {
			if ((value & item.Index) != 0) {
				return item;
			}
		}

		return default;
	}
	
	public TEnumItem GetOnce(string key) {
		foreach (var item in Items) {
			if (item.Key == key) {
				return item;
			}
		}

		return default;
	}

	public List<TEnumItem> GetListSortByIndex() {
		var enumItems = this.Items;
		var items = new List<TEnumItem>();
		if (enumItems.Length > 0) {
			items = enumItems.Where((i) => {
				if (i == null || string.IsNullOrEmpty(i.Key)) {
					return false;
				}
				return true;
			}).OrderBy(i => {
				return i.Index;
			}).Select((i) => {
				return i;
			}).ToList();
		}

		return items;
	}
	
}

}