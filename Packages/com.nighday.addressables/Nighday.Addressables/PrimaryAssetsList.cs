using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Nighday {

[CreateAssetMenu(fileName = "PrimaryAssetsList", menuName = "Nighday/Addressables/PrimaryAssetsList", order = 0)]
public class PrimaryAssetsList : ScriptableObject {

    internal static PrimaryAssetsList _instance;
    public static PrimaryAssetsList Instance => _instance;

    [SerializeField] private List<AssetReference> _assetReferenceList = new List<AssetReference>();

    public List<AssetReference> AssetReferenceList => _assetReferenceList;

}

public class PrimaryAssetsListInit {

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static async void Init() {
        return;
        var primaryAssetsListAssetReference = new AssetReference("PrimaryAssetsList");
        var primaryAssetsList = await primaryAssetsListAssetReference.LoadAssetAsync<PrimaryAssetsList>().Task;
        Debug.Log("PrimaryAssetsList Init 1");
        List<Task> _tasks = new List<Task>();

        foreach (var assetReference in primaryAssetsList.AssetReferenceList) {
			Debug.Log("assetReference = " + assetReference);
            if (assetReference.Asset == null) {
                if (assetReference.OperationHandle.IsValid() && !assetReference.OperationHandle.IsDone) {
                    while (!assetReference.IsDone) {
                        await Task.Yield();
                    }
                } else {
                    var task = assetReference.LoadAssetAsync<ScriptableObject>().Task;
                    _tasks.Add(task);
                }
            }
        }
        Debug.Log("PrimaryAssetsList Init 2");

        await Task.WhenAll(_tasks);
        Debug.Log("PrimaryAssetsList Init 3");

        foreach (var task in _tasks) {
            var res = await (task as Task<ScriptableObject>);
            if (res is AsyncLoadedScriptableObjectSingletonBase singletonSO) {
                singletonSO.Constructor();
                
                Debug.Log("PrimaryAssetsList Init 3-1 res = " + res);

            }
            //_objects.Add(res);
        }
        Debug.Log("PrimaryAssetsList Init 4");

        PrimaryAssetsList._instance = primaryAssetsList;
    }
    
}

}