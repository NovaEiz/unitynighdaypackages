using System;
using System.Collections.Generic;
using UnityEngine;

namespace Nighday {

public class ScriptableObjectSingleton<TType> : ScriptableObject
	where TType : ScriptableObject
{

	public static TType _instance = null;
	public static TType Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = Resources.Load<TType>(typeof(TType).Name);
			}

			return _instance;
		}
	}
	
}

}