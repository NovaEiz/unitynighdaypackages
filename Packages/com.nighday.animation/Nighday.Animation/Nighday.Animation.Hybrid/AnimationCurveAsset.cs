using UnityEngine;

namespace Nighday.Animation.Hybrid {
[CreateAssetMenu(fileName = "AnimationCurveAsset", menuName = "Nighday/UI Utils/AnimationCurveAsset", order = 0)]
public class AnimationCurveAsset : ScriptableObject {

	[SerializeField] private AnimationCurve _animationCurve;

	public AnimationCurve AnimationCurve => _animationCurve;

}
}