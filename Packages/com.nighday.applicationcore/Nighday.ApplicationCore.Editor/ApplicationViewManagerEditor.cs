using System;
using Nighday.UIToolkit;
using UnityEditor;

namespace Nighday.ApplicationCore {

[CustomEditor(typeof(ApplicationViewManager))]
public class ApplicationViewManagerEditor : Editor {

    public override void OnInspectorGUI() {
        var target_ = (target as ApplicationViewManager);
        target_.OnValidate();
        
        base.OnInspectorGUI();
    }

    // protected override void DrawInspector(Action onChanged) {
    //     UnityEngine.Debug.Log("ApplicationManagerEditor > Awake = " + this);
    //
    //     var applicationManager = (target as ApplicationViewManager);
    //     applicationManager.OnValidate();
    //     
    //     base.DrawInspector(onChanged);
    // }

}

}