using System;
using Nighday.UIToolkit;
using UnityEditor;

namespace Nighday.ApplicationCore {

[CustomEditor(typeof(View))]
public class ViewEditor : Editor {

    public override void OnInspectorGUI() {
        var target_ = (target as View);
        target_.OnValidate();
        
        base.OnInspectorGUI();
    }

    // protected override void DrawInspector(Action onChanged) {
    //     UnityEngine.Debug.Log("ApplicationManagerEditor > Awake = " + this);
    //
    //     var applicationManager = (target as ApplicationViewManager);
    //     applicationManager.OnValidate();
    //     
    //     base.DrawInspector(onChanged);
    // }

}

}