using Unity.VisualScripting;
using UnityEngine;

[assembly: UnityEngine.Scripting.AlwaysLinkAssembly]
namespace Nighday.ApplicationCore {

[UnityEngine.Scripting.Preserve]
[UnitTitle("SystemInfo.DeviceId")]
[UnitCategory("_Nighday/ApplicationCore")]
public class ApplicationCoreDeviceIdNode : Unit {

    [DoNotSerialize]
    public ValueOutput deviceIdOutput;
    
    protected override void Definition()
    {
        deviceIdOutput = ValueOutput<string>("deviceId", (flow) => { return GetDeviceId(); });
    }

    private string GetDeviceId() {
        var deviceId = SystemInfo.deviceUniqueIdentifier;
        if (DevelopmentDeviceUniqueIdentifierComponent.Instance != null) {
            deviceId += DevelopmentDeviceUniqueIdentifierComponent.Instance.additionDeviceId;
        }

        return deviceId;
    }
    
}

}