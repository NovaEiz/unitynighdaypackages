using Unity.VisualScripting;

namespace Nighday.ApplicationCore {

[UnityEngine.Scripting.Preserve]
[UnitTitle("NotificationCenter.SendEvent")]
[UnitCategory("_Nighday/ApplicationCore/NotificationCenter")]
public class NotificationCenterSendEventNode : Unit {

    [DoNotSerialize]
    public ValueInput keyInput;
    
    [DoNotSerialize]
    public ValueInput senderInput;
    
    [DoNotSerialize]
    public ValueInput eventDataInput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        keyInput = ValueInput<string>("key", null);
        senderInput = ValueInput<object>("sender", null);
        eventDataInput = ValueInput<object>("eventData", null);
        
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var key = flow.GetValue<string>(keyInput);
        object sender = null;
        object eventData = null;
        if (senderInput.hasValidConnection) {
            sender = flow.GetValue<object>(senderInput);
        }
        if (eventDataInput.hasValidConnection) {
            eventData = flow.GetValue<object>(eventDataInput);
        }

        NotificationCenter.SendEvent(key, sender, eventData);

        return exit;
    }

    
}

}