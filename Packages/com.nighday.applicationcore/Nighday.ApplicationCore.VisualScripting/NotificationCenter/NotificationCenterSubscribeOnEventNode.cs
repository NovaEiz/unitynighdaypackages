using System;
using Nighday.VisualScripting;
using Unity.VisualScripting;

namespace Nighday.ApplicationCore {

[UnityEngine.Scripting.Preserve]
[UnitTitle("NotificationCenter.SubscribeOnEvent")]
[UnitCategory("_Nighday/ApplicationCore/NotificationCenter")]
public class NotificationCenterSubscribeOnEventNode : CustomEventUnit<EmptyEventArgs> {
    
    [DoNotSerialize]
    private object _sender;
    
    [DoNotSerialize]
    private object _eventData;
    
    [DoNotSerialize]
    private Action<object, object> _subscribedCallback;
    
    [DoNotSerialize]
    public ValueInput keyInput;

    [DoNotSerialize]
    public ValueOutput senderOutput;

    [DoNotSerialize]
    public ValueOutput eventDataOutput;

    [DoNotSerialize]
    public ValueOutput subscribedCallbackOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    [Serialize]
    [Inspectable, UnitHeaderInspectable("Once call")]
    [InspectorToggleLeft]
    public bool onceCall { get; set; }
    
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow flow, EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;
    public override EventHook GetHook(GraphReference reference) {
        _hook = new EventHook("NotificationCenterSubscribeOnEvent_Event_" + Guid.NewGuid().ToString(), reference.machine);
        return _hook;
    }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        keyInput = ValueInput<string>("key", null);
        
        senderOutput = ValueOutput<object>("sender", (flow) => { return _sender;});
        eventDataOutput = ValueOutput<object>("eventData", (flow) => { return _eventData;});
        subscribedCallbackOutput = ValueOutput<object>("subscribedCallback", (flow) => { return _subscribedCallback;});
        
        Succession(enter, exit);

        base.Definition();
    }
    
    private ControlOutput Trigger(Flow flow) {
        var key = flow.GetValue<string>(keyInput);

        _subscribedCallback = (sender,
                               eventData) => {
            if (onceCall) {
                NotificationCenter.UnsubscribeOnEvent(key, _subscribedCallback);
            }
            _sender = sender;
            _eventData = eventData;
            EventBus.Trigger(_hook);
        };
        
        NotificationCenter.SubscribeOnEvent(key, _subscribedCallback);

        return exit;
    }

    public override void Dispose() {
        base.Dispose();
    }

}

}