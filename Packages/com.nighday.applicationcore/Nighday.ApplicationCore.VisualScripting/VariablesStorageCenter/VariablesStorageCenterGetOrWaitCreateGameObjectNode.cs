using System;
using Nighday.VisualScripting;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.ApplicationCore {

[UnityEngine.Scripting.Preserve]
[UnitTitle("VariablesStorageCenter.GetOrWaitCreateGameObject")]
[UnitCategory("_Nighday/ApplicationCore/VariablesStorageCenter")]
public class VariablesStorageCenterGetOrWaitCreateGameObjectNode : CustomEventUnit<EmptyEventArgs> {
    
    [DoNotSerialize]
    private object _value;
    
    [DoNotSerialize]
    public ValueInput keyInput;

    [DoNotSerialize]
    public ValueOutput valueOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow flow, EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;
    public override EventHook GetHook(GraphReference reference) {
        _hook = new EventHook("VariablesStorageCenterGetOrWaitCreateGameObjectEvent_" + Guid.NewGuid().ToString(), reference.machine);
        return _hook;
    }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        keyInput = ValueInput<string>("key", null);
        
        valueOutput = ValueOutput<object>("value", (flow) => { return _value;});
        
        Succession(enter, exit);

        base.Definition();
    }
    
    private ControlOutput Trigger(Flow flow) {
        try {
            var key = flow.GetValue<string>(keyInput);

            Action<object> action = null;
            action = (value_) => {
                _value = value_;
                try {
                    EventBus.Trigger(_hook);
                } catch (Exception e) {
                    this.SetException(flow.stack, e);
                    UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
                }
            };

            var obj = VariablesStorageCenter.GetGameObject(key);
            if (obj != null) {
                action(obj);
            } else {
                VariablesStorageCenter.SubscribeOnCreateVariable(key, action);
            }
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }

        return exit;
    }

    
}

}