using System;
using Unity.VisualScripting;

namespace Nighday.ApplicationCore {

[UnityEngine.Scripting.Preserve]
[UnitTitle("VariablesStorageCenter.GetVariable")]
[UnitCategory("_Nighday/ApplicationCore/VariablesStorageCenter")]
public class VariablesStorageCenterGetVariableNode : Unit {

    [DoNotSerialize]
    private object _value;
    
    [DoNotSerialize]
    public ValueInput keyInput;
    
    [DoNotSerialize]
    public ValueOutput valueOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        keyInput = ValueInput<string>("key", null);
        valueOutput = ValueOutput<object>("value", (flow) => { return _value; });
        
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        try {
            var key = flow.GetValue<string>(keyInput);
            _value = VariablesStorageCenter.GetVariable(key);
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }

        return exit;
    }

    
}

}