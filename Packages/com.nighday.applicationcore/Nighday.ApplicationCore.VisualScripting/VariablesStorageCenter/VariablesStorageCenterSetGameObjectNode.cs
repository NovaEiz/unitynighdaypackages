using System;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.ApplicationCore {

[UnityEngine.Scripting.Preserve]
[UnitTitle("VariablesStorageCenter.SetGameObject")]
[UnitCategory("_Nighday/ApplicationCore/VariablesStorageCenter")]
public class VariablesStorageCenterSetGameObjectNode : Unit {
    
    [DoNotSerialize]
    public ValueInput keyInput;
    
    [DoNotSerialize]
    public ValueInput valueInput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        keyInput = ValueInput<string>("key", null);
        valueInput = ValueInput<GameObject>("value", null);
        
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        try {
            var key = flow.GetValue<string>(keyInput);
            var value = flow.GetValue<GameObject>(valueInput);
        
            VariablesStorageCenter.SetGameObject(key, value);
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }

        return exit;
    }

    
}

}