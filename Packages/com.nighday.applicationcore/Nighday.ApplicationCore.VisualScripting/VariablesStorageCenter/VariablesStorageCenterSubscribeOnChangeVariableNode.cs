using System;
using Nighday.VisualScripting;
using Unity.VisualScripting;

namespace Nighday.ApplicationCore {

[UnityEngine.Scripting.Preserve]
[UnitTitle("VariablesStorageCenter.SubscribeOnChangeVariable")]
[UnitCategory("_Nighday/ApplicationCore/VariablesStorageCenter")]
public class VariablesStorageCenterSubscribeOnChangeVariableNode : CustomEventUnit<EmptyEventArgs> {
    
    [DoNotSerialize]
    private object _value;
    
    [DoNotSerialize]
    private object _previousValue;
    
    [DoNotSerialize]
    public ValueInput keyInput;

    [DoNotSerialize]
    public ValueOutput valueOutput;

    [DoNotSerialize]
    public ValueOutput previousValueOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    [Serialize]
    [Inspectable, UnitHeaderInspectable("Call Once")]
    [InspectorWide]
    public bool callOnce { get; set; }
    
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow flow, EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;
    public override EventHook GetHook(GraphReference reference) {
        _hook = new EventHook("VariablesStorageCenterSubscribeOnChangeValueEvent_" + Guid.NewGuid().ToString(), reference.machine);
        return _hook;
    }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        keyInput = ValueInput<string>("key", null);
        
        valueOutput = ValueOutput<object>("value", (flow) => { return _value;});
        previousValueOutput = ValueOutput<object>("previousValue", (flow) => { return _previousValue;});
        
        Succession(enter, exit);

        base.Definition();
    }
    
    private ControlOutput Trigger(Flow flow) {
        try {
            var key = flow.GetValue<string>(keyInput);

            Action<object, object> action = null;
            action = (newValue_, previousValue_) => {
                _value = newValue_;
                _previousValue = previousValue_;
                EventBus.Trigger(_hook);
            };

            if (callOnce) {
                VariablesStorageCenter.SubscribeOnceChangeVariable(key, action);
            } else {
                VariablesStorageCenter.SubscribeOnChangeVariable(key, action);
            }
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }

        return exit;
    }

    
}

}