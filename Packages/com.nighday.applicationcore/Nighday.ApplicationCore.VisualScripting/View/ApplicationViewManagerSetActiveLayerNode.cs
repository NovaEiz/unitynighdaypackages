using System;
using Unity.VisualScripting;

namespace Nighday.ApplicationCore {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ApplicationViewManager.SetActiveLayer")]
[UnitCategory("_Nighday/ApplicationCore/ApplicationViewManager")]
public class ApplicationViewManagerSetActiveLayerNode : Unit {

    [DoNotSerialize]
    public ValueInput layerValueInput;

    [DoNotSerialize]
    public ValueInput stateValueInput;

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        layerValueInput = ValueInput<string>("layer", null);
        stateValueInput = ValueInput<bool>("state", false);

        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        Succession(enter, exit);
    }

    private ControlOutput Trigger(Flow flow) {
        try {
            var layer = flow.GetValue<string>(layerValueInput);
            var state = flow.GetValue<bool>(stateValueInput);
        
            ApplicationViewManager.Instance.SetActiveViewLayer(layer, state);
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }

        return exit;
    }

}

}