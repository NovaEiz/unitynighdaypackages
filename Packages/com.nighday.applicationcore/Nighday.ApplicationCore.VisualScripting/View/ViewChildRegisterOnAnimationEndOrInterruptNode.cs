using System;
using Nighday.VisualScripting;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.ApplicationCore {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ViewChild.RegisterOnAnimationEndOrInterrupt")]
[UnitCategory("_Nighday/ApplicationCore/View")]
public class ViewChildRegisterOnAnimationEndOrInterruptNode : CustomEventUnit<EmptyEventArgs> {

    [DoNotSerialize]
    public ValueInput viewChildInput;

    [DoNotSerialize]
    public ValueInput animationNameInput;

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override bool register => true;

    protected override bool ShouldTrigger(Flow           flow,
                                          EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;

    public override EventHook GetHook(GraphReference reference) {
        _hook = new EventHook("ViewChildRegisterOnAnimationEndEvent_" + Guid.NewGuid()
                                  .ToString(),
            reference.machine);

        return _hook;
    }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        viewChildInput = ValueInput<object>("viewChild", null);
        animationNameInput = ValueInput<string>("animationName", null);

        Succession(enter, exit);

        base.Definition();
    }

    private ControlOutput Trigger(Flow flow) {
        try {
            var viewChildObj = flow.GetValue<object>(viewChildInput);
            var animationName = flow.GetValue<string>(animationNameInput);

            ViewChild viewChild = null;
            if (viewChildObj is ViewChild viewChild_) {
                viewChild = viewChild_;
            }

            if (viewChildObj is GameObject viewGameObject_) {
                viewChild = viewGameObject_.GetComponent<ViewChild>();
            }

            viewChild.RegisterOnAnimationEndOrInterrupt((animationName_) => {
                Debug.Log("animationName_ = " + animationName_);
                if (animationName != animationName_) {
                    return;
                }

                try {
                    EventBus.Trigger(_hook);
                } catch (Exception e) {
                    this.SetException(flow.stack, e);
                    UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
                }
            });
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }

        return exit;
    }

}

}