using System;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.ApplicationCore {

[UnityEngine.Scripting.Preserve]
[UnitTitle("View.CurrentStateAnimationIsName")]
[UnitCategory("_Nighday/ApplicationCore/View")]
public class ViewCurrentStateAnimationIsNameNode : Unit {

    [DoNotSerialize]
    private bool _result;

    [DoNotSerialize]
    public ValueInput viewInput;

    [DoNotSerialize]
    public ValueInput animationNameInput;

    [DoNotSerialize]
    public ValueOutput resultInput;

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        viewInput = ValueInput<object>("view");
        animationNameInput = ValueInput<string>("animationName", null);

        resultInput = ValueOutput<bool>("result",
            (flow) => {
                return _result;
            });

        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        Succession(enter, exit);
    }

    private ControlOutput Trigger(Flow flow) {
        try {
            var viewObj = flow.GetValue<object>(viewInput);
            var animationName = flow.GetValue<string>(animationNameInput);

            View view = null;
            if (viewObj is View view_) {
                view = view_;
            } if (viewObj is GameObject viewGameObject_) {
                view = viewGameObject_.GetComponent<View>();
            }
            _result = view.CurrentStateAnimationIsName(animationName);
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }

        return exit;
    }

}

}