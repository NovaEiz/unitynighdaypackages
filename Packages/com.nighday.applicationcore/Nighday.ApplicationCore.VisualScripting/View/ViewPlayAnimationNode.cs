using System;
using Nighday.VisualScripting;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.ApplicationCore {

[UnityEngine.Scripting.Preserve]
[UnitTitle("View.PlayAnimation")]
[UnitCategory("_Nighday/ApplicationCore/View")]
public class ViewPlayAnimationNode : CustomEventUnit<EmptyEventArgs> {
    
    [DoNotSerialize]
    public ValueInput viewInput;
    
    [DoNotSerialize]
    public ValueInput animationNameInput;
    
    [DoNotSerialize]
    public ValueInput elapsedTimeInput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow flow, EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;
    public override EventHook GetHook(GraphReference reference) {
        _hook = new EventHook("ViewPlayAnimationEvent_" + Guid.NewGuid().ToString(), reference.machine);
        return _hook;
    }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
            
        viewInput = ValueInput<object>("view", null);
        animationNameInput = ValueInput<string>("animationName", null);
        elapsedTimeInput = ValueInput<float>("elapsedTime", 0f);
        
        Succession(enter, exit);

        base.Definition();
    }
    
    private ControlOutput Trigger(Flow flow) {
        try {
            var viewObj = flow.GetValue<object>(viewInput);
            var animationName = flow.GetValue<string>(animationNameInput);

            float elapsedTime = 0f;
            if (elapsedTimeInput.hasValidConnection) {
                elapsedTime = flow.GetValue<float>(elapsedTimeInput);
            }

            View view = null;
            if (viewObj is View view_) {
                view = view_;
            } if (viewObj is GameObject viewGameObject_) {
                view = viewGameObject_.GetComponent<View>();
            }
            view.PlayAnimation(animationName, elapsedTime,
                () => {
                    try {
                        EventBus.Trigger(_hook);
                    } catch (Exception e) {
                        this.SetException(flow.stack, e);
                        UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
                    }
                });
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }

        return exit;
    }

    
}

}