using UnityEngine;

namespace Nighday.ApplicationCore {

[RequireComponent(typeof(AudioSource))]
public class AddAudioSourceAndSetAudioClipFromAudioAsset : MonoBehaviour {
	[SerializeField] private AudioAsset _audioAsset;
	[SerializeField] private bool _isMusic;

	private AudioSource _audioSource;
	
	private void Awake() {
		_audioSource = gameObject.GetComponent<AudioSource>();
		_audioSource.clip = _audioAsset.Clip;
		_audioSource.volume = _audioAsset.Volume;

		AudioSystem.AddAudioSource(_audioSource, _isMusic);
		if (_audioSource.playOnAwake) _audioSource.Play();
	}

}
}