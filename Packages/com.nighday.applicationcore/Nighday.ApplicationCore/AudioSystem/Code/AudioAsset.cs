using UnityEngine;

namespace Nighday.ApplicationCore {

[CreateAssetMenu(fileName = "AudioAsset", order = 0, menuName = "Nighday/Audio/AudioAsset")]
public class AudioAsset : ScriptableObject {

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
    public static void StaticInitialize() {
	    _audioSystem = null;
    }

	private static AudioSystem _audioSystem;

	private static AudioSystem AudioSystem {
		get {
			if (_audioSystem == null) {
				_audioSystem = AudioSystem.instance;
			}

			return _audioSystem;
		}
	}

	[SerializeField] private AudioClip _audioClip;

	[Range(0f, 1f)]
	[SerializeField] private float _volume = 1;
	
	public virtual AudioClip Clip => _audioClip;
	public float Volume => _volume;

	public bool Playing { get; protected set; }

	public virtual AudioSourceInPool Play(PlayMode playMode, Vector3 point = default) {
		Playing = true;
		return AudioSystem.Play(this, playMode, point);
	}

	public void PlayAtPoint(Vector3 point) {
		AudioSystem.PlayAtPoint(this, point);
	}
	
	public void PlayAtPoint(Vector3 point, float volume) {
		AudioSystem.PlayAtPoint(this, point, volume);
	}

	public void Stop() {
		Playing = false;
		AudioSystem.Stop(this);
	}

}
}