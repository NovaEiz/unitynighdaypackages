using System;
using System.Collections.Generic;
using UnityEngine;

namespace Nighday.ApplicationCore {

public static class ArrayUtils {

	public static T RandomItem<T>(this IList<T> items) {
		return items.Count > 0 ? items[UnityEngine.Random.Range(0, items.Count)] : default;
	}
}

[CreateAssetMenu(fileName = "AudioListAsset", order = 0, menuName = "Nighday/Audio/AudioListAsset")]
public class AudioListAsset : AudioAsset {

	private static AudioSystem _audioSystem;

	private static AudioSystem AudioSystem {
		get {
			if (_audioSystem == null) {
				_audioSystem = AudioSystem.instance;
			}

			return _audioSystem;
		}
	}

	[SerializeField] private List<AudioClip> _audioClipList;

	[NonSerialized] private int _currentClipIndex = 0;
	public AudioClip[] Clips => _audioClipList.ToArray();

	public override AudioClip Clip => _audioClipList.RandomItem();

	// if (_currentClipIndex >= _audioClipList.Count) {
	// 	_currentClipIndex = 0;
	// }
	//
	// var clip = _audioClipList[_currentClipIndex];
	// _currentClipIndex++;
	// return clip;
	
	public override AudioSourceInPool Play(PlayMode playMode, Vector3 point = default) {
		if (AudioSystem == null) {
			return null;
		}
		Playing = true;
		return AudioSystem.Play(this, playMode, point);
	}

}
}