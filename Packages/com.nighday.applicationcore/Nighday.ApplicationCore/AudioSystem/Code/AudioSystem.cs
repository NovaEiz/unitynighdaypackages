﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Nighday.ApplicationCore {

public class AudioSourceInPool {
	private AudioSource _audioSource;
	public AudioSource AudioSource => _audioSource;
	public bool likeMusic;
	
	public event Action OnIntoPool;

	public void IntoPool() {
		OnIntoPool?.Invoke();
	}

	public AudioSourceInPool(AudioSource audioSource) {
		_audioSource = audioSource;
	}
	
}
public class AudioSourcePool : MonoBehaviour {
	private List<AudioSourceInPool> _itemsInPool = new List<AudioSourceInPool>();
	private List<AudioSourceInPool> _itemsOutPool = new List<AudioSourceInPool>();

	public List<AudioSourceInPool> ItemsInPool => _itemsInPool;
	public List<AudioSourceInPool> ItemsOutPool => _itemsOutPool;

	private Dictionary<AudioAsset, AudioSourceInPool> _dict = new Dictionary<AudioAsset, AudioSourceInPool>();
	

	public AudioSourceInPool Get() {
		if (_itemsInPool.Count == 0) {
			var newObj = new GameObject();
			newObj.transform.SetParent(transform);
			var audioSourceInPool = new AudioSourceInPool(newObj.AddComponent<AudioSource>());
			return audioSourceInPool;
		}

		var item = _itemsInPool[0];
		item.OnIntoPool += () => {
			_itemsInPool.Add(item);
		};
		_itemsInPool.RemoveAt(0);
		return item;
	}

	public AudioSourceInPool Get(AudioAsset audioAsset) {
		if (_dict.TryGetValue(audioAsset, out AudioSourceInPool audioSourceInPool_)) {
			return audioSourceInPool_;
		}

		AudioSourceInPool audioSourceInPool = null;
		if (_itemsInPool.Count == 0) {
			var newObj = new GameObject("for : " + audioAsset.name);
			newObj.transform.SetParent(transform);
			audioSourceInPool = new AudioSourceInPool(newObj.AddComponent<AudioSource>());
			// _itemsInPool.Add(audioSourceInPool);
		} else {
			audioSourceInPool = _itemsInPool[0];
			audioSourceInPool.OnIntoPool += () => {
				_itemsInPool.Add(audioSourceInPool);
			};
			_itemsInPool.RemoveAt(0);
		}
		
		_dict.Add(audioAsset, audioSourceInPool);

		return audioSourceInPool;
	}
	
}

public class AudioSystem : MonoBehaviour {

	private static AudioSystem _instance;

	public static AudioSystem instance {
		get {
			if (_instance == null) {
				var gameObject = new GameObject("AudioSystem");
				_instance = gameObject.AddComponent<AudioSystem>();
				DontDestroyOnLoad(gameObject);
			}

			return _instance;
		}
	}

	private AudioSourceInPool _audioSourceInPool;

	private AudioSource _audioSource;
	private AudioSource AudioSource {
		get {
			if (_audioSource == null) {
				_audioSource = _audioSourceInPool.AudioSource;
			}

			return _audioSource;
		}
	}

	private AudioSourcePool _audioSourcePool;

	private void Awake() {
		_instance = this;
		
		_audioSourcePool = gameObject.AddComponent<AudioSourcePool>();
		_audioSourceInPool = _audioSourcePool.Get();

		_musicState = GetSavedMusicState();
		AudioSource.enabled = GetSavedSoundState();
	}

	public AudioSourceInPool Play(AudioAsset audioAsset, PlayMode playMode, Vector3 point = default) {
		switch (playMode) {
			case PlayMode.Play:
				return Play(audioAsset);
				break;
			case PlayMode.PlayRepeat:
				return PlayRepeat(audioAsset);
				break;
			case PlayMode.PlayOneShot:
				return null;
				break;
			case PlayMode.PlayRepeatAtPoint:
				return PlayRepeatAtPoint(audioAsset, point);
				break;
		}

		return null;
	}

	public void Stop(AudioAsset audioAsset) {
		var audioSourceInPool = _audioSourcePool.Get(audioAsset);
		audioSourceInPool.AudioSource.Stop();
		audioSourceInPool.IntoPool();
	}

	public AudioSourceInPool Play(AudioAsset audioAsset) {
		var audioSourceInPool = _audioSourcePool.Get(audioAsset);
		audioSourceInPool.AudioSource.enabled = _musicState;
		audioSourceInPool.AudioSource.clip = audioAsset.Clip;
		audioSourceInPool.AudioSource.volume = audioAsset.Volume;
		audioSourceInPool.AudioSource.Play();
		audioSourceInPool.likeMusic = true;
		
		AddAudioSource(audioSourceInPool.AudioSource, true);

		return audioSourceInPool;
	}

	public AudioSourceInPool PlayRepeat(AudioAsset audioAsset) {
		var audioSourceInPool = _audioSourcePool.Get(audioAsset);
		audioSourceInPool.AudioSource.enabled = _musicState;
		audioSourceInPool.AudioSource.clip = audioAsset.Clip;
		audioSourceInPool.AudioSource.volume = audioAsset.Volume;
		//AudioSource.loop = true;
		audioSourceInPool.AudioSource.Play();
		audioSourceInPool.likeMusic = true;

		StartCoroutine(WaitEndClip(audioSourceInPool.AudioSource, () => {
			if (audioAsset.Playing) {
				PlayRepeat(audioAsset);
			}
		}));

		return audioSourceInPool;
	}

	public AudioSourceInPool PlayRepeatAtPoint(AudioAsset audioAsset, Vector3 point) {
		var audioSourceInPool = _audioSourcePool.Get(audioAsset);
		audioSourceInPool.AudioSource.enabled = _musicState;
		audioSourceInPool.AudioSource.clip = audioAsset.Clip;
		audioSourceInPool.AudioSource.volume = audioAsset.Volume;
		audioSourceInPool.AudioSource.spatialBlend = 1f;
		audioSourceInPool.AudioSource.dopplerLevel = 0;
		audioSourceInPool.AudioSource.transform.position = point;
		audioSourceInPool.AudioSource.Play();
		Debug.Log("point = " + point);

		StartCoroutine(WaitEndClip(audioSourceInPool.AudioSource, () => {
			if (audioAsset.Playing) {
				PlayRepeatAtPoint(audioAsset, point);
			}
		}));

		return audioSourceInPool;
	}

	private IEnumerator WaitEndClip(AudioSource audioSource, Action callback) {
		while (audioSource.isPlaying || !audioSource.enabled) {
			yield return null;
		}
		yield return null;

		callback();
	}

	public void PlayOneShot(AudioAsset audioAsset) {
		AudioSource.PlayOneShot(audioAsset.Clip, audioAsset.Volume);
	}

	public void PlayAtPoint(AudioAsset audioAsset, Vector3 point) {
		if (!SoundState) return;
		AudioSource.PlayClipAtPoint(audioAsset.Clip, point, audioAsset.Volume);
	}
	
	public void PlayAtPoint(AudioAsset audioAsset, Vector3 point, float volume) {
		if (!SoundState) return;
		AudioSource.PlayClipAtPoint(audioAsset.Clip, point, volume);
	}
	

	private List<AudioSource> _addedAudioSourcesLikeSound = new List<AudioSource>();
	private List<AudioSource> _addedAudioSourcesLikeMusic = new List<AudioSource>();

	public bool SoundState => AudioSource.enabled;
	
	private bool _musicState = true;
	public bool MusicState => _musicState;
	
	public void SetSoundState(bool enabled) {
		AudioSource.Stop();
		AudioSource.enabled = enabled;
		
		SetSavedSoundState(enabled);
	}
	
	public void SetMusicState(bool enabled) {
		if (_musicState != enabled) {
			foreach (var item in _audioSourcePool.ItemsInPool) {
				if (item.likeMusic) {
					item.AudioSource.enabled = enabled;
				}
			}
			foreach (var item in _audioSourcePool.ItemsOutPool) {
				if (item.likeMusic) {
					item.AudioSource.enabled = enabled;
				}
			}
		}
		
		_musicState = enabled;
		
		SetSavedMusicState(enabled);
	}

	public static void AddAudioSource(AudioSource audioSource, bool music = false) {
		instance.AddAudioSource_(audioSource, music);
	}

	private void AddAudioSource_(AudioSource audioSource, bool music = false) {
		if (music) {
			_addedAudioSourcesLikeMusic.Add(audioSource);

			audioSource.enabled = MusicState;
		} else {
			_addedAudioSourcesLikeSound.Add(audioSource);

			audioSource.enabled = SoundState;
		}
	}

#region MyRegion

	private string _savedSoundStateKey = "SoundState";
	private string _savedMusicStateKey = "MusicState";

	private bool GetSavedSoundState() {
		return !Convert.ToBoolean(PlayerPrefs.GetInt(_savedSoundStateKey));
	}

	private bool GetSavedMusicState() {
		return !Convert.ToBoolean(PlayerPrefs.GetInt(_savedMusicStateKey));
	}

	private void SetSavedSoundState(bool value) {
		PlayerPrefs.SetInt(_savedSoundStateKey, Convert.ToInt32(!value));
	}

	private void SetSavedMusicState(bool value) {
		PlayerPrefs.SetInt(_savedMusicStateKey, Convert.ToInt32(!value));
	}

#endregion
	
}

}