using System;
using UnityEngine;
using UnityEngine.UI;

namespace Nighday.ApplicationCore {

public class PlayAudioByClickButtonEvent : MonoBehaviour {
	[SerializeField] private PlayMode _playMode;
	[SerializeField] private AudioAsset _audioAsset;
	
	[Header("(Не обязательные поля)")]
	[SerializeField] private Button _button;

	private void Awake() {
		if (_button == null) {
			_button = GetComponent<Button>();
		}
		if (_button == null) {
			return;
		}
		_button.onClick.AddListener(Run);
	}

	private void Run() {
		try {
			_audioAsset.Play(_playMode);
		} catch (Exception e) {
			UnityEngine.Debug.LogException(e, gameObject);
		}
	}

}
}