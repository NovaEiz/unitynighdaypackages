using System;
using UnityEngine;
using UnityEngine.UI;

namespace Nighday.ApplicationCore {

public class PlayAudioByClickOnButtonOrToggle : MonoBehaviour {
	[SerializeField] private PlayMode _playMode;
	[SerializeField] private AudioAsset _audioAsset;
	
	private Button _button;
	private Button button {
		get {
			if (_button == null) {
				_button = transform.GetComponent<Button>();
				if (_button == null) {
					_button = transform.parent.GetComponent<Button>();
				}
			}

			return _button;
		}
	}
	
	private Toggle _toggle;
	private Toggle toggle {
		get {
			if (_toggle == null) {
				_toggle = transform.GetComponent<Toggle>();
				if (_toggle == null) {
					_toggle = transform.parent.GetComponent<Toggle>();
				}
			}

			return _toggle;
		}
	}

	private void Awake() {
		var button = this.button;
		if (button != null) {
			button.onClick.AddListener(Run);
			return;
		}
		var toggle = this.toggle;
		if (toggle != null) {
			toggle.onValueChanged.AddListener((newValue) => {
				Run();
			});
			return;
		}
	}

	private void Run() {
		try {
			_audioAsset.Play(_playMode);
		} catch (Exception e) {
			UnityEngine.Debug.LogException(e, gameObject);
		}
	}

}
}