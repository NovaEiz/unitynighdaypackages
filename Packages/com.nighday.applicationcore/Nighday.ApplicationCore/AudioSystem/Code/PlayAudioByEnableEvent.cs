using System.Threading.Tasks;
using UnityEngine;

namespace Nighday.ApplicationCore {

public class PlayAudioByEnableEvent : MonoBehaviour {

	[SerializeField] private PlayMode   _playMode;
	[SerializeField] private AudioAsset _audioAsset;
	[SerializeField] private bool _audioPointUpdate;

	private AudioSourceInPool _audioSourceInPool;

	private async void OnEnable() {
		while (_audioSourceInPool == null) {
			_audioSourceInPool = _audioAsset.Play(_playMode, transform.position);
			await Task.Yield();
		}
		
	}
	
	private void OnDisable() {
		_audioAsset.Stop();
		_audioSourceInPool = null;
	}

	private void Update() {
		if (_audioPointUpdate) {
			_audioSourceInPool.AudioSource.transform.position = transform.position;
		}
	}

}
}