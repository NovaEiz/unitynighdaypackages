using System;
using System.Collections.Generic;
using UnityEngine;

namespace Nighday.ApplicationCore {

public class PlayAudioCalledByViewAnimatorEvents : MonoBehaviour {

    [SerializeField] private ViewAnimator _viewAnimator;

    private ViewAnimator viewAnimator {
        get {
            if (_viewAnimator == null) {
                _viewAnimator = transform.GetComponent<ViewAnimator>();
                if (_viewAnimator == null) {
                    _viewAnimator = transform.parent.GetComponent<ViewAnimator>();
                }
            }

            return _viewAnimator;
        }
    }

    private ViewAnimationEventDispatcher viewAnimationEventDispatcher => viewAnimator.viewAnimationEventDispatcher;

    [Serializable]
    public class Item {
        [SerializeField] public PlayMode   playMode;
        [SerializeField] public AudioAsset audioAsset;
        [SerializeField] public string runByStartAnimationName;

        public void Run() {
            audioAsset.Play(playMode);
        }
    }

    [SerializeField] private List<Item> _items = new List<Item>();

    private void Awake() {
        viewAnimationEventDispatcher.OnAnimationStart.AddListener((animationName_) => {
            foreach (var item_ in _items) {
                var item = item_;
                if (item.runByStartAnimationName == animationName_) {
                    item.Run();
                }
            }
        });
    }

}
}