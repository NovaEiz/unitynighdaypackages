using UnityEngine;

namespace Nighday.ApplicationCore {

public class PlayAudioCalledFromCode : MonoBehaviour {
	[SerializeField] private PlayMode   _playMode;
	[SerializeField] private AudioAsset _audioAsset;
	
	public void Run() {
		_audioAsset.Play(_playMode);
	}

}
}