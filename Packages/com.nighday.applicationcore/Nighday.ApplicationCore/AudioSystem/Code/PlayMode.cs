
namespace Nighday.ApplicationCore {

public enum PlayMode {
	Play,
	PlayRepeat,
	PlayOneShot,
	PlayRepeatAtPoint,
	PlayAtPoint
}

}