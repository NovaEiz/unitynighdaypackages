using System;
using UnityEngine;

namespace Nighday.ApplicationCore {

public class DevelopmentDeviceUniqueIdentifierComponent : MonoBehaviour {

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
    public static void StaticInitialization() {
        Instance = null;
    }

    public static DevelopmentDeviceUniqueIdentifierComponent Instance { get; private set; }
    
    [SerializeField] private string _additionDeviceId;

    public string additionDeviceId => _additionDeviceId;

    private void Awake() {
        Instance = this;
    }
    
}

}