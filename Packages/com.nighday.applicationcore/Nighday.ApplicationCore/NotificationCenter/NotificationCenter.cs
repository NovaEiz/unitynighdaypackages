using System;
using System.Collections.Generic;
using UnityEngine;

namespace Nighday.ApplicationCore {

public class NotificationCenter {

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
    public static void StaticInitialization() {
        _subscribes = new Dictionary<int, Action<object, object>>();
    }

    private static Dictionary<int, Action<object, object>> _subscribes;
    
    public static void SendEvent(string keyStr, object sender = null, object eventData = null) {
        var key = GetItemCode(keyStr);
        if (!_subscribes.TryGetValue(key, out var callback_)) {
            return;
        }

        callback_(sender, eventData);
    }
    
    public static void SubscribeOnEvent(string keyStr, Action<object, object> callback) {
        var key = GetItemCode(keyStr);
        if (!_subscribes.TryGetValue(key, out var callback_)) {
            callback_ = (_,
                         __) => { };
            _subscribes.Add(key, callback_);
        }
        
        _subscribes[key] += callback;
    }
    
    public static void UnsubscribeOnEvent(string keyStr, Action<object, object> callback) {
        var key = GetItemCode(keyStr);
        if (!_subscribes.TryGetValue(key, out var callback_)) {
            return;
        }

        _subscribes[key] -= callback;
    }
    
    private static int GetItemCode(string id) {
        return System.StringComparer.OrdinalIgnoreCase.GetHashCode(id);
    }
    
}

}