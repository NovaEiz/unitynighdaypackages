using System;
using UnityEngine;

namespace Nighday.ApplicationCore {

public class SetGameObjectToVariablesStorageCenterComponent : MonoBehaviour {

    [SerializeField] private string _key;

    private void Awake() {
        if (string.IsNullOrEmpty(_key)) {
            _key = name;
        }
        VariablesStorageCenter.SetGameObject(_key, gameObject);
    }

    private void OnDestroy() {
        VariablesStorageCenter.RemoveGameObject(_key);
    }

}

}