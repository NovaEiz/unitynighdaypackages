using System;
using System.Collections.Generic;
using UnityEngine;

namespace Nighday.ApplicationCore {

public class VariablesStorageCenter {

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
    public static void StaticInitialize() {
        _dictSubscribesOnCreate = new Dictionary<string, Action<object>>();
        _dictSubscribesOnChange = new Dictionary<string, Action<object, object>>();
        _dictVariables = new Dictionary<string, object>();
        // _dictGameObjects = new Dictionary<string, GameObject>();
    }

    private static Dictionary<string, Action<object>>         _dictSubscribesOnCreate;
    private static Dictionary<string, Action<object, object>> _dictSubscribesOnChange;
    private static Dictionary<string, object>                 _dictVariables;
    // private static Dictionary<string, GameObject> _dictGameObjects;
    
    public static void SubscribeOnCreateVariable(string key, Action<object> callback) {
        if (!_dictSubscribesOnCreate.TryGetValue(key, out var callback_)) {
            _dictSubscribesOnCreate.Add(key, (_) => {});
        }

        _dictSubscribesOnCreate[key] += callback;
    }
    
    private static void RunOnCreateVariable(string key, object obj) {
        Debug.Log("RunOnCreateVariable key = " + key + "; obj = " + obj);
        if (!_dictSubscribesOnCreate.TryGetValue(key, out var callback_)) {
            return;
        }

        callback_(obj);
    }
    
    public static void SubscribeOnChangeVariable(string key, Action<object, object> callback) {
        if (!_dictSubscribesOnChange.TryGetValue(key, out var callback_)) {
            _dictSubscribesOnChange.Add(key, (_, __) => {});
        }

        _dictSubscribesOnChange[key] += callback;
    }
    
    public static void SubscribeOnceChangeVariable(string key, Action<object, object> callback) {
        if (!_dictSubscribesOnChange.TryGetValue(key, out var callback_)) {
            _dictSubscribesOnChange.Add(key, (_, __) => {});
        }

        Action<object, object> action = null;
        action = (_, __) => {
            _dictSubscribesOnChange[key] -= action;

            callback(_, __);
        };
        _dictSubscribesOnChange[key] += action;
    }
    
    public static void UnsubscribeOnChangeVariable(string key, Action<object, object> callback) {
        if (!_dictSubscribesOnChange.TryGetValue(key, out var callback_)) {
            return;
        }

        _dictSubscribesOnChange[key] -= callback;
    }
    
    private static void RunOnChangedVariable(string key, object newValue, object previousValue) {
        if (!_dictSubscribesOnChange.TryGetValue(key, out var callback_)) {
            return;
        }

        callback_(newValue, previousValue);
    }

    public static void SetVariable(string key, object value) {
        if (!_dictVariables.TryGetValue(key, out var value_)) {
            _dictVariables.Add(key, value);
            RunOnCreateVariable(key, value);
        } else {
            _dictVariables[key] = value;
        }

        if (value != value_) {
            RunOnChangedVariable(key, value, value_);
        }
    }

    public static object GetVariable(string key) {
        if (!_dictVariables.TryGetValue(key, out var value_)) {
            return null;
        }

        return value_;
    }

    public static GameObject GetGameObject(string key) {
        return GetVariable(key) as GameObject;
    }

    public static void SetGameObject(string name, GameObject gameObject) {
        SetVariable(name, gameObject);
    }

    public static void RemoveGameObject(string name) {
        if (!_dictVariables.TryGetValue(name, out var value_)) {
            return;
        }

        _dictVariables.Remove(name);
    }
    
    
    
}

}