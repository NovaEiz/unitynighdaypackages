using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace Nighday.ApplicationCore {

public class AddUICameraToThisCameraStack : MonoBehaviour {
    
    private Camera _camera;

    public Camera camera {
        get {
            if (_camera == null) {
                _camera = GetComponent<Camera>();
            }

            return _camera;
        }
    }
    
    private Camera _targetCamera;

    public Camera targetCamera {
        get {
            return _targetCamera;
        }
    }
    
    // private void Start() {
    //     SetUICameraToStack();
    // }
    //
    // private async void SetUICameraToStack() {
    //     UICamera uiCamera = null;
    //     while ((uiCamera = UICamera.Instance) == null) {
    //         await Task.Yield();
    //     }
    //     _targetCamera = uiCamera.camera;
    //     var cameraData = camera.GetUniversalAdditionalCameraData();
    //     if (!cameraData.cameraStack.Contains(targetCamera)) {
    //         cameraData.cameraStack.Add(targetCamera);
    //     }
    // }
    //
    // private void OnDisable() {
    //     if (_targetCamera == null) {
    //         return;
    //     }
    //     var cameraData = _targetCamera.GetUniversalAdditionalCameraData();
    //     cameraData.renderType = CameraRenderType.Base;
    // }
    //
    // private async void OnEnable() {
    //     UICamera uiCamera = null;
    //     while ((uiCamera = UICamera.Instance) == null) {
    //         await Task.Yield();
    //     }
    //     _targetCamera = uiCamera.camera;
    //     
    //     var cameraData = _targetCamera.GetUniversalAdditionalCameraData();
    //     cameraData.renderType = CameraRenderType.Overlay;
    // }

}

}