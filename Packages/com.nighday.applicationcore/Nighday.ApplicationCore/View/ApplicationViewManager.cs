using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Nighday.ApplicationCore {

public class ApplicationViewManager : MonoBehaviour {

    private static Action<ApplicationViewManager> _onAdd;

    public static void GetOrWaitAddition(Action<ApplicationViewManager> callback) {
        if (Instance == null) {
            _onAdd += callback;
            return;
        }

        callback(Instance);
    }
    
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
    public static void RuntimeInitClear() {
        Instance = null;
        _onAdd = null;
    }
    
    public static ApplicationViewManager Instance { get; private set; }
    
    
    // [SerializeField] private Transform _uiLayersContainer;
    // [SerializeField] private Transform _environmentLayersContainer;
    [SerializeField] private ViewChild _layersContainer;

    public ViewChild layersContainer => _layersContainer;
    

    [Header("Debug fields")]
    [SerializeField] private List<ViewLayer> _layers = new List<ViewLayer>();

    public void SetActiveViewLayer(string layerName, bool state) {
        if (this == null || gameObject == null) {
            return;
        }
        foreach (var layer in _layers) {
            if (layer.name == layerName) {
                layer.SetActive(state);
                return;
            }
        }
        Debug.LogError("Нет слоя layerName = " + layerName);
    }
    
    public ViewLayer GetLayer_(string layerName) {
        foreach (var layer in _layers) {
            if (layer.name == layerName) {
                return layer;
            }
        }

        return null;
    }

    public static ViewLayer GetLayer(string layerName) {
        return Instance.GetLayer_(layerName);
    }
    
    
    private static Transform _viewsContainer;
    public Transform ViewsContainer {
        get {
            if (_viewsContainer == null) {
                _viewsContainer = new GameObject("ViewsContainer").transform;
                _viewsContainer.SetParent(transform);
            }

            return _viewsContainer;
        }
    }

    private void Awake() {
        if (Instance != null) {
            gameObject.SetActiveRecursively(false);
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        Instance = this;
        _onAdd?.Invoke(Instance);
        _onAdd = null;

        var viewsContainer = ViewsContainer;

#if UNITY_EDITOR
        OnValidate();
#endif

    }

    private void OnEnable() {
    }

#if UNITY_EDITOR

    public void OnValidate() {
        _layers.Clear();
        if (_layersContainer != null) {
            foreach (Transform childTr in _layersContainer.transform) {
                var childTrName = childTr.name;
                var list = _layers.Where((i) => { return i.name == childTrName; })
                    .ToList();
                var res = list.Count() == 0;

                ViewLayer layer = null;
                if (res) {
                    layer = childTr.gameObject.GetComponent<ViewLayer>();
                    if (!layer) {
                        layer = childTr.gameObject.AddComponent<ViewLayer>();
                    }
                } else {
                    layer = list[0];
                }

                layer.applicationViewManager = this;
                _layers.Add(layer);
            }
        }
        // if (_environmentLayersContainer != null) {
        //     foreach (Transform childTr in _environmentLayersContainer) {
        //         var childTrName = childTr.name;
        //         var list = _layers.Where((i) => { return i.name == childTrName; })
        //             .ToList();
        //         var res = list.Count() == 0;
        //
        //         ViewLayer layer = null;
        //         if (res) {
        //             layer = new ViewLayer();
        //             layer.name = childTrName;
        //             _layers.Add(layer);
        //         } else {
        //             layer = list[0];
        //         }
        //         layer.environmentLayer = childTr;
        //     }
        // }

        // foreach (var layer in _layers) {
        //     layer.applicationViewManager = this;
        // }
    }

#endif
    
}

}