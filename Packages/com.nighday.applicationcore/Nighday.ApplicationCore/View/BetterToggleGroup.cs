using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Nighday.ApplicationCore {

public class BetterToggleGroup : ToggleGroup {

    public List<Toggle> toggles => m_Toggles;

    private int _previousActiveToggleIndex = -1;
    
    private ToggleViewControllerComponent _lastShowAnimation;

    private ToggleViewControllerComponent _currentHideAnimation;
    private ToggleViewControllerComponent _currentShowAnimation;

    private ToggleViewControllerComponent _nextHideAnimation;
    private ToggleViewControllerComponent _nextShowAnimation;

    private bool _isRunning;

    protected override void OnEnable() {
        base.OnEnable();

        OnEnableAfterOneFrame();
        
    }

    private async void OnEnableAfterOneFrame() {
        await Task.Yield();
        
        var currentActiveToggleIndex = -1;
        var toggles = this.toggles;
        var len = toggles.Count;
        for (var i = 0; i < len; i++) {
            if (toggles[i].isOn) {
                currentActiveToggleIndex = i;
                break;
            }
        }

        if (currentActiveToggleIndex != -1) {
            var toggleViewControllerComponent = toggles[currentActiveToggleIndex].GetComponent<ToggleViewControllerComponent>();
            UpdateStateToggles(toggleViewControllerComponent.ifOnEnableToggleIsOnTrueThenSetElapsedTime, true);
        }
    }

    public void UpdateStateToggles(float elapsedTime = 0f, bool onEnable = false) {
        var currentActiveToggleIndex = -1;
        var toggles = this.toggles;
        var len = toggles.Count;
        for (var i = 0; i < len; i++) {
            if (toggles[i].isOn) {
                currentActiveToggleIndex = i;
                break;
            }
        }

        if (_previousActiveToggleIndex != currentActiveToggleIndex) {
            if (_previousActiveToggleIndex != -1) {
                var toggleManyViewsControllerComponentPrevious = toggles[_previousActiveToggleIndex].GetComponent<ToggleViewControllerComponent>();
                var toggleManyViewsControllerComponentCurrent = toggles[currentActiveToggleIndex].GetComponent<ToggleViewControllerComponent>();
                
                if (_lastShowAnimation != null) {
                    toggleManyViewsControllerComponentPrevious = _lastShowAnimation;
                }
                
                AddToHideAnimationQueue(toggleManyViewsControllerComponentPrevious, elapsedTime);
                AddToShowAnimationQueue(toggleManyViewsControllerComponentCurrent, elapsedTime);
            } else {
                var toggleManyViewsControllerComponentCurrent = toggles[currentActiveToggleIndex].GetComponent<ToggleViewControllerComponent>();
                AddToShowAnimationQueue(toggleManyViewsControllerComponentCurrent, elapsedTime);
            }
            
            _previousActiveToggleIndex = currentActiveToggleIndex;
        } else if (onEnable) {
            if (currentActiveToggleIndex != -1) {
                var toggleManyViewsControllerComponentCurrent = toggles[currentActiveToggleIndex].GetComponent<ToggleViewControllerComponent>();
                AddToShowAnimationQueue(toggleManyViewsControllerComponentCurrent, elapsedTime);
            }
        }
    }

    public void AddToHideAnimationQueue(ToggleViewControllerComponent toggle, float elapsedTime = 0f) {
        _nextHideAnimation = toggle;
        
        CheckForRun(true, elapsedTime);
    }

    public void AddToShowAnimationQueue(ToggleViewControllerComponent toggle, float elapsedTime = 0f) {
        _nextShowAnimation = toggle;

        CheckForRun(true, elapsedTime);
    }

    private void CheckForRun(bool first = true, float elapsedTime = 0f) {
        if (_isRunning && first) {
            return;
        }

        _isRunning = true;
        if (_nextHideAnimation != null && _nextShowAnimation != null) {
            var nextHideAnimation = _nextHideAnimation;
            _nextHideAnimation = null;
            nextHideAnimation.Hide(() => {
                if (_nextShowAnimation != null) {
                    var nextShowAnimation = _nextShowAnimation;
                    _lastShowAnimation = nextShowAnimation;

                    _nextShowAnimation = null;
                    nextShowAnimation.Show(() => {
                        CheckForRun(false);
                    }, elapsedTime);
                }
            }, elapsedTime);
        } else if (_nextShowAnimation != null) {
            var nextShowAnimation = _nextShowAnimation;
            _nextShowAnimation = null;
            nextShowAnimation.Show(() => {
                CheckForRun(false);
            }, elapsedTime);
        } else {
            _isRunning = false;
        }
    }

}

}