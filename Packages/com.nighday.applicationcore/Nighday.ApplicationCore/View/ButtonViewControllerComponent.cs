using UnityEngine;
using UnityEngine.UI;

namespace Nighday.ApplicationCore {

public class ButtonViewControllerComponent : MonoBehaviour {

    [SerializeField] protected GameObject _viewChildOrAnimator;

    [Space]
    [SerializeField] private string _animationName;

    private Button _button;

    protected virtual void Awake() {
        _button = GetComponent<Button>();

        _button.onClick.AddListener(OnClick);
    }

    protected virtual void OnClick() {
        if (_viewChildOrAnimator == null) {
            return;
        }

        _button.interactable = false;
        
        var animationName = _animationName;

        var viewChild = _viewChildOrAnimator.GetComponent<ViewChild>();
        if (viewChild) {
            viewChild.PlayAnimation(animationName,
                0,
                () => {
                    if (_button != null) {
                        _button.interactable = true;
                    }
                });
        } else {
            var animator = _viewChildOrAnimator.GetComponent<ViewAnimator>();
            if (animator) {
                animator.PlayAnimation(animationName,
                    0,
                    () => {
                        if (_button != null) {
                            _button.interactable = true;
                        }
                    });
            } else {
                _button.interactable = true;
            }
        }
    }

}

}