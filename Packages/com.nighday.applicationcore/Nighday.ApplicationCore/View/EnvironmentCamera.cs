using System;
using System.Collections.Generic;
using System.Linq;
using Nighday.UI;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace Nighday.ApplicationCore {

public class EnvironmentCamera : MonoBehaviour {

    private static Action<List<EnvironmentCamera>> _onEnableCamera;
    private static Action<List<EnvironmentCamera>> _onDisableCamera;

    private static List<UICamera> _stack;

    public static EnvironmentCamera GetActiveEnvironmentCamera() {
        var priority = 0;
        EnvironmentCamera environmentCamera = null;
        foreach (var environmentCamera_ in _environmentCameraList) {
            if (environmentCamera_.camera.depth > priority) {
                environmentCamera = environmentCamera_;
            }
        }

        return environmentCamera;
    }

    public static void OnEnableCamera(Action<List<EnvironmentCamera>> action) {
        _onEnableCamera += action;
    }

    public static void OnDisableCamera(Action<List<EnvironmentCamera>> action) {
        _onDisableCamera += action;
    }

    public static List<EnvironmentCamera> _environmentCameraList;
    
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
    public static void RuntimeInitClear() {
        _onEnableCamera = null;
        _onDisableCamera = null;
        _environmentCameraList = new List<EnvironmentCamera>();
        _stack = new List<UICamera>();
        
        ApplicationViewManager.GetOrWaitAddition((applicationViewManager) => {
            applicationViewManager.layersContainer.OnUpdateOrderViewChildren += () => {
                AddUICameraToStack2();
            };
        });
    }

    public static void AddUICameraToStack(UICamera uiCamera_) {
        var layers = ApplicationViewManager.Instance.transform.Find("View/Layers");
        _stack.Clear();
        foreach (var uiCamera in layers.GetComponentsInChildren<UICamera>()) {
            _stack.Add(uiCamera);
        }
        
        foreach (var environmentCamera in _environmentCameraList) {
            environmentCamera.UpdateCameraStack();
        }
        return;
        if (!_stack.Contains(uiCamera_)) {
            _stack.Add(uiCamera_);

            foreach (var environmentCamera in _environmentCameraList) {
                environmentCamera.UpdateCameraStack();
            }
        }
    }

    public static void AddUICameraToStack2() {
        var layers = ApplicationViewManager.Instance.transform.Find("View/Layers");
        _stack.Clear();
        foreach (var uiCamera in layers.GetComponentsInChildren<UICamera>(true)) {
            _stack.Add(uiCamera);
        }
        
        foreach (var environmentCamera in _environmentCameraList) {
            environmentCamera.UpdateCameraStack();
        }
    }

    private void UpdateCameraStack() {
        var array = _stack.Select((i) => { return i.camera; }).ToArray();
        var environmentCameraData = this.camera.GetUniversalAdditionalCameraData();
        environmentCameraData.cameraStack.Clear();
        environmentCameraData.cameraStack.AddRange(array);
    }
    
    private void Awake() {
    }

    private void OnEnable() {
        foreach (var environmentCamera_ in _environmentCameraList) {
            var audioListener = environmentCamera_.GetComponent<AudioListener>();
            audioListener.enabled = false;
        }

        var audioListener_ = gameObject.GetComponent<AudioListener>();
        if (audioListener_ != null) {
            audioListener_.enabled = true;
        }
        
        _environmentCameraList.Add(this);
        _onEnableCamera?.Invoke(_environmentCameraList);
        UpdateCameraStack();

        ActiveCameraForRaycast.camera = GetActiveEnvironmentCamera()
            .camera;
    }

    private void OnDisable() {
        _environmentCameraList.Remove(this);
        _onDisableCamera?.Invoke(_environmentCameraList);
    }

    //===
    
    private Camera _camera;

    public Camera camera {
        get {
            if (_camera == null) {
                _camera = GetComponent<Camera>();
            }

            return _camera;
        }
    }

}

}