using System;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace Nighday.ApplicationCore {

public class HorizontalUpdateOrderViewChildOverrideComponent : UpdateOrderViewChildOverrideComponent {

    [SerializeField] private float _progress;

    private void SetProgressViewChild(ViewChild viewChild, float childProgress) {
        foreach (Transform viewChildTr in viewChild.transform) {
            var viewChildCanvas = viewChildTr.GetComponent<Canvas>();
            if (viewChildCanvas == null) {
                continue;
            }
                
            var viewChildCanvasTr = viewChildCanvas.transform;

            if (viewChildCanvasTr.childCount == 0) {
                continue;
            }
            Transform viewChildCanvasContainerTr = viewChildCanvasTr.GetChild(0);
            if (viewChildCanvasContainerTr == null) {
                continue;
            }
            RectTransform viewChildCanvasContainerRectTr = viewChildCanvasContainerTr as RectTransform;

            var anchorMin = viewChildCanvasContainerRectTr.anchorMin;
            var anchorMax = viewChildCanvasContainerRectTr.anchorMax;

            anchorMin.x = childProgress;
            anchorMax.x = childProgress + 1f;
                
            viewChildCanvasContainerRectTr.anchorMin = anchorMin;
            viewChildCanvasContainerRectTr.anchorMax = anchorMax;
        }
    }

    public override void UpdateOrderViewChildren(ViewChild viewChild_) {
        base.UpdateOrderViewChildren(viewChild_);
        
        var list = viewChild_.viewChilds;

        var progress = -_progress;
        
        var currentPositionX = 0f;
        foreach (var viewChild in list) {
            var childProgress = progress + currentPositionX;

            foreach (var viewChildChild in viewChild.gameObject.GetComponentsInChildren<ViewChild>()) {
                SetProgressViewChild(viewChildChild, childProgress);
            }
            SetProgressViewChild(viewChild, childProgress);
            
            currentPositionX += 1;
        }
    }

    private void UpdateProgress() {
        var maxProgress = 0f;
        
        var viewChild = GetComponent<ViewChild>();
        if (viewChild != null) {
            var count = viewChild.viewChilds.Count;
            maxProgress = (count - 1);
        }
        if (_progress > maxProgress) {
            _progress = maxProgress;
        }
        
        if (_progress < 0) {
            _progress = 0;
        }
    }

#if UNITY_EDITOR

    private void OnValidate() {
        UpdateProgress();
        
        UpdateOrderViewChildren(GetComponent<ViewChild>());
    }

#endif
    
}

}