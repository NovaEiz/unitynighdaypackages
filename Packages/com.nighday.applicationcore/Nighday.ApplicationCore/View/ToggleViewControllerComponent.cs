using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;
using System.Threading.Tasks;

namespace Nighday.ApplicationCore {

public class ToggleViewControllerComponent : MonoBehaviour {

    [SerializeField] private List<GameObject> _viewChildOrViewAnimatorTargets;
    [SerializeField] private List<string> _viewAssetKeys;
    
    private Dictionary<string, GameObject> _assetByViewAssetKeys = new Dictionary<string, GameObject>();
    
    [Space]
    [SerializeField] private float _ifOnEnableToggleIsOnTrueThenSetElapsedTime;
    [SerializeField] private float _ifOnEnableToggleIsOnFalseThenSetElapsedTime;
    
    public float ifOnEnableToggleIsOnTrueThenSetElapsedTime => _ifOnEnableToggleIsOnTrueThenSetElapsedTime;
    
    [Space]
    [SerializeField] private bool  _waitViewAnimationEndOtherToggle;
    
    protected Toggle _toggle;
    
    private bool _showingIsOn;
    private bool _shownIsOn;
    private bool _isOn;
    
    private Action _currentAnimationTaskWaitCallback;

    protected virtual void Awake() {
        _toggle = GetComponent<Toggle>();
        _isOn = _toggle.isOn;

        _toggle.onValueChanged.AddListener(ToggleOnValueChanged);
    }

    protected virtual void ToggleOnValueChanged(bool newValue) {
        if (newValue == _isOn) {
            return;
        }

        _isOn = newValue;
        
        _toggle.interactable = false;
        if (!newValue) {
            _toggle.interactable = true;
        }
        
        RemoveNullElements();
        
        OnChanged(newValue, 0);
    }

    // private async void QWE(GameObject gameObject) {
    //     gameObject.SetActive(false);
    //     await System.Threading.Tasks.Task.Yield();
    //     gameObject.SetActive(true);
    //     Debug.Break();
    // }

    private bool _initialized;
    private void FirstInitialize() {
        if (_initialized) {
            return;
        }

        foreach (var viewChildOrViewAnimatorTarget in _viewChildOrViewAnimatorTargets) {
            viewChildOrViewAnimatorTarget.GetComponent<ViewChild>().UnregisterOnAnimationStart(OnChangeToHide);
        }

        _initialized = true;
        if (_viewChildOrViewAnimatorTargets.Count > 0) {
            var viewChildOrViewAnimatorTarget = _viewChildOrViewAnimatorTargets[0];
            viewChildOrViewAnimatorTarget.GetComponent<ViewChild>().RegisterOnAnimationStart(OnChangeToHide);
        }
    }

    private void OnChangeToHide(string animationName_) {
        if (animationName_ == "Hide") {
            _toggle.SetIsOnWithoutNotify(false);
            _isOn = _toggle.isOn;
        }
    }

    private void LoadAssets(Action onEnd) {
        var count = 0;
        Action checkEnd = () => {
            count--;
            if (count == 0) {
                onEnd();
            }
        };
        
        foreach (var viewAssetKey in _viewAssetKeys) {
            var viewAssetKey_ = viewAssetKey;
            var findResult = _assetByViewAssetKeys.TryGetValue(viewAssetKey_, out var instance_);

            if (!findResult || instance_ == null) {
                count++;
                if (!findResult) {
                    _assetByViewAssetKeys.Add(viewAssetKey_, null);
                } else {
                    _initialized = false;
                }

                LoadAsset(viewAssetKey_,
                    (asset__) => {
                        var instance = GameObject.Instantiate(asset__);
                        _viewChildOrViewAnimatorTargets.Add(instance);
                        _assetByViewAssetKeys[viewAssetKey_] = instance;

                        checkEnd();
                    });
            }
        }
        
        if (count == 0) {
            onEnd();
        }
    }

    private void LoadAsset(string assetKey, Action<GameObject> callback) {
        var assetReference = new AssetReference(assetKey);
        assetReference.PerfectLoadAssetAsync<GameObject>(callback);
    }

    protected virtual void OnEnable() {
        if (!_waitViewAnimationEndOtherToggle) {
            if (_toggle.isOn) {
                OnChanged(_toggle.isOn, _ifOnEnableToggleIsOnTrueThenSetElapsedTime, true);
            } else {
                OnChanged(_toggle.isOn, _ifOnEnableToggleIsOnFalseThenSetElapsedTime, true);
            }
        }
    }

    private void RemoveNullElements() {
        var len = _viewChildOrViewAnimatorTargets.Count;
        for (var i = 0; i < len; i++) {
            if (_viewChildOrViewAnimatorTargets[i] == null) {
                _viewChildOrViewAnimatorTargets.RemoveAt(i);
                i--;
                len--;
            }
        }
    }

    public void Hide(Action onEnd, float elapsedTime = 0f) {
        var newValue = true;
        var animationName = "Hide";
        
        var count = 0;
        Action checkEnd = () => {
            count--;
            if (count == 0) {
                if (_toggle != null) {
                    _toggle.interactable = true;
                }
                _shownIsOn = newValue;
                
                _currentAnimationTaskWaitCallback?.Invoke();
                _currentAnimationTaskWaitCallback = null;

                onEnd();
            }
        };

        Action action = () => {
            _showingIsOn = _toggle.isOn;

            foreach (var item_ in _viewChildOrViewAnimatorTargets) {
                var viewChildOrViewAnimator = item_;
                count++;
                
                if (newValue) {
                    viewChildOrViewAnimator.gameObject.SetActive(true);

                    var view = viewChildOrViewAnimator.GetComponent<View>();
                    if (view != null) {
                        view.SetSiblingLast();
                    } else {
                        if (viewChildOrViewAnimator.transform.parent != null) {
                            viewChildOrViewAnimator.transform.SetSiblingIndex(viewChildOrViewAnimator.transform.parent.childCount - 1);
                        }
                    }
                }
                
                var viewChild = viewChildOrViewAnimator.GetComponent<ViewChild>();
                if (viewChild) {
                    viewChild.PlayAnimation(animationName,
                        elapsedTime,
                        () => {
                            checkEnd();
                        },
                        () => {
                            checkEnd();
                        });
                } else {
                    var animator = viewChildOrViewAnimator.GetComponent<ViewAnimator>();
                    if (animator) {
                        animator.PlayAnimation(animationName,
                            elapsedTime,
                            () => {
                                checkEnd();
                            },
                            () => {
                                checkEnd();
                            });
                    } else {
                        if (newValue) {
                            viewChildOrViewAnimator.gameObject.SetActive(true);
                        } else {
                            viewChildOrViewAnimator.gameObject.SetActive(false);
                        }

                        checkEnd();
                    }
                }
            }
            
            count++;
            checkEnd();
        };

        action();
    }

    public void Show(Action onEnd, float elapsedTime = 0f) {
        var newValue = true;
        var animationName = "Show";
        
        var count = 0;
        Action checkEnd = () => {
            count--;

            if (count == 0) {
                if (_toggle != null) {
                    _toggle.interactable = true;
                }
                _shownIsOn = newValue;
                
                _currentAnimationTaskWaitCallback?.Invoke();
                _currentAnimationTaskWaitCallback = null;

                onEnd();
            }
        };

        Action action = () => {
            _showingIsOn = _toggle.isOn;

            foreach (var item_ in _viewChildOrViewAnimatorTargets) {
                var viewChildOrViewAnimator = item_;
                if (viewChildOrViewAnimator == null) {
                    continue;
                }
                count++;
                
                if (newValue) {
                    viewChildOrViewAnimator.gameObject.SetActive(true);

                    var view = viewChildOrViewAnimator.GetComponent<View>();
                    if (view != null) {
                        view.SetSiblingLast();
                    } else {
                        if (viewChildOrViewAnimator.transform.parent != null) {
                            viewChildOrViewAnimator.transform.SetSiblingIndex(viewChildOrViewAnimator.transform.parent.childCount - 1);
                        }
                    }
                }
                
                var viewChild = viewChildOrViewAnimator.GetComponent<ViewChild>();
                if (viewChild != null) {
                    viewChild.PlayAnimation(animationName,
                        elapsedTime,
                        () => {
                            checkEnd();
                        },
                        () => {
                            checkEnd();
                        });
                } else {
                    var animator = viewChildOrViewAnimator.GetComponent<ViewAnimator>();
                    if (animator) {
                        animator.PlayAnimation(animationName,
                            elapsedTime,
                            () => {
                                checkEnd();
                            },
                            () => {
                                checkEnd();
                            });
                    } else {
                        if (newValue) {
                            viewChildOrViewAnimator.gameObject.SetActive(true);
                        } else {
                            viewChildOrViewAnimator.gameObject.SetActive(false);
                        }

                        checkEnd();
                    }
                }
            }

            count++;
            checkEnd();
        };

        LoadAssets(() => {
            FirstInitialize();
            action();
        });
    }

    private void OnChanged(bool newValue, float elapsedTime = 0f, bool onEnable = false) {
        if (_toggle.group != null && _waitViewAnimationEndOtherToggle) {
            var betterToggleGroup = (_toggle.group as BetterToggleGroup);
            betterToggleGroup.UpdateStateToggles(elapsedTime, onEnable);
        } else {
            if (newValue) {
                Show(() => {
                    
                }, elapsedTime);
            } else {
                Hide(() => {
                    
                }, elapsedTime);
            }
        }
    }

}

}