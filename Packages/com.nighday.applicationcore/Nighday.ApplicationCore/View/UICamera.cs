using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace Nighday.ApplicationCore {

public class UICamera : MonoBehaviour {
    
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
    public static void RuntimeInitClear() {
    }
    
    private void Awake() {
        // ChangeEnvironmentCameraList(EnvironmentCamera._environmentCameraList);
        // EnvironmentCamera.OnEnableCamera(ChangeEnvironmentCameraList);
        // EnvironmentCamera.OnDisableCamera(ChangeEnvironmentCameraList);
    }

    private void Start() {
        
    }

    private void ChangeEnvironmentCameraList(List<EnvironmentCamera> list = null) {
        return;
        var cameraData = camera.GetUniversalAdditionalCameraData();
        if (list == null || list.Count == 0) {
            cameraData.renderType = CameraRenderType.Base;
        } else {
            cameraData.renderType = CameraRenderType.Overlay;

            EnvironmentCamera.AddUICameraToStack(this);
        }
    }
    
    //===
    
    private Camera _camera;

    public Camera camera {
        get {
            if (_camera == null) {
                _camera = GetComponent<Camera>();
            }

            return _camera;
        }
    }

}

}