using System.Linq;
using UnityEngine;

namespace Nighday.ApplicationCore {

[DisallowMultipleComponent]
public class UpdateOrderViewChildOverrideComponent : MonoBehaviour {

    public virtual void UpdateOrderViewChildren(ViewChild viewChild_) {
        var list = viewChild_.viewChilds.ToArray().ToList();
        list.Reverse();
        
        var currentPositionZ = 0f;
        foreach (var viewChild in list) {
            if (viewChild != null && viewChild.gameObject != null && viewChild.gameObject.activeSelf) {
                var position = viewChild.transform.localPosition;
                position.z = currentPositionZ;
                viewChild.transform.localPosition = position;
                currentPositionZ += viewChild.far;
            }
        }
    }
    
}

}