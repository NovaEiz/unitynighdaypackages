using System;
using System.Collections;
using UnityEditor;
using UnityEngine;

namespace Nighday.ApplicationCore {

public class View : ViewChild {
    
    [SerializeField] protected string _layerName;
    [SerializeField] protected bool _destroyAfterHide;

    private void Start() {
        StartCoroutine(WaitOneFrameForAddView());
    }

    private IEnumerator WaitOneFrameForAddView() {
        yield return null;
#if UNITY_EDITOR
        if (EditorApplication.isPlaying) {
#endif
        var layer = ApplicationViewManager.GetLayer(_layerName);
        layer.AddView(this);
        layer.OnDisableEvent += () => {
            gameObject.SetActive(false);
        };
        layer.OnEnableEvent += () => {
            gameObject.SetActive(true);
        };
#if UNITY_EDITOR
        }
#endif
    }

    protected override void RebuildViewChildsList() {
#if UNITY_EDITOR
        if (!EditorApplication.isPlayingOrWillChangePlaymode) {
            _viewChilds.Clear();
            foreach (Transform childTr in transform) {
                var viewChild = childTr.GetComponent<ViewChild>();
                if (viewChild != null) {
                    if (!_viewChilds.Contains(viewChild)) {
                        _viewChilds.Add(viewChild);
                    }
                }
            }
        }
#endif
    }
    
    protected override void OnDestroy() {
        base.OnDestroy();

#if UNITY_EDITOR
        if (!EditorUtility.IsPersistent(gameObject)) {
            if (EditorApplication.isPlayingOrWillChangePlaymode) {
#endif
        foreach (var viewChild in _viewChilds) {
            GameObject.Destroy(viewChild.gameObject);
        }
#if UNITY_EDITOR
            }
        }
#endif
    }

    
    protected override void OnDisable() {
        base.OnDisable();

#if UNITY_EDITOR
        if (!EditorUtility.IsPersistent(gameObject)) {
            if (EditorApplication.isPlayingOrWillChangePlaymode) {
#endif
                // Это не надо.
                // foreach (var viewChild in _viewChilds) {
                //     viewChild.gameObject.SetActive(false);
                // }
#if UNITY_EDITOR
            }
        }
#endif
    }

    protected override void OnEnable() {
        base.OnEnable();

        // Это не надо.
        // foreach (var viewChild in _viewChilds) {
        //     viewChild.gameObject.SetActive(true);
        // }
    }

    [SerializeField] private string _currentStateAnimationName;
    private string currentStateAnimationName => _currentStateAnimationName;

    public bool CurrentStateAnimationIsName(string animationName) {
        return _currentStateAnimationName == animationName;
    }
    
#if UNITY_EDITOR

    public override void OnValidate() {
        base.OnValidate();

        //
        // var len = _viewChilds.Count;
        // for (var i = len-1; i > 0; i--) {
        //     var viewChild = _viewChilds[i];
        //     var found = false;
        //     foreach (Transform childTr in transform) {
        //         if (viewChild == childTr.GetComponent<ViewChild>()) {
        //             found = true;
        //             break;
        //         }
        //     }
        //
        //     if (!found) {
        //         _viewChilds.RemoveAt(i);
        //         len--;
        //     }
        // }
    }

#endif

#region Show/Hide

    public override void PlayAnimation(string animationName, float elapsedTime = 0f, Action onEndCallback = null, Action onInterruptCallback = null) {
        onAnimationStart?.Invoke(animationName);

        if (!string.IsNullOrEmpty(_currentAnimation)) {
            if (_currentAnimation != animationName) {
                onAnimationInterrupt?.Invoke(_currentAnimation);
            }
        }
        _currentAnimation = animationName;
        
        Action onEndCallback_ = null;
        Action<string> onInterruptCallback_ = null;
        
        onEndCallback_ = () => {
            onAnimationInterrupt -= onInterruptCallback_;
            _currentAnimation = null;
            onAnimationEnd?.Invoke(animationName);
            onEndCallback?.Invoke();
        };
        
        onInterruptCallback_ = (animationName__) => {
            if (animationName__ == animationName) {
                onAnimationInterrupt -= onInterruptCallback_;
                onInterruptCallback?.Invoke();
            }
        };

        onAnimationInterrupt += onInterruptCallback_;

        _currentStateAnimationName = animationName;

        var waitCount = 0;
        Action onEndItem = () => {
            waitCount--;
            if (waitCount == 0) {
                onEndCallback_?.Invoke();

                if (animationName == "Hide" && _currentStateAnimationName == animationName) {
                    if (_destroyAfterHide) {
                        GameObject.Destroy(gameObject);
                    } else {
                        SetActiveViewChild(false);
                    }
                }
            }
        };
        foreach (var viewChild in _viewChilds) {
            waitCount++;
            viewChild.PlayAnimation(animationName, elapsedTime, onEndItem);
        }

        waitCount++;
        this.PlayAnimationView(animationName, elapsedTime, onEndItem);
    }
    
    private void PlayAnimationView(string    animationName,
                                      float  elapsedTime   = 0f,
                                      Action onEndCallback = null) {

        var currentViewAnimator = _viewAnimator;
        if (currentViewAnimator != null) {
            currentViewAnimator.PlayAnimation(animationName, elapsedTime, onEndCallback); 
        } else {
            onEndCallback?.Invoke();
        }
    }

    // public void Show(float elapsedTime = 0f, Action onEndCallback = null) {
    //     var waitCount = 0;
    //     Action onEndItem = () => {
    //         waitCount--;
    //         if (waitCount == 0) {
    //             onEndCallback?.Invoke();
    //         }
    //     };
    //     foreach (var viewChild in _viewChilds) {
    //         waitCount++;
    //         viewChild.Show(elapsedTime, onEndItem);
    //     }
    // }
    //
    // public void Hide(float elapsedTime = 0f, Action onEndCallback = null) {
    //     var waitCount = 0;
    //     Action onEndItem = () => {
    //         waitCount--;
    //         if (waitCount == 0) {
    //             onEndCallback?.Invoke();
    //         }
    //     };
    //     foreach (var viewChild in _viewChilds) {
    //         waitCount++;
    //         viewChild.Hide(elapsedTime, onEndItem);
    //     }
    // }

#endregion

    public void SetSiblingLast() {
        if (transform.parent != null) {
            transform.SetSiblingIndex(transform.parent.childCount - 1);
        }
        foreach (var viewChild in _viewChilds) {
            if (viewChild.transform.parent != null) {
                viewChild.transform.SetSiblingIndex(viewChild.transform.parent.childCount - 1);
            }
        }
    }

}

}