using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace Nighday.ApplicationCore {

[System.Serializable]
public class UnityAnimationEvent : UnityEvent<string> { };

public class ViewAnimationEventDispatcher : MonoBehaviour {

    public UnityAnimationEvent OnAnimationStart = new UnityAnimationEvent();

    public UnityAnimationEvent OnAnimationComplete = new UnityAnimationEvent();

    public UnityAnimationEvent OnAnimationInterrupted = new UnityAnimationEvent();

    private Animator animator;

    [SerializeField] private bool _playCurrentAnimationOnEnable;
    [SerializeField] private float _playCurrentAnimationElapsedTimeOnEnable;

    [Header("Debug fields")]
    [SerializeField] 
    private string _currentAnimationNameInProcess;

    private void OnEnable() {
        if (_playCurrentAnimationOnEnable) {
            Play(_currentAnimationNameInProcess, -1, _playCurrentAnimationElapsedTimeOnEnable);
        }
    }

    public void Play(string animationName,
                     int    layer          = -1,
                     float  normalizedTime = 0f) {
        if (!string.IsNullOrEmpty(_currentAnimationNameInProcess)) {
            if (_currentAnimationNameInProcess != animationName) {
                OnAnimationInterrupted?.Invoke(_currentAnimationNameInProcess);
            }
        }

        _currentAnimationNameInProcess = animationName;

        if (animator != null) {
            animator.Play(animationName, layer, normalizedTime);
        } else {
            AnimationStartHandler(animationName);
            AnimationCompleteHandler(animationName);
        }
    }

    private void Awake() {
        animator = GetComponent<Animator>();
        if (animator != null) {
            for (var i = 0; i < animator.runtimeAnimatorController.animationClips.Length; i++) {
                AnimationClip clip = animator.runtimeAnimatorController.animationClips[i];

                AnimationEvent animationStartEvent = new AnimationEvent();
                animationStartEvent.time = 0;
                animationStartEvent.functionName = "AnimationStartHandler";
                animationStartEvent.stringParameter = clip.name;

                AnimationEvent animationEndEvent = new AnimationEvent();
                animationEndEvent.time = clip.length;
                animationEndEvent.functionName = "AnimationCompleteHandler";
                animationEndEvent.stringParameter = clip.name;

                clip.events = clip.events
                    .Where((item_) => {
                        if (item_.functionName == "AnimationStartHandler" || item_.functionName == "AnimationCompleteHandler") {
                            return false;
                        }

                        return false;
                    })
                    .Select((item_) => { return item_; })
                    .ToArray();

                var arr = clip.events
                    .Where((item_) => {
                        if (Mathf.Approximately(animationStartEvent.time, item_.time)
                          &&
                            (animationStartEvent.functionName == item_.functionName)) {
                            return true;
                        }

                        return false;
                    })
                    .Select((item_) => { return item_; })
                    .ToArray();

                if (arr.Length == 0) {
                    clip.AddEvent(animationStartEvent);
                }

                var arr2 = clip.events
                    .Where((item_) => {
                        if (Mathf.Approximately(animationEndEvent.time, item_.time)
                          &&
                            (animationEndEvent.functionName == item_.functionName)) {
                            return true;
                        }

                        return false;
                    })
                    .Select((item_) => { return item_; })
                    .ToArray();

                if (arr2.Length == 0) {
                    clip.AddEvent(animationEndEvent);
                }
            }
        }
    }

    public void AnimationStartHandler(string name) {
        OnAnimationStart?.Invoke(_currentAnimationNameInProcess);
    }

    public void AnimationCompleteHandler(string name) {
        OnAnimationComplete?.Invoke(_currentAnimationNameInProcess);
    }

}

}