using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace Nighday.ApplicationCore {

public class ViewAnimator : MonoBehaviour {

    public event Action<string> OnAnimationEvent;

    [SerializeField] protected bool _playAnimationWithHierarchy;

    [SerializeField] protected bool _queueWaitEndAnimationForStartNext;

    [SerializeField] protected bool _startNewAnimationFromReversedCurrentAnimationElapsedTime;

    [SerializeField] protected Animator _animator;

    private ViewAnimationEventDispatcher _viewAnimationEventDispatcher;

    public ViewAnimationEventDispatcher viewAnimationEventDispatcher {
        get {
            if (_viewAnimationEventDispatcher == null) {

                if (_animator != null) {
                    _viewAnimationEventDispatcher = _animator.gameObject.GetComponent<ViewAnimationEventDispatcher>();
                    if (_viewAnimationEventDispatcher == null) {
                        _viewAnimationEventDispatcher = _animator.gameObject.AddComponent<ViewAnimationEventDispatcher>();
                    }
                } else {
                    _viewAnimationEventDispatcher = gameObject.GetComponent<ViewAnimationEventDispatcher>();
                    if (_viewAnimationEventDispatcher == null) {
                        _viewAnimationEventDispatcher = gameObject.AddComponent<ViewAnimationEventDispatcher>();
                    }
                }
            }

            return _viewAnimationEventDispatcher;
        }
    }

    public Animator animator {
        get {
            return _animator;
        }
    }

    private Task _currentAnimationTask;

    private void Awake() {
        var viewAnimationEventDispatcher = this.viewAnimationEventDispatcher;
    }

    private static void GetHierarchyViewAnimators(Transform              transform,
                                                  out List<ViewAnimator> childViewAnimators) {
        childViewAnimators = new List<ViewAnimator>();
        foreach (Transform childTr in transform) {
            var childViewAnimator = childTr.GetComponent<ViewAnimator>();
            if (childViewAnimator != null) {
                childViewAnimators.Add(childViewAnimator);
            } else {
                GetHierarchyViewAnimators(childTr, out var childViewAnimators_);
                if (childViewAnimators_.Count > 0) {
                    childViewAnimators.AddRange(childViewAnimators_);
                }
            }
        }
    }

    public virtual void PlayAnimation(string animationName,
                                      float  elapsedTime      = 0f,
                                      Action onEndCallback    = null,
                                      Action onCancelCallback = null) {
        if (_queueWaitEndAnimationForStartNext) {
            PlayAnimationQueue(this, animationName, elapsedTime, onEndCallback, onCancelCallback);
        } else {
            _currentAnimationTask = RunProcessPlayAnimation(animationName, elapsedTime, onEndCallback, onCancelCallback);
        }

    }
    
    private Action _actionNeedRunByOnEnable;
    private bool _waitOnEnable;

    private string _currentAnimationName;

    private void OnEnable() {
        if (_waitOnEnable) {
            _actionNeedRunByOnEnable();
        }
    }

    private void OnDisable() {
        if (animator != null) {
            if (_currentAnimationName == "Hide") {
                var container = transform.Find("Container");
                if (container != null) {
                    container.gameObject.SetActive(false);
                }
            }
        }
    }

    public virtual async Task RunProcessPlayAnimation(string animationName,
                                                      float  elapsedTime      = 0f,
                                                      Action onEndCallback    = null,
                                                      Action onCancelCallback = null) {
        _currentAnimationName = animationName;
        if (!gameObject.activeInHierarchy && gameObject.activeSelf) {
            _waitOnEnable = true;
            _actionNeedRunByOnEnable = () => {
                _waitOnEnable = false;
                _actionNeedRunByOnEnable = null;
            };

            while (_waitOnEnable) {
                await Task.Yield();
            }
        }
        OnAnimationEvent?.Invoke(animationName);

        var inProcessCount = 0;
        Action onUpdateProcess = () => {
            inProcessCount--;

            if (inProcessCount == 0) {

                onEndCallback?.Invoke();
            }
        };
        
        //Надо 2. Потому что надо сначала запустить анимацию родителя, то есть этого gameObject. А потом запускать дочерние
        inProcessCount++;
        inProcessCount++;

        var task = PlayAnimationAsync(animator,
            animationName,
            elapsedTime,
            onUpdateProcess,
            () => {
                inProcessCount--;
                onCancelCallback?.Invoke();
            });

        if (_playAnimationWithHierarchy) {
            GetHierarchyViewAnimators(this.transform, out var childViewAnimator);
            foreach (ViewAnimator viewAnimator in childViewAnimator) {
                inProcessCount++;
                viewAnimator.PlayAnimation(animationName, elapsedTime, onUpdateProcess, onUpdateProcess);
            }
        }

        onUpdateProcess();
        
        await task;

        while (inProcessCount > 0) {
            await Task.Yield();
        }
    }

    private void PlayAnimationQueue(ViewAnimator viewAnimator,
                                    string       animationName,
                                    float        elapsedTime      = 0f,
                                    Action       onEndCallback    = null,
                                    Action       onCancelCallback = null) {
        var task = SchedulePlayAnimation(viewAnimator, animationName, elapsedTime, onEndCallback, onCancelCallback);
        _animationTasks.Add(task);
        if (_runTasks == null) {
            _runTasks = new Task(RunTasks);
            TaskScheduler taskScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            _runTasks.Start(taskScheduler);
        }
    }

    private Task _runTasks;

    private async void RunTasks() {
        TaskScheduler taskScheduler = TaskScheduler.FromCurrentSynchronizationContext();

        if (_animationTasks.Count > 0) {
            try {
                while (_animationTasks.Count > 0) {

                    var task = _animationTasks[0];

                    await task();

                    _animationTasks.RemoveAt(0);
                }
            } catch (Exception e) {
                Debug.LogError(e);
                throw;
            }
        }

        _runTasks = null;
    }

    private List<Func<Task>> _animationTasks = new List<Func<Task>>();

    public static Func<Task> SchedulePlayAnimation(ViewAnimator viewAnimator,
                                                   string       animationName,
                                                   float        elapsedTime      = 0f,
                                                   Action       onEndCallback    = null,
                                                   Action       onCancelCallback = null) {

        return async () => { await viewAnimator.RunProcessPlayAnimation(animationName, elapsedTime, onEndCallback, onCancelCallback); };
    }

    public async Task PlayAnimationAsync(Animator animator,
                                         string   animationName,
                                         float    elapsedTime      = 0f,
                                         Action   onEndCallback    = null,
                                         Action   onCancelCallback = null) {

        ViewAnimationEventDispatcher viewAnimationEventDispatcher = this.viewAnimationEventDispatcher;
        try {

            // if (animator == null) {
            //     onEndCallback?.Invoke();
            //     return;
            // }

            UnityAction<string> actionComplete = null;
            UnityAction<string> actionInterrupted = null;

            actionComplete = (animationName_) => {
                if (animationName_ != animationName) {
                    return;
                }

                viewAnimationEventDispatcher.OnAnimationComplete.RemoveListener(actionComplete);
                viewAnimationEventDispatcher.OnAnimationInterrupted.RemoveListener(actionInterrupted);

                try {
                    onEndCallback?.Invoke();
                } catch (Exception e) {
                    GameObject obj = null;
                    if (animator != null) {
                        obj = animator.gameObject;
                    }

                    UnityEngine.Debug.LogException(e, obj);
                }
            };

            actionInterrupted = (animationName_) => {
                if (animationName_ != animationName) {
                    return;
                }

                viewAnimationEventDispatcher.OnAnimationComplete.RemoveListener(actionComplete);
                viewAnimationEventDispatcher.OnAnimationInterrupted.RemoveListener(actionInterrupted);

                try {
                    onCancelCallback?.Invoke();
                } catch (Exception e) {
                    GameObject obj = null;
                    if (animator != null) {
                        obj = animator.gameObject;
                    }

                    UnityEngine.Debug.LogException(e, obj);
                }
            };

            viewAnimationEventDispatcher.OnAnimationComplete.AddListener(actionComplete);
            viewAnimationEventDispatcher.OnAnimationInterrupted.AddListener(actionInterrupted);

        } catch (Exception e) {
            Debug.LogException(e);
            throw;
        }

        try {
            viewAnimationEventDispatcher.Play(animationName, 0, elapsedTime);
        } catch (Exception e) {
            Debug.LogException(e);
            throw;
        }
    }

}

}