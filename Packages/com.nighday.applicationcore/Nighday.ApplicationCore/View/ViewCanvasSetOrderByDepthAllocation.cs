using System;
using UnityEngine;

namespace Nighday.ApplicationCore {

public class ViewCanvasSetOrderByDepthAllocation : MonoBehaviour {

    [SerializeField] private int _order;

    private ViewChild _viewChild;

    private void Awake() {
        _viewChild = gameObject.GetComponentInParent<ViewChild>();
    }

    private void OnEnable() {
        if (_viewChild != null) {
            _viewChild.OnUpdateOrderDepth += OnUpdateOrderDepth;
        }

        OnUpdateOrderDepth();
    }

    private void OnDisable() {
        _viewChild.OnUpdateOrderDepth -= OnUpdateOrderDepth;
    }

    private void OnUpdateOrderDepth() {
        var order = _viewChild.currentOrderPlane + _order;
        var canvas = GetComponent<Canvas>();
        canvas.sortingOrder = order;
    }

}

}