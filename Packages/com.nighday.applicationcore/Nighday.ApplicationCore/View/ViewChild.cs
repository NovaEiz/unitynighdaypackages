using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nighday.UI;
using UnityEditor;
using UnityEngine;

namespace Nighday.ApplicationCore {

[ExecuteInEditMode]
public class ViewChild : MonoBehaviour {

    [SerializeField] protected ViewAnimator _viewAnimator;

    public ViewAnimator viewAnimator => _viewAnimator;

    [Space]
    [SerializeField] protected List<ViewChild> _viewChilds = new List<ViewChild>();

    public List<ViewChild> viewChilds => _viewChilds;
    
    [SerializeField] private int _orderDepth = 1;

    [Space]
    [SerializeField] private int _currentOrderPlane;
    [SerializeField] private int _allChildrenOrderDepth;

    [Space]
    [SerializeField] private int _allOrderDepth;

    public int currentOrderPlane => _currentOrderPlane;
    public int orderDepth        => _orderDepth;
    
    [Space]
    [Space]
    [Space]

    [SerializeField] private float _far;

    public float far => _far;

    public event Action OnUpdateOrderViewChildren;
    public event Action OnUpdateOrderDepth;

    private Camera _camera;
    private Camera camera {
        get {
            if (_camera == null) {
                _camera = GetComponentInChildren<Camera>();
            }

            return _camera;
        }
    }

    private Canvas[] GetCanvases() {
        var list = new List<Canvas>();
        foreach (Transform itemTr in transform) {
            var canvas = itemTr.GetComponent<Canvas>();
            if (canvas != null) {
                list.Add(canvas);
            }
        }

        return list.ToArray();
    }

    private void OnTransformChildrenChanged() {
        UpdateOrderViewChildren();
    }

    public void UpdateOrderDepth() {
        _allChildrenOrderDepth = 0;
        if (_viewChilds.Count > 0) {
            var childs = _viewChilds;
            foreach (var viewChild in childs) {
                if (viewChild.gameObject.activeSelf) {

                    viewChild._currentOrderPlane = _allChildrenOrderDepth + _orderDepth + _currentOrderPlane;
                    
                    viewChild.UpdateOrderDepth();
                    
                    _allChildrenOrderDepth += viewChild._allOrderDepth;
                }
            }
        }
        _allOrderDepth = _orderDepth + _allChildrenOrderDepth;

        OnUpdateOrderDepth?.Invoke();
    }

    public void UpdateOrderViewChildren() {
        
        RebuildViewChildsList();

        foreach (Transform childTr in transform) {
            var uiCamera = childTr.gameObject.GetComponent<UICamera>();
            if (!uiCamera) {
                continue;
            }

            var camera = uiCamera.camera;

            if (camera.nearClipPlane < 0.01f) {
                camera.nearClipPlane = 0.01f;
            }

            camera.farClipPlane = _far;
        }

        var updateOrderViewChildOverrideComponent = gameObject.GetComponent<UpdateOrderViewChildOverrideComponent>();
        if (updateOrderViewChildOverrideComponent == null) {
            updateOrderViewChildOverrideComponent = gameObject.AddComponent<UpdateOrderViewChildOverrideComponent>();
        }

        updateOrderViewChildOverrideComponent.UpdateOrderViewChildren(this);

        _allChildrenOrderDepth = 0;
        if (_viewChilds.Count > 0) {
            _far = 0;
            foreach (var viewChild in _viewChilds) {
                if (viewChild.gameObject.activeSelf) {
                    _far += viewChild._far;
                    
                    viewChild._currentOrderPlane = _allChildrenOrderDepth + _orderDepth;
                    _allChildrenOrderDepth += viewChild._allOrderDepth;
                }
            }
        }
        _allOrderDepth = _orderDepth + _allChildrenOrderDepth;

        if (GetParentViewChild() == null) {
            UpdateOrderDepth();
        } else {
            UpdateOrderParentViewChildren();
        }

        OnUpdateOrderViewChildren?.Invoke();
    }

    protected virtual void OnDestroy() {
        GetParentViewChild()
            ?.RemoveViewChild(this);
    }

    protected virtual void OnDisable() {
        UpdateOrderParentViewChildren();
    }

    protected virtual void OnEnable() {
        UpdateOrderParentViewChildren();
    }

    private void UpdateOrderParentViewChildren() {
        var parentViewChild = GetParentViewChild();
        if (parentViewChild != null) {
            parentViewChild.UpdateOrderViewChildren();
        }
    }

    private ViewChild GetParentViewChild() {
        if (transform != null && transform.parent != null) {
            var parentViewChild = transform.parent.GetComponentInParent<ViewChild>();
            return parentViewChild;
        }

        return null;
    }

    protected virtual void RebuildViewChildsList() {
        _viewChilds.Clear();
        foreach (Transform childTr in transform) {
            var viewChild = childTr.GetComponent<ViewChild>();
            if (viewChild != null) {
                if (!_viewChilds.Contains(viewChild)) {
                    _viewChilds.Add(viewChild);
                }
            }
        }
    }

    public void AddViewChild(ViewChild viewChild_) {
        _viewChilds.Add(viewChild_);
        viewChild_.transform.SetParent(transform, false);
        UpdateOrderViewChildren();
    }

    public void RemoveViewChild(ViewChild viewChild_) {
        _viewChilds.Remove(viewChild_);
        UpdateOrderViewChildren();
    }

    protected Action<string> onAnimationStart;
    protected Action<string> onAnimationEnd;
    protected Action<string> onAnimationInterrupt;
    
    public void RegisterOnAnimationStart(Action<string> callback) {
        onAnimationStart += callback;
    }
    
    public void UnregisterOnAnimationStart(Action<string> callback) {
        onAnimationStart -= callback;
    }
    
    public void RegisterOnAnimationEndOrInterrupt(Action<string> callback) {
        onAnimationEnd += callback;
        onAnimationInterrupt += callback;
    }
    public void UnregisterOnAnimationEndOrInterrupt(Action<string> callback) {
        onAnimationEnd -= callback;
        onAnimationInterrupt -= callback;
    }

#if UNITY_EDITOR

    public virtual void OnValidate() {
        if (_orderDepth < 1) {
            _orderDepth = 1;
        }
        
        UpdateOrderViewChildren();

        if (_viewAnimator == null) {
            _viewAnimator = GetComponent<ViewAnimator>();
        }
    }

#endif

#region Show/Hide
    
    protected string _currentAnimation;

    private bool _wasSearchAnimator;

    public virtual void PlayAnimation(string animationName,
                                      float  elapsedTime   = 0f,
                                      Action onEndCallback = null,
                                      Action onCancelCallback = null) {
        onAnimationStart?.Invoke(animationName);
        
        if (!string.IsNullOrEmpty(_currentAnimation)) {
            if (_currentAnimation != animationName) {
                onAnimationInterrupt?.Invoke(_currentAnimation);
            }
        }
        _currentAnimation = animationName;
        
        Action onEndCallback_ = null;
        onEndCallback_ = () => {
            onAnimationEnd?.Invoke(animationName);
            onEndCallback?.Invoke();
            if (animationName == "Hide" && _currentAnimation == animationName) {
                SetActiveViewChild(false);
            }
            _currentAnimation = null;
        };
        
        if (viewAnimator == null) {
            if (animationName == "Show") {
                SetActiveViewChild(true);
            } else {
                SetActiveViewChild(false);
            }
            
            onEndCallback_?.Invoke();
            return;
        } else {
            if (animationName == "Show") {
                SetActiveViewChild(true);
            }
        }
        
        Action onCancelCallback_ = null;
        onCancelCallback_ = () => {
            onCancelCallback?.Invoke();
        };

        viewAnimator.PlayAnimation(animationName, elapsedTime, onEndCallback_, onCancelCallback_);
    }

    protected void SetActiveViewChild(bool state) {
        gameObject.SetActive(state);
    }

#endregion

}

}