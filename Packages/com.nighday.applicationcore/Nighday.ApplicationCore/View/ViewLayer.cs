using System;
using UnityEngine;

namespace Nighday.ApplicationCore {

[Serializable]
public class ViewLayer : ViewChild {
    
    // [Header("Debug fields")]
    [HideInInspector] 
    public ApplicationViewManager applicationViewManager;

    public void SetActive(bool value) {
        gameObject.SetActive(value);
    }

    public void AddView(View view) {
        // foreach (var environmentView in view.EnvironmentViews) {
        // 	environmentView.transform.SetParent(environmentLayer, false);
        // }
        // foreach (var uiViews in view.UiViews) {
        // 	uiViews.transform.SetParent(uiLayer.transform);
        // 	(uiViews.transform as RectTransform).SetFullSize();
        // 
        foreach (var viewChild in view.viewChilds) {
            AddViewChild(viewChild);
        }

        UpdateOrderViewChildren();
        
        view.transform.SetParent(applicationViewManager.ViewsContainer, false);
    }

    public event Action OnEnableEvent;
    public event Action OnDisableEvent;

    protected override void OnEnable() {
        base.OnEnable();
        
        OnEnableEvent?.Invoke();
    }

    protected override void OnDisable() {
        base.OnDisable();
        
        OnDisableEvent?.Invoke();
    }

}

}