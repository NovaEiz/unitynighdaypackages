using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using System.Threading.Tasks;

#if UNITY_EDITOR
using UnityEditor;
#endif

[assembly: UnityEngine.Scripting.AlwaysLinkAssembly]
namespace Nighday.CheckUpdates {

[UnityEngine.Scripting.Preserve]
[UnitTitle("Task check IsDone")]
[UnitCategory("_Nighday/CheckUpdates")]
public class TaskCheckIsDone : Unit {

    [DoNotSerialize] // Mandatory attribute, to make sure we don’t serialize data that should never be serialized.
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlInput inputTrigger { get; private set; }

    [DoNotSerialize]
    public ValueInput taskValueInput;
    
    [DoNotSerialize]
    public ValueOutput isDoneValueOutput;

    [DoNotSerialize]
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlOutput outputTrigger { get; private set; }

    [DoNotSerialize]
    private bool _isDone;

    protected override void Definition() {
        inputTrigger = ControlInput(nameof(inputTrigger), Trigger);
        taskValueInput = ValueInput<Task>("taskInput", null);
        isDoneValueOutput = ValueOutput<bool>("isDoneOutput",
            (flow) => {
                return _isDone;
            });
        outputTrigger = ControlOutput(nameof(outputTrigger));
        Succession(inputTrigger, outputTrigger);
    }

    //Sending the Event MyCustomEvent with the integer value from the ValueInput port myValueA.
    private ControlOutput Trigger(Flow flow) {
        var task = flow.GetValue<Task>(taskValueInput);
        _isDone = false;
        

        WaitTask(task);
//         task.ContinueWith((t) => {
// #if UNITY_EDITOR
//             if (!EditorApplication.isPlaying) {
//                 return;
//             }       
// #endif
//             _isDone = true;
//         });

        return outputTrigger;
    }

    private async void WaitTask(Task task) {
        await task;
#if UNITY_EDITOR
        if (!EditorApplication.isPlaying) {
            return;
        }       
#endif
        _isDone = true;
    }
    
    public override void Uninstantiate(GraphReference instance) {
        base.Uninstantiate(instance);

        Debug.Log("yes, Uninstantiate");
    }

}

}