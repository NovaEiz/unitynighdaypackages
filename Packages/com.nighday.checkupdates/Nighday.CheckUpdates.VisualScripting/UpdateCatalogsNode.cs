using System;
using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using Nighday.VisualScripting;

namespace Nighday.CheckUpdates {

[UnityEngine.Scripting.Preserve]
[UnitTitle("UpdateCatalogs")]
[UnitCategory("_Nighday/CheckUpdates")]
public class UpdateCatalogsNode : CustomEventUnit<EmptyEventArgs> {

    [DoNotSerialize]
    public ValueInput catalogsListValueInput;
    
    [DoNotSerialize]
    public ValueOutput progressValueOutput;
    
    [DoNotSerialize] // Mandatory attribute, to make sure we don’t serialize data that should never be serialized.
    [PortLabel("Enter")] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabel("Exit")] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlOutput exit { get; private set; }
    
    [DoNotSerialize]
    [PortLabel("OnChangedProgress")] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlOutput progressOutput { get; private set; }
    
    [DoNotSerialize]
    private float _progress;
    
    protected override bool register => true;
    
    protected override bool ShouldTrigger(Flow flow, EmptyEventArgs args)
    {
        return true;
    }

    private EventHook _completedHook;
    public override EventHook GetHook(GraphReference reference) {
        if (_completedHook.IsUnityNull()) {
            _completedHook = new EventHook("UpdateCatalogsCompleted_" + Guid.NewGuid().ToString(), reference.machine);
        }
        return _completedHook;
    }
    public EventHook GetHookStartDownloading(GraphReference reference) {
        var hook = new EventHook("UpdateCatalogsStartDownloading_" + Guid.NewGuid().ToString(), reference.machine);
        return hook;
    }
    public EventHook GetHookProgressChanged(GraphReference reference) {
        var hook = new EventHook("UpdateCatalogsProgressChanged_" + Guid.NewGuid().ToString(), reference.machine);
        return hook;
    }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);

        catalogsListValueInput = ValueInput<IList>("catalogsList", null);
        progressValueOutput = ValueOutput<float>("progress", (flow) => { return _progress; });
        exit = ControlOutput(nameof(exit));
        progressOutput = ControlOutput(nameof(progressOutput));
        Succession(enter, exit);
        
        base.Definition();
    }

    private EventHook              _startDownloadingHook;
    private Action<EmptyEventArgs> _startDownloadingHandler;

    private EventHook              _progressChangedHook;
    private Action<EmptyEventArgs> _progressChangedHandler;
    
    public override void StartListening(GraphStack stack) {
        base.StartListening(stack);
        
        var reference = stack.ToReference();

        _startDownloadingHook = GetHookStartDownloading(reference);
        _startDownloadingHandler = args => {
            var flow = Flow.New(reference);
            flow.Run(progressOutput);
        };
        EventBus.Register(_startDownloadingHook, _startDownloadingHandler);
        
        _progressChangedHook = GetHookProgressChanged(reference);
        _progressChangedHandler = args => {
            var flow = Flow.New(reference);
            flow.Run(progressOutput);
        };
        EventBus.Register(_progressChangedHook, _progressChangedHandler);
    }

    public override void StopListening(GraphStack stack) {
        base.StopListening(stack);
        
        EventBus.Unregister(_startDownloadingHook, _startDownloadingHandler);
        EventBus.Unregister(_progressChangedHook, _progressChangedHandler);
    }

    private ControlOutput Trigger(Flow flow) {
        
        CoroutineRunner.instance.StartCoroutine(LoopCoroutine(flow));
        return exit;
    }

    protected IEnumerator LoopCoroutine(Flow flow) {
        var catalogsList = flow.GetValue<string[]>(catalogsListValueInput);

        var isDone = false;
        CheckUpdatesUtilities.UpdateCatalogs(catalogsList,
            () => {
                EventBus.Trigger(_startDownloadingHook);
            },
            (progress) => {
                _progress = progress;
                EventBus.Trigger(_progressChangedHook);
            },
            () => {
                isDone = true;
            });
        
        while (!isDone) {

            yield return flow.Yield();
        }

        EventBus.Trigger(_completedHook);
    }

}

}