
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.AsyncOperations;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Nighday.CheckUpdates {

// [UpdateInLobbyWorld(UpdateInLobbyWorld.TargetWorld.ClientAndServer)]
public class CheckUpdatesUtilities {//: SystemBase {

    public static async Task UpdateCatalogsTest(string[] catalogUrls, Action<float> onChangedProgress, Action onCompleted) {
        var delaySeconds = 5f;
        var elapsedTime = 0f;
        while (elapsedTime < delaySeconds) {
            var deltaTime = 0.1f;
            var delayMilliseconds = (int)(deltaTime * 1000);
            await Task.Delay(delayMilliseconds);
            elapsedTime += deltaTime;
            var progress = elapsedTime / delaySeconds;
            onChangedProgress?.Invoke(progress);
#if UNITY_EDITOR
            if (!EditorApplication.isPlaying) {
                break;
            }       
#endif
        }

        onCompleted();
    }

    public static async Task UpdateCatalogs(string[]      catalogsList,
                                            Action        onStartDownloading,
                                            Action<float> onChangedProgress,
                                            Action        onCompleted) {
        
        
        foreach (var catalog in catalogsList) {
            await UpdateCatalog(catalog,
                () => {
                    onStartDownloading?.Invoke();
                    onStartDownloading = null;//Потому что надо вызвать лишь 1 раз для всех каталогов
                },
                (progress) => {
                    onChangedProgress?.Invoke(progress);
                },
                () => {
                    
                });
        }

        onCompleted?.Invoke();
    }

    public static async Task UpdateCatalog(string catalogPath, Action onStartDownloading, Action<float> onChangedProgress, Action onCompleted) {
        var list = new List<object>();

        AsyncOperationHandle<IResourceLocator> updateHandle1 = default;
        try {
            updateHandle1 =
                Addressables.LoadContentCatalogAsync(catalogPath);

            while (!updateHandle1.IsDone) {
                await Task.Yield();
            }
			
            var resourceLocator = (IResourceLocator)updateHandle1.Result;//29b5e8fb9c0114577999db6dbeda2d4f
			
            foreach (object key_ in resourceLocator.Keys) {
                // if (key_ is string key) {
                // 	var val = "https://";
                // 	var valLen = val.Length;
                //
                // 	if (key.Length >= valLen && key.Substring(0, valLen) == val) {
                // 	}
                // } else {
                // }
			
                list.Add(key_);
            }
        }
        catch (Exception e) {
            Debug.LogError(e);
            throw;
        }

        try {
            AsyncOperationHandle<long> updateHandle2 = Addressables.GetDownloadSizeAsync(list);
            await updateHandle2.Task;
            if (updateHandle2.Result > 0) {
                onStartDownloading?.Invoke();
                
                AsyncOperationHandle updateHandle =
                    Addressables.DownloadDependenciesAsync(list, Addressables.MergeMode.Union);

                var totalDownloadBytes = updateHandle.GetDownloadStatus().TotalBytes;

                while (!updateHandle.IsDone) {
                    while (!updateHandle.GetDownloadStatus().IsDone) {
                        var currentDownloadedBytes = updateHandle.GetDownloadStatus().DownloadedBytes;
                        var currentProgress = (float)((double)currentDownloadedBytes / totalDownloadBytes);
                        onChangedProgress?.Invoke(currentProgress);

                        await Task.Yield();
                    }
                    await Task.Yield();
                }
				
                onCompleted?.Invoke();
            }
        }
        catch (Exception e) {
            Debug.LogError(e);
            throw;
        }
    }
}

}