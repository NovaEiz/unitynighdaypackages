using Unity.VisualScripting;

[assembly: UnityEngine.Scripting.AlwaysLinkAssembly]
namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ClientItemData.ActualizeState")]
[UnitCategory("_Nighday/ItemDataSystem/ClientItemData")]
public class ClientDataActualizeStateNode : Unit {

    [DoNotSerialize]
    private ClientData _clientData;

    [DoNotSerialize]
    public ValueInput clientDataInput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);
        
        clientDataInput = ValueInput<ClientData>("clientData", null);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var clientData = flow.GetValue<ClientData>(clientDataInput);

        clientData.ActualizeOwnerState();

        return exit;
    }

}

}