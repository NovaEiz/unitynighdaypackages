using System;
using Nighday.Json;
using Unity.VisualScripting;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ClientData.DeserializeChanges")]
[UnitCategory("_Nighday/ItemDataSystem/ClientData")]
public class ClientDataDeserializeChanges : Unit {
    
    [DoNotSerialize]
    public ValueInput jsonNodeInput;

    [DoNotSerialize]
    public ValueInput clientDataInput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        jsonNodeInput = ValueInput<JSONNode>("jsonNode");
        clientDataInput = ValueInput<ClientData>("clientData");
        
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var jsonNode = flow.GetValue<JSONNode>(jsonNodeInput);
        var clientData = flow.GetValue<ClientData>(clientDataInput);
        
        try {
            clientData.DeserializeChanges(jsonNode);
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }
        return exit;
    }

    
}

}