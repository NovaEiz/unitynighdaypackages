using Nighday.Json;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ClientItemData.GetChanges")]
[UnitCategory("_Nighday/ItemDataSystem/ClientItemData")]
public class ClientDataGetChangesNode : Unit {

    [DoNotSerialize]
    private JSONNode _changes;

    [DoNotSerialize]
    public ValueInput clientDataInput;

    [DoNotSerialize]
    public ValueOutput clientDataOutput;

    [DoNotSerialize]
    public ValueOutput jsonNodeOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);
        
        clientDataInput = ValueInput<ClientData>("clientData", null);
        clientDataOutput = ValueOutput<JSONNode>("changes", (flow) => { return _changes; });
    }
    
    private ControlOutput Trigger(Flow flow) {
        var clientData = flow.GetValue<ClientData>(clientDataInput);

        _changes = clientData.GetItemDataChanges();
        Debug.LogFormat("check _changes = {0}", _changes);
        return exit;
    }

}

}