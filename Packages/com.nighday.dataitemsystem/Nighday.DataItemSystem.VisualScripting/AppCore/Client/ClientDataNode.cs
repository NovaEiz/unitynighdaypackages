using Nighday.Json;
using Nighday.Socket;
using Unity.VisualScripting;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ClientItemData")]
[UnitCategory("_Nighday/ItemDataSystem/ClientItemData")]
public class ClientDataNode : Unit {

    [DoNotSerialize]
    private ClientData _clientData;

    [DoNotSerialize]
    public ValueInput clientDataInput;

    [DoNotSerialize]
    public ValueOutput clientDataOutput;

    [DoNotSerialize]
    public ValueOutput jsonNodeOutput;

    [DoNotSerialize]
    public ValueOutput connectionOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);
        
        clientDataInput = ValueInput<ClientData>("clientData", null);
        clientDataOutput = ValueOutput<ClientData>("clientData_", (flow) => { return _clientData; });
        jsonNodeOutput = ValueOutput<JSONNode>("jsonNode", (flow) => { return _clientData.jsonNode; });
        connectionOutput = ValueOutput<WebSocketConnection_>("connection", (flow) => { return _clientData.connection; });
    }
    
    private ControlOutput Trigger(Flow flow) {
        var clientData = flow.GetValue<ClientData>(clientDataInput);

        _clientData = clientData;
        return exit;
    }

}

}