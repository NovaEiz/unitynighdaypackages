using Nighday.Socket;
using Unity.VisualScripting;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("new ClientItemData")]
[UnitCategory("_Nighday/ItemDataSystem/ClientItemData")]
public class NewClientDataNode : Unit {

    [DoNotSerialize]
    private ClientData _clientData;

    [DoNotSerialize]
    public ValueInput connectionValueInput;

    [DoNotSerialize]
    public ValueOutput clientDataValueOutput;

    public enum Side {
        Client,
        Server
    }
    
    [Serialize]
    [Inspectable, UnitHeaderInspectable("Side")]
    [InspectorWide]
    public Side side { get; set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);
        
        connectionValueInput = ValueInput<WebSocketConnection_>("connection", null);
        clientDataValueOutput = ValueOutput<ClientData>("clientData", (flow) => { return _clientData; });
    }
    
    private ControlOutput Trigger(Flow flow) {
        WebSocketConnection_ connection = null;
        if (connectionValueInput.hasValidConnection) {
            connection = flow.GetValue<WebSocketConnection_>(connectionValueInput);
        }

        _clientData = new ClientData(connection);
        
        return exit;
    }

}

}