using Nighday.Json;
using Unity.VisualScripting;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("CommandItemData.ApplyFromClient")]
[UnitCategory("_Nighday/ItemDataSystem/CommandItemData")]
public class CommandItemDataApplyFromClientNode : Unit {
    
    [DoNotSerialize]
    private JSONNode _resultData;
    
    [DoNotSerialize]
    public ValueInput receiveDataJsonNodeInput;

    [DoNotSerialize]
    public ValueInput itemDataInput;

    [DoNotSerialize]
    public ValueOutput resultDataOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        receiveDataJsonNodeInput = ValueInput<JSONNode>("receiveDataJsonNode");
        itemDataInput = ValueInput<ItemData>("commandItemData");

        resultDataOutput = ValueOutput<JSONNode>("resultData", (flow) => { return _resultData;});

        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var receiveDataJsonNode = flow.GetValue<JSONNode>(receiveDataJsonNodeInput);
        var itemData = flow.GetValue<ItemData>(itemDataInput);
        _resultData = itemData.ApplyCommand(receiveDataJsonNode);
        return exit;
    }

    
}

}