using System;
using Unity.VisualScripting;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("CommandItemData")]
[UnitCategory("_Nighday/ItemDataSystem/CommandItemData")]
public class CommandItemDataNode : Unit {

    [DoNotSerialize]
    private ItemData _itemData;

    [DoNotSerialize]
    public ValueInput itemDataInput;

    [DoNotSerialize]
    public ValueInput keyPathValueInput;

    [DoNotSerialize]
    public ValueOutput itemDataOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        itemDataInput = ValueInput<ItemData>("itemData");
        keyPathValueInput = ValueInput<string>("keyPath", null);
        itemDataOutput = ValueOutput<ItemData>("itemData", (flow) => { return _itemData; });
        
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var itemData = flow.GetValue<ItemData>(itemDataInput);
        var keyPath = flow.GetValue<string>(keyPathValueInput);

        var childItemData = itemData.GetItemDataByKeyPath(keyPath);
        if (childItemData != null) {
            _itemData = childItemData;
        } else {
            var e = new Exception(string.Format("Нет ItemData по пути keyPath = {0}", keyPath));
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }
        return exit;
    }

}

}