using System;
using Nighday.Json;
using Nighday.VisualScripting;
using Unity.VisualScripting;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("CommandItemData.OnExecute")]
[UnitCategory("_Nighday/ItemDataSystem/CommandItemData")]
public class CommandItemDataOnExecuteNode : CustomEventUnit<EmptyEventArgs> {

    [DoNotSerialize]
    private ItemData _itemData;

    [DoNotSerialize]
    private JSONNode _receivedData;

    [DoNotSerialize]
    public ValueInput itemDataInput;

    [DoNotSerialize]
    public ValueOutput itemDataOutput;

    [DoNotSerialize]
    public ValueOutput _receivedDataValueOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override bool register => true;

    protected override bool ShouldTrigger(Flow           flow,
                                          EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;

    public override EventHook GetHook(GraphReference reference) {
        _hook = new EventHook("CommandItemDataRunComplete_" + Guid.NewGuid()
                                  .ToString(),
            reference.machine);

        return _hook;
    }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        itemDataInput = ValueInput<ItemData>("commandItemData");

        itemDataOutput = ValueOutput<ItemData>("itemData", (flow) => { return _itemData; });
        _receivedDataValueOutput = ValueOutput<JSONNode>("receivedData", (flow) => { return _receivedData; });

        Succession(enter, exit);

        base.Definition();
    }

    private ControlOutput Trigger(Flow flow) {
        var itemData = flow.GetValue<ItemData>(itemDataInput);
        if (itemData != null) {
            itemData.OnExecute((itemData_, receiveData_) => {
                try {
                    _receivedData = receiveData_;
                    _itemData = itemData_;
                    EventBus.Trigger(_hook);
                } catch (Exception e) {
                    this.SetException(flow.stack, e);
                    UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
                }
            });

        } else {
            var e = new Exception(string.Format("Этот ItemData не является командой. ( {0} )", itemData));

            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }

        return exit;
    }

}

}