using System;
using Nighday.Json;
using Nighday.VisualScripting;
using Unity.VisualScripting;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("CommandItemData.Run")]
[UnitCategory("_Nighday/ItemDataSystem/CommandItemData")]
public class CommandItemDataRunNode : CustomEventUnit<EmptyEventArgs> {
    
    [DoNotSerialize]
    private JSONNode _resultData;
    
    [DoNotSerialize]
    public ValueInput sendDataJsonNodeInput;

    [DoNotSerialize]
    public ValueInput itemDataInput;

    [DoNotSerialize]
    public ValueOutput resultDataOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow flow, EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;
    public override EventHook GetHook(GraphReference reference) {
        _hook = new EventHook("CommandItemDataRunComplete_" + Guid.NewGuid().ToString(), reference.machine);
        return _hook;
    }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        sendDataJsonNodeInput = ValueInput<JSONNode>("sendDataJsonNode");
        itemDataInput = ValueInput<ItemData>("commandItemData");
        
        resultDataOutput = ValueOutput<JSONNode>("resultData", (flow) => { return _resultData;});
        
        Succession(enter, exit);

        base.Definition();
    }
    
    private ControlOutput Trigger(Flow flow) {
        var sendDataJsonNode = flow.GetValue<JSONNode>(sendDataJsonNodeInput);
        var itemData = flow.GetValue<ItemData>(itemDataInput);
        // itemData.ApplyCommand(jsonNode);
        if (itemData != null) {
            itemData.SendRunRequest(sendDataJsonNode,
                (resultData) => {
                    try {
                        _resultData = resultData;
                        EventBus.Trigger(_hook);
                    } catch (Exception e) {
                        this.SetException(flow.stack, e);
                        UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
                    }
                });

        } else {
            var e = new Exception(string.Format("Этот ItemData не является командой. ( {0} )", itemData));

            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }

        return exit;
    }

    
}

}