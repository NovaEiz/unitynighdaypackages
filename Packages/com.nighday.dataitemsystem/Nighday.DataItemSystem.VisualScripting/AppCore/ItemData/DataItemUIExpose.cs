using System;
using Unity.VisualScripting;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("DataItemUI")]
[UnitCategory("_Nighday/ItemDataSystem/DataItemUI")]
public class DataItemUIExpose : Unit {

    [DoNotSerialize]
    private DataItemUI _itemDataUI;

    [DoNotSerialize]
    private ItemData _itemData;

    [DoNotSerialize]
    public ValueInput itemDataUIInput;

    [DoNotSerialize]
    public ValueInput itemDataInput;

    [DoNotSerialize]
    public ValueOutput itemDataUIOutput;

    [DoNotSerialize]
    public ValueOutput itemDataOutput;

    [DoNotSerialize]
    public ValueOutput keyPathOutput;

    [DoNotSerialize]
    public ValueOutput valueByKeyPathOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        itemDataUIInput = ValueInput<DataItemUI>("dataItemUI");
        itemDataInput = ValueInput<ItemData>("dataItem");
        
        itemDataUIOutput = ValueOutput<DataItemUI>("dataItemUI_", (flow) => { return _itemDataUI; });
        itemDataOutput = ValueOutput<ItemData>("dataItem_", (flow) => { return _itemDataUI.dataItem; });
        keyPathOutput = ValueOutput<string>("keyPath", (flow) => { return _itemDataUI.keyPath; });
        valueByKeyPathOutput = ValueOutput<object>("valueByKeyPath", (flow) => { return _itemDataUI.valueByKeyPath; });
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        try {
            _itemDataUI = flow.GetValue<DataItemUI>(itemDataUIInput);
            
            if (itemDataInput.hasValidConnection) {
                var itemData = flow.GetValue<ItemData>(itemDataInput);
                _itemDataUI.data = itemData;
            }
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }
        return exit;
    }

}

}