using System;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("DataItemUI.Get")]
[UnitCategory("_Nighday/UI/DataItemUI")]
public class DataItemUIGet : Unit {

    [DoNotSerialize]
    public ValueInput targetInput;

    [DoNotSerialize]
    public ValueOutput dataItemUIOutput;

    protected override void Definition() {
        targetInput = ValueInput<GameObject>("target", null).NullMeansSelf();
        dataItemUIOutput = ValueOutput<DataItemUI>("dataItemUI",
            (flow) => {
                var targetGameObject = flow.GetValue<GameObject>(targetInput);
                return targetGameObject.GetComponent<DataItemUI>();
            });
    }

}

}