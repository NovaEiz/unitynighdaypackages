using System;
using System.Collections;
using System.Collections.Generic;
using Mono.Collections.Generic;
using Nighday.VisualScripting;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("DataItemUI.OnChangeDataItem")]
[UnitCategory("_Nighday/ItemDataSystem/ItemDataUI")]
public class DataItemUIOnChangeDataItem : MultipleCustomEventUnit<EmptyEventArgs> {

    [DoNotSerialize]
    private DataItemUI _dataItemUI;
    
    [DoNotSerialize]
    public ValueInput dataItemUIInput;
    
    [DoNotSerialize]
    public ValueOutput dataItemOutput;
    
    [DoNotSerialize]
    public ValueOutput dataItemUIOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override bool register => true;

    protected override bool ShouldTrigger(Flow           flow,
                                          EmptyEventArgs args) {
        return true;
    }

    [DoNotSerialize]
    [PortLabel("OnChangeDataItem")]
    public ControlOutput onChangeDataItem { get; private set; }

    [DoNotSerialize]
    private EventHook _onChangeDataItemHook;

    public EventHook GetHook_OnChangeDataItem(GraphReference reference) {
        _onChangeDataItemHook = new EventHook("DataItemUI_OnChangeDataItem_" + Guid.NewGuid()
                                                  .ToString(),
            reference.machine);

        return _onChangeDataItemHook;
    }
    
    
    public override void StartListening(GraphStack stack) {
        var data = stack.GetElementData<Data>(this);

        if (data.isListening) {
            return;
        }

        if (register) {
            var reference = stack.ToReference();
            
            //data.RegisterEventPoint(GetHook_OnChangeDataItem(reference), (args) => { Trigger_OnChangeDataItem(reference, args); });
        }

        data.isListening = true;
    }

    public override void StopListening(GraphStack stack) {
        var data = stack.GetElementData<Data>(this);

        if (!data.isListening) {
            return;
        }

        // The coroutine's flow will dispose at the next frame, letting us
        // keep the current flow for clean up operations if needed
        foreach (var activeCoroutine in data.activeCoroutines) {
            activeCoroutine.StopCoroutine(false);
        }

        if (register) {
            //data.UnregisterEventPoints();
        }

        data.isListening = false;
    }

    //===

    protected override void Definition() {
        dataItemUIInput = ValueInput<DataItemUI>("dataItemUI");
        dataItemOutput = ValueOutput<ItemData>("dataItem",
            (flow) => {
                return _dataItemUI.dataItem;
            });
        dataItemUIOutput = ValueOutput<DataItemUI>("dataItemUI_",
            (flow) => {
                return _dataItemUI;
            });
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        onChangeDataItem = ControlOutput(nameof(onChangeDataItem));
        
        Succession(enter, exit);

        base.Definition();
    }

    private ControlOutput Trigger(Flow flow) {
        var dataItemUI = flow.GetValue<DataItemUI>(dataItemUIInput);
        
        var data = flow.stack.GetElementData<Data>(this);
        var reference = flow.stack.ToReference();
        var onChangeDataItemHook = GetHook_OnChangeDataItem(reference);
        var eventPoint_Complete = data.RegisterEventPoint(onChangeDataItemHook, (args) => { Trigger_OnChangeDataItem(reference, args); });

        Action action = null;
        action = () => {
            try {
                _dataItemUI = dataItemUI;
                EventBus.Trigger(onChangeDataItemHook);
            } catch (Exception e) {
                this.SetException(flow.stack, e);
                UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
            }
        };

        dataItemUI.OnChangeDataItem += action;
        _dataItemUI = dataItemUI;

        return exit;
    }

    public void Trigger_OnChangeDataItem(GraphReference reference,
                                         EmptyEventArgs args) {
        var flow = Flow.New(reference);

        if (!ShouldTrigger(flow, args)) {
            flow.Dispose();
            return;
        }

        AssignArguments(flow, args);

        Run_OnChangeDataItem(flow);
    }
    
    private void Run_OnChangeDataItem(Flow flow) {
        if (flow.enableDebug) {
            var editorData = flow.stack.GetElementDebugData<IUnitDebugData>(this);

            editorData.lastInvokeFrame = EditorTimeBinding.frame;
            editorData.lastInvokeTime = EditorTimeBinding.time;
        }

        if (coroutine) {
            flow.StartCoroutine(onChangeDataItem,
                flow.stack.GetElementData<Data>(this)
                    .activeCoroutines);
        } else {
            flow.Run(onChangeDataItem);
        }
    }

    public override void Dispose() {
        base.Dispose();
    }

    public override void Uninstantiate(GraphReference instance) {
        var data = instance.ToStackPooled().GetElementData<Data>(this);
        data.UnregisterEventPoints();
        base.Uninstantiate(instance);
    }

}

}