using Nighday.VisualScripting;
using Unity.VisualScripting;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("DataItemUI.OnSetData")]
[UnitCategory("_Nighday/ItemDataSystem/ItemDataUI")]
public class DataItemUIOnSetData : CustomEventUnit<object> {
    
    [DoNotSerialize]
    public DataItemUI _dataItemUI;

    [DoNotSerialize]
    public ValueOutput dataItemUIOutput;

    [DoNotSerialize]
    public ValueOutput dataItemOutput;

    [DoNotSerialize]
    public ValueOutput valueByKeyPathOutput;
    
    //===
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow   flow,
                                          object args) {

        _dataItemUI = args as DataItemUI;
        UnityEngine.Debug.LogFormat("ShouldTrigger args = {0}", args);

        return true;
    }

    private EventHook _hook;

    public override EventHook GetHook(GraphReference reference) {
        if (_hook.Equals(default)) {
            _hook = new EventHook("DataItemUIOnSetData",
                reference.machine);
        }

        return _hook;
    }
    //===
    
    protected override void Definition()
    {
        dataItemUIOutput = ValueOutput<DataItemUI>("dataItemUI",
            (flow) => {
                return _dataItemUI;
            });
        dataItemOutput = ValueOutput<ItemData>("dataItem",
            (flow) => {
                return _dataItemUI.dataItem;
            });
        valueByKeyPathOutput = ValueOutput<object>("valueByKeyPath",
            (flow) => {
                return _dataItemUI.valueByKeyPath;
            });
        
        base.Definition();
    }
    
}

}