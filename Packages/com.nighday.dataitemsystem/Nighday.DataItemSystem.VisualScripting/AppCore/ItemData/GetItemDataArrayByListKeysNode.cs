using Unity.VisualScripting;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("GetItemDataArrayByListKeysNode")]
[UnitCategory("_Nighday/ItemDataSystem/ItemData")]
public class GetItemDataArrayByListKeysNode : Unit {

    [DoNotSerialize]
    private ItemData[] _itemDataArray;

    [DoNotSerialize]
    public ValueInput itemDataInput;

    [DoNotSerialize]
    public ValueInput listKeysInput;

    [DoNotSerialize]
    public ValueOutput itemDataArrayOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        itemDataInput = ValueInput<ItemData>("itemData", null);
        listKeysInput = ValueInput<string[]>("listKeys", null);
        itemDataArrayOutput = ValueOutput<ItemData[]>("itemDataArray", (flow) => { return _itemDataArray; });

        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var itemData = flow.GetValue<ItemData>(itemDataInput);
        var listKeys = flow.GetValue<string[]>(listKeysInput);

        _itemDataArray = itemData.GetItemDataArrayByListKeys(listKeys);
        
        return exit;
    }

}

}