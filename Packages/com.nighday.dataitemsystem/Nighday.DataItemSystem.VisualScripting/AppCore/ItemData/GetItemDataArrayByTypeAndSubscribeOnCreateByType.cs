using System;
using Nighday.VisualScripting;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("GetItemDataArrayByTypeAndSubscribeOnCreateByType")]
[UnitCategory("_Nighday/ItemDataSystem/ItemData")]
public class GetItemDataArrayByTypeAndSubscribeOnCreateByType : MultipleCustomEventUnit<EmptyEventArgs> {

    [DoNotSerialize]
    private ItemData[] _itemDataArray;

    [DoNotSerialize]
    public ValueInput itemDataInput;

    [DoNotSerialize]
    public ValueInput typeValueInput;

    [DoNotSerialize]
    public ValueOutput itemDataArrayOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    [Serialize]
    [Inspectable, UnitHeaderInspectable("Once call")]
    [InspectorToggleLeft]
    public bool onceCall { get; set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        onChangeArrayByType = ControlOutput(nameof(onChangeArrayByType));

        itemDataInput = ValueInput<ItemData>("itemData");
        typeValueInput = ValueInput<string>("type", null);
        itemDataArrayOutput = ValueOutput<ItemData[]>("itemDataArray", (flow) => { return _itemDataArray; });
        
        Succession(enter, exit);
        // Succession(enter, onChangeArrayByType);

        base.Definition();
    }
    
    private ControlOutput Trigger(Flow flow) {
        try {
            var data = flow.stack.GetElementData<Data>(this);
            var reference = flow.stack.ToReference();
            var onChangeArrayByType = GetHook_OnChangeArrayByType(reference);
            var eventPoint_Complete = data.RegisterEventPoint(onChangeArrayByType, (args) => { Trigger_OnChangeArrayByType(reference, args); });

            var itemData = flow.GetValue<ItemData>(itemDataInput);
            var type = flow.GetValue<string>(typeValueInput);

            _itemDataArray = itemData.GetItemDataArrayByType(type);

            Action<ItemData> action = null;
            action = (_) => {
                try {
                    // if (!onceCall) {
                    //     itemData.UnsubscribeOnCreateItemDataByType(type, action);
                    // }
                    if (onceCall) {
                        itemData.UnsubscribeOnCreateItemDataByType(type, action);
                        data.UnregisterEventPoint(eventPoint_Complete);
                        // } else {
                        //     itemData.SubscribeOnCreateItemDataByType(type, action);
                    }

                    _itemDataArray = itemData.GetItemDataArrayByType(type);

                    EventBus.Trigger(onChangeArrayByType);
                } catch (Exception e) {
                    this.SetException(flow.stack, e);
                    UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
                }
            };
            
            itemData.SubscribeOnCreateItemDataByType(type, action);
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }
        return exit;
    }
    
    //===
    
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow           flow,
                                          EmptyEventArgs args) {
        return true;
    }
    
    [DoNotSerialize]
    [PortLabel("OnChangeArrayByType")]
    public ControlOutput onChangeArrayByType { get; private set; }

    [DoNotSerialize]
    private EventHook _hook_OnChangeArrayByType;

    public EventHook GetHook_OnChangeArrayByType(GraphReference reference) {
        _hook_OnChangeArrayByType = new EventHook("GetItemDataArrayByTypeAndSubscribeOnCreateByType_OnChangeArrayByType_" + Guid.NewGuid()
                                          .ToString(),
            reference.machine);

        return _hook_OnChangeArrayByType;
    }
    
    public override void StartListening(GraphStack stack) {
        var data = stack.GetElementData<Data>(this);

        if (data.isListening) {
            return;
        }

        if (register) {
            var reference = stack.ToReference();
            
            //data.RegisterEventPoint(GetHook_OnChangeArrayByType(reference), (args) => { Trigger_OnChangeArrayByType(reference, args); });
        }

        data.isListening = true;
    }

    public override void StopListening(GraphStack stack) {
        var data = stack.GetElementData<Data>(this);

        if (!data.isListening) {
            return;
        }

        // The coroutine's flow will dispose at the next frame, letting us
        // keep the current flow for clean up operations if needed
        foreach (var activeCoroutine in data.activeCoroutines) {
            activeCoroutine.StopCoroutine(false);
        }

        if (register) {
            //data.UnregisterEventPoints();
        }

        data.isListening = false;
    }

    public override void Uninstantiate(GraphReference instance) {
        var data = instance.ToStackPooled().GetElementData<Data>(this);
        data.UnregisterEventPoints();
        base.Uninstantiate(instance);
    }

    public void Trigger_OnChangeArrayByType(GraphReference reference,
                                            EmptyEventArgs args) {
        var flow = Flow.New(reference);

        if (!ShouldTrigger(flow, args)) {
            flow.Dispose();
            return;
        }

        AssignArguments(flow, args);

        Run_OnChangeArrayByType(flow);
    }
    
    private void Run_OnChangeArrayByType(Flow flow) {
        if (flow.enableDebug) {
            var editorData = flow.stack.GetElementDebugData<IUnitDebugData>(this);

            editorData.lastInvokeFrame = EditorTimeBinding.frame;
            editorData.lastInvokeTime = EditorTimeBinding.time;
        }

        if (coroutine) {
            flow.StartCoroutine(onChangeArrayByType,
                flow.stack.GetElementData<Data>(this)
                    .activeCoroutines);
        } else {
            flow.Run(onChangeArrayByType);
        }
    }
    
    //===

}

}