using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("GetItemDataArrayByType")]
[UnitCategory("_Nighday/ItemDataSystem/ItemData")]
public class GetItemDataArrayByTypeNode : Unit {

    [DoNotSerialize]
    private ItemData[] _itemDataArray;

    [DoNotSerialize]
    public ValueInput itemDataInput;

    [DoNotSerialize]
    public ValueInput typeValueInput;

    [DoNotSerialize]
    public ValueOutput itemDataArrayOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        itemDataInput = ValueInput<ItemData>("itemData");
        typeValueInput = ValueInput<string>("type", null);
        itemDataArrayOutput = ValueOutput<ItemData[]>("itemDataArray", (flow) => { return _itemDataArray; });

        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var itemData = flow.GetValue<ItemData>(itemDataInput);
        var type = flow.GetValue<string>(typeValueInput);

        _itemDataArray = itemData.GetItemDataArrayByType(type);
        
        return exit;
    }

}

}