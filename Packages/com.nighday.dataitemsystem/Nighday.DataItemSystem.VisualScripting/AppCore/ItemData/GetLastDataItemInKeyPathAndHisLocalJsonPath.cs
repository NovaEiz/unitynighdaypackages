using System;
using Nighday.Json;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("DataItem.GetLastDataItemInKeyPathAndHisLocalJsonPath")]
[UnitCategory("_Nighday/ItemDataSystem/ItemData")]
public class GetLastDataItemInKeyPathAndHisLocalJsonPath : Unit {

    [DoNotSerialize]
    private string _localJsonPath;

    [DoNotSerialize]
    private ItemData _itemData;

    [DoNotSerialize]
    public ValueInput itemDataInput;

    [DoNotSerialize]
    public ValueInput keyPathValueInput;

    [DoNotSerialize]
    public ValueOutput itemDataOutput;

    [DoNotSerialize]
    public ValueOutput jsonNodeOutput;

    [DoNotSerialize]
    public ValueOutput localJsonPathOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        itemDataInput = ValueInput<ItemData>("itemData");
        keyPathValueInput = ValueInput<string>("keyPath", null);
        
        itemDataOutput = ValueOutput<ItemData>("itemData_", (flow) => { return _itemData; });
        jsonNodeOutput = ValueOutput<JSONNode>("jsonNode", (flow) => { return _itemData.jsonNode; });
        localJsonPathOutput = ValueOutput<string>("localJsonPath", (flow) => { return _localJsonPath; });
        
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var itemData = flow.GetValue<ItemData>(itemDataInput);
        var keyPath = flow.GetValue<string>(keyPathValueInput);

        try {
            _itemData = itemData.GetItemDataByKeyPathReturnLastFoundInKeyPath(keyPath) as ItemData;

            var localJsonPathLen = keyPath.Length - _itemData.keyPath.Length;
            _localJsonPath = keyPath.Substring(_itemData.keyPath.Length, localJsonPathLen);
            if (_localJsonPath.Length > 0) {
                _localJsonPath = _localJsonPath.Substring(1, _localJsonPath.Length - 1);
            }
            Debug.Log("localJsonPathLen = " + localJsonPathLen + "; _itemData = " + _itemData + "; _localJsonPath = " + _localJsonPath + "; _itemData.keyPath = " + _itemData.keyPath + "; _itemData.key = " + _itemData.key);
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }
        return exit;
    }

}

}