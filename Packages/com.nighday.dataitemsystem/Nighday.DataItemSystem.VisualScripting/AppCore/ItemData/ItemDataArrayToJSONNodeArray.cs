using System.Collections.Generic;
using Nighday.Json;
using Unity.VisualScripting;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ItemDataArrayToJSONNodeArray")]
[UnitCategory("_Nighday/ItemDataSystem/ItemData")]
public class ItemDataArrayToJSONNodeArray : Unit {

    [DoNotSerialize]
    public ValueInput itemDataArrayInput;

    [DoNotSerialize]
    public ValueOutput jsonNodeArrayOutput;
    
    protected override void Definition() {
        itemDataArrayInput = ValueInput<ItemData[]>("itemDataArray");
        jsonNodeArrayOutput = ValueOutput<JSONNode[]>("jsonNodeArray",
            (flow) => {
                var itemDataArray = flow.GetValue<ItemData[]>(itemDataArrayInput);

                var jsonList = new List<JSONNode>();
                foreach (var itemData in itemDataArray) {
                    jsonList.Add(itemData.jsonNode);
                }

                return jsonList.ToArray();
            });
        
    }

}

}