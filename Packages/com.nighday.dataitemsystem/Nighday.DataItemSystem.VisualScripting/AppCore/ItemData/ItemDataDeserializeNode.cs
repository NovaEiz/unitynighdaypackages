using System;
using Nighday.Json;
using Unity.VisualScripting;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ItemData.Deserialize")]
[UnitCategory("_Nighday/ItemDataSystem/ItemData")]
public class ItemDataDeserializeNode : Unit {
    
    [DoNotSerialize]
    public ValueInput jsonNodeInput;

    [DoNotSerialize]
    public ValueInput itemDataInput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        jsonNodeInput = ValueInput<JSONNode>("jsonNode");
        itemDataInput = ValueInput<ItemData>("itemData");
        
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var jsonNode = flow.GetValue<JSONNode>(jsonNodeInput);
        var itemData = flow.GetValue<ItemData>(itemDataInput);
        
        try {
            itemData.Deserialize(jsonNode);
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }
        return exit;
    }

    
}

}