using System;
using Nighday.VisualScripting;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ItemData.GetOrWaitCreationByType")]
[UnitCategory("_Nighday/ItemDataSystem/ItemData")]
public class ItemDataGetOrWaitCreationItemDataByType : CustomEventUnit<EmptyEventArgs> {

    [DoNotSerialize]
    public ValueInput itemDataInput;

    [DoNotSerialize]
    public ValueInput itemTypeInput;

    [DoNotSerialize]
    public ValueOutput itemDataOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    [Serialize]
    [Inspectable, UnitHeaderInspectable("Once call")]
    [InspectorToggleLeft]
    public bool onceCall { get; set; }
    
    //===
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow           flow,
                                          EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;

    public override EventHook GetHook(GraphReference reference) {
        _hook = new EventHook("ItemDataGetOrWaitCreationItemDataByType_" + Guid.NewGuid().ToString(),
            reference.machine);

        return _hook;
    }
    //===

    private ItemData _itemData;

    protected override void Definition() {
        itemDataInput = ValueInput<ItemData>("itemData");
        itemTypeInput = ValueInput<string>("itemType", null);
        itemDataOutput = ValueOutput<ItemData>("itemData_", (flow) => { return _itemData; });
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        Succession(enter, exit);

        base.Definition();
    }
    
    private ControlOutput Trigger(Flow flow) {
        try {
            var itemData = flow.GetValue<ItemData>(itemDataInput);
            var itemType = flow.GetValue<string>(itemTypeInput);

            Action<ItemData> action = null;
            action = (targetItemData) => {
                try {
                    if (onceCall) {
                        itemData.UnsubscribeOnCreateItemDataByType(itemType, action);
                    }
                    
                    _itemData = targetItemData;
                    EventBus.Trigger(_hook);
                } catch (Exception e) {
                    this.SetException(flow.stack, e);
                    UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
                }
            };
            
            itemData.SubscribeOnCreateItemDataByType(itemType, action);
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }

        return exit;
    }


}

}