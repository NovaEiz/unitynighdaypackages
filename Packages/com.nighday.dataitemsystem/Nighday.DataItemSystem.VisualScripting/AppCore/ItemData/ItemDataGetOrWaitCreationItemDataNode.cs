using System;
using Nighday.VisualScripting;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ItemData.GetOrWaitCreation")]
[UnitCategory("_Nighday/ItemDataSystem/ItemData")]
public class ItemDataGetOrWaitCreationItemDataNode : CustomEventUnit<EmptyEventArgs> {

    [DoNotSerialize]
    public ValueInput itemDataInput;

    [DoNotSerialize]
    public ValueInput keyPathInput;

    [DoNotSerialize]
    public ValueOutput itemDataOutput;
    
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    //===
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow           flow,
                                          EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;

    public override EventHook GetHook(GraphReference reference) {
        _hook = new EventHook("ItemDataGetOrWaitCreationItemDataNode_" + Guid.NewGuid().ToString(),
            reference.machine);

        return _hook;
    }
    //===

    private ItemData _itemData;

    protected override void Definition() {
        itemDataInput = ValueInput<ItemData>("itemData");
        keyPathInput = ValueInput<string>("keyPath", null);
        itemDataOutput = ValueOutput<ItemData>("itemData_", (flow) => { return _itemData; });
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        Succession(enter, exit);

        base.Definition();
    }
    
    private ControlOutput Trigger(Flow flow) {
        var itemData = flow.GetValue<ItemData>(itemDataInput);
        var keyPath = flow.GetValue<string>(keyPathInput);

        var targetItemData = itemData.GetItemDataByKeyPath(keyPath);

        if (targetItemData != null) {
            try {
                _itemData = targetItemData;
                EventBus.Trigger(_hook);
            } catch (Exception e) {
                this.SetException(flow.stack, e);
                UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
            }
        } else {
            itemData.SubscribeOnCreateItemData(keyPath,
                (targetItemData) => {
                    try {
                        _itemData = targetItemData;
                        EventBus.Trigger(_hook);
                    } catch (Exception e) {
                        this.SetException(flow.stack, e);
                        UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
                    }
                });
        }
        

        return exit;
    }


}

}