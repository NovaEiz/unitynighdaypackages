using System;
using Nighday.Json;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ItemData")]
[UnitCategory("_Nighday/ItemDataSystem/ItemData")]
public class ItemDataNode : Unit {

    [DoNotSerialize]
    private ItemData _itemData;

    [DoNotSerialize]
    public ValueInput itemDataInput;

    [DoNotSerialize]
    public ValueInput keyPathValueInput;

    [DoNotSerialize]
    public ValueInput typeValueInput;

    [DoNotSerialize]
    public ValueOutput itemDataOutput;

    [DoNotSerialize]
    public ValueOutput ownerItemDataOutput;

    [DoNotSerialize]
    public ValueOutput parentItemDataOutput;

    [DoNotSerialize]
    public ValueOutput jsonNodeOutput;

    [DoNotSerialize]
    public ValueOutput keyOutput;

    [DoNotSerialize]
    public ValueOutput keyPathOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    [Serialize]
    [Inspectable, UnitHeaderInspectable("With create if not found")]
    [InspectorToggleLeft]
    public bool withCreateIfNotFound { get; set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        itemDataInput = ValueInput<ItemData>("itemData");
        keyPathValueInput = ValueInput<string>("keyPath", null);
        typeValueInput = ValueInput<string>("type", null);
        itemDataOutput = ValueOutput<ItemData>("itemData_", (flow) => { return _itemData; });
        ownerItemDataOutput = ValueOutput<ItemData>("ownerItemData_", (flow) => { return _itemData.ownerItemData; });
        parentItemDataOutput = ValueOutput<ItemData>("parentItemData", (flow) => { return _itemData.parentItemData; });
        jsonNodeOutput = ValueOutput<JSONNode>("jsonNode", (flow) => { return _itemData.jsonNode; });
        keyOutput = ValueOutput<string>("key", (flow) => { return _itemData.key; });
        keyPathOutput = ValueOutput<string>("keyPath", (flow) => { return _itemData.keyPath; });
        
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        try {
            var itemData = flow.GetValue<ItemData>(itemDataInput);
            var keyPath = flow.GetValue<string>(keyPathValueInput);
            var type = flow.GetValue<string>(typeValueInput);

            _itemData = itemData.GetItemDataByKeyPath(keyPath, withCreateIfNotFound, type) as ItemData;
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }
        return exit;
    }

}

}