using Unity.VisualScripting;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ItemData.Remove")]
[UnitCategory("_Nighday/ItemDataSystem/ItemData")]
public class ItemDataRemoveNode : Unit {

    [DoNotSerialize]
    public ValueInput itemDataInput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        Succession(enter, exit);

        itemDataInput = ValueInput<ItemData>("itemData");
    }
    
    private ControlOutput Trigger(Flow flow) {
        var itemData = flow.GetValue<ItemData>(itemDataInput);

        itemData.RemoveItemData();
        
        return exit;
    }

}

}