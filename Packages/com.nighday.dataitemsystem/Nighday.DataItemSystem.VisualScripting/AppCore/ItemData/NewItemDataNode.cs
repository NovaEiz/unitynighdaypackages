using Nighday.Socket;
using Unity.VisualScripting;

namespace Nighday.DataItemSystem {

[UnityEngine.Scripting.Preserve]
[UnitTitle("new ItemData")]
[UnitCategory("_Nighday/ItemDataSystem/ItemData")]
public class NewItemDataNode : Unit {

    [DoNotSerialize]
    private ItemData _itemData;

    [DoNotSerialize]
    public ValueOutput itemDataValueOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);
        
        itemDataValueOutput = ValueOutput<ItemData>("itemData", (flow) => { return _itemData; });
    }
    
    private ControlOutput Trigger(Flow flow) {
        _itemData = new ItemData();
        
        return exit;
    }

}

}