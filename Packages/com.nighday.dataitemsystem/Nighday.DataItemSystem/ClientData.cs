using Nighday.Json;
using Nighday.Socket;

namespace Nighday.DataItemSystem {

public class ClientData : ItemData {

    public static ClientData Instance;//TODO: УБРАТЬ!

    public WebSocketConnection_ connection { get; private set; }

    public ClientData(WebSocketConnection_ connection) : base() {
        Instance = this;
        this.connection = connection;
        ownerItemData = this;
        // isDirty = true;
        _itemDataOwnerManagement = new ItemDataOwnerManagement();

        ClientDataUpdater.update += Update;
    }

    public override void Deserialize(JSONNode data) {
        DeserializeOwner(data);
    }

    public void DeserializeOwner(JSONNode data) {
        DeserializeChanges(data["meta"]["data"]);
        ServerTime.Update(data["serverTime"]);
        
        _itemDataOwnerManagement.ActualizeOwnerState();
    }

    private void Update() {
        ActualizeOwnerState();
    }
    


}

}