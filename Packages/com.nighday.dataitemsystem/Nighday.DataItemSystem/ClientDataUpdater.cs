using System;
using UnityEngine;

namespace Nighday.DataItemSystem {

public class ClientDataUpdater : MonoBehaviour {

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
    public static void StaticInitialization() {
        _instance = null;
    }

    public static ClientDataUpdater _instance;
    public static ClientDataUpdater instance {
        get {
            if (_instance == null) {
                var clientDataUpdaterGameObject = new GameObject("ClientDataUpdater");
                _instance = clientDataUpdaterGameObject.AddComponent<ClientDataUpdater>();
            }

            return _instance;
        }
    }

    public static Action update;

    private void Update() {
        update?.Invoke();
    }

}

}