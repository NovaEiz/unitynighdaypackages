using Nighday.UI;

namespace Nighday.DataItemSystem {

public class DataItemListItemView : ListItemView {

    private DataItemListView _listView;

    public new DataItemListView listView {
        get {
            if (_listView == null) {
                _listView = base.listView as DataItemListView;
            }

            return _listView;
        }
    }

    public string keyPath => listView.keyPath + ItemData.SEPARATOR_SYMBOL + index;

}

}