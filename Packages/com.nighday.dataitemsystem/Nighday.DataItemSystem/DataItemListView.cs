using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Nighday.Json;
using Nighday.UI;
using UnityEngine;

namespace Nighday.DataItemSystem {

public class DataItemListView : ListView {

    [SerializeField] private string _localKeyPath;
    [SerializeField] private bool _getDataItemElseGetJsonNode;
    
    private ItemData _dataItem;
    public ItemData dataItem {
        get {
            return _dataItem;
        }
        set {
            _dataItem = value;
        }
    }

    public string keyPath => dataItem.keyPath + ItemData.SEPARATOR_SYMBOL + _localKeyPath;

    public override void Bind(object dataItemObject) {
        dataItem = dataItemObject as ItemData;

        var dataItems = dataItem.jsonNode.GetByKeyPath(_localKeyPath);
        var array = dataItems.AsArray;
        var len = array.Count;
        var objectDataItemsArray = new object[len];

        for (var i = 0; i < len; i++) {
            var item = array[i];
            object itemValue = null;
            if (item.IsString) {
                itemValue = item.Value;
            } else if (item.IsNumber) {
                itemValue = item.AsFloat;
            } else {
                itemValue = item;
            }
            objectDataItemsArray[i] = itemValue;
        }
        
        base.Bind(objectDataItemsArray);
    }

}

}