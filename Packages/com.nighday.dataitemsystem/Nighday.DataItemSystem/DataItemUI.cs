using System;
using Nighday.Json;
using Nighday.UI;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.DataItemSystem {

public class DataItemUI : DataUI {

    [SerializeField] private string _localKeyPath;
    public                   string localKeyPath => _localKeyPath;

    public override object data {
        get {
            return _data;
        }
        set {
            _dataItem = value as ItemData;
            base.data = valueByKeyPath;
            OnChangeDataItem?.Invoke();
            EventBus.Trigger(new EventHook("DataItemUIOnSetData", gameObject.GetComponent<EventMachine<FlowGraph, ScriptGraphAsset>>()), this as DataItemUI);
        }
    }
    private ItemData _dataItem;
    public ItemData dataItem {
        get {
            return _dataItem;
        }
    }

    public Action OnChangeDataItem;

    public override string keyPath => dataItem.keyPath + ItemData.SEPARATOR_SYMBOL + _localKeyPath;

    public override object valueByKeyPath {
        get {
            var valueJsonNode = _dataItem.jsonNode.GetValueByKeyPath(localKeyPath);
            object value = null;
            if (valueJsonNode.IsString) {
                value = valueJsonNode.Value;
            } else if (valueJsonNode.IsNumber) {
                value = valueJsonNode.AsFloat;
            } else if (valueJsonNode.IsNull) {
                value = null;
            } else {
                value = valueJsonNode;
            }

            return value;
        }
    }

}

}