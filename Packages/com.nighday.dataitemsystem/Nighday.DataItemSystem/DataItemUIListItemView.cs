using System;
using Nighday.UI;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.DataItemSystem {

public class DataItemUIListItemView : DataUI {

    private DataItemListItemView _dataItemListItemView;
    public DataItemListItemView dataItemListItemView {
        get {
            if (_dataItemListItemView == null) {
                _dataItemListItemView = GetComponent<DataItemListItemView>();
            }
            return _dataItemListItemView;
        }
    }

    public override string keyPath => dataItemListItemView.keyPath;

}

}