using System;
using System.Collections.Generic;
using System.Linq;
using Nighday.Json;
using UnityEngine;

namespace Nighday.DataItemSystem {

public class ItemData {
    
    public const char SEPARATOR_SYMBOL = '.';

    public string guid;

    public ItemData() {
        jsonNode = new JSONObject();
        guid = Guid.NewGuid().ToString();
    }

    public void Init(string key,
                     string type) {
        jsonNode.Add("key", key);
        jsonNode.Add("type", type);
    }

    public JSONNode jsonNode { get; protected set; }

    public virtual void Deserialize(JSONNode changes) {
        DeserializeChanges(changes);
        ChangeItemData();
    }

    public void DeserializeChanges(JSONNode changes) {
        if (changes == null) {
            return;
        }
        
        foreach (var itemPair in changes) {
            var keyPath = itemPair.Key;
            if (string.IsNullOrEmpty(keyPath)) {
                keyPath = itemPair.Value["key"];
            }
            var keyPathSplit = keyPath.Split('.');
            if (keyPathSplit.Length > 1) {
                var changedItemData = GetItemDataByKeyPath(keyPath, true);
                
                // var parentItemData = changedItemData.parentItemData;
                // if (parentItemData.jsonNode[changedItemData.key] == null) {
                //     parentItemData.jsonNode.Add(changedItemData.key, itemPair.Value);
                // } else {
                //     parentItemData.jsonNode[changedItemData.key] = itemPair.Value;
                // }

                changedItemData.Deserialize(itemPair.Value);
            } else {

                var justData = true;
                if (itemPair.Value.IsObject) {
                    var itemType = itemPair.Value["type"];
                    if (itemType != null) {
                        var changedItemData = GetItemDataByKeyPath(keyPath, true, itemType.Value);

                        changedItemData.Deserialize(itemPair.Value);
                        justData = false;
                    }
                }

                if (justData) {
                    if (this.jsonNode[itemPair.Key] == null) {
                        this.jsonNode.Add(itemPair.Key, itemPair.Value);
                    } else {
                        this.jsonNode[itemPair.Key] = itemPair.Value;
                    }
                }
            }
        }
    }

    public void DeserializeRemoves(JSONNode removes) {
        if (removes == null) {
            return;
        }
        foreach (var itemPair in removes) {
            RemoveItemDataByKeyPath(itemPair.Value);
        }
    }

    public void ActualizeOwnerState() {
        ownerItemData._itemDataOwnerManagement.ActualizeOwnerState();
    }
    
    public JSONNode GetItemDataChanges() {
        return ownerItemData._itemDataOwnerManagement.GetItemDataChanges();
    }

    public JSONArray GetItemDataRemoves() {
        return ownerItemData._itemDataOwnerManagement.GetItemDataRemoves();
    }

    // /// <summary>
    // /// Принимается на сервере. Отправляется клиентом
    // /// </summary>
    // /// <param name="jsonNode"></param>
    // public virtual JSONNode ApplyCommand(JSONNode jsonNode) {
    //     var key = jsonNode["key"].Value;
    //
    //     var itemData = GetItemDataByKeyPath(key);
    //     CommandItemData commandItemData = itemData as CommandItemData;
    //     
    //     var result = commandItemData.Execute();
    //     
    //     isDirty = true;
    //
    //     return result;
    // }
    
    // public ItemData GetCommandItemDataByKey(string key, bool createIfNotFound = false) {
    //     foreach (var itemPair in jsonNode) {
    //         var itemDataJsonNode = itemPair.Value;
    //         var itemDataKeyJsonNode = itemDataJsonNode["key"];
    //         if (itemDataKeyJsonNode != null) {
    //             var itemDataKey = itemDataKeyJsonNode.Value;
    //             if (itemDataKey == key) {
    //                 return GetItemData(itemDataKey, itemDataJsonNode);
    //             }
    //         }
    //     }
    //
    //     if (createIfNotFound) {
    //         return GetItemData(key, new JSONObject());
    //     }
    //     return null;
    // }
    
    // public virtual void NotificationOnChanged() {
    //     foreach (var itemPair in _itemDatas) {
    //         itemPair.Value.NotificationOnChanged();
    //     }
    //     if (!isDirty) {
    //         return;
    //     }
    //
    //     RunOnChange();
    // }
    
#region Events/Notifications

    public void SubscribeOnCreateItemData(string           keyPath_,
                                          Action<ItemData> callback) {
        ownerItemData._itemDataOwnerManagement.SubscribeOnCreateItemData(AppendPathKey(keyPath_), callback);
    }
    
    public void UnsubscribeOnCreateItemData(string           keyPath_,
                                            Action<ItemData> callback) {
        ownerItemData._itemDataOwnerManagement.UnsubscribeOnCreateItemData(AppendPathKey(keyPath_), callback);
    }

    public void SubscribeOnCreateItemDataByType(string           itemType,
                                                Action<ItemData> callback) {
        ownerItemData._itemDataOwnerManagement.SubscribeOnCreateItemDataByType(itemType, callback);
    }

    public void UnsubscribeOnCreateItemDataByType(string           itemType,
                                                  Action<ItemData> callback) {
        ownerItemData._itemDataOwnerManagement.UnsubscribeOnCreateItemDataByType(itemType, callback);
    }

    public void SubscribeOnCreateItemDataArrayByType(string           itemType,
                                                Action callback) {
        ownerItemData._itemDataOwnerManagement.SubscribeOnCreateItemDataArrayByType(itemType, callback);
    }

    public void UnsubscribeOnCreateItemDataArrayByType(string           itemType,
                                                  Action callback) {
        ownerItemData._itemDataOwnerManagement.UnsubscribeOnCreateItemDataArrayByType(itemType, callback);
    }
    
    private Action<ItemData> onChange;
    private Action<ItemData> onRemove;

    public void OnChange() {
        if (!isChanged) {
            return;
        }
        onChange?.Invoke(this);
        isChanged = false;
    }
    public void SubscribeOnChange(Action<ItemData> callback) {
        onChange += callback;
    }
    public void UnsubscribeOnChange(Action<ItemData> callback) {
        onChange -= callback;
    }

    public void OnRemove() {
        onRemove?.Invoke(this);
    }
    public void SubscribeOnRemove(Action<ItemData> callback) {
        onRemove += callback;
    }
    public void UnsubscribeOnRemove(Action<ItemData> callback) {
        onRemove -= callback;
    }
    
#endregion

#region Создание/Получение/Удаление

    protected Dictionary<string, ItemData> _itemDatas = new Dictionary<string, ItemData>();

    protected ItemData GetOrCreateItemData(string itemDataKey, JSONNode itemDataJsonNode, string type = null) {
        if (!_itemDatas.TryGetValue(itemDataKey, out ItemData itemData) && !string.IsNullOrEmpty(type)) {

            itemData = CreateItemData(itemDataKey, itemDataJsonNode, type);
            
            // _listAddedItems.Add((itemData.keyPath, itemData));
            _itemDatas.Add(itemDataKey, itemData);
        }

        return itemData;
    }

    private Action _actionsAfterForeach;

    private ItemData CreateItemData(string itemDataKey, JSONNode itemDataJsonNode, string type) {
        ItemData itemData = new ItemData();
        itemData.ownerItemData = this.ownerItemData;
        itemData.parentItemData = this;
        itemData.Init(itemDataKey, type);

        Action action = () => {
            if (this.jsonNode[itemDataKey] == null) {
                this.jsonNode.Add(itemDataKey, itemData.jsonNode);
            } else {
                this.jsonNode[itemDataKey] = itemData.jsonNode;
            }
            
            itemData.Deserialize(itemDataJsonNode);

            ownerItemData._itemDataOwnerManagement.AddCreated(itemData);
        
            parentItemData?.ChildItemDataChanged();
        };
        if (_inForeach) {
            _actionsAfterForeach += action;
        } else {
            action();
        }


        return itemData;
    }

    public void ChildItemDataChanged() {
        ownerItemData._itemDataOwnerManagement.AddChildChanged(this);
    }

    public void ChangeItemData() {
        this.isChanged = true;
        ownerItemData._itemDataOwnerManagement.AddChanged(this);

        parentItemData?.ChildItemDataChanged();
    }

    public void RemoveItemData() {
        parentItemData._itemDatas.Remove(this.key);// Удаляется из списка ItemData's в родителе
        ownerItemData._itemDataOwnerManagement.AddRemoved(this);
        
        parentItemData?.ChildItemDataChanged();
    }
    
#endregion

#region ItemData Methods
    
    public ItemData RemoveItemDataByKeyPath(string keyPath) {
        if (string.IsNullOrEmpty(keyPath)) {
            return this;
        }
        // var keyPathSplit = keyPath.Split(SEPARATOR_SYMBOL).ToList();
        // var currentKey = keyPathSplit[keyPathSplit.Count-1];
        // keyPathSplit.RemoveAt(keyPathSplit.Count-1);
        // var parentKeyPath = string.Join(SEPARATOR_SYMBOL.ToString(), keyPathSplit);
        // var parentItemData = GetItemDataByKeyPath(parentKeyPath);
        var removedItemData = GetItemDataByKeyPath(keyPath);
        removedItemData.RemoveItemData();
        
        return removedItemData;
    }
    
    
    public ItemData GetItemDataByKeyPath(string keyPath, bool createIfNotFound = false, string type = null) {
        if (string.IsNullOrEmpty(keyPath)) {
            return this;
        }
        var keyPathSplit = keyPath.Split(SEPARATOR_SYMBOL).ToList();
        var currentKey = keyPathSplit[0];
        keyPathSplit.RemoveAt(0);
        var childItemData = GetChildItemDataByKey(currentKey, createIfNotFound, type);

        if (childItemData != null) {
            if (keyPathSplit.Count > 0) {
                keyPath = string.Join(SEPARATOR_SYMBOL.ToString(), keyPathSplit);
                return childItemData.GetItemDataByKeyPath(keyPath, createIfNotFound, type);
            } else {
                return childItemData;
            }
        }

        return null;
    }
    
    public ItemData GetItemDataByKeyPathReturnLastFoundInKeyPath(string keyPath, bool createIfNotFound = false, string type = null) {
        if (string.IsNullOrEmpty(keyPath)) {
            return this;
        }
        var keyPathSplit = keyPath.Split(SEPARATOR_SYMBOL).ToList();
        var currentKey = keyPathSplit[0];
        keyPathSplit.RemoveAt(0);
        var childItemData = GetChildItemDataByKey(currentKey, createIfNotFound, type);

        if (childItemData != null) {
            if (keyPathSplit.Count > 0) {
                keyPath = string.Join(SEPARATOR_SYMBOL.ToString(), keyPathSplit);
                return childItemData.GetItemDataByKeyPathReturnLastFoundInKeyPath(keyPath, createIfNotFound, type);
            } else {
                return childItemData;
            }
        }

        return this;
    }
    
    public ItemData[] GetItemDataArrayByListKeys(string[] listKeys) {
        List<ItemData> itemDataList = new List<ItemData>();

        foreach (var itemDataKey in listKeys) {
            var itemData = GetChildItemDataByKey(itemDataKey);
            itemDataList.Add(itemData);
        }

        return itemDataList.ToArray();
    }
    
    public ItemData[] GetItemDataArrayByType(string type) {
        if (string.IsNullOrEmpty(type)) {
            return new ItemData[0];
        }

        var array = GetChildItemDataByType(type);
        return array;
    }
    
    public ItemData GetChildItemDataByKey(string key, bool createIfNotFound = false, string type = null) {
        foreach (var itemPair in jsonNode) {
            var itemDataJsonNode = itemPair.Value;
            if (itemPair.Key == key) {
                return GetOrCreateItemData(key, itemDataJsonNode);
            }

            var itemDataKeyJsonNode = itemDataJsonNode["key"];
            if (itemDataKeyJsonNode != null) {
                var itemDataKey = itemDataKeyJsonNode.Value;
                if (itemDataKey == key) {
                    return GetOrCreateItemData(key, itemDataJsonNode);
                }
            }
        }

        if (createIfNotFound) {
            return GetOrCreateItemData(key, null, type);
        }
        return null;
    }
    
    public ItemData RemoveChildItemDataByKey(string key) {
        foreach (var itemPair in jsonNode) {
            var itemDataJsonNode = itemPair.Value;
            if (itemPair.Key == key) {
                return GetOrCreateItemData(key, itemDataJsonNode);
            }

            var itemDataKeyJsonNode = itemDataJsonNode["key"];
            if (itemDataKeyJsonNode != null) {
                var itemDataKey = itemDataKeyJsonNode.Value;
                if (itemDataKey == key) {
                    return GetOrCreateItemData(key, itemDataJsonNode);
                }
            }
        }

        return null;
    }

    private bool _inForeach;

    private void RunActionsAfterForeach() {
        _actionsAfterForeach?.Invoke();
        _actionsAfterForeach = null;
    }
    
    public ItemData[] GetChildItemDataByType(string type) {
        var list = new List<ItemData>();

        _inForeach = true;

        var index = 0;

        foreach (var itemPair in jsonNode) {
            var key = itemPair.Key;
            var value = itemPair.Value;

            var itemDataJsonNode = value;
            
            var itemDataKeyJsonNode = itemDataJsonNode["key"];
            var itemDataTypeJsonNode = itemDataJsonNode["type"];
            
            if (itemDataKeyJsonNode != null) {
                var itemDataType = itemDataTypeJsonNode.Value;
                var itemDataKey = itemDataKeyJsonNode.Value;
                if (itemDataType == type) {
                    var itemData = GetOrCreateItemData(itemDataKey, itemDataJsonNode, type);
                    index++;
                    list.Add(itemData);
                }
            }
            
        }

        _inForeach = false;
        RunActionsAfterForeach();
        
        return list.ToArray();
    }
    
#endregion

#region KeyPath
    
    public string type => this.jsonNode["type"];
    
    public bool isChanged { get; private set; }
    public bool isRemoved { get; private set; }
    
    public void ClearMarks() {
        isChanged = false;
    }

    public bool AnyParentIsChanged() {//TODO: остановился тут
        if (parentItemData != null) {
            if (parentItemData.isChanged || parentItemData.AnyParentIsChanged()) {
                return true;
            }
        } else {
            return this.isChanged;
        }

        return false;
    }
    
    protected ItemDataOwnerManagement _itemDataOwnerManagement;
    
    //===
    public ItemData ownerItemData;
    public ItemData parentItemData;

    public string key           => this.jsonNode["key"];

    public string keyPath {
        get {
            if (_keyPath == null) {
                _keyPath = GetKeyPath();
            }

            return _keyPath;
        }
    }
    private string parentKeyPath => parentItemData.GetKeyPath();

    private string _keyPath;

    private string GetKeyPath() {
        if (parentItemData != null) {
            var keyPath = parentItemData.GetKeyPath();
            if (keyPath != null) {
                keyPath += SEPARATOR_SYMBOL + key;
            } else {
                keyPath = key;
            }
            return keyPath;
        } else {
            return key;
        }
    }

    private string AppendPathKey(string key) {
        var keyPath = this.keyPath;
        if (!string.IsNullOrEmpty(keyPath)) {
            keyPath += ".";
        }

        keyPath += key;
        return keyPath;
    }
#endregion
    
#region CommandItemData

    public Action<ItemData, JSONNode> _onExecute;

    public void OnExecute(Action<ItemData, JSONNode> callback) {
        _onExecute += callback;
    }

    private JSONNode ExecuteCommand(JSONNode receivedData) {
        var result = new JSONObject();
        
        //TODO: тут выполнить проверку условий, и потом выполнить `incs`
        Debug.LogFormat("Выполняю команду с данными receivedData = {0}", receivedData);
        _onExecute?.Invoke(this, receivedData);
        Debug.LogFormat("2 Выполняю команду с данными receivedData = {0}", receivedData);

        return result;
    }

    /// <summary>
    /// Отправляется клинтом. Принимается на сервере.
    /// </summary>
    /// <param name="sendData"></param>
    /// <param name="resultCallback"></param>
    public void SendRunRequest(JSONNode sendData, Action<JSONNode> resultCallback) {
        var commandSendData = new JSONObject();
        commandSendData.Add("key", parentKeyPath);
        commandSendData.Add("command", key);
        commandSendData.Add("data", sendData);
        var clientData = ownerItemData as ClientData;
        clientData.connection.SendWithResult("/flow", commandSendData, resultCallback);
    }
    
    /// <summary>
    /// Принимается на сервере. Отправляется клиентом
    /// </summary>
    /// <param name="jsonNode"></param>
    public JSONNode ApplyCommand(JSONNode data) {
        var itemData = this.GetItemDataByKeyPath(data["key"]);
        var commandItemData = itemData.GetChildItemDataByKey(data["command"]);
        var result = commandItemData.ExecuteCommand(data["data"]);
        
        // ownerItemData._itemDataOwnerManagement.AddChanged(itemData);
        // isDirty = true;

        return result;
    }

#endregion
    
}

}