// using System;
// using UnityEngine;
//
// namespace Nighday.DataItemSystem {
//
// public class ItemDataActionManager : MonoBehaviour {
//
//     private static ItemDataActionManager _instance;
//
//     [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
//     public static void StaticInitialize() {
//         var obj = new GameObject("ItemDataActionManager");
//         DontDestroyOnLoad(obj);
//         _instance = obj.AddComponent<ItemDataActionManager>();
//     }
//
//     public static ItemDataActionManager Instance => _instance;
//
//     public void Add(Action action) {
//         _onUpdate += action;
//     }
//
//     private Action _onUpdate;
//
//     private void Update() {
//         if (_onUpdate != null) {
//             _onUpdate();
//             _onUpdate = null;
//         }
//     }
//
// }
//
// }