using System;
using System.Collections.Generic;
using UnityEngine;

namespace Nighday.DataItemSystem {

[CreateAssetMenu(fileName = "DataItemAsset", menuName = "Nighday/DataItemSystem/DataItemAsset", order = 0)]
public class DataItemAsset : ScriptableObject {

    [Serializable]
    public class JSONField {
        public string key;
        public string value;
    }

    [SerializeField] private List<JSONField> _fields = new List<JSONField>();

    
    
}

}