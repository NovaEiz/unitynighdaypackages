using System;
using System.Collections.Generic;
using Nighday.Json;
using UnityEngine;

namespace Nighday.DataItemSystem {

public class ItemDataOwnerManagement {
    private List<ItemData> creates       = new List<ItemData>();
    private List<ItemData> changes       = new List<ItemData>();
    private List<ItemData> parentChanges = new List<ItemData>();
    private List<ItemData> removes       = new List<ItemData>();
    
    private List<ItemData> creates_       = new List<ItemData>();
    private List<ItemData> changes_       = new List<ItemData>();
    private List<ItemData> parentChanges_ = new List<ItemData>();
    private List<ItemData> removes_       = new List<ItemData>();

    private JSONNode actualCreates;
    private JSONNode actualChanges;
    private JSONArray actualRemoves;
    
    public void AddCreated(ItemData itemData) {
        if (!creates.Contains(itemData)) {
            creates.Add(itemData);
        }

        AddChanged(itemData);
    }
    
    public void AddChildChanged(ItemData itemData) {
        if (!parentChanges.Contains(itemData)) {
            parentChanges.Add(itemData);
        }
    }

    public void AddChanged(ItemData itemData) {
        if (!changes.Contains(itemData)) {
            changes.Add(itemData);
        }
    }

    public void AddRemoved(ItemData itemData) {
        if (!removes.Contains(itemData)) {
            removes.Add(itemData);
        }
    }

    public JSONNode GetItemDataCreates() {
        return actualCreates;
    }
    public JSONNode GetItemDataChanges() {
        return actualChanges;
    }
    public JSONArray GetItemDataRemoves() {
        return actualRemoves;
    }
    
    public void CalculateItemDataCreates() {
        actualCreates = new JSONObject();
        foreach (var itemData in creates_) {
            actualCreates.Add(itemData.keyPath, itemData.jsonNode);
        }
        // creates_ = new List<ItemData>();
    }
    public void CalculateItemDataChanges() {
        actualChanges = new JSONObject();
        foreach (var itemData in changes_) {
            if (!itemData.AnyParentIsChanged()) {
                actualChanges.Add(itemData.keyPath, itemData.jsonNode);
            }
            itemData.ClearMarks();
        }
    }
    public void CalculateItemDataRemoves() {
        actualRemoves = new JSONArray();
        foreach (var itemData in removes_) {
            actualRemoves.Add(itemData.keyPath);
        }
        // removes_ = new List<ItemData>();
    }

    private void NotificationOnChildChanges() {
        foreach (var itemData in parentChanges_) {
            itemData.ChangeItemData();
            itemData.OnChange();
        }
    }

    private void NotificationOnCreated() {
        List<string> onCreatedByType = new List<string>();
        foreach (var itemData in creates_) {
            OnCreateItemData(itemData);
            if (!onCreatedByType.Contains(itemData.type)) {
                onCreatedByType.Add(itemData.type);
            }
        }
        foreach (var itemType in onCreatedByType) {
            OnCreateItemDataArrayByType(itemType);
        }
    }
    private void NotificationOnChanged() {
        foreach (var itemData in changes_) {
            itemData.OnChange();
        }
    }
    private void NotificationOnRemoves() {
        foreach (var itemData in removes_) {
            itemData.OnRemove();
        }
    }
    
    public void ActualizeOwnerState() {
        parentChanges_ = parentChanges.GetRange(0, parentChanges.Count);
        parentChanges.Clear();
        
        creates_ = creates.GetRange(0, creates.Count);
        creates.Clear();
        
        changes_ = changes.GetRange(0, changes.Count);
        changes.Clear();
        
        removes_ = removes.GetRange(0, removes.Count);
        removes.Clear();
        
        NotificationOnCreated();
        NotificationOnChanged();
        NotificationOnRemoves();
        NotificationOnChildChanges();
        
        CalculateItemDataCreates();
        CalculateItemDataChanges();
        CalculateItemDataRemoves();
    }
    
#region Events/Notifications
    
    private Dictionary<string, Action<ItemData>> _onCreateItemDataCallbacks       = new Dictionary<string, Action<ItemData>>();
    private Dictionary<string, Action<ItemData>> _onCreateItemDataByTypeCallbacks = new Dictionary<string, Action<ItemData>>();
    private Dictionary<string, Action> _onCreateItemDataArrayByTypeCallbacks = new Dictionary<string, Action>();

    public void OnCreateItemData(ItemData itemData) {
        var keyPath = itemData.keyPath;

        if (_onCreateItemDataCallbacks.TryGetValue(keyPath, out Action<ItemData> callback_)) {
            callback_(itemData);
        }

        OnCreateItemDataByType(itemData);
    }
    public void SubscribeOnCreateItemData(string keyPath, Action<ItemData> callback) {
        if (!_onCreateItemDataCallbacks.TryGetValue(keyPath, out Action<ItemData> callback_)) {
            _onCreateItemDataCallbacks.Add(keyPath, (_) => {});
        }

        _onCreateItemDataCallbacks[keyPath] += callback;
    }
    public void UnsubscribeOnCreateItemData(string keyPath, Action<ItemData> callback) {
        if (!_onCreateItemDataCallbacks.TryGetValue(keyPath, out Action<ItemData> callback_)) {
            return;
        }

        _onCreateItemDataCallbacks[keyPath] -= callback;
    }
    
    private void OnCreateItemDataByType(ItemData itemData) {
        var type = itemData.type;

        if (_onCreateItemDataByTypeCallbacks.TryGetValue(type, out Action<ItemData> callback_)) {
            callback_(itemData);
        }
    }
    public void SubscribeOnCreateItemDataByType(string itemType, Action<ItemData> callback) {
        if (!_onCreateItemDataByTypeCallbacks.TryGetValue(itemType, out Action<ItemData> callback_)) {
            _onCreateItemDataByTypeCallbacks.Add(itemType, (_) => {});
        }

        _onCreateItemDataByTypeCallbacks[itemType] += callback;
    }
    public void UnsubscribeOnCreateItemDataByType(string itemType, Action<ItemData> callback) {
        if (!_onCreateItemDataByTypeCallbacks.TryGetValue(itemType, out Action<ItemData> callback_)) {
            return;
        }

        _onCreateItemDataByTypeCallbacks[itemType] -= callback;
    }
    
    private void OnCreateItemDataArrayByType(string itemType) {
        if (_onCreateItemDataArrayByTypeCallbacks.TryGetValue(itemType, out Action callback_)) {
            callback_();
        }
    }
    public void SubscribeOnCreateItemDataArrayByType(string itemType, Action callback) {
        if (!_onCreateItemDataArrayByTypeCallbacks.TryGetValue(itemType, out Action callback_)) {
            _onCreateItemDataArrayByTypeCallbacks.Add(itemType, () => {});
        }

        _onCreateItemDataArrayByTypeCallbacks[itemType] += callback;
    }
    public void UnsubscribeOnCreateItemDataArrayByType(string itemType, Action callback) {
        if (!_onCreateItemDataArrayByTypeCallbacks.TryGetValue(itemType, out Action callback_)) {
            return;
        }

        _onCreateItemDataArrayByTypeCallbacks[itemType] -= callback;
    }
    
    
    
#endregion
    
}

}