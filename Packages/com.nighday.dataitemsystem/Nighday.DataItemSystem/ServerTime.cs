using Nighday.Json;
using UnityEngine;

namespace Nighday.DataItemSystem {

public class ServerTime {
    private static double _serverTime;
    private static float  _serverTimeAt;

    public static void Update(JSONNode serverTimeJsonNode) {
        _serverTimeAt = Time.unscaledTime;
        _serverTime = serverTimeJsonNode.AsDouble;
    }

    public static double currentTime => _serverTime + (Time.unscaledTime - _serverTimeAt) * 1000;
}
}