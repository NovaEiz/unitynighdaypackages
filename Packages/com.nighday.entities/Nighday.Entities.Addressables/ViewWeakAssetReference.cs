using Unity.Entities;

namespace Nighday {

[GenerateAuthoringComponent]
public struct ViewWeakAssetReference : IComponentData {

	public WeakAssetReference WeakAssetReference;

	
}
}