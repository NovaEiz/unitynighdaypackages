using Unity.Entities;
using UnityEngine;

namespace Nighday.Entities.Camera {
public class CameraTargetComponentRuntimeHybridAuthoring : MonoBehaviour, IRuntimeHybridConvertGameObjectToEntity {

	public void Convert(Entity entity, EntityManager EntityManager, RuntimeHybridGameObjectConversionSystem conversionSystem) {
		EntityManager.AddComponentData(entity, default(CameraTarget));
		// EntityManager.AddComponentObject(entity, transform.GetComponent<UnityEngine.Camera>());
	}
	
}
}