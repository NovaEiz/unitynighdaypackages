using Unity.Entities;
using UnityEngine;

namespace Nighday.Entities.Camera {
public class GameCameraComponentRuntimeHybridAuthoring : MonoBehaviour, IRuntimeHybridConvertGameObjectToEntity {

    public void Convert(Entity entity, EntityManager EntityManager, RuntimeHybridGameObjectConversionSystem conversionSystem) {
        EntityManager.AddComponentData(entity, default(CameraGameComponent));
        EntityManager.AddComponentObject(entity, transform.GetComponent<UnityEngine.Camera>());
    }
	
}
}