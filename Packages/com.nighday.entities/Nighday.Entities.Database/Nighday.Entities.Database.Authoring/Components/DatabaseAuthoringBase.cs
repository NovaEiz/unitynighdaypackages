using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Entities;
using Unity.Transforms;
using UnityEditor;
using UnityEngine;

namespace Nighday.Entities.Database {
public class DatabaseAuthoringBase<ComponentAuthoring, DatabaseSystem, MonoBehaviourComponentAuthoring, DatabaseSystemAuthoring> : DatabaseAuthoringBasePrimary
	where ComponentAuthoring : struct, IComponentData, IDatabaseItemComponentAuthoring
	where DatabaseSystem : DatabaseSystemBase<ComponentAuthoring>
	where MonoBehaviourComponentAuthoring : MonoBehaviour, IConvertGameObjectToEntity, IDatabaseItemComponentAuthoring
	where DatabaseSystemAuthoring : DatabaseSystemAuthoringBase<ComponentAuthoring, DatabaseSystem, MonoBehaviourComponentAuthoring> {

	[SerializeField] private List<MonoBehaviourComponentAuthoring> _items = new List<MonoBehaviourComponentAuthoring>();

	public override System.Type GetItemAuthoringType() {
		return typeof(MonoBehaviourComponentAuthoring);
	}
	
	public override IList GetItems() {
		return _items;
	}

	public override void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs) {
		foreach (var item in _items) {
			referencedPrefabs.Add(item.gameObject);
		}
	}
	
	public override void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		var buffer = dstManager.AddBuffer<Child>(entity);
		foreach (var item in _items) {
			buffer.Add(new Child {
				Value = conversionSystem.GetPrimaryEntity(item.gameObject)
			});
		}
		dstManager.AddComponentData(entity, new Prefab());
	}
	
#if UNITY_EDITOR
	public override void UpdateDatabase() {
		var databaseSystemAuthoring = Activator.CreateInstance<DatabaseSystemAuthoring>();
		var items = databaseSystemAuthoring.FindAllItemsInAssetDatabase();
		
		_items.Clear();
		_items.AddRange(items.AsEnumerable());
		EditorUtility.SetDirty(this);
	}
#endif
	
}
}