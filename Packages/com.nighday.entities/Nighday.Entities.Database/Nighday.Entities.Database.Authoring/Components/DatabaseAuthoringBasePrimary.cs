using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace Nighday.Entities.Database {
public abstract class DatabaseAuthoringBasePrimary : MonoBehaviour, IDeclareReferencedPrefabs, IConvertGameObjectToEntity {
	
	public virtual void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {

	}
	
	public virtual void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs) {

	}

	public virtual void UpdateDatabase() {
		
	}

	public abstract System.Type GetItemAuthoringType();

	public abstract IList GetItems();

}
}