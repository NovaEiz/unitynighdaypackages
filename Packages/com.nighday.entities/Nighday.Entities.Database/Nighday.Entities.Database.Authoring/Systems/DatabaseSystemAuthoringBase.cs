using System;
using System.Collections.Generic;
using Unity.Entities;
using UnityEditor;
using UnityEngine;

namespace Nighday.Entities.Database {


public abstract class DatabaseSystemAuthoringBase<ComponentAuthoring, DatabaseSystem, MonoBehaviourComponentAuthoring> : ComponentSystem
	where ComponentAuthoring : struct, IComponentData, IDatabaseItemComponentAuthoring
	where DatabaseSystem : DatabaseSystemBase<ComponentAuthoring>
	where MonoBehaviourComponentAuthoring : MonoBehaviour, IConvertGameObjectToEntity, IDatabaseItemComponentAuthoring
{
	protected override void OnUpdate() {
		
	}
	
#if UNITY_EDITOR
#region For AssetDatabase
	
	// Ускорить кешированием.
	public MonoBehaviourComponentAuthoring[] FindAllItemsInAssetDatabase() {
		List<MonoBehaviourComponentAuthoring> list = new List<MonoBehaviourComponentAuthoring>();
		var databaseSystem = Activator.CreateInstance<DatabaseSystem>();
		var allAssetPaths = AssetDatabase.GetAllAssetPaths();
		foreach (var assetPath in allAssetPaths) {
			var type = AssetDatabase.GetMainAssetTypeAtPath(assetPath);
			if (type == typeof(GameObject)) {
				var asset = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
				var item = asset.GetComponent<MonoBehaviourComponentAuthoring>();

				if (item != null) {
					var key = databaseSystem.GetKeyFromId(item.Id);
					if (asset.name != key) {
						AssetDatabase.RenameAsset(assetPath, key + ".prefab");//string.Join("/", assetPathSplit));
					}
					list.Add(item);
					// try {
					// 	var asyncOperationHandle = UnityEngine.AddressableAssets.Addressables.LoadAssetAsync<GameObject>(key);
					// 	asyncOperationHandle.Completed += (asyncOperationHandle_) => {
					// 		if (asyncOperationHandle_.Status == AsyncOperationStatus.Failed) {
					// 			var assetPathSplit = assetPath.Split('/');
					// 			assetPathSplit[assetPathSplit.Length - 1] = key + ".prefab";
					// 			AssetDatabase.RenameAsset(assetPath, key + ".prefab");//string.Join("/", assetPathSplit));
					//
					// 			SetAssetToAddressable(item, item.name, "ItemsWithoutGroup");
					// 		}
					// 	};
					// }
					// catch (Exception e) {
					// 	//Debug.LogError(e);
					// 	//throw;
					// }
				}
			}
		}

		return list.ToArray();
	}

#endregion
#endif

}
}