
using System;
using System.Collections.Generic;
using Nighday.Entities.Database;
using Nighday.UIToolkit;
using UnityEditor.UIElements;

namespace Nighday.Entities.Database {

#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

[CanEditMultipleObjects]
[CustomEditor(typeof(DatabaseAuthoringBasePrimary), true)]
public class DatabaseAuthoringBasePrimaryEditor : UnityEditor.Editor {

    private const string _fileName = "DatabaseAuthoringBasePrimaryEditor";

    private const string _pathFolder = "Packages/com.nighday.entities/Nighday.Entities.Database/Nighday.Entities.Database.Editor/";

    private const string _pathUXML = _pathFolder + "UXML/" + _fileName + ".uxml";
    private const string _pathUSS = _pathFolder + "USS/" + _fileName + ".uss";
    //private const string _pathUXML_ItemView = _pathFolder + _fileName + "_ItemView.uxml";

    private DatabaseAuthoringBasePrimary _target;
    private VisualElement _root;
    //private VisualTreeAsset _itemViewPrefab;

    //private Dictionary<int, GameObject> _prefabsList = new Dictionary<int, GameObject>();

    public void OnEnable() {
        _target = (DatabaseAuthoringBasePrimary)target;
        //_target.OnValidateEvent += OnValidateEvent;
    }
    
    // private void OnValidateEvent() {
    //     if (_target == null) {
    //         return;
    //     }
    //     if (_root == null) {
    //         return;
    //     }
    //     _target.OnValidateEvent += OnValidateEvent;
    //     
    //     var simpleListView = _root.Q<SimpleListView>();
    //     
    //     var targetSourceDataList = _target.Effects;
    //     simpleListView.itemsSource = targetSourceDataList;
    //     simpleListView.Refresh();
    // }
    //
    // private void GetSourceObject(int id, Action<GameObject> callback) {
    //     if (_prefabsList.TryGetValue(id, out GameObject prefabTry)) {
    //         callback(prefabTry.GetComponent<EffectAuthoring>());
    //         return;
    //     }
    //     
    //     var prefab = EffectsDatabaseSystem.LoadById_Editor<EffectsDatabaseSystem>(id);
    //
    //     callback(prefab.GetComponent<EffectAuthoring>());
    //     if (!_prefabsList.ContainsKey(id)) {
    //         _prefabsList.Add(id, prefab);
    //     }
    // }
    
    public override VisualElement CreateInspectorGUI() {
        if (_root != null) {
            return null;
        }

        var viewPrefab = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(_pathUXML);
        var styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>(_pathUSS);
        
        _root = viewPrefab.Instantiate();
        _root.styleSheets.Add(styleSheet);

        var updateDatabaseButton = _root.Q<Button>();

        updateDatabaseButton.clicked += () => {
            _target.UpdateDatabase();
            
            _listView.itemsSource = _target.GetItems();
        };

        RegisterList();
        
        return _root;
    }

#region MyRegion
    
    private VisualTreeAsset _itemViewPrefab;

    private const string _pathUXML_ItemView = _pathFolder + "UXML/" + _fileName + "_ItemView.uxml";
    
    private VisualTreeAsset GetItemViewPrefab() {
        if (_itemViewPrefab == null) {
            _itemViewPrefab = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(_pathUXML_ItemView);
        }
        return _itemViewPrefab;
    }

    private SimpleListView _listView;
    
    private void RegisterList() {
        var simpleListView = _root.Q<SimpleListView>();
        _listView = simpleListView;

        var itemAuthoringType = _target.GetItemAuthoringType();
        var targetSourceDataList = _target.GetItems();

        simpleListView.OnSizeChanged += () => {
            EditorUtility.SetDirty(_target.gameObject);
        };

        simpleListView.makeItem += () => {
            var itemView = GetItemViewPrefab().Instantiate();
            //itemView.Q<ObjectField>().objectType = typeof(EffectAuthoring);
            itemView.Q<ObjectField>().objectType = itemAuthoringType;
            return itemView;
        };
        simpleListView.makeEmptySourceItem += () => {
            return null;
        };

        simpleListView.bindItem += (itemView, i) => {
            try {
                var objectField = itemView.Q<ObjectField>();

                EventCallback<ChangeEvent<Object>> onChangedObjectField = (changeEvent) => {
                    //var newObj = (EffectAuthoring)changeEvent.newValue;
                    var newObj = changeEvent.newValue;
                    targetSourceDataList[i] = newObj;
                    // var newId = 0;
                    // if (newObj != null) {
                    //     newId = (newObj as IDatabaseItemComponentAuthoring).Id;
                    // }
                    // _target.Effects[i] = newId;
                    
                    EditorUtility.SetDirty(_target.gameObject);
                };

                itemView.userData = new List<object>() {onChangedObjectField};

                objectField.RegisterValueChangedCallback(onChangedObjectField);

                if (targetSourceDataList[i] != null) {
                    objectField.value = ((Component)targetSourceDataList[i]).gameObject;
                } else {
                    objectField.value = null;
                }
            }
            catch (Exception e) {
                Debug.LogError(e);
                throw;
            }
        };
        simpleListView.unbindItem += (itemView, i) => {
            try {
                var objectField = itemView.Q<ObjectField>();

                var listDataActions = (List<object>)(itemView.userData);
                var onChangedObjectField = (EventCallback<ChangeEvent<Object>>)listDataActions[1];
                
                objectField.UnregisterValueChangedCallback(onChangedObjectField);
            }
            catch (Exception e) {
                Debug.LogError(e);
                throw;
            }
        };

        simpleListView.itemsSource = targetSourceDataList;
    }


#endregion

}

#endif

}