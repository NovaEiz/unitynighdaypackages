using Unity.Entities;

namespace Nighday.Entities.Database {

public interface IDatabaseItemComponentAuthoring : IComponentData {
	int Id { get; }
}
}