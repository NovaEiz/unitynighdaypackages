using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace Nighday.Entities.Database {

public static class IntExt {
	public static string ToStringTiny2(this int value, FixedString32 format) {
		var valueStr = IntToString(value);
		for (int i = valueStr.Length; i < 4; i++) {
			valueStr = "0" + valueStr;
		}

		return valueStr;
	}
	public static string ToStringTiny(this int value, string format) {
		var valueStr = IntToString(value);
		while (valueStr.Length < format.Length) {
			var diff = format.Length - valueStr.Length;

			valueStr = format[diff-1] + valueStr;
		}

		return valueStr;
	}
	public static string IntToString(int a)
	{    
		var chars = new[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
		var str = string.Empty;
		if (a == 0)
		{
			str = chars[0];
		}
		else if (a == int.MinValue)
		{
			str = "-2147483648";
		}
		else
		{
			bool isNegative = (a < 0);
			if (isNegative)
			{
				a = -a;
			}

			while (a > 0)
			{
				str = chars[a % 10] + str;
				a /= 10;
			}

			if (isNegative)
			{
				str = "-" + str;
			}
		}

		return str;
	}
}

public interface IDatabaseSystem {
	public Entity GetPrefabById(int id);
}

public abstract class DatabaseSystemBase<Component> : ComponentSystem, IDatabaseSystem
	where Component : struct, IComponentData, IDatabaseItemComponentAuthoring
{
	
	private EntityQuery _entityQuery;

	protected override void OnCreate() {
		_entityQuery = GetEntityQuery(new EntityQueryDesc {
			All = new[] {
				ComponentType.ReadOnly<Prefab>(),
				ComponentType.ReadOnly<Component>()
			}
		});
	}
	
	protected override void OnUpdate() {
		
	}
	
	private static int _symbolsAmountInId = 4;

	private static string IdFormat {
		get {
			var format = "";
			for (var i = 0; i < _symbolsAmountInId; i++) {
				format += "0";
			}

			return format;
		}
	}

	protected virtual string AssetKeyPrefix => null;
	
	

	public string GetKeyFromId(int id) {
		var idStr = id.ToStringTiny(IdFormat);
		var key = AssetKeyPrefix + idStr;
		return key;
	}

	// public AsyncOperationHandle<GameObject> LoadAsyncById(int id) {
	// 	return UnityEngine.AddressableAssets.Addressables.LoadAssetAsync<GameObject>(GetKeyFromId(id));
	// }
	//
	// private Dictionary<int, Entity> _map = new Dictionary<int, Entity>();
	//
	// private Dictionary<int, Action<Entity>> _subscribesOnLoadedPrefabEntity = new Dictionary<int, Action<Entity>>();
	// public void GetPrefabByIdAsync(int id, Action<Entity> callback) {
	// 	foreach (var itemPair in _map) {
	// 		if (itemPair.Key == id) {
	// 			if (itemPair.Value.Equals(Entity.Null)) {
	// 				_subscribesOnLoadedPrefabEntity[id] += callback;
	// 				return;
	// 			}
	// 			callback(itemPair.Value);
	// 			return;
	// 		}
	// 	}
	//
	// 	_map.Add(id, Entity.Null);
	// 	_subscribesOnLoadedPrefabEntity.Add(id, (entity_) => {});
	// 	LoadAsyncById(id).Completed += (asyncOperationHandle_) => {
	// 		var prefab = asyncOperationHandle_.Result;
	// 		if (prefab == null) {
	// 			callback(Entity.Null);
	// 			return;
	// 		}
	// 		var instance = GameObject.Instantiate(prefab);
	// 		var convertToEntityInWorld = instance.AddComponent<ConvertToEntityInWorld>();
	// 		convertToEntityInWorld.World = World;
	// 		convertToEntityInWorld.Completed += (entity_) => {
	// 			EntityManager.AddComponentData(entity_, default(Prefab));
	//
	// 			_map[id] = entity_;
	// 			_subscribesOnLoadedPrefabEntity[id](entity_);
	// 			_subscribesOnLoadedPrefabEntity.Remove(id);
	// 			callback(entity_);
	// 		};
	// 		convertToEntityInWorld.Convert();
	// 	};
	// }
	//
	// public async Task<Entity> GetPrefabByIdAsync(int id) {
	// 	foreach (var itemPair in _map) {
	// 		if (itemPair.Key == id) {
	// 			if (itemPair.Value.Equals(Entity.Null)) {
	// 				//_subscribesOnLoadedPrefabEntity[id] += callback;
	// 				return Entity.Null;
	// 			}
	// 			//callback(itemPair.Value);
	// 			return itemPair.Value;
	// 		}
	// 	}
	//
	// 	_map.Add(id, Entity.Null);
	// 	_subscribesOnLoadedPrefabEntity.Add(id, (entity_) => {});
	//
	// 	Entity result = default;
	// 	var isDone = false;
	// 	LoadAsyncById(id).Completed += (asyncOperationHandle_) => {
	// 		var prefab = asyncOperationHandle_.Result;
	// 		var instance = GameObject.Instantiate(prefab);
	// 		var convertToEntityInWorld = instance.AddComponent<ConvertToEntityInWorld>();
	// 		convertToEntityInWorld.World = World;
	// 		convertToEntityInWorld.Completed += (entity_) => {
	// 			_map[id] = entity_;
	// 			_subscribesOnLoadedPrefabEntity[id](entity_);
	// 			_subscribesOnLoadedPrefabEntity.Remove(id);
	// 			result = entity_;
	// 			//callback(entity_);
	// 			isDone = true;
	// 		};
	// 		convertToEntityInWorld.Convert();
	// 	};
	// 	while (!isDone) {
	// 		await Task.Yield();
	// 	}
	//
	// 	return result;
	// }
	
	public Entity GetPrefabById(int id) {
		var componentArray = _entityQuery.ToComponentDataArray<Component>(Allocator.Temp);
		var entityArray = _entityQuery.ToEntityArray(Allocator.Temp);
		var len = entityArray.Length;
		for (var i = 0; i < len; i++) {
			var component = componentArray[i];

			if (component.Id == id) {
				return entityArray[i];
			}
		}
		
		return default;
	}
	
}
}