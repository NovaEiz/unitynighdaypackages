using System.Collections.Generic;
using Nighday.Entities;
using Unity.Entities;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Nighday.Entities {

public class PrefabConversionRuntimeHybridAuthoring : MonoBehaviour, IRuntimeHybridDeclareAssetReferencedPrefabs, IRuntimeHybridConvertGameObjectToEntity {

	[SerializeField] private AssetReference _prefabAssetReference;

	public void Convert(Entity                                  entity,
	                    EntityManager                           dstManager,
	                    RuntimeHybridGameObjectConversionSystem conversionSystem) {
		dstManager.AddComponentData(entity, new PrefabConversion {
			Entity = conversionSystem.GetPrimaryEntityFromAssetReference(_prefabAssetReference)
		});
}

	public void DeclareAssetReferencedPrefabs(List<AssetReference> assetReferencedPrefabs) {
		assetReferencedPrefabs.Add(_prefabAssetReference);
	}
	
}
}