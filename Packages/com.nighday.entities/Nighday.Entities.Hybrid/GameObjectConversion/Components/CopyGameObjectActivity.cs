using UnityEditor;
using UnityEngine;

namespace Nighday.Entities {

public class CopyGameObjectActivity : MonoBehaviour {

	[SerializeField] private GameObject _target;

	public void SetTarget(GameObject target) {
		_target = target;
	}

	private void OnDisable() {
		if (_target != null) {
			_target.SetActive(false);
		}
	}

	private void OnEnable() {
		if (_target != null) {
			_target.SetActive(true);
		}
	}

	private void OnDestroy() {
		if (_target != null) {
			Destroy(_target);
		}
	}

}
}