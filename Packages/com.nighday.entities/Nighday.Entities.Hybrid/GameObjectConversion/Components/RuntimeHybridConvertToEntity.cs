using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Nighday.Entities {

public interface IRuntimeHybridDeclareReferencedPrefabs {

	void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs);
	
	Transform transform { get; }
}
public interface IRuntimeHybridDeclareAssetReferencedPrefabs {

	void DeclareAssetReferencedPrefabs(List<AssetReference> referencedPrefabs);
	
	Transform transform { get; }
}



public interface IRuntimeHybridConvertGameObjectToEntity {
	void Convert(Entity                     entity,
	             EntityManager              EntityManager,
	             RuntimeHybridGameObjectConversionSystem conversionSystem);
	
	Transform transform { get; }
}

[DisallowMultipleComponent]
public class RuntimeHybridConvertToEntity : MonoBehaviour {

	[SerializeField] private RuntimeHybridConvertToEntitySystem.WorldType _worldType;
	
	public RuntimeHybridConvertToEntitySystem.WorldType WorldType { get => _worldType; set => _worldType = value; }

	private List<World> _worldsForConvert;
	private List<World> _inWorldsConverted = new List<World>();
	
	public bool wasConverted { get; private set; }

	public void AddWorldForConvert(World world) {
		if (_worldsForConvert == null) {
			_worldsForConvert = new List<World>();
		}

		if (!_inWorldsConverted.Contains(world)) {
			_inWorldsConverted.Add(world);
			_worldsForConvert.Add(world);
		}
	}
	
	public void Convert() {
		Debug.Log("gameObject = " + gameObject + "; Convert(); wasConverted = " + wasConverted);
		if (_worldsForConvert != null) {
			foreach (var world in _worldsForConvert) {
				RuntimeHybridConvertToEntitySystem.ConvertToWorld(world, gameObject);
			}
			_worldsForConvert = null;
			Debug.Log("1 gameObject = " + gameObject + "; Convert(); wasConverted = " + wasConverted);

		} else {
			if (!wasConverted) {
				Debug.Log("2 gameObject = " + gameObject + "; Convert(); wasConverted = " + wasConverted);

				RuntimeHybridConvertToEntitySystem.ConvertToWorld(_worldType, gameObject);
			}
		}
		wasConverted = true;
	}

	public void Awake() {
		Convert();
	}
	
}

}