using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Nighday.Entities {

public class RuntimeHybridConvertToEntitySystem : ComponentSystem {

    private struct NeedConvertToEntity : IComponentData { }

    [Flags]
    public enum WorldType {
        Any = 30,

        LobbyClient = 2,

        LobbyServer = 4,

        GameClient = 8,

        GameServer = 16,

        Default = 1
    }

    private static Dictionary<World, List<GameObject>> _convertComponents = new Dictionary<World, List<GameObject>>();

    public static void ConvertToWorld(WorldType  worldType,
                                      GameObject gameObject) {

        foreach (var world in GetWorldsByWorldType(worldType)) {
            ConvertToWorld(world, gameObject);
        }
    }

    public static void ConvertToWorld(World      world,
                                      GameObject gameObject) {

        if (!_convertComponents.TryGetValue(world, out List<GameObject> list)) {
            list = new List<GameObject>();
            _convertComponents.Add(world, list);

            world.EntityManager.CreateEntity(typeof(NeedConvertToEntity));
        }

        if (!list.Contains(gameObject)) {
            list.Add(gameObject);
        }
    }

    public static WorldType ConvertWorldToWorldType(World world) {
        WorldType worldType = WorldType.Default;
        if (world.GetExistingSystem<LobbyClientWorldSystem>() != null) {
            worldType = WorldType.LobbyClient;
        }
        else if (world.GetExistingSystem<LobbyServerWorldSystem>() != null) {
            worldType = WorldType.LobbyServer;
        }
        else if (world.GetExistingSystem<GameClientWorldSystem>() != null) {
            worldType = WorldType.GameClient;
        }
        else if (world.GetExistingSystem<GameServerWorldSystem>() != null) {
            worldType = WorldType.GameServer;
        }

        return worldType;
    }

    private static World[] GetWorldsByWorldType(WorldType worldType) {
        List<World> worlds = new List<World>();
        foreach (var world in World.All) {
            if (worldType == WorldType.Any) {
                worlds.Add(world);
            } else {
                if ((worldType & WorldType.LobbyClient) != 0) {
                    if (world.GetExistingSystem<LobbyClientWorldSystem>() != null) {
                        worlds.Add(world);
                    }
                }

                if ((worldType & WorldType.LobbyServer) != 0) {
                    if (world.GetExistingSystem<LobbyServerWorldSystem>() != null) {
                        worlds.Add(world);
                    }
                }

                if ((worldType & WorldType.GameClient) != 0) {
                    if (world.GetExistingSystem<GameClientWorldSystem>() != null) {
                        worlds.Add(world);
                    }
                }

                if ((worldType & WorldType.GameServer) != 0) {
                    if (world.GetExistingSystem<GameServerWorldSystem>() != null) {
                        worlds.Add(world);
                    }
                }

                if ((worldType & WorldType.Default) != 0) {
                    if (world.GetExistingSystem<LobbyClientWorldSystem>() == null
                      &&
                        world.GetExistingSystem<LobbyServerWorldSystem>() == null
                      &&
                        world.GetExistingSystem<GameClientWorldSystem>() == null
                      &&
                        world.GetExistingSystem<GameServerWorldSystem>() == null) {
                        worlds.Add(world);
                    }
                }
            }
        }

        return worlds.ToArray();
    }

    private List<GameObject> _declaredPrefabs = new List<GameObject>();

    private List<AssetReference> _declaredAssetReferencePrefabs = new List<AssetReference>();

    private RuntimeHybridGameObjectConversionSystem _runtimeHybridGameObjectConversionSystem;

    protected override void OnCreate() {
        base.OnCreate();

        _runtimeHybridGameObjectConversionSystem = World.GetExistingSystem<RuntimeHybridGameObjectConversionSystem>();
    }

    protected override void OnUpdate() {
        Entities
            .WithAll<NeedConvertToEntity>()
            .ForEach(async (Entity entity
            ) => {
                _convertComponents.TryGetValue(World, out List<GameObject> list);
                _convertComponents.Remove(World);
                EntityManager.DestroyEntity(entity);

                List<Task> tasks = new List<Task>();
                foreach (var item in list) {
                    var task = ConvertToEntityInWorld(item, World, false);
                    tasks.Add(task);
                }

                await Task.WhenAll(tasks);

                _declaredPrefabs.Clear();
                _declaredAssetReferencePrefabs.Clear();
            });
    }

    public async Task<(Entity entity, GameObject instance)> PrefabConvertToEntityInWorld(GameObject prefab,
                                                                                         World      World,
                                                                                         bool       fromCode = true,
                                                                                         bool needSetActive = true) {

        prefab.SetActive(false);
        var instance = GameObject.Instantiate(prefab);
        prefab.SetActive(true);

        var entity = await ConvertToEntityInWorld(instance, World, fromCode, needSetActive);

        return (entity, instance);
    }

    public async Task<Entity> ConvertToEntityInWorld(GameObject instance,
                                                     World      World,
                                                     bool       fromCode      = true,
                                                     bool       needSetActive = true) {
        if (fromCode) {
            var worldType = ConvertWorldToWorldType(World);
            // if (instance.GetComponent<RuntimeHybridConvertToEntity>() == null) {
            //    var runtimeHybridConvertToEntity = instance.AddComponent<RuntimeHybridConvertToEntity>();
            //    runtimeHybridConvertToEntity.WorldType = WorldType.Any;
            // }
            foreach (var item in instance.GetComponentsInChildren<RuntimeHybridConvertToEntity>(true)) {
                Debug.Log("Check runtimeHybrid item = " + item + "; item.WorldType = " + item.WorldType + "; worldType = " + worldType);
                if ((item.WorldType == 0) || ((item.WorldType & worldType) != 0)) {
                    item.AddWorldForConvert(World);
                    item.Convert();
                }
            }
        }
        instance.SetActive(true);//Здесь срабатывает Awake. Потому что перед созданием экземпляра, префаб был выключен.
        instance.SetActive(false);//Выключается сразу, чтобы следующие события не сработали.

        var EntityManager = World.EntityManager;
        var entity = EntityManager.CreateEntity(typeof(LocalToWorld), typeof(Translation), typeof(Rotation));
#if UNITY_EDITOR
        EntityManager.SetName(entity, instance.transform.name);
#endif
        EntityManager.AddComponentObject(entity, instance.transform);
        
        EntityManager.AddComponentData(entity, default(DestroyEntityWithDestroyedTransformComponentTag));

        var componentsDeclareReferencedPrefabs = GetIRuntimeHybridDeclareReferencedPrefabsComponent_BeforeChild_RuntimeHybridConvertToEntity(instance.transform);
        foreach (var component in componentsDeclareReferencedPrefabs) {
            component.DeclareReferencedPrefabs(_declaredPrefabs);
        }

        List<AssetReference> declaredAssetReferencePrefabs = new List<AssetReference>();

        var componentsDeclareAssetReferencedPrefabs = GetIRuntimeHybridDeclareAssetReferencedPrefabsComponent_BeforeChild_RuntimeHybridConvertToEntity(instance.transform);
        foreach (var component in componentsDeclareAssetReferencedPrefabs) {
            component.DeclareAssetReferencedPrefabs(declaredAssetReferencePrefabs);
        }

        foreach (var declaredAssetPrefab in declaredAssetReferencePrefabs) {
            if (!_declaredAssetReferencePrefabs.Contains(declaredAssetPrefab)) {
                _declaredAssetReferencePrefabs.Add(declaredAssetPrefab);
                
                if (declaredAssetPrefab.Asset == null) {
                    await declaredAssetPrefab.LoadAssetAsync<UnityEngine.Object>()
                        .Task;
                
                    _runtimeHybridGameObjectConversionSystem.DeclareReferencedPrefab(declaredAssetPrefab.Asset as GameObject);
                }
            }
        }

        declaredAssetReferencePrefabs.Clear();

        foreach (var declaredPrefab in _declaredPrefabs) {
            _runtimeHybridGameObjectConversionSystem.DeclareReferencedPrefab(declaredPrefab);
        }

        //TODO: зачем это? 
        // Ответ: Для того чтобы конвертировать объект, который конвертируется через код.
            var components = GetIRuntimeHybridConvertGameObjectToEntityComponent_BeforeChild_RuntimeHybridConvertToEntity(instance.transform);
            foreach (var component in components) {
                Debug.Log("thisCheck: instance = " + instance + "; component = " + component);
                component.Convert(entity, EntityManager, _runtimeHybridGameObjectConversionSystem);
            }

        if (needSetActive) {
            instance.SetActive(true);
        }
        
        return entity;
    }

    private static IRuntimeHybridConvertGameObjectToEntity[] GetIRuntimeHybridConvertGameObjectToEntityComponent_BeforeChild_RuntimeHybridConvertToEntity(Transform transform) {
        List<IRuntimeHybridConvertGameObjectToEntity> list = new List<IRuntimeHybridConvertGameObjectToEntity>();
        var components = transform.GetComponents<IRuntimeHybridConvertGameObjectToEntity>();
        list.AddRange(components);
        foreach (Transform item in transform) {
            if (item.GetComponent<RuntimeHybridConvertToEntity>() != null || item.GetComponent<StopRuntimeHybridConvertToEntity>() != null) {
                continue;
            }

            list.AddRange(GetIRuntimeHybridConvertGameObjectToEntityComponent_BeforeChild_RuntimeHybridConvertToEntity(item));
        }

        return list.ToArray();
    }

    private static IRuntimeHybridDeclareReferencedPrefabs[] GetIRuntimeHybridDeclareReferencedPrefabsComponent_BeforeChild_RuntimeHybridConvertToEntity(Transform transform) {
        List<IRuntimeHybridDeclareReferencedPrefabs> list = new List<IRuntimeHybridDeclareReferencedPrefabs>();
        var components = transform.GetComponents<IRuntimeHybridDeclareReferencedPrefabs>();
        list.AddRange(components);
        foreach (Transform item in transform) {
            if (item.GetComponent<RuntimeHybridConvertToEntity>() != null || item.GetComponent<StopRuntimeHybridConvertToEntity>() != null) {
                continue;
            }

            list.AddRange(GetIRuntimeHybridDeclareReferencedPrefabsComponent_BeforeChild_RuntimeHybridConvertToEntity(item));
        }

        return list.ToArray();
    }

    private static IRuntimeHybridDeclareAssetReferencedPrefabs[] GetIRuntimeHybridDeclareAssetReferencedPrefabsComponent_BeforeChild_RuntimeHybridConvertToEntity(Transform transform) {
        List<IRuntimeHybridDeclareAssetReferencedPrefabs> list = new List<IRuntimeHybridDeclareAssetReferencedPrefabs>();
        var components = transform.GetComponents<IRuntimeHybridDeclareAssetReferencedPrefabs>();
        list.AddRange(components);
        foreach (Transform item in transform) {
            if (item.GetComponent<RuntimeHybridConvertToEntity>() != null || item.GetComponent<StopRuntimeHybridConvertToEntity>() != null) {
                continue;
            }

            list.AddRange(GetIRuntimeHybridDeclareAssetReferencedPrefabsComponent_BeforeChild_RuntimeHybridConvertToEntity(item));
        }

        return list.ToArray();
    }

}

}