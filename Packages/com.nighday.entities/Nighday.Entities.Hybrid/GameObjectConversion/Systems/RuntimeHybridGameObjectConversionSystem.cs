using System.Collections.Generic;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Nighday.Entities {

public class RuntimeHybridGameObjectConversionSystem : ComponentSystem {

    protected override void OnUpdate() {
        
    }
    
    private Dictionary<int, Entity> _declaredPrefabEntityByPrefabDict = new Dictionary<int, Entity>();

    public void DeclareReferencedPrefab(GameObject prefab) {
        var declaredPrefabEntity = EntityManager.CreateEntity(typeof(LocalToWorld), typeof(Translation), typeof(Rotation));
#if UNITY_EDITOR
        EntityManager.SetName(declaredPrefabEntity, prefab.transform.name);
#endif
        EntityManager.AddComponentObject(declaredPrefabEntity, prefab.transform);

        _declaredPrefabEntityByPrefabDict.Add(prefab.GetInstanceID(), declaredPrefabEntity);
    }
    
    public Entity GetPrimaryEntity(UnityEngine.Object obj) {
        _declaredPrefabEntityByPrefabDict.TryGetValue(obj.GetInstanceID(), out Entity entity);
        return entity;
    }
    public Entity GetPrimaryEntityFromAssetReference(AssetReference asset) {
        var obj = (asset.Asset as GameObject);
        return GetPrimaryEntity(obj);
    }

}

}