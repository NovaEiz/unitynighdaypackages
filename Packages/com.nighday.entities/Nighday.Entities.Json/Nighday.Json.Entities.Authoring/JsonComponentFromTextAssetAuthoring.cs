using UnityEngine;
using Unity.Entities;

namespace Nighday.Json {

public class JsonComponentFromTextAssetAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

	[SerializeField] private TextAsset _textAsset;

	public TextAsset TextAsset => _textAsset;

	public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		dstManager.SetComponentData(entity, new JsonComponent {
			Value = _textAsset.text
		});
	}
	
}

}