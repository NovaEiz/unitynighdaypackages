using Unity.Entities;

namespace Nighday.Json {

public static class EntityExt {
	public static JSONArray ToJson(this Entity entity) {
		var entityJsonArray = new JSONArray();
		entityJsonArray.Add(entity.Index);
		entityJsonArray.Add(entity.Version);
		return entityJsonArray;
	}
}

}