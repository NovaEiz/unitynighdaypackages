using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Entities;

namespace Nighday.Json {

[GenerateAuthoringComponent]
public class JsonComponent : IComponentData {
    
#if UNITY_EDITOR
    public void UpdateValueForInspector() {
        Value = jsonNode.ToString(4);
    }
#endif

    private Dictionary<string, List<(string, JSONNode, JSONNode)>> _nextNotificationPathes =
        new Dictionary<string, List<(string, JSONNode, JSONNode)>>();

    private Dictionary<string, Action<List<(string eventKeyPath, JSONNode previousValue, JSONNode newValue)>>> _registerNotificationOnChangedDict =
        new Dictionary<string, Action<List<(string eventKeyPath, JSONNode previousValue, JSONNode newValue)>>>();

    // ===

    public string Value;

    private JSONNode _jsonNode;

    public JSONNode jsonNode {
        get {
            if (_jsonNode == null) {
                if (string.IsNullOrEmpty(Value)) {
                    return null;
                }

                _jsonNode = JSON.Parse(Value);
            }

            return _jsonNode;
        }
    }

    public JSONNode JsonNode => jsonNode;

    private void SetValue(string value) {
        Value = value;
        
#if UNITY_EDITOR
        UpdateValueForInspector();
#endif
    }

    // ===

    public void FullChanges_(JSONNode value) {
        var previousParentValue = GetValueByPath("");
        var newParentValue = value;

        RecursiveReplaceValues("", previousParentValue, newParentValue);
    }

    public void ChangeValue_(string   keyPath,
                             JSONNode value
    ) {
        var previousParentValue = GetValueByPath(keyPath);
        JSONNode newParentValue = value;

        string pStr = "null";
        string nStr = "null";
        if (previousParentValue != null) {
            pStr = previousParentValue.ToString();
        }

        if (newParentValue != null) {
            nStr = newParentValue.ToString();
        }

        RecursiveReplaceValues(keyPath, previousParentValue, newParentValue);
    }

    // =========================================================================================================================================================================================

    private JSONNode GetParentValueByPath(string keyPath) {
        var keyPathSplit = keyPath.Split('.')
            .ToList();

        if (keyPathSplit.Count < 0) {
            return null;
        }

        if (keyPathSplit.Count == 1) {
            return jsonNode;
        }

        var lastIndex = keyPathSplit.Count - 1;
        var itemKey = keyPathSplit[lastIndex];

        var parentIsArray = ThisKeyIsArrayChildElement(itemKey);

        keyPathSplit.RemoveAt(lastIndex);
        keyPath = string.Join(".", keyPathSplit);
        return GetValueByPath(keyPath, jsonNode, true, parentIsArray);
    }

    private string GetLastKeyInPath(string keyPath) {
        var keyPathSplit = keyPath.Split('.')
            .ToList();

        if (keyPathSplit.Count < 1) {
            return null;
        }

        return keyPathSplit[keyPathSplit.Count - 1];
    }

    private string GetWithRemoveLastKeyInPath(string keyPath) {
        var keyPathSplit = keyPath.Split('.')
            .ToList();

        if (keyPathSplit.Count < 1) {
            return null;
        }

        keyPathSplit.RemoveAt(keyPathSplit.Count - 1);

        return string.Join(".", keyPathSplit);
    }

    public void RecursiveReplaceValues(string   keyPath,
                                       JSONNode previousValue,
                                       JSONNode newValue
    ) {
        AddPathToNextNotificationForChildElementsIfHasRegister_(
            keyPath,
            previousValue,
            newValue);

        var previousParentValue = GetParentValueByPath(keyPath);
        if (previousParentValue != null) {
            previousParentValue[GetLastKeyInPath(keyPath)] = newValue;
        } else {
            _jsonNode = newValue;
        }

        NextNotificationExecute();
    }

    private void AddPathToNextNotificationForChildElementsIfHasRegister_(string   keyPath,
                                                                         JSONNode previousParentValueAkaOrigin,
                                                                         JSONNode newParentValue
    ) {
        var keyPathLen = keyPath.Length;

        List<(string, JSONNode, JSONNode)> changedKeyPathItemList = new List<(string, JSONNode, JSONNode)>();

        foreach (var itemPair in _registerNotificationOnChangedDict) {
            var keyPathItem = itemPair.Key;

            var keyPathItemLen = keyPathItem.Length;

            if (keyPathItemLen >= keyPathLen) {

                if (keyPathItem.Substring(0, keyPathLen) == keyPath) {
                    var relativeKeyPath = "";

                    if (keyPathItemLen > keyPathLen) {
                        var startIndex = keyPathLen;
                        if (startIndex > 0) {
                            startIndex += 1;
                        }

                        relativeKeyPath = keyPathItem.Substring(startIndex, keyPathItemLen - startIndex);
                    }

                    JSONNode pValue = null;
                    JSONNode nValue = null;

                    if (string.IsNullOrEmpty(relativeKeyPath)) {
                        pValue = previousParentValueAkaOrigin;
                        nValue = newParentValue;
                    } else {
                        pValue = GetValueByPath(relativeKeyPath, previousParentValueAkaOrigin);
                        nValue = GetValueByPath(relativeKeyPath, newParentValue);
                    }

                    if ((pValue == null || nValue == null) || pValue.ToString() != nValue.ToString()) {
                        AddPathToNextNotification(keyPathItem, (keyPathItem, pValue, nValue));
                        changedKeyPathItemList.Add((keyPathItem, pValue, nValue));
                    }
                }
            }
        }

        // Второй обход, чтобы оповестить родительские события, и передать список дочерних изменений.
        foreach (var changedKeyPathItem in changedKeyPathItemList) {
            var keyPath_ = changedKeyPathItem.Item1;
            var keyPathLen_ = keyPath_.Length;

            foreach (var itemPair in _registerNotificationOnChangedDict) {
                var keyPathItem = itemPair.Key;
                var keyPathItemLen = keyPathItem.Length;

                if (keyPathItemLen < keyPathLen_) {
                    if (keyPath_.Substring(0, keyPathItemLen) == keyPathItem) {
                        JSONNode pValue = null;
                        JSONNode nValue = null;

                        pValue = changedKeyPathItem.Item2;
                        nValue = changedKeyPathItem.Item3;

                        string pStr = "null";
                        string nStr = "null";
                        if (pValue != null) {
                            pStr = pValue.ToString();
                        }

                        if (nValue != null) {
                            nStr = nValue.ToString();
                        }

                        AddPathToNextNotification(keyPathItem, (keyPath_, pValue, nValue));
                    }
                }
            }
        }
    }

    private string CombineKeyPath(params string[] keys) {
        var str = "";
        foreach (var key in keys) {
            if (string.IsNullOrEmpty(key)) {
                continue;
            }

            str += "." + key;
        }

        return str;
    }

    private string GetCorrectKeyElementByParent(string   key,
                                                JSONNode parent
    ) {
        if (parent.IsArray) {
            key = "[" + key + "]";
        }

        return key;
    }

    private string GetRealKeyElementByParent(string   key,
                                             JSONNode parent
    ) {
        if (parent.IsArray) {
            key = key.Substring(1, key.Length - 2);
        }

        return key;
    }

    private bool ThisKeyIsArrayChildElement(string key) {
        var isArray = key.Substring(0, 1) == "[";
        return isArray;
    }

    //==== ^ ====

    private void NextNotificationExecute() {
        foreach (var itemPair in _nextNotificationPathes) {
            var itemPairValue = itemPair.Value;

            var eventKeyPath = itemPair.Key;

            foreach (var registeredItemPair in _registerNotificationOnChangedDict) {
                var registeredEventKeyPath = registeredItemPair.Key;

                if (eventKeyPath == registeredEventKeyPath) {
                    RunNotification(itemPairValue, registeredItemPair.Value);
                }
            }
        }

        _nextNotificationPathes.Clear();
    }

    private void AddPathToNextNotification(string                                                           keyPath,
                                           (string eventKeyPath, JSONNode previousValue, JSONNode newValue) eventData
    ) {
        if (!_nextNotificationPathes.TryGetValue(keyPath, out List<(string, JSONNode, JSONNode)> list)) {
            list = new List<(string, JSONNode, JSONNode)>();
            _nextNotificationPathes.Add(keyPath, list);
        }

        list.Add(eventData);
    }

    private void RunNotification(List<(string eventKeyPath, JSONNode previousValue, JSONNode newValue)>         eventData,
                                 Action<List<(string eventKeyPath, JSONNode previousValue, JSONNode newValue)>> notificationCallback
    ) {
        notificationCallback(eventData);
    }

    public JSONNode GetValueByPath(string keyPath
    ) {
        if (string.IsNullOrEmpty(keyPath)) {
            return jsonNode;
        }

        var keyPathSplit = keyPath.Split('.')
            .ToList();

        return RecursiveGetValueByPath(keyPathSplit, jsonNode);
    }

    public JSONNode GetValueByPath(string   keyPath,
                                   JSONNode fromData,
                                   bool     needCreateObject    = false,
                                   bool     createObjectIsArray = false
    ) {
        var keyPathSplit = keyPath.Split('.')
            .ToList();

        var value = RecursiveGetValueByPath(keyPathSplit, fromData);
        if (needCreateObject && value == null) {
            if (createObjectIsArray) {
                value = CreateValueByPath(keyPath, new JSONArray());
            } else {
                value = CreateValueByPath(keyPath, new JSONObject());
            }
        }

        return value;
    }

    private JSONNode RecursiveGetValueByPath(List<string> keys,
                                             JSONNode     parent
    ) {
        if (parent == null) {
            return null;
        }

        var currentKey = keys[0];
        keys.RemoveAt(0);

        currentKey = GetCorrectKeyElementByParent(currentKey, parent);

        JSONNode value = null;

        if (parent.IsArray) {
            int.TryParse(currentKey, out int indexTry);

            value = parent.AsArray[indexTry];
        } else {
            value = parent[currentKey];
        }

        if (keys.Count == 0) {
            return value;
        } else {
            return RecursiveGetValueByPath(keys, value);
        }
    }

    private JSONNode CreateValueByPath(string   keyPath,
                                       JSONNode value
    ) {
        var parentValue = GetParentValueByPath(keyPath);
        var lastKey = GetLastKeyInPath(keyPath);

        if (parentValue == null) {
            if (ThisKeyIsArrayChildElement(lastKey)) {
                parentValue = CreateValueByPath(GetWithRemoveLastKeyInPath(keyPath), new JSONArray());
            } else {
                parentValue = CreateValueByPath(GetWithRemoveLastKeyInPath(keyPath), new JSONObject());
            }
        }

        if (parentValue.IsArray) {
            var parentValueArray = parentValue.AsArray;
            int.TryParse(GetRealKeyElementByParent(lastKey, parentValue), out int keyInt);
            while (parentValueArray.Count < keyInt + 1) {
                parentValueArray.Add(null);
            }

            parentValue[keyInt] = value;
        } else {
            parentValue[lastKey] = value;
        }

        return value;
    }

    public void RegisterNotificationOnChangedValue(string                                                                         key,
                                                   Action<List<(string eventKeyPath, JSONNode previousValue, JSONNode newValue)>> callback
    ) {
        if (!_registerNotificationOnChangedDict.TryGetValue(key,
                out Action<List<(string eventKeyPath, JSONNode previousValue, JSONNode newValue)>> action)) {
            action = (_) => { };
            _registerNotificationOnChangedDict.Add(key, action);
        }

        action += callback;
        _registerNotificationOnChangedDict[key] = action;
    }

    public void UnregisterNotificationOnChangedValue(string                                                                         key,
                                                     Action<List<(string eventKeyPath, JSONNode previousValue, JSONNode newValue)>> callback
    ) {
        if (!_registerNotificationOnChangedDict.TryGetValue(key,
                out Action<List<(string eventKeyPath, JSONNode previousValue, JSONNode newValue)>> action)
        ) {
            return;
        }

        action -= callback;
        _registerNotificationOnChangedDict[key] = action;
    }


}

}