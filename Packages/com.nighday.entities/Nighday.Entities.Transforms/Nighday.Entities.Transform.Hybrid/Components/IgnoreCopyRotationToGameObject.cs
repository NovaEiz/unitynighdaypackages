using Unity.Entities;

namespace Nighday.Entities.Transforms {

[GenerateAuthoringComponent]
public struct IgnoreCopyRotationToGameObject : IComponentData {
}
}