using Nighday.Mathematics;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Jobs;

namespace Nighday.Entities.Transforms {

[ExecuteAlways]
[UpdateInGroup(typeof(TransformSystemGroup))]
// [UpdateBefore(typeof(EndFrameTRSToLocalToWorldSystem))]
[UpdateAfter(typeof(EndFrameTRSToLocalToParentSystem))]
public class CopyTransformWithoutScaleWithoutRotationToGameObjectSystem : JobComponentSystem
{
	
    [BurstCompile]
    struct CopyTransforms : IJobParallelForTransform
    {
        [DeallocateOnJobCompletion]
        [ReadOnly] public NativeArray<LocalToWorld> LocalToWorlds;

        public void Execute(int index, TransformAccess transform)
        {
            var value = LocalToWorlds[index];
            transform.position = value.Position;
        }
    }

    EntityQuery m_TransformGroup;

    protected override void OnCreate() {
        m_TransformGroup = GetEntityQuery(new EntityQueryDesc {
            All = new[] {
                ComponentType.ReadOnly(typeof(CopyTransformWithScaleToGameObject)),
                ComponentType.ReadOnly<LocalToWorld>(),
                typeof(Transform),
                ComponentType.ReadOnly<IgnoreCopyRotationToGameObject>(),
                ComponentType.ReadOnly<IgnoreCopyScaleToGameObject>()
            }
        });
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        var transforms = m_TransformGroup.GetTransformAccessArray();
        var copyTransformsJob = new CopyTransforms
        {
            LocalToWorlds = m_TransformGroup.ToComponentDataArrayAsync<LocalToWorld>(Allocator.TempJob, out inputDeps),
        };

        return copyTransformsJob.Schedule(transforms, inputDeps);
    }
}





}