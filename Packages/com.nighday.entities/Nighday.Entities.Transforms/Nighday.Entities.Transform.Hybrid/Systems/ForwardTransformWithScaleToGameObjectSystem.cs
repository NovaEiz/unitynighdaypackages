using Nighday.Mathematics;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Jobs;

namespace Nighday.Entities.Transforms {

public static class float3Extensions {
	public static float3 moveTowards(float3 current, float3 target, float maxDistanceDelta) {
		float deltaX = target.x - current.x;
		float deltaY = target.y - current.y;
		float deltaZ = target.z - current.z;
		float sqdist = deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ;
		if (sqdist == 0 || sqdist <= maxDistanceDelta * maxDistanceDelta)
			return target;
		var dist = (float)math.sqrt(sqdist);
		return new float3(current.x + deltaX / dist * maxDistanceDelta,
						current.y + deltaY / dist * maxDistanceDelta,
						current.z + deltaZ / dist * maxDistanceDelta);
	}
}

public static class quaternion3Extensions {
	public static quaternion rotateTowards(
		quaternion from,
		quaternion to,
		float maxDegreesDelta) {
		float num = angle(from, to);
		return (double)num == 0.0 ? to : math.slerp(from, to, Mathf.Min(1f, maxDegreesDelta / num));//SlerpUnclamped
	}

	public static float angle(quaternion a, quaternion b) {
		float num = Mathf.Min(Mathf.Abs(math.dot(a, b)), 1f);
		return isEqualUsingDot(num) ? 0.0f : (float)((double)Mathf.Acos(num) * 2.0 * 57.295780181884766);
	}

	private static bool isEqualUsingDot(float dot) => (double)dot > 0.9999989867210388;

}

[ExecuteAlways]
[UpdateInGroup(typeof(TransformSystemGroup))]
[UpdateAfter(typeof(EndFrameLocalToParentSystem))]
public class ForwardTransformWithScaleToGameObjectSystem : JobComponentSystem {

	[BurstCompile]
	struct CopyTransforms : IJobParallelForTransform {
		[DeallocateOnJobCompletion]
		[ReadOnly] public NativeArray<LocalToWorld> LocalToWorlds;

		[ReadOnly] public float deltaTime;

		public void Execute(int index, TransformAccess transform) {
			var value = LocalToWorlds[index];

			transform.position = float3Extensions.moveTowards(transform.position, value.Position, deltaTime);
			transform.rotation = quaternion3Extensions.rotateTowards((((quaternion)transform.rotation).value),
																	new quaternion(value.Value).value, deltaTime);
			// transform.rotation = new quaternion(value.Value);

			transform.localScale = float3Extensions.moveTowards(transform.localScale, value.Value.GetScale(), deltaTime);

			// transform.localScale = value.Value.GetScale();
		}
	}

	EntityQuery m_TransformGroup;

	protected override void OnCreate() {
		m_TransformGroup = GetEntityQuery(new EntityQueryDesc {
			All = new[] {
				ComponentType.ReadOnly(typeof(ForwardTransformWithScaleToGameObject)),
				ComponentType.ReadOnly<LocalToWorld>(),
				typeof(Transform)
			},
			None = new[] {
				ComponentType.ReadOnly<IgnoreCopyRotationToGameObject>(),
			}
		});
	}

	protected override JobHandle OnUpdate(JobHandle inputDeps) {
		var deltaTime = Time.DeltaTime;
		var transforms = m_TransformGroup.GetTransformAccessArray();
		var copyTransformsJob = new CopyTransforms {
			LocalToWorlds = m_TransformGroup.ToComponentDataArrayAsync<LocalToWorld>(Allocator.TempJob, out inputDeps),
			deltaTime = deltaTime
		};

		return copyTransformsJob.Schedule(transforms, inputDeps);
	}
}


[ExecuteAlways]
[UpdateInGroup(typeof(TransformSystemGroup))]
[UpdateAfter(typeof(EndFrameLocalToParentSystem))]
public class ForwardTransformWithScaleWithIgnoreRotationToGameObjectSystem : JobComponentSystem {

	[BurstCompile]
	struct CopyTransforms : IJobParallelForTransform {
		[DeallocateOnJobCompletion]
		[ReadOnly] public NativeArray<LocalToWorld> LocalToWorlds;

		[ReadOnly] public float deltaTime;

		public void Execute(int index, TransformAccess transform) {
			var value = LocalToWorlds[index];
			transform.position = float3Extensions.moveTowards(transform.position, value.Position, deltaTime);
			transform.localScale = float3Extensions.moveTowards(transform.localScale, value.Value.GetScale(), deltaTime);
		}
	}

	EntityQuery m_TransformGroup;

	protected override void OnCreate() {
		m_TransformGroup = GetEntityQuery(ComponentType.ReadOnly(typeof(ForwardTransformWithScaleToGameObject)),
										ComponentType.ReadOnly<LocalToWorld>(),
										ComponentType.ReadOnly<IgnoreCopyRotationToGameObject>(), typeof(Transform));
	}

	protected override JobHandle OnUpdate(JobHandle inputDeps) {
		var deltaTime = Time.DeltaTime;
		var transforms = m_TransformGroup.GetTransformAccessArray();
		var copyTransformsJob = new CopyTransforms {
			LocalToWorlds = m_TransformGroup.ToComponentDataArrayAsync<LocalToWorld>(Allocator.TempJob, out inputDeps),
			deltaTime = deltaTime
		};

		return copyTransformsJob.Schedule(transforms, inputDeps);
	}
}


}