using Nighday.Mathematics;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Jobs;

namespace Nighday.Entities.Transforms {

[ExecuteAlways]
[UpdateInGroup(typeof(TransformSystemGroup))]
[UpdateAfter(typeof(EndFrameLocalToParentSystem))]
public class LerpTransformWithScaleToGameObjectSystem : JobComponentSystem {

	[BurstCompile]
	struct CopyTransforms : IJobParallelForTransform {
		[DeallocateOnJobCompletion]
		[ReadOnly] public NativeArray<LocalToWorld> LocalToWorlds;

		[ReadOnly] public float deltaTime;

		public void Execute(int index, TransformAccess transform) {
			var value = LocalToWorlds[index];
			transform.position = math.lerp(transform.position, value.Position, deltaTime);
			transform.rotation = math.slerp((((quaternion)transform.rotation)),
														new quaternion(value.Value), deltaTime);
			// transform.rotation = new quaternion(value.Value);

			transform.localScale = math.lerp(transform.localScale, value.Value.GetScale(), deltaTime);

			// transform.localScale = value.Value.GetScale();
		}
	}

	EntityQuery m_TransformGroup;

	protected override void OnCreate() {
		m_TransformGroup = GetEntityQuery(new EntityQueryDesc {
			All = new[] {
				ComponentType.ReadOnly(typeof(LerpTransformWithScaleToGameObject)),
				ComponentType.ReadOnly<LocalToWorld>(),
				typeof(Transform)
			},
			None = new[] {
				ComponentType.ReadOnly<IgnoreCopyRotationToGameObject>(),
			}
		});
	}

	protected override JobHandle OnUpdate(JobHandle inputDeps) {
		var deltaTime = Time.DeltaTime;
		var transforms = m_TransformGroup.GetTransformAccessArray();
		var copyTransformsJob = new CopyTransforms {
			LocalToWorlds = m_TransformGroup.ToComponentDataArrayAsync<LocalToWorld>(Allocator.TempJob, out inputDeps),
			deltaTime = deltaTime
		};

		return copyTransformsJob.Schedule(transforms, inputDeps);
	}
}


[ExecuteAlways]
[UpdateInGroup(typeof(TransformSystemGroup))]
[UpdateAfter(typeof(EndFrameLocalToParentSystem))]
public class LerpTransformWithScaleWithIgnoreRotationToGameObjectSystem : JobComponentSystem {

	[BurstCompile]
	struct CopyTransforms : IJobParallelForTransform {
		[DeallocateOnJobCompletion]
		[ReadOnly] public NativeArray<LocalToWorld> LocalToWorlds;

		[ReadOnly] public float deltaTime;

		public void Execute(int index, TransformAccess transform) {
			var value = LocalToWorlds[index];
			transform.position = math.lerp(transform.position, value.Position, deltaTime);
			transform.localScale = math.lerp(transform.localScale, value.Value.GetScale(), deltaTime);
		}
	}

	EntityQuery m_TransformGroup;

	protected override void OnCreate() {
		m_TransformGroup = GetEntityQuery(ComponentType.ReadOnly(typeof(LerpTransformWithScaleToGameObject)),
										ComponentType.ReadOnly<LocalToWorld>(),
										ComponentType.ReadOnly<IgnoreCopyRotationToGameObject>(), typeof(Transform));
	}

	protected override JobHandle OnUpdate(JobHandle inputDeps) {
		var deltaTime = Time.DeltaTime;
		var transforms = m_TransformGroup.GetTransformAccessArray();
		var copyTransformsJob = new CopyTransforms {
			LocalToWorlds = m_TransformGroup.ToComponentDataArrayAsync<LocalToWorld>(Allocator.TempJob, out inputDeps),
			deltaTime = deltaTime
		};

		return copyTransformsJob.Schedule(transforms, inputDeps);
	}
}


}