using System;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Nighday.Entities.Transform.Authoring {
public class ChildAuthoring : MonoBehaviour, IDeclareReferencedPrefabs, IConvertGameObjectToEntity {

	public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		DynamicBuffer<Child> childBuffer = default;
		if (dstManager.HasComponent<Child>(entity)) {
			childBuffer = dstManager.GetBuffer<Child>(entity);
		} else {
			childBuffer = dstManager.AddBuffer<Child>(entity);
		}
		
		foreach (UnityEngine.Transform item in transform) {
			childBuffer.Add(new Child {
				Value = conversionSystem.GetPrimaryEntity(item.gameObject)
			});
		}
	}

	public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs) {
		foreach (UnityEngine.Transform item in transform) {
			referencedPrefabs.Add(item.gameObject);
		}
	}
}

}