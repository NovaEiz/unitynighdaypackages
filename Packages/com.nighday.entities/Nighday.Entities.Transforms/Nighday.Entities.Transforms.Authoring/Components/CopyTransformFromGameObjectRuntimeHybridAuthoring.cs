using Nighday.Entities.Transforms;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Nighday.Entities.Transform {
public class CopyTransformFromGameObjectRuntimeHybridAuthoring : MonoBehaviour, IRuntimeHybridConvertGameObjectToEntity {

    [SerializeField] private CopyTransformMode _mode = CopyTransformMode.All;

    public void Convert(Entity entity, EntityManager EntityManager, RuntimeHybridGameObjectConversionSystem conversionSystem) {
        if (_mode == CopyTransformMode.All) {
            EntityManager.AddComponentData(entity, default(CopyTransformWithScaleFromGameObject));
        } else 
        if (_mode == (CopyTransformMode.Translation | CopyTransformMode.Rotation)) {
            EntityManager.AddComponentData(entity, default(CopyTransformFromGameObject));
        } else 
        if (_mode == (CopyTransformMode.Translation | CopyTransformMode.Scale)) {
            EntityManager.AddComponentData(entity, default(CopyTransformWithScaleFromGameObject));
            EntityManager.AddComponentData(entity, default(IgnoreCopyRotationFromGameObject));
        } else 
        if (_mode == (CopyTransformMode.Translation)) {
            EntityManager.AddComponentData(entity, default(CopyTransformWithScaleFromGameObject));
            EntityManager.AddComponentData(entity, default(IgnoreCopyRotationFromGameObject));
            EntityManager.AddComponentData(entity, default(IgnoreCopyScaleFromGameObject));
        }
        switch (_mode) {
        case (CopyTransformMode.All):
            break;
        case CopyTransformMode.Translation | CopyTransformMode.Rotation:
            break;
        case CopyTransformMode.Translation | CopyTransformMode.Scale:
            break;
        case CopyTransformMode.Rotation | CopyTransformMode.Scale:

            break;
        case CopyTransformMode.Translation:

            break;
        case CopyTransformMode.Rotation:

            break;
        case CopyTransformMode.Scale:

            break;
        }
    }

}

}