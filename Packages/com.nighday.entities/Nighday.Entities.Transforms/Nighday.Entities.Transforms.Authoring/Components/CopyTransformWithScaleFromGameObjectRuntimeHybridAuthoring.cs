using Nighday.Entities.Transforms;
using Unity.Entities;
using UnityEngine;

namespace Nighday.Entities.Transform {
public class CopyTransformWithScaleFromGameObjectRuntimeHybridAuthoring : MonoBehaviour, IRuntimeHybridConvertGameObjectToEntity {

    public void Convert(Entity entity, EntityManager EntityManager, RuntimeHybridGameObjectConversionSystem conversionSystem) {
        EntityManager.AddComponentData(entity, default(CopyTransformWithScaleFromGameObject));
    }

}

}