using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace BladeMaster.GameCore.Weapon.Authoring {

public class LinkedEntityGroupAuthoring : MonoBehaviour, IDeclareReferencedPrefabs, IConvertGameObjectToEntity {

	private List<GameObject> _list = new List<GameObject>();
	
	public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs) {
		ForEachRecursive(transform, referencedPrefabs);
	}
	public void ForEachRecursive(Transform transform, List<GameObject> referencedPrefabs) {
		foreach (Transform item in transform) {
			if (item.GetComponent<StopConvertToEntity>() != null) {
				continue;
			}

			var itemObj = item.gameObject;
			referencedPrefabs.Add(itemObj);
			_list.Add(itemObj);
			
			ForEachRecursive(item, referencedPrefabs);
		}
	}
	
	public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		var buffer = dstManager.AddBuffer<LinkedEntityGroup>(entity);

		buffer.Add(new LinkedEntityGroup {
			Value = entity
		});
		
		foreach (var item in _list) {
			Entity primary = conversionSystem.GetPrimaryEntity(item);

			buffer.Add(new LinkedEntityGroup {
				Value = primary
			});
		}
	}
}

}