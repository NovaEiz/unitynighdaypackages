﻿using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Nighday.Entities.Physics {

public class ScaleAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

	public void Convert(
		Entity entity,
		EntityManager dstManager,
		GameObjectConversionSystem conversionSystem
	) {
		dstManager.AddComponentData(entity, new Scale {
			Value = transform.localScale.x								
		});
	}

}

}