using Nighday.Entities;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Nighday.Entities.Transforms {

public class ScaleRuntimeHybridAuthoring : MonoBehaviour, IRuntimeHybridConvertGameObjectToEntity {

    public void Convert(Entity                                  entity,
                        EntityManager                           EntityManager,
                        RuntimeHybridGameObjectConversionSystem conversionSystem) {
        
        EntityManager.AddComponentData(entity, new Scale {
            Value = transform.localScale.x
        });
    }
}

}