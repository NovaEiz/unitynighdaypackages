namespace Nighday.Entities.Transforms {

[System.Flags]
public enum CopyTransformMode {
    Translation = 1,
    Rotation    = 2,
    Scale       = 4,
    All         = 7
}

}