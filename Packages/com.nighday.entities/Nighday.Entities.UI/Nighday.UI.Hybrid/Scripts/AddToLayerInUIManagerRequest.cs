using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Nighday.UI;
using Unity.Entities;
using UnityEngine;
using UnityEngine.UI;

namespace Nighday.UI {

public class AddToLayerInUIManagerRequest : MonoBehaviour, IConvertGameObjectToEntity {

	[SerializeField] private string _layerName;

	public async void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		UIManager uiManager = null;
		while (uiManager == null) {
			uiManager = UIManager.GetFromWorld(dstManager.World);
			await Task.Yield();
		}
            
		var canvasScaler = transform.GetComponent<CanvasScaler>();
		if (canvasScaler != null) {
			canvasScaler.enabled = false;
		}
		uiManager.AddToLayer(this.transform as RectTransform, _layerName);
		if (canvasScaler != null) {
			canvasScaler.enabled = true;
		}
	}

}
}