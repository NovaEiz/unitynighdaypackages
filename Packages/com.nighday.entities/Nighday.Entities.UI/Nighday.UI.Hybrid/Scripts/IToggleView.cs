using System;

namespace Nighday.UI {

public interface IToggleView {
	
	/// <summary>
	/// Привязать окно
	/// </summary>
	void BindLoadWindow(Action<Action<IWindowView>> callback);
	
}
}