using System;

namespace Nighday.UI {

public interface IViewCloser {
	void Close();
	event Action OnClosed;
}
}