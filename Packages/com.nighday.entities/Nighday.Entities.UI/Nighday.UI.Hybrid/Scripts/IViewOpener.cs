using System;

namespace Nighday.UI {

public interface IViewOpener {
	void Open();
	event Action OnOpened;
}
}