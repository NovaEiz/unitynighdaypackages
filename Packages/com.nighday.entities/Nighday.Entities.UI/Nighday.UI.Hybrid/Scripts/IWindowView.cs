using System;

namespace Nighday.UI {

public interface IWindowView {
	
	IViewOpener ViewOpener { get; }
	IViewCloser ViewCloser { get; }
	
}
}