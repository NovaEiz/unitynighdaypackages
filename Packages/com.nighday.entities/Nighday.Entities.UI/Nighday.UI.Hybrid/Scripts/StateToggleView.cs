using UnityEngine;
using UnityEngine.UI;

namespace Nighday.UI {

[DisallowMultipleComponent]
[ExecuteInEditMode]
public class StateToggleView : MonoBehaviour {

	[HideInInspector] [SerializeField] private Toggle _targetToggle;
	[SerializeField] private GameObject _onSprite;
	[SerializeField] private GameObject _offSprite;

	private void OnEnable() {
		if (_targetToggle == null) {
			return;
		}
		_targetToggle.toggleTransition = Toggle.ToggleTransition.None;
		_targetToggle.onValueChanged.RemoveListener(OnTargetToggleValueChanged);
		_targetToggle.onValueChanged.AddListener(OnTargetToggleValueChanged);

		OnTargetToggleValueChanged(_targetToggle.isOn);
	}

	public void OnTargetToggleValueChanged(bool newValue) {
		if (_onSprite == null || _offSprite == null) {
			return;
		}
		if (newValue) {
			_onSprite.SetActive(true);
			_offSprite.SetActive(false);
		} else {
			_onSprite.SetActive(false);
			_offSprite.SetActive(true);
		}
	}

#if UNITY_EDITOR

	public void OnValidate() {
		if (_targetToggle == null) {
			_targetToggle = gameObject.GetComponentInChildren<Toggle>();
		}
		OnTargetToggleValueChanged(_targetToggle.isOn);
	}

#endif

}
}