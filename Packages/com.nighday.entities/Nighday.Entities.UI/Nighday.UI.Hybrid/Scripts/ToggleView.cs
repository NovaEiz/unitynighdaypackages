using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Nighday.UI {

public class ToggleView : MonoBehaviour, IToggleView {

	[SerializeField] private Toggle _toggle;
	public Toggle Toggle => _toggle;

	public void BindLoadWindow(Action<Action<IWindowView>> callback) {
		IWindowView _windowView = null;
		_toggle.onValueChanged.AddListener((newValue) => {
			if (newValue) {
				callback((IWindowView windowView) => {
					windowView.ViewOpener.Open();
					windowView.ViewCloser.OnClosed += () => {
						_toggle.isOn = false;
					};
					_windowView = windowView;
				});
			} else {
				_windowView?.ViewCloser.Close();
			}
		});
	}
}
}