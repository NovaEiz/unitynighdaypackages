using Nighday.Animation.Hybrid;
using UnityEngine;

namespace Nighday.UI {

[CreateAssetMenu(fileName = "ToggleViewScalerSettingsAsset", menuName = "Nighday/UI Utils/ToggleViewScalerSettingsAsset", order = 0)]
public class ToggleViewScalerSettingsAsset : ScriptableObject {

	[SerializeField] private AnimationCurveAsset _towardsAnimationCurveAsset;
	[SerializeField] private AnimationCurveAsset _backwardsAnimationCurveAsset;
	

	[SerializeField] private float _towardsDurationTime;
	[SerializeField] private float _backwardsDurationTime;

	public AnimationCurveAsset TowardsAnimationCurveAsset => _towardsAnimationCurveAsset;
	public AnimationCurveAsset BackwardsAnimationCurveAsset => _backwardsAnimationCurveAsset;

	public float TowardsDurationTime => _towardsDurationTime;
	public float BackwardsDurationTime => _backwardsDurationTime;
	
}
}