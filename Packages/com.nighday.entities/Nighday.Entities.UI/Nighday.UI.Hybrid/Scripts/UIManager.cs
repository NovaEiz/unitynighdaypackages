using System;
using System.Collections.Generic;
using Nighday.Entities;
using Unity.Entities;
using UnityEngine;
using UnityEngine.UI;

namespace Nighday.UI {

[Serializable]
public class UILayer {
	public string    name;
	public Canvas    windowLayer;
	public Transform environmentLayer;
	public Canvas    uiLayer;

	public object applicationManager;

	public UILayer(object applicationManager) {
		this.applicationManager = applicationManager;
	}

	public UILayer() {
	}

	public void SetActive(bool value) {
		if (windowLayer != null) {
			windowLayer.gameObject.SetActive(value);
		}
		if (uiLayer != null) {
			uiLayer.gameObject.SetActive(value);
		}
		if (environmentLayer != null) {
			environmentLayer.gameObject.SetActive(value);
		}
	}

	public void AddView(object view) {
		// foreach (var environmentView in view.EnvironmentViews) {
		// 	environmentView.transform.SetParent(environmentLayer, false);
		// }
		// foreach (var uiViews in view.UiViews) {
		// 	uiViews.transform.SetParent(uiLayer.transform);
		// 	(uiViews.transform as RectTransform).SetFullSize();
		// }
		//
		// view.transform.SetParent(applicationManager.ViewsContainer, false);
	}
}

public class UIManager : MonoBehaviour, IRuntimeHybridConvertGameObjectToEntity {
	
	public void Convert(Entity entity, EntityManager dstManager, RuntimeHybridGameObjectConversionSystem conversionSystem) {
		Debug.Log("pfff lol 1");

		if (UIManager.GetFromWorld(dstManager.World) != null) {
			//dstManager.DestroyEntity(entity);
			GameObject.Destroy(gameObject);
			return;
		}

		UIManager.Add(this, dstManager.World);
		dstManager.AddComponentObject(entity, this as UIManager);
		
		Debug.Log("pfff lol 2");
	}

	public static UIManager Instance { get; private set; }

	// public Transform transform;
	//
	// public GameObject gameObject;
	//
	// public UIManager(Transform transform) {
	// 	this.transform = transform;
	// 	this.gameObject = transform.gameObject;
	// }

	[SerializeField] private List<UILayer> _layers = new List<UILayer>();

	public void SetLayers(List<UILayer> value) {
		_layers = value;
	}

	public void AddToLayer(RectTransform rectTransform, string layerName) {
		foreach (var layer in _layers) {
			if (layer.name == layerName) {
				var tr = layer.windowLayer.transform;
				rectTransform.SetParent(tr as RectTransform, false);
				rectTransform.SetFullSize();
				break;
			}
		}
	}

	public UILayer GetLayer(string layerName) {
		foreach (var layer in _layers) {
			if (layer.name == layerName) {
				return layer;
			}
		}

		return null;
	}

	public static UILayer GetLayer_(string layerName) {
		return Instance.GetLayer(layerName);
	}

	// public void SetActiveOnlyThisLayer(string layerName) {
	// 	foreach (Transform item in transform.GetComponentInChildren<Canvas>().transform) {
	// 		item.gameObject.SetActive(item.name == layerName);
	// 	}
	// }

	public void SetActiveLayer(string layerName, bool state) {
		if (this == null || gameObject == null) {
			return;
		}
		foreach (var layer in _layers) {
			if (layer.name == layerName) {
				layer.SetActive(state);
				return;
			}
		}
		Debug.LogError("Нет слоя layerName = " + layerName);
	}
	
	private static Dictionary<World, UIManager> _uiManagersByWorld = new Dictionary<World, UIManager>();

	[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
	public static void RuntimeInitClear() {
		_uiManagersByWorld = new Dictionary<World, UIManager>();
	}

	public static UIManager GetFromWorld(World world) {
		_uiManagersByWorld.TryGetValue(world, out var uiManager);
		return uiManager;
	}
	public static void Add(UIManager uiManager, World world) {
		_uiManagersByWorld.Add(world, uiManager);
	}

	private static Transform _viewsContainer;
	
	

	public void AddView(MonoBehaviour view) {
		view.transform.SetParent(ViewsContainer, false);
	}

	private Transform ViewsContainer {
		get {
			if (_viewsContainer == null) {
				_viewsContainer = new GameObject("ViewsContainer").transform;
				_viewsContainer.SetParent(transform);
			}

			return _viewsContainer;
		}
	}

	private void Awake() {
		Instance = this;
		Debug.Log("uiManager Awake = " + this);

		var viewsContainer = ViewsContainer;
	}

}
}