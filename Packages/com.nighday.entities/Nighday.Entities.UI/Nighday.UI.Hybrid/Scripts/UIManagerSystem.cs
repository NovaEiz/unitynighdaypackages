using System;
using Unity.Entities;

namespace Nighday.UI {

[UpdateInGameWorld(UpdateInGameWorld.TargetWorld.Client)]
[UpdateInLobbyWorld(UpdateInLobbyWorld.TargetWorld.Client)]
public class UIManagerSystem : SystemBase {

	protected override void OnUpdate() {
		
	}

	public UIManager GetUIManager() {
		if (!HasSingleton<UIManager>()) {
			return null;
		}
		return EntityManager.GetComponentObject<UIManager>(GetSingletonEntity<UIManager>());
	}
	
}
}