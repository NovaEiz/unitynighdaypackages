using System;
using UnityEngine;

namespace Nighday.UI {

public class ViewOpener : MonoBehaviour, IViewOpener {

	//[SerializeField] private Curve _openAnimation;
	//[SerializeField] private Curve _closeAnimation;

	public void Open() {
		gameObject.SetActive(true);
		OnOpened?.Invoke();
	}

	public event Action OnOpened;

}
}