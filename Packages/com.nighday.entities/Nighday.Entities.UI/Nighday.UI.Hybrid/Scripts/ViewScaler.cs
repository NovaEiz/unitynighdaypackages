﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Nighday.UI {

public class ViewScaler : MonoBehaviour {

	[SerializeField] private ViewScalerSettingsAsset _viewScalerSettingsAsset;

	[SerializeField] private Transform _container;

	private float _baseScale;

	private float _moveToScale;
	private Coroutine _coroutine;

	private void Awake() {
		_baseScale = _container.localScale.x;
	}

	public float GetNearestCurveTime(AnimationCurve animationCurve) {
		var diff = _viewScalerSettingsAsset.ToScale - _baseScale;

		var currentScale = _container.localScale.x;

		var minDistance = 0f;

		var minDistanceTime = -1f;
		for (float i = 0; i < 1f; i += 0.05f) {
			var keyValue = animationCurve.Evaluate(i);
			var newScale = diff * keyValue + _baseScale;

			var distance = Mathf.Abs(currentScale - newScale);

			if (distance < minDistance || minDistanceTime < 0) {
				minDistance = distance;
				minDistanceTime = i;
			}
		}

		return minDistanceTime;
	}

	public void RunScaling(AnimationCurve animationCurve, float durationTime) {
		var minDistanceTime = GetNearestCurveTime(animationCurve);
		ToScaleTowards(minDistanceTime, durationTime, animationCurve);
	}

	private void ToScaleTowards(float time, float durationTime, AnimationCurve animationCurve) {
		if (_coroutine != null) {
			StopCoroutine(_coroutine);
		}

		_coroutine = StartCoroutine(ToScaleTowardsIe(time, durationTime, animationCurve));
	}

	private IEnumerator ToScaleTowardsIe(float time, float durationTime, AnimationCurve animationCurve) {
		var elapsedTime = time / 1f * durationTime;

		var diff = _viewScalerSettingsAsset.ToScale - _baseScale;

		while (elapsedTime < durationTime) {
			var progressTime = elapsedTime / durationTime;
			var keyValue = animationCurve.Evaluate(progressTime);
			var newScale = diff * keyValue + _baseScale;
			var newLocalScale = Vector3.one * newScale;
			_container.localScale = newLocalScale;

			yield return null;
			elapsedTime += Time.deltaTime;
		}

		_coroutine = null;
	}

}

}