using Unity.Entities;

namespace Nighday.UI {

public interface ISetWorldAndEntityToMonoBehaviourReceiver {
    void SetWorld(World world, Entity entity);
}

}