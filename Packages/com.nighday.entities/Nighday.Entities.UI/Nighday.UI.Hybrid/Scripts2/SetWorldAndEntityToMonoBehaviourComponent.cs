using System;

using Nighday.Entities;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Nighday.UI {

public class SetWorldAndEntityToMonoBehaviourComponent : MonoBehaviour, IRuntimeHybridConvertGameObjectToEntity {

	public void Convert(Entity entity, EntityManager dstManager, RuntimeHybridGameObjectConversionSystem conversionSystem) {
		foreach (var component in transform.GetComponents<ISetWorldAndEntityToMonoBehaviourReceiver>()) {
			component.SetWorld(dstManager.World, entity);
		}
	}

}
}