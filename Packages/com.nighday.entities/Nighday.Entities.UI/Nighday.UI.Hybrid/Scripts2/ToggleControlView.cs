using System;
using System.Threading.Tasks;

using Nighday.Entities;
using Nighday.UI;
using UnityEngine;
using Unity.Entities;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace Nighday.UI {

public class DisableControlMonoBehaviourComponent : MonoBehaviour {
    public event Action DisableEvent;
    private void OnDisable() {
        DisableEvent?.Invoke();
    }
}

public class ToggleControlView : MonoBehaviour, IComponentData {

    public bool withDestroyAfterHiding;

    public AssetReference viewAssetReference;

    private Type _viewType;
    private string _viewKey;
    
    private UIViewBaseBase _viewPrefab;

    private UIViewBaseBase _view;

    private Task _animationTask;

    // private bool _toggleEnabledByShownWindowOutsideCall;

    private Toggle          _toggle;
    private StateToggleView _stateToggleView;

    private void OnDestroy() {
        _toggle = null;
    }

    private World World;

    private bool isOn;

    public void SetWorld(World World) {
        this.World = World;
    }

    public void SetToggle(Toggle toggle) {
        var disableControlMonoBehaviourComponent = toggle.gameObject.AddComponent<DisableControlMonoBehaviourComponent>();
        disableControlMonoBehaviourComponent.DisableEvent += () => {
            if (_view != null) {
                _view.OnHidden -= OnHiddenView;
                _view.DestroyView(World.EntityManager);
                _view = null;
            }

            if (_toggle != null) {
                _toggle.SetIsOnWithoutNotify(false);
            }
            if (_stateToggleView != null) {
                _stateToggleView.OnTargetToggleValueChanged(false);
            }
        };
        
        _toggle = toggle;
        _stateToggleView = _toggle.GetComponent<StateToggleView>();
        LoadPrefab(toggle);

        toggle.onValueChanged.AddListener(async (newValue) => {
            // if (_toggleEnabledByShownWindowOutsideCall) {
            //     _toggleEnabledByShownWindowOutsideCall = false;
            //     return;
            // }

            if (_animationTask != null) {
                return;
            }

            SetToggleInteractable(false);
            // toggle.interactable = false;
            if (newValue) {
                if (_view == null) {
                    // GameObject prefab = null;
                    // if (viewAssetReference.Asset == null) {
                    //     prefab = await viewAssetReference.LoadAssetAsync<GameObject>()
                    //                  .Task;
                    // } else {
                    //     prefab = viewAssetReference.Asset as GameObject;
                    // }
                    //
                    // var instance = GameObject.Instantiate(prefab);
                    // _view = instance.GetComponent<UIViewBaseBase>();
                    //
                    // _view.gameObject.AddComponent<WithHierarchyAuthoring>();
                    // var convertToEntityInWorld = _view.gameObject.GetComponent<ConvertToEntityInWorld>();
                    // if (convertToEntityInWorld == null) {
                    //     convertToEntityInWorld = _view.gameObject.AddComponent<ConvertToEntityInWorld>();
                    // }
                    //
                    // convertToEntityInWorld.SetWorld(World);
                    // convertToEntityInWorld.Convert();

                    _view = await UIViewBaseBase.InstantiateAsync(World, _viewKey);

                    _view.OnHidden += OnHiddenView;
                    _view.OnHide += OnHideView;
                    _view.OnShow += OnShowView;
                    _view.OnShown += OnShownView;
                }

                _animationTask = _view.Show();
                await _animationTask;
                _animationTask = null;
            } else {
                if (_view != null) {
                    _view.OnHidden -= OnHiddenView;
                    _view.OnHide -= OnHideView;
                    _view.OnShow -= OnShowView;
                    _view.OnShown -= OnShownView;
                    
                    _animationTask = _view.Hide();
                    if (withDestroyAfterHiding) {
                        await _animationTask;

                        _view.DestroyView(World.EntityManager);
                    }

                    await _animationTask;
                    _animationTask = null;
                }
            }

            SetToggleInteractable(true);
            // toggle.interactable = true;
        });
    }

    private void SetToggleInteractable(bool value) {
        _toggle.interactable = value;
        _stateToggleView = null;
    }

    private async void LoadPrefab(Toggle toggle) {
        GameObject prefab = null;
        if (viewAssetReference.Asset == null) {
            prefab = await viewAssetReference.LoadAssetAsync<GameObject>()
                         .Task;
        } else {
            prefab = viewAssetReference.Asset as GameObject;
        }

        _viewPrefab = prefab.GetComponent<UIViewBaseBase>();
        if (_viewPrefab == null) {
            return;
        }
        // _viewType = _viewPrefab.GetType();
        _viewKey = _viewPrefab.name;
        UIViewBaseBase.RegisterEventOnShow(_viewKey,
            (view_) => {
                if (_view != view_) {
                    if (view_.World != World) {
                        return;
                    }
                    _view = view_;

                    _toggle.SetIsOnWithoutNotify(true);
                    _stateToggleView.OnTargetToggleValueChanged(true);

                    _view.OnHidden += OnHiddenView;
                    _view.OnHide += OnHideView;
                    _view.OnShow += OnShowView;
                    _view.OnShown += OnShownView;

                    RuntimeHybridConvertToEntitySystem.ConvertToWorld(World, view_.gameObject);
                }
            });
    }

    private void OnShowView() {
        SetToggleInteractable(false);
    }
    private void OnShownView() {
        SetToggleInteractable(true);
    }

    private void OnHideView() {
        SetToggleInteractable(false);
    }

    private void OnHiddenView() {
        SetToggleInteractable(true);

        if (_view != null) {
            _view.OnHide -= OnHideView;
            _view.OnHidden -= OnHiddenView;
        }

        if (withDestroyAfterHiding) {
            _view.DestroyView(World.EntityManager);
        }
        // _toggleEnabledByShownWindowOutsideCall = true;

        if (_toggle != null) {
            _toggle.SetIsOnWithoutNotify(false);
        }
        if (_stateToggleView != null) {
            _stateToggleView.OnTargetToggleValueChanged(false);
        }
    }

}

}