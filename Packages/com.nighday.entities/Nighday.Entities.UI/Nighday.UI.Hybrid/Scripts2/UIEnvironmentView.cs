using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Nighday.UI {

public class UIEnvironmentView : MonoBehaviour {

	[HideInInspector] [SerializeField] private Animator _animator;
	
	private void FindAnimator() {
		if (_animator == null) {
			_animator = GetComponent<Animator>();
		}
	}
	
#if UNITY_EDITOR

	private void OnValidate() {
		FindAnimator();
	}

#endif
	
	public async Task Show(float normalizedTime = 0f, int layer = 0) {
		transform.gameObject.SetActive(true);

		if (_animator != null) {
			var animName = "Show";
			_animator.Play(animName, layer, normalizedTime);
			AnimatorStateInfo state = default;
			do {
				await Task.Yield();
				state = _animator.GetCurrentAnimatorStateInfo(0);
			} while (!state.IsName(animName) || state.normalizedTime < 0.99f);
		}
	}
	public async Task Hide(float normalizedTime = 0f, int layer = 0) {
		if (normalizedTime >= 1f) {
			transform.gameObject.SetActive(false);
			return;
		}
		if (_animator != null) {
			var animName = "Hide";
			_animator.Play(animName, layer, normalizedTime);
			AnimatorStateInfo state = default;
			do {
				await Task.Yield();
				state = _animator.GetCurrentAnimatorStateInfo(0);
			} while (!state.IsName(animName) || state.normalizedTime < 0.99f);
		}

		transform.gameObject.SetActive(false);
	}
	
}

}