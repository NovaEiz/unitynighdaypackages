
using Nighday.Entities;
using Unity.Entities;
using UnityEngine;

namespace Nighday.UI {

public class UIViewAuthoring : MonoBehaviour, IRuntimeHybridConvertGameObjectToEntity {

    public void Convert(Entity entity, EntityManager dstManager, RuntimeHybridGameObjectConversionSystem conversionSystem) {
        var component = gameObject.GetComponent<UIViewBaseBase>();
        dstManager.AddComponentObject(entity, component);
    }
    
}

}