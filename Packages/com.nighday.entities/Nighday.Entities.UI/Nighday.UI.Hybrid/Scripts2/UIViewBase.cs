using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Nighday.UI;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Nighday.UI {

public class UIViewBase<T> : UIViewBaseBase
	where T : UIViewBase<T> {


	private Entity _dataEntity;
	public Entity DataEntity => _dataEntity;
	public void SetDataEntity(Entity value) {
		_dataEntity = value;
	}

	public static async Task<T> InstantiateAsync() {
		var task = Addressables.LoadAssetAsync<GameObject>(typeof(T).Name).Task;
		var result = await task;
		return result.GetComponent<T>();
	}

	// public static void AddToWorld(T instance, World world) {
	// 	var entity = world.EntityManager.CreateEntity(typeof(LocalToWorld), typeof(Translation), typeof(Rotation));
	//
	// 	var withHierarchyAuthoring = instance.gameObject.AddComponent<WithHierarchyAuthoring>();
	// 	withHierarchyAuthoring.Convert(entity, world.EntityManager, null);
	// }
	
	private static Dictionary<World, T> _viewsByWorld = new Dictionary<World,T>();
	
	public static T GetFromWorld(World world) {
		var type = typeof(T);
		return GetFromWorld(world, type) as T;
		// _viewsByWorld.TryGetValue(world, out T view);
		// return view;
	}

	public static async Task<T> InstantiateAsync(World world) {
		var type = typeof(T);
// 		
// 		var EntityManager = world.EntityManager;
//
// 		var task = Addressables.LoadAssetAsync<GameObject>(typeof(T).Name).Task;
// 		var prefab = await task;
// 		var instance = GameObject.Instantiate(prefab);
// 		instance.name = prefab.name;
// 		
//
// 		var entity = EntityManager.CreateEntity(typeof(LocalToWorld), typeof(Translation), typeof(Rotation));
// #if UNITY_EDITOR
// 		EntityManager.SetName(entity, instance.name);
// #endif
//
// 		var withHierarchyAuthoring = instance.AddComponent<WithHierarchyAuthoring>();
// 		EntityManager.AddComponentObject(entity, instance.transform);
//
// 		withHierarchyAuthoring.Convert(entity, world.EntityManager, null);
//
// 		var view = instance.GetComponent<T>();
//
// 		var uiManager = UIManager.GetFromWorld(world);
// 		uiManager.AddView(view as MonoBehaviour);
// 		view.SetUIManager(uiManager);
//
// 		view.Init();
// 		
// 		_viewsByWorld.Add(world, view);
// 		
// 		EntityManager.AddComponentObject(entity, view);

		var viewBase = await InstantiateAsync(world, type);
		return viewBase as T;
	}

}

}