using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using Nighday;
using Nighday.Entities;
using Nighday.UI;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Nighday.UI {

public class UIViewBaseBase : MonoBehaviour, ISetWorldAndEntityToMonoBehaviourReceiver {
    
    public event Action OnHidden {
        add {
            _windowView.OnHidden += value;
        }
        remove {
            _windowView.OnHidden -= value;
        }
    }
    
    public event Action OnHide {
        add {
            _windowView.OnHide += value;
        }
        remove {
            _windowView.OnHide -= value;
        }
    }
    
    public event Action OnShow {
        add {
            _windowView.OnShow += value;
        }
        remove {
            _windowView.OnShow -= value;
        }
    }
    
    public event Action OnShown {
        add {
            _windowView.OnShown += value;
        }
        remove {
            _windowView.OnShown -= value;
        }
    }
    
#if UNITY_EDITOR

    private void OnValidate() {
        FindUIWindowView();
        FindUIEnvironmentView();
    }

#endif

    private bool _isDestroyed;

    public World World { get; private set; }

    public void SetWorld(World world, Entity entity) {
        this.World = world;
        this._entity = entity;
    }
    
    [SerializeField] protected bool   _needWaitDataExistsBeforeShowing;
    [SerializeField] protected string _addToLayer;
    
    [HideInInspector] [SerializeField] protected UIWindowView _windowView;

    [HideInInspector] [SerializeField] protected UIEnvironmentView _environmentView;

    public UIWindowView      WindowView      => _windowView;
    public UIEnvironmentView EnvironmentView => _environmentView;

    private Task _activeEnvironmentViewTask;
    private Task _activeWindowViewTask;

    protected virtual void OnDestroy() {
        _isDestroyed = true;

        if (_activeEnvironmentViewTask != null) {
            var status = _activeEnvironmentViewTask.Status;
            switch (status) {
            case TaskStatus.RanToCompletion:
            case TaskStatus.Faulted:
            case TaskStatus.Canceled:
                _activeEnvironmentViewTask.Dispose();
                break;
            }
        }
        if (_activeWindowViewTask != null) {
            var status = _activeWindowViewTask.Status;
            switch (status) {
            case TaskStatus.RanToCompletion:
            case TaskStatus.Faulted:
            case TaskStatus.Canceled:
                _activeWindowViewTask.Dispose();
                break;
            }
        }
    }

    public virtual Task Show(float normalizedTime = 0f,
                             int   layer          = 0) {
        return null;
        Task task = null;
        if (_environmentView == null && _windowView == null) {
            return task;
        }

        if (_environmentView != null) {
            task = _environmentView.Show(normalizedTime, layer);
            _activeEnvironmentViewTask = task;
        }

        if (_windowView != null) {
            task = _windowView.Show(normalizedTime, layer);
            _activeWindowViewTask = task;
        }

        return task;
    }

    public virtual Task Hide(float normalizedTime = 0f,
                            int   layer          = 0) {
        return null;

        Task task = null;
        if (_environmentView == null && _windowView == null) {
            return task;
        }

        if (_environmentView != null) {
            task = _environmentView.Hide(normalizedTime, layer);
        }

        if (_windowView != null) {
            task = _windowView.Hide(normalizedTime, layer);
        }

        return task;
    }

    public float GetTheProgressOfTheHideAnimation() {
        return _windowView.GetTheProgressOfTheHideAnimation();
    }
	
    public float GetTheProgressOfTheShowAnimation() {
        return _windowView.GetTheProgressOfTheShowAnimation();
    }

    private Entity _entity;

    public void DestroyView(EntityManager EntityManager) {
        if (_windowView != null) {
            Destroy(_windowView.gameObject);
        }

        if (_environmentView != null) {
            Destroy(_environmentView.gameObject);
        }
        Destroy(gameObject);
        
        // EntityManager.AddComponentData(_entity, default(DestroyRequest));
    }

    protected virtual void Start() {
        OnStart();
    }
    
    

    protected virtual async void OnStart() {
        while (UIManager.Instance == null) {
            await Task.Yield();
        }
        var uiManager = UIManager.Instance;
        Debug.Log("uiManager = " + uiManager);

        FindUIWindowView();
        FindUIEnvironmentView();

        if (string.IsNullOrEmpty(_addToLayer)) {
            return;
        }
        
        var layer = uiManager.GetLayer(_addToLayer);

        UIManager.Instance.AddView(this as MonoBehaviour);

        if (_windowView != null) {
            gameObject.AddComponent<CopyGameObjectActivity>()
                .SetTarget(_windowView.gameObject);

            // _windowView.gameObject.name = "(" + transform.name + ") - " + _windowView.gameObject.name;
            _windowView.transform.SetParent(layer.windowLayer.transform);
            (_windowView.transform as RectTransform).SetFullSize();
        }

        if (_environmentView != null) {
            gameObject.AddComponent<CopyGameObjectActivity>()
                .SetTarget(_environmentView.gameObject);

            // _environmentView.gameObject.name = "(" + transform.name + ") - " + _environmentView.gameObject.name;

            _environmentView.transform.SetParent(layer.environmentLayer);
            _environmentView.transform.localScale = Vector3.one;
            _environmentView.transform.localEulerAngles = Vector3.zero;
        }
    }

    private void FindUIWindowView() {
        if (_windowView == null) {
            _windowView = GetComponentInChildren<UIWindowView>();
        }
    }

    private void FindUIEnvironmentView() {
        if (_environmentView == null) {
            _environmentView = GetComponentInChildren<UIEnvironmentView>();
        }
    }

    protected virtual void Awake() {
        gameObject.AddComponent<SetWorldAndEntityToMonoBehaviourComponent>();
    }

    private static Dictionary<World, List<UIViewBaseBase>> _viewsByWorld = new Dictionary<World, List<UIViewBaseBase>>();

    public static UIViewBaseBase GetFromWorld(World world,
                                              Type  type) {
        if (_viewsByWorld.TryGetValue(world, out List<UIViewBaseBase> list)) {
            foreach (var item in list) {
                if (item.GetType()
                    .Equals(type)) {
                    return item;
                }
            }
        }

        return null;
    }

    public static async Task<UIViewBaseBase> InstantiateAsync(World world,
                                                              Type  type) {
        var EntityManager = world.EntityManager;

        var task = Addressables.LoadAssetAsync<GameObject>(type.Name)
            .Task;

        var prefab = await task;
        
        var (entity, instance) = await world.GetExistingSystem<RuntimeHybridConvertToEntitySystem>().PrefabConvertToEntityInWorld(prefab, world, true, false);
        
        var view = instance.GetComponent<UIViewBaseBase>();
        
        if (!_viewsByWorld.TryGetValue(world, out List<UIViewBaseBase> list)) {
            list = new List<UIViewBaseBase>();
            _viewsByWorld.Add(world, list);
        }

        list.Add(view);

        EntityManager.AddComponentObject(entity, view);
        
        while (UIManager.Instance == null) {
            await Task.Yield();
            if (view._isDestroyed) {
                return null;
            }
        }
        UIManager.Instance.AddView(view as MonoBehaviour);

        if (view._needWaitDataExistsBeforeShowing) {
            while (!world.EntityManager.HasComponent<UIViewDataIsExistsComponentTag>(entity)) {
                await Task.Yield();
                if (view._isDestroyed) {
                    return null;
                }
            }
        }
        instance.SetActive(true);

        WaitOneFrame(() => {
            RunEventOnShow(type, view);
        });

        return view;
    }

    public static async Task<UIViewBaseBase> InstantiateAsync(World world,
                                                              string  viewKey) {
        var EntityManager = world.EntityManager;

        var task = Addressables.LoadAssetAsync<GameObject>(viewKey)
            .Task;

        var prefab = await task;
        
        var (entity, instance) = await world.GetExistingSystem<RuntimeHybridConvertToEntitySystem>().PrefabConvertToEntityInWorld(prefab, world, true, false);
        
        var view = instance.GetComponent<UIViewBaseBase>();
        
        if (!_viewsByWorld.TryGetValue(world, out List<UIViewBaseBase> list)) {
            list = new List<UIViewBaseBase>();
            _viewsByWorld.Add(world, list);
        }

        list.Add(view);

        EntityManager.AddComponentObject(entity, view);
        
        while (UIManager.Instance == null) {
            await Task.Yield();
            if (view._isDestroyed) {
                return null;
            }
        }
        UIManager.Instance.AddView(view as MonoBehaviour);

        if (view._needWaitDataExistsBeforeShowing) {
            while (!world.EntityManager.HasComponent<UIViewDataIsExistsComponentTag>(entity)) {
                await Task.Yield();
                if (view._isDestroyed) {
                    return null;
                }
            }
        }
        instance.SetActive(true);

        WaitOneFrame(() => {
            RunEventOnShow(viewKey, view);
        });

        return view;
    }

    private static async void WaitOneFrame(Action callback) {
        await Task.Yield();
        callback();
    }

    private static Dictionary<Type, List<Action<UIViewBaseBase>>> _registerEventOnShowDict = new Dictionary<Type, List<Action<UIViewBaseBase>>>();
    public static void RunEventOnShow(Type viewType, UIViewBaseBase view) {
        if (_registerEventOnShowDict.TryGetValue(viewType, out List<Action<UIViewBaseBase>> list)) {
            foreach (var item in list) {
                item(view);
            }
        }
    }
    public static void RegisterEventOnShow(Type viewType, Action<UIViewBaseBase> callback) {
        if (!_registerEventOnShowDict.TryGetValue(viewType, out List<Action<UIViewBaseBase>> list)) {
            list = new List<Action<UIViewBaseBase>>();
            _registerEventOnShowDict.Add(viewType, list);
        }
        list.Add(callback);
    }
    public static void UnregisterEventOnShow(Type viewType, Action<UIViewBaseBase> callback) {
        if (!_registerEventOnShowDict.TryGetValue(viewType, out List<Action<UIViewBaseBase>> list)) {
            return;
        }
        list.Remove(callback);
    }
    
    private static Dictionary<string, List<Action<UIViewBaseBase>>> _registerEventOnShowDict2 = new Dictionary<string, List<Action<UIViewBaseBase>>>();
    public static void RunEventOnShow(string viewKey, UIViewBaseBase view) {
        if (_registerEventOnShowDict2.TryGetValue(viewKey, out List<Action<UIViewBaseBase>> list)) {
            foreach (var item in list) {
                item(view);
            }
        }
    }
    public static void RegisterEventOnShow(string viewKey, Action<UIViewBaseBase> callback) {
        if (!_registerEventOnShowDict2.TryGetValue(viewKey, out List<Action<UIViewBaseBase>> list)) {
            list = new List<Action<UIViewBaseBase>>();
            _registerEventOnShowDict2.Add(viewKey, list);
        }
        list.Add(callback);
    }
    public static void UnregisterEventOnShow(string viewKey, Action<UIViewBaseBase> callback) {
        if (!_registerEventOnShowDict2.TryGetValue(viewKey, out List<Action<UIViewBaseBase>> list)) {
            return;
        }
        list.Remove(callback);
    }

}

}