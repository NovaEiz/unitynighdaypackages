using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Nighday.UI;
using UnityEngine;

namespace Nighday.UI {

public class UIWindowView : MonoBehaviour {

	public event Action OnHide;
	public event Action OnShow;
	public event Action OnHidden;
	public event Action OnShown;
	
	[HideInInspector] [SerializeField] private Animator _animator;

	private void FindAnimator() {
		if (_animator == null) {
			_animator = GetComponent<Animator>();
		}
	}

#if UNITY_EDITOR

	private void OnValidate() {
		FindAnimator();
	}

#endif

	// private Transform _transform;
	//
	// public Transform transform {
	// 	get { return _transform; }
	// 	internal set { _transform = value; }
	// }

	protected virtual void OnEnable() {
		(transform as RectTransform).SetFullSize();
	}

	public async Task Show(float normalizedTime = 0f, int layer = 0) {
		OnShow?.Invoke();

		transform.gameObject.SetActive(true);

		if (_animator != null && _animator.enabled) {
			var animName = "Show";
			_animator.Play(animName, layer, normalizedTime);
			AnimatorStateInfo state = default;
			do {
				await Task.Yield();
				state = _animator.GetCurrentAnimatorStateInfo(0);
			} while (!state.IsName(animName) || state.normalizedTime < 0.99f);
		}
		
		OnShown?.Invoke();
	}

	public async Task Hide(float normalizedTime = 0f, int layer = 0) {
		OnHide?.Invoke();

		if (normalizedTime >= 1f) {
			transform.gameObject.SetActive(false);
			return;
		}
		if (_animator != null && _animator.enabled) {
			var animName = "Hide";
			_animator.Play(animName, layer, normalizedTime);
			AnimatorStateInfo state = default;
			do {
				await Task.Yield();
				state = _animator.GetCurrentAnimatorStateInfo(0);
			} while (!state.IsName(animName) || state.normalizedTime < 0.99f);
		}

		transform.gameObject.SetActive(false);
		
		OnHidden?.Invoke();
	}
	
	public float GetTheProgressOfTheHideAnimation() {
		return GetTheProgressOfTheAnimation("Hide");
	}
	public float GetTheProgressOfTheShowAnimation() {
		return GetTheProgressOfTheAnimation("Show");
	}
	
	public float GetTheProgressOfTheAnimation(string animationName) {
		var stateInfo = _animator.GetCurrentAnimatorStateInfo(0);
		if (stateInfo.IsName(animationName)) {
			return stateInfo.normalizedTime;
		}

		return 1f;
	}

}

}