using System;
using Nighday.Entities;
using Nighday.Mathematics;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.OnScreen;

namespace Nighday.UI.Joystick.Hybrid {
public class MovementJoystickView : OnScreenControl, IRuntimeHybridConvertGameObjectToEntity {

	public void Convert(Entity entity, EntityManager dstManager, RuntimeHybridGameObjectConversionSystem conversionSystem) {
		dstManager.AddComponentObject(entity, this);
	}
	
	/// <summary>
	/// Event that is triggered when the action has been started.
	/// </summary>
	/// <remarks>
	/// See <see cref="phase"/> for details of how an action progresses through phases
	/// and triggers this callback.
	/// </remarks>
	/// <see cref="InputActionPhase.Started"/>
	public static event Action<Vector2> started;

	/// <summary>
	/// Event that is triggered when the action has been <see cref="started"/>
	/// but then canceled before being fully <see cref="performed"/>.
	/// </summary>
	/// <remarks>
	/// See <see cref="phase"/> for details of how an action progresses through phases
	/// and triggers this callback.
	/// </remarks>
	/// <see cref="InputActionPhase.Canceled"/>
	public static event Action<Vector2> canceled;

	/// <summary>
	/// Event that is triggered when the action has been fully performed.
	/// </summary>
	/// <remarks>
	/// See <see cref="phase"/> for details of how an action progresses through phases
	/// and triggers this callback.
	/// </remarks>
	/// <see cref="InputActionPhase.Performed"/>
	public static event Action<Vector2> performed;
	
	[InputControl(layout = "Vector2")]
	[SerializeField]
	private string m_ControlPath;

	protected override string controlPathInternal
	{
		get => m_ControlPath;
		set => m_ControlPath = value;
	}

	[SerializeField] protected JoystickView _joystickView;

	public JoystickView JoystickView => _joystickView;
	
	
	private float _movementAngle = -1f;
	public bool Movement => _movementAngle >= 0;
	public float MovementAngle => _movementAngle;

	private bool _joystickActive;
	private Vector2 _currentValue;

	private void Update() {
		if (_joystickActive) {
			SendValueToControl(_currentValue);
		}
	}

	private void Awake() {

		var joystickView = JoystickView;

		joystickView.OnBeginDragEvent += () => {
			_joystickActive = true;

			if (joystickView.PointerInCancelArea) {
				started?.Invoke(Vector2.zero);
			} else {
				started?.Invoke((Vector2)_movementAngle.ConvertAngleToDirectionXZ() * joystickView.GetDistance());
			}
		};
		joystickView.OnDragEvent += () => {
			if (joystickView.PointerInCancelArea) {
				_movementAngle = -1f;
				var value = Vector2.zero;
				_currentValue = value;
				performed?.Invoke(_currentValue);
			} else {
				_movementAngle = _joystickView.GetAngle();
				var value = (Vector2)_movementAngle.ConvertAngleToDirectionXZ() * joystickView.GetDistance();
				_currentValue = value;
				performed?.Invoke(_currentValue);
			}
		};
		

		joystickView.OnEndDragEvent += () => {
			_movementAngle = -1f;
			_joystickActive = false;
			
			var value = Vector2.zero;
			_currentValue = value;
			//SendValueToControl(value);
			
			canceled?.Invoke(value);
		};
		joystickView.OnClickInCancelAreaEvent += () => {
			
		};
	}

// #if UNITY_STANDALONE || UNITY_EDITOR
// 	private void Update() {
// 		if (_joystickActive) {
// 			return;
// 		}
// 		var horizontal = 0;
// 		var vervical = 0;
// 		//
// 		// var qwe = Keyboard.current.FindKeyOnCurrentKeyboardLayout("q");
// 		//
// 		// var keyboard = InputSystem.FindControl<Keyboard>();
// 		if (Input.GetKey(KeyCode.W)) {
// 			vervical = 1;
// 		}
// 		if (Input.GetKey(KeyCode.S)) {
// 			vervical = -1;
// 		}
// 		if (Input.GetKey(KeyCode.A)) {
// 			horizontal = -1;
// 		}
// 		if (Input.GetKey(KeyCode.D)) {
// 			horizontal = 1;
// 		}
// 		if (horizontal != 0 || vervical != 0) {
// 			_movementAngle = Quaternion.LookRotation(new Vector3(horizontal, 0, vervical)).eulerAngles.y;
// 		} else {
// 			_movementAngle = -1;
// 		}
// 	}
// #endif
	
}
}