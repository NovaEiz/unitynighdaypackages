using Nighday.Entities;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using Nighday.UIToolkit;
using Unity.Entities;

namespace Nighday.UIToolkit {
	
[CustomFieldEditor(typeof(EntityField))]
public class EntityFieldEditor : CustomFieldEditor {

	public override VisualElement CreateInspector() {
		var root = new VisualElement();
		var field = new ObjectField(this.Name);
		
		var entityField = (EntityField)fieldInfo.GetValue(obj);
		field.objectType = typeof(GameObject);
		field.value = entityField.GameObject;

		field.RegisterValueChangedCallback((changeEvent) =>
		{
			entityField.GameObject = (GameObject)changeEvent.newValue;
			fieldInfo.SetValue(obj, entityField);
			EditorUtility.SetDirty((UnityEngine.Object)target);
			OnChanged?.Invoke();
		});
		
		root.Add(field);
		return root;
	}
	
}

}
