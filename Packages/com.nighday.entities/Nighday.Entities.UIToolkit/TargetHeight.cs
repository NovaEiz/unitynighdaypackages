using Unity.Entities;

namespace Nighday.UIToolkit {

[GenerateAuthoringComponent]
public struct TargetHeight : IComponentData {

	public float Value;

}
}