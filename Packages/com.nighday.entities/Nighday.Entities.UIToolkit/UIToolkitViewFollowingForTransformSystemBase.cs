using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

namespace Nighday.UIToolkit {

[UpdateInGroup(typeof(ClientPresentationSystemGroup))]
[UpdateAfter(typeof(RenderMeshSystemV2))]
public class UIToolkitViewFollowingForTransformSystemBase<ViewComponentObject,CameraComponent> : ComponentSystem
	where ViewComponentObject : Component, IViewComponentObject
	where CameraComponent : struct, IComponentData
{

	private Camera _camera;

	private Camera Camera {
		get {
			if (_camera == null) {
				_camera = EntityManager.GetComponentObject<Camera>(GetSingletonEntity<CameraComponent>());
			}

			return _camera;
		}
	}

	protected override void OnCreate() {
		RequireSingletonForUpdate<CameraComponent>();
	}

	protected override void OnUpdate() {
		Entities
			.ForEach((Entity entity,
					ViewComponentObject viewComponentObject,
					ref TargetHeight targetHeight,
					ref LocalToWorld localToWorld
					) => {
						var visualElement = viewComponentObject.VisualElement;
						
						var inCanvasPosition = GetPositionInCanvas(
							localToWorld.Position,
							new float3(0, targetHeight.Value, 0)
						);
						inCanvasPosition.x -= visualElement.layout.width / 2;

						viewComponentObject.VisualElement.transform.position = inCanvasPosition;
			});
	}

	private float3 GetPositionInCanvas(float3 targetWorldPosition, float3 offsetPosition) {
		var worldPosition = targetWorldPosition + offsetPosition;
		var position = (float3)Camera.WorldToScreenPoint(worldPosition);
		
		position.y = Screen.height - position.y;
		position.z = 0;
		
		return position;
	}
	
}
}