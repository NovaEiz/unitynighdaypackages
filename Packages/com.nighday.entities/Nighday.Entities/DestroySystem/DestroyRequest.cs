using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;

namespace Nighday {

public struct DestroyRequest : IComponentData { }

public static class DestroyRequestExt {
    public static void AddDestroyRequestWithLinkedEntityGroup(EntityManager EntityManager,
                                                              Entity        entity) {

        if (EntityManager.HasComponent<LinkedEntityGroup>(entity)) {
            var linkedEntityGroupBuffer = EntityManager.GetBuffer<LinkedEntityGroup>(entity)
                .ToNativeArray(Allocator.Temp);

            foreach (var item in linkedEntityGroupBuffer) {
                EntityManager.AddComponentData(item.Value, default(DestroyRequest));
            }
        } else {
            EntityManager.AddComponentData(entity, default(DestroyRequest));
        }
    }

    public static void AddDestroyRequestWithChilds(EntityManager EntityManager,
                                                   Entity        entity) {

        EntityManager.AddComponentData(entity, default(DestroyRequest));

        if (EntityManager.HasComponent<Child>(entity)) {
            var childBuffer = EntityManager.GetBuffer<Child>(entity)
                .ToNativeArray(Allocator.Temp);

            foreach (var item in childBuffer) {
                EntityManager.AddComponentData(item.Value, default(DestroyRequest));
            }
        }
    }
}

}