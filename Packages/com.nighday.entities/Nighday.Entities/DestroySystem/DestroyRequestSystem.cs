using Unity.Entities;
using UnityEngine;

namespace Nighday {

// [UpdateInWorld(UpdateInWorld.TargetWorld.Server)]
[UpdateInLobbyWorld(UpdateInLobbyWorld.TargetWorld.Server)]
[UpdateInGameWorld(UpdateInGameWorld.TargetWorld.Server)]
[UpdateInGroup(typeof(DestroyRequestSystemGroup))]
public class DestroyRequestServerSystem : SystemBase {

	protected override void OnUpdate() {
		Entities
			.WithAll<DestroyRequest>()
			.ForEach((Entity entity
					) => {
						EntityManager.DestroyEntity(entity);
					}).WithStructuralChanges().Run();
	}
}

// [UpdateInWorld(UpdateInWorld.TargetWorld.Client)]
[UpdateInLobbyWorld(UpdateInLobbyWorld.TargetWorld.Client)]
[UpdateInGameWorld(UpdateInGameWorld.TargetWorld.Client)]
[UpdateInGroup(typeof(DestroyRequestSystemGroup))]
public class DestroyRequestClientSystem : SystemBase {

	protected override void OnUpdate() {
		//===
		
		Entities
			.WithNone<DestroyRequest>()
			.WithAll<DestroyEntityWithDestroyedTransformComponentTag>()
			.ForEach((Entity        entity,
			          RectTransform rectTransform
			) => {
				if (rectTransform == null) {
					EntityManager.AddComponentData(entity, default(DestroyRequest));
				}
			}).WithStructuralChanges().Run();
		
		Entities
			.WithNone<DestroyRequest>()
			.WithAll<DestroyEntityWithDestroyedTransformComponentTag>()
			.ForEach((Entity        entity,
			          Transform transform
			) => {
				if (transform == null) {
					EntityManager.AddComponentData(entity, default(DestroyRequest));
				}
			}).WithStructuralChanges().Run();
		
		Entities
			.WithAll<DestroyRequest>()
			.ForEach((Entity entity
			) => {
				EntityManager.DestroyEntity(entity);
			}).WithStructuralChanges().Run();
	}
}

}