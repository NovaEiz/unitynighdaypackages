using Unity.Entities;

namespace Nighday {

[UpdateInGroup(typeof(SimulationSystemGroup))]
[UpdateAfter(typeof(FixedStepSimulationSystemGroup))]
public class DestroyRequestSystemGroup : ComponentSystemGroup {
	
}

}