using System;
using Unity.Entities;
using UnityEngine;

namespace Nighday.Entities {
    [Serializable]
    public struct EntityField
    {
        [SerializeField] private GameObject _value;

        public GameObject GameObject
        {
            get => _value;
            set => _value = value;
        }

        private Entity _entity;
        public Entity Value => _entity;
        
        public EntityField(GameObject value) {
            _value = value;
            _entity = default;
        }

        public override bool Equals(object obj) {
            if (obj is EntityField) {
                var other = (EntityField)obj;
                if (GameObject == other.GameObject) {
                    return true;
                }
            }

            return false;
        }

        public bool Equals(EntityField other) {
            return GameObject == other.GameObject;
        }

        public override int GetHashCode() {
            return GameObject.GetHashCode();
        }
    }
    
}
