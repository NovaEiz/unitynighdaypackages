using System;
using Unity.Entities;

namespace Nighday {

public class UpdateInGameWorld : Attribute
{
	[Flags]
	public enum TargetWorld
	{
		Default = 0,
		Client = 1,
		Server = 2,
		ClientAndServer = 3
	}

	private TargetWorld m_world;
	public TargetWorld World => m_world;

	public UpdateInGameWorld(TargetWorld w)
	{
		m_world = w;
	}
}

}