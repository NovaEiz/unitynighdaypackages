namespace Facebook.Unity {

public class AccessToken {

    public static AccessToken CurrentAccessToken { get; internal set; }

    public string TokenString { get; private set; }

  }

}