namespace Facebook.Unity {

public class AuthenticationToken
{

    public string TokenString { get; private set; }

    public string Nonce { get; private set; }
}

}