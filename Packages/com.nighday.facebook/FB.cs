using System.Collections.Generic;

namespace Facebook.Unity {

public delegate void InitDelegate();
public delegate void HideUnityDelegate(bool isUnityShown);

public delegate void FacebookDelegate<T>(T result) where T : IResult;

public class FB {
    
    public static bool IsLoggedIn => false;

    public static bool IsInitialized => false;
    
    public static void LogInWithReadPermissions(
        IEnumerable<string>            permissions = null,
        FacebookDelegate<ILoginResult> callback    = null)
    {
        
    }
    
    public static void Init(
        InitDelegate      onInitComplete = null,
        HideUnityDelegate onHideUnity    = null,
        string            authResponse   = null)
    {
    }
    public static void LogInWithPublishPermissions(
        IEnumerable<string>            permissions = null,
        FacebookDelegate<ILoginResult> callback    = null)
    {
    }
    
}

}