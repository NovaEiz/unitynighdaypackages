using System.Threading.Tasks;
using System;
using System.Collections.Generic;

namespace Firebase.Messaging {

public sealed class FirebaseMessage {
    public static event EventHandler<TokenReceivedEventArgs>   TokenReceived;
    public static event EventHandler<MessageReceivedEventArgs> MessageReceived;

    public static Task SubscribeAsync(string topic) {
        return null;
    }

    public string From;

    public IDictionary<string, string> Data = new Dictionary<string, string>();

}

}