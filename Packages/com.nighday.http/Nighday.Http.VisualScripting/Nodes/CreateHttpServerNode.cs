using System;
using System.Collections;
using System.Collections.Generic;
using Nighday.Http;
using Unity.VisualScripting;
using UnityEngine;

[assembly: UnityEngine.Scripting.AlwaysLinkAssembly]
namespace Nighday.Socket {

[UnityEngine.Scripting.Preserve]
[UnitTitle("new HttpServer")]
[UnitCategory("_Nighday/Http")]
public class CreateHttpServerNode : Unit {

    [DoNotSerialize]
    private HttpServer _httpServer;

    [DoNotSerialize]
    public ValueInput portValueInput;

    [DoNotSerialize]
    public ValueOutput httpServerValueOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    [Serialize]
    [Inspectable, UnitHeaderInspectable("Auto start")]
    [InspectorToggleLeft]
    public bool autoStart { get; set; }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);

        portValueInput = ValueInput<ushort>("port", 0);
        httpServerValueOutput = ValueOutput<object>("httpServer", (flow) => { return _httpServer; });
        
        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var port = flow.GetValue<ushort>(portValueInput);
        
        _httpServer = new HttpServer(port);

        if (autoStart) {
            _httpServer.Start();
        }
        
        return exit;
    }

    public override void Dispose() {
        if (_httpServer != null) {
            _httpServer.Dispose();
        }
    }

    public override void Uninstantiate(GraphReference instance) {
        if (_httpServer != null) {
            _httpServer.Dispose();
        }
    }

}

}