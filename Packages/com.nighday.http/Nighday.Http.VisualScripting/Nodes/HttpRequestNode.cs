using System;
using System.Net;
using Nighday.Json;
using Nighday.VisualScripting;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Networking;

namespace Nighday.Http {

[UnityEngine.Scripting.Preserve]
[UnitTitle("HttpRequest")]
[UnitCategory("_Nighday/Http")]
public class HttpRequestNode : CustomEventUnit<EmptyEventArgs> {

    [DoNotSerialize]
    private JSONNode _receivedData;

    [DoNotSerialize]
    public ValueInput urlValueInput;
    
    [DoNotSerialize]
    public ValueInput dataValueInput;
    
    [DoNotSerialize]
    public ValueOutput dataValueOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
#region EventUnit
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow flow, EmptyEventArgs args) {
        return true;
    }
    

    private EventHook _hook;
    public override EventHook GetHook(GraphReference reference) {
        if (_hook.Equals(default)) {
            _hook = new EventHook("HttpRequestEvent_" + System.Guid.NewGuid().ToString(), reference.machine);
        }
        return _hook;
    }
#endregion

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);

        urlValueInput = ValueInput<string>("url", null);
        dataValueInput = ValueInput<JSONNode>("data", null);

        dataValueOutput = ValueOutput<JSONNode>("receivedData",
            (flow) => {
                return _receivedData;
            });

        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);
        
        base.Definition();
    }
    
    private ControlOutput Trigger(Flow flow) {
        var url = flow.GetValue<string>(urlValueInput);

        JSONNode data = null;
        if (dataValueInput.hasValidConnection) {
            data = flow.GetValue<JSONNode>(dataValueInput);
        }

        Action<string> action = null;
        action = (receivedText_) => {
            try {
                _receivedData = JSON.Parse(receivedText_);

                EventBus.Trigger(_hook);
            } catch (Exception e) {
                this.SetException(flow.stack, e);
                UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
            }
        };

        HttpRequest.Post(url, data?.ToString(), action);
        
        return exit;
    }

}

}