using System;
using System.Net;
using Nighday.Json;
using Nighday.VisualScripting;
using Unity.VisualScripting;

namespace Nighday.Http {

[UnityEngine.Scripting.Preserve]
[UnitTitle("HttpServer.OnData")]
[UnitCategory("_Nighday/Http")]
public class HttpServerOnDataNode : CustomEventUnit<EmptyEventArgs> {

    [DoNotSerialize]
    private HttpListenerContext _context;

    [DoNotSerialize]
    private JSONNode _data;

    [DoNotSerialize]
    public ValueInput pathValueInput;
    
    [DoNotSerialize]
    public ValueInput httpServerValueInput;//socket its - WebSocketClient or WebSocketServer
    
    [DoNotSerialize]
    public ValueOutput httpListenerContextValueOutput;
    
    [DoNotSerialize]
    public ValueOutput dataValueOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
#region EventUnit
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow flow, EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;
    public override EventHook GetHook(GraphReference reference) {
        if (_hook == null) {
            _hook = new EventHook("HttpServerOnDataEvent_" + System.Guid.NewGuid().ToString(), reference.machine);
        }
        return _hook;
    }
#endregion

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);

        pathValueInput = ValueInput<string>("path", null);
        httpServerValueInput = ValueInput<object>("httpServer", null);

        httpListenerContextValueOutput = ValueOutput<object>("httpListenerContext",
            (flow) => {
                return _context;
            });
        dataValueOutput = ValueOutput<JSONNode>("data",
            (flow) => {
                return _data;
            });

        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);
        
        base.Definition();
    }
    
    private ControlOutput Trigger(Flow flow) {
        var path = flow.GetValue<string>(pathValueInput);
        var httpServer = flow.GetValue<object>(httpServerValueInput) as HttpServer;

        Action<HttpListenerContext> action = null;
        action = (httpListenerContext_) => {
            try {
                _context = httpListenerContext_;
                var body = _context.Request.GetBody();
                if (string.IsNullOrEmpty(body)) {
                    body = "{}";
                }

                _data = JSON.Parse(body);
                EventBus.Trigger(_hook);
            } catch (Exception e) {
                this.SetException(flow.stack, e);
                UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
            }
        };

        httpServer.OnData(path, action);
        
        return exit;
    }

}

}