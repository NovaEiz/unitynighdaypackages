using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using Nighday.Json;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Http {

[UnityEngine.Scripting.Preserve]
[UnitTitle("HttpServer.SendResponse")]
[UnitCategory("_Nighday/Http")]
public class HttpServerSendResponseNode : Unit {

    [DoNotSerialize]
    public ValueInput httpListenerContextValueInput;

    [DoNotSerialize]
    public ValueInput dataValueInput;

    [DoNotSerialize] // Mandatory attribute, to make sure we don’t serialize data that should never be serialized.
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);

        httpListenerContextValueInput = ValueInput<object>("httpListenerContext", null);
        dataValueInput = ValueInput<JSONNode>("data", null);
        
        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var httpListenerContext = flow.GetValue<object>(httpListenerContextValueInput) as HttpListenerContext;
        var data = flow.GetValue<JSONNode>(dataValueInput);

        httpListenerContext.SendResponse(data);
        
        return exit;
    }

}

}