using System;
using System.Collections;
using System.Collections.Generic;
using Nighday.Http;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Socket {

[UnityEngine.Scripting.Preserve]
[UnitTitle("HttpServer.Start")]
[UnitCategory("_Nighday/Http")]
public class HttpServerStartNode : Unit {

    [DoNotSerialize]
    public ValueInput httpServerValueInput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    [Serialize]
    [Inspectable, UnitHeaderInspectable("Auto start")]
    [InspectorToggleLeft]
    public bool autoStart { get; set; }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);

        httpServerValueInput = ValueInput<object>("httpServer", 0);
        
        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var httpServer = flow.GetValue<object>(httpServerValueInput) as HttpServer;
        
        httpServer.Start();
        
        return exit;
    }

}

}