using System;
using UnityEngine;

namespace Nighday.Http {

public class HttpMainThread : MonoBehaviour {

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static void Init() {
        OnUpdateOneTime = null;
        _instance = null;
        var instance = Instance;
    }

    private static HttpMainThread _instance;

    public static HttpMainThread Instance {
        get {
            if (_instance == null) {
                _instance = new GameObject("HttpMainThread").AddComponent<HttpMainThread>();
                DontDestroyOnLoad(_instance.gameObject);
            }
            return _instance;
        }
    }

    public static event Action OnUpdateOneTime;

    public void Update() {
        if (OnUpdateOneTime != null) {
            var action = OnUpdateOneTime;
            OnUpdateOneTime = null;
            action?.Invoke();
        }
    }

}

}