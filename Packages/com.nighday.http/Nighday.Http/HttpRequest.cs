using System;
using System.Collections;
using System.Text;
using System.Threading.Tasks;
using Nighday.Json;
using UnityEngine;
using UnityEngine.Networking;

namespace Nighday.Http {

public class HttpRequest {
    public static void Post(string url, string body, Action<string> callback, int timeout = 30000) {
        HttpMainThread.Instance.StartCoroutine(PostCoroutine(url, body, callback, timeout));
    }
    
    private static IEnumerator PostCoroutine(string url, string body, Action<string> callback, int timeout = 30000) {
        if (string.IsNullOrEmpty(body)) body = "{}";

#if UNITY_EDITOR
        Debug.LogFormat("Post to url = {0}, body = {1}", url, body);
#endif
        var req = new UnityWebRequest(url, "POST");
        try {
            var bodyRaw = Encoding.UTF8.GetBytes(body);
            req.uploadHandler = new UploadHandlerRaw(bodyRaw);


            req.downloadHandler = new DownloadHandlerBuffer();
            req.SetRequestHeader("Content-Type", "application/json");

            req.timeout = timeout;

            var asyncOperation = req.SendWebRequest();
            yield return asyncOperation;

            switch (req.result)
            {
            case UnityWebRequest.Result.ConnectionError:
            case UnityWebRequest.Result.DataProcessingError:
                break;
            case UnityWebRequest.Result.ProtocolError:
                break;
            case UnityWebRequest.Result.Success:
                
                break;
            }

            var resultText = asyncOperation.webRequest.downloadHandler.text;
            if (string.IsNullOrEmpty(resultText)) {
                resultText = "{}";
            }
#if UNITY_EDITOR

            Debug.LogFormat("Post result: url = {0}, body = {1}, result = {2}", url, JSON.Parse(body).ToString(4), JSON.Parse(resultText).ToString(4));
#endif

            yield return null;
            callback(resultText);
        }
        finally {
            req.Dispose();
        }
            
    }
    public static async Task<string> PostAsync(string url, string body, int timeout = 30000) {
        if (body == null) body = "{}";

#if UNITY_EDITOR
        Debug.LogFormat("Post to url = {0}, body = {1}", url, body);
#endif
        var req = new UnityWebRequest(url, "POST");
        try {
            var bodyRaw = Encoding.UTF8.GetBytes(body);
            req.uploadHandler = new UploadHandlerRaw(bodyRaw);


            req.downloadHandler = new DownloadHandlerBuffer();
            req.SetRequestHeader("Content-Type", "application/json");

            req.timeout = timeout;

            var asyncOperation = req.SendWebRequest();

            while (!asyncOperation.isDone) await Task.Yield();
            if (asyncOperation.webRequest.error != null) {
                Debug.LogError(string.Format("error = {0}, url = {1}, body = {2}", asyncOperation.webRequest.error, url, body));
            }

            var resultText = asyncOperation.webRequest.downloadHandler.text;
            if (string.IsNullOrEmpty(resultText)) {
                resultText = "{}";
            }
#if UNITY_EDITOR
            Debug.LogFormat("Post result: url = {0}, body = {1}, result = {2}", url, JSON.Parse(body).ToString(4), JSON.Parse(resultText).ToString(4));
#endif

            return resultText;
        }
        finally {
            req.Dispose();
        }
            
    }
}

}