using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;

namespace Nighday.Http {

public static class HttpListenerRequestExt {
    public static string GetBody(this HttpListenerRequest request) {
        string body = "";
        if (request.InputStream != System.IO.Stream.Null) {
            using (var reader = new StreamReader(request.InputStream, request.ContentEncoding)) {
                body = reader.ReadToEnd();
            }
        }

        return body;
    }
}

public static class HttpListenerContextExt {
    
    public static void SendResponse(this HttpListenerContext context, object data) {
        // Obtain a response object.
        HttpListenerResponse response = context.Response;
        // Construct a response.
        string responseString = data.ToString();
        byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
        // Get a response stream and write the response to it.
        response.ContentLength64 = buffer.Length;
        System.IO.Stream output = response.OutputStream;
        output.Write(buffer,0,buffer.Length);
        // You must close the output stream.
        output.Close();
    }
    
    public static void Close(this HttpListenerContext context) {
        HttpListenerResponse response = context.Response;
        System.IO.Stream output = response.OutputStream;
        output.Close();
    }
}

public class HttpServer {

    private HttpListener _listener;

    public void Dispose() {
        _listener.Close();
    }

    public void Start() {
        _listener.Start();
        IAsyncResult result = _listener.BeginGetContext(new AsyncCallback(ListenerCallback), _listener);
    }

    private Dictionary<string, Action<HttpListenerContext>> _subscribes = new Dictionary<string, Action<HttpListenerContext>>();

    public void OnData(string path, Action<HttpListenerContext> callback) {
        if (!_subscribes.TryGetValue(path, out var callback_)) {
            callback_ = (_) => { };
            _subscribes.Add(path, callback_);
        }
        
        callback_ += callback;
    }

    private void RunOnData(Uri uri, HttpListenerContext context) {
        var path = uri.AbsolutePath;

        if (!_subscribes.TryGetValue(path, out var callback_)) {
            context.Close();
            return;
        }

        callback_(context);
    }


    public HttpServer(ushort port = 13222) {
        var prefix = "http://*:" + port + "/";

        var prefixes = new List<string>() {
            prefix
        };
        
        Debug.Log("prefix = " + prefix);
        
        _listener = new HttpListener();
        foreach (string s in prefixes)
        {
            _listener.Prefixes.Add(s);
        }
    }
    
    public void ListenerCallback(IAsyncResult result)
    {
        HttpListener listener = (HttpListener) result.AsyncState;
        // Call EndGetContext to complete the asynchronous operation.
        HttpListenerContext context = listener.EndGetContext(result);
        _listener.BeginGetContext(new AsyncCallback(ListenerCallback), _listener);

        RunOnData(context.Request.Url, context);
        
        Debug.Log("ПРишел запрос )) uri = " + context.Request.Url.AbsolutePath);
    }
    
}

}