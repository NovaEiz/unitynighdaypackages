using Unity.VisualScripting;
using UnityEngine;

[assembly: UnityEngine.Scripting.AlwaysLinkAssembly]
namespace Nighday.Json {

[UnityEngine.Scripting.Preserve]
[UnitTitle("JSON.AddField")]
[UnitCategory("_Nighday/JSON")]
public class JSONAddField : Unit {

    [DoNotSerialize] // Mandatory attribute, to make sure we don’t serialize data that should never be serialized.
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlInput inputTrigger { get; private set; }

    [DoNotSerialize]
    public ValueInput keyPathValueInput;

    [DoNotSerialize]
    public ValueInput jsonNodeValueInput;

    [DoNotSerialize]
    public ValueInput jsonNodeFieldValueInput;

    [DoNotSerialize]
    public ValueOutput operationResultValueOutput;

    [DoNotSerialize]
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlOutput outputTrigger { get; private set; }
    
    [Serialize]
    [Inspectable, UnitHeaderInspectable("With result")]
    [InspectorToggleLeft]
    public bool withResult { get; set; }
    
    [Serialize]
    [Inspectable, UnitHeaderInspectable("NeedCreateObjectsInPath")]
    [InspectorToggleLeft]
    public bool needCreateObjectsInPath { get; set; }

    private bool _operationResult;

    protected override void Definition() {

        inputTrigger = ControlInput(nameof(inputTrigger), Trigger);
        keyPathValueInput = ValueInput<string>("keyPathInput", null);
        jsonNodeValueInput = ValueInput<JSONNode>("jsonNodeInput", null);
        jsonNodeFieldValueInput = ValueInput<JSONNode>("jsonNodeFieldInput", null);
        if (withResult) {
            operationResultValueOutput = ValueOutput<bool>("resultOutput", (flow) => { return _operationResult; });
        }
        outputTrigger = ControlOutput(nameof(outputTrigger));
        Succession(inputTrigger, outputTrigger);
    }

    //Sending the Event MyCustomEvent with the integer value from the ValueInput port myValueA.
    private ControlOutput Trigger(Flow flow) {
        var keyPath = flow.GetValue<string>(keyPathValueInput);
        var jsonNode = flow.GetValue<JSONNode>(jsonNodeValueInput);
        var jsonNodeField = flow.GetValue<JSONNode>(jsonNodeFieldValueInput);

        _operationResult = jsonNode.AddField(keyPath, jsonNodeField, needCreateObjectsInPath);

        return outputTrigger;
    }

}

}