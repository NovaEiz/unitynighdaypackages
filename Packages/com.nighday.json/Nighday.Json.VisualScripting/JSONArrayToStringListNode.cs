using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Json {

[UnityEngine.Scripting.Preserve]
[UnitTitle("JSONArray.ToStringList")]
[UnitCategory("_Nighday/JSON")]
public class JSONArrayToStringListNode : Unit {

    [DoNotSerialize]
    private List<string> _list;

    [DoNotSerialize]
    public ValueInput jsonArrayValueInput;

    [DoNotSerialize]
    public ValueOutput listValueOutput;

    [DoNotSerialize]
    [PortLabelHidden] 
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabelHidden] 
    public ControlOutput exit { get; private set; }
    
    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        jsonArrayValueInput = ValueInput<JSONArray>("jsonArray", null);
        listValueOutput = ValueOutput<List<string>>("list", (flow) => { return _list; });
        
        Succession(enter, exit);
    }

    //Sending the Event MyCustomEvent with the integer value from the ValueInput port myValueA.
    private ControlOutput Trigger(Flow flow) {
        var jsonArray = flow.GetValue<JSONArray>(jsonArrayValueInput);

        var list = new List<string>();
        var len = jsonArray.Count;
        for (var i = 0; i < len; i++) {
            list.Add(jsonArray[i].Value);
        }

        _list = list;

        return exit;
    }

}

}