using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Json {

[UnityEngine.Scripting.Preserve]
[UnitTitle("JSON.AsDouble")]
[UnitCategory("_Nighday/JSON")]//Setting the path to find the unit in the fuzzy finder in Events > My Events.
public class JSONAsDouble : Unit
{
    
    [DoNotSerialize]
    private double _doubleOutput;

   [DoNotSerialize]// Mandatory attribute, to make sure we don’t serialize data that should never be serialized.
   [PortLabelHidden]// Hiding the port label as we normally hide the label for default Input and Output triggers.
   public ControlInput inputTrigger { get; private set; }
   [DoNotSerialize]
   public ValueInput keyPathValueInput;
   [DoNotSerialize]
   public ValueInput jsonNodeValueInput;
   [DoNotSerialize]
   public ValueOutput doubleValueOutput;
   [DoNotSerialize]
   [PortLabelHidden]// Hiding the port label as we normally hide the label for default Input and Output triggers.
   public ControlOutput outputTrigger { get; private set; }

   protected override void Definition()
   {

       inputTrigger = ControlInput(nameof(inputTrigger), Trigger);
       keyPathValueInput = ValueInput<string>("keyPathInput","");
       jsonNodeValueInput = ValueInput<JSONNode>("jsonNodeInput",new JSONObject());
       doubleValueOutput = ValueOutput<double>("doubleOutput", (flow) => {
           return _doubleOutput;
       });
       outputTrigger = ControlOutput(nameof(outputTrigger));
       Succession(inputTrigger, outputTrigger);
   }

   //Sending the Event MyCustomEvent with the integer value from the ValueInput port myValueA.
   private ControlOutput Trigger(Flow flow) {
       var keyPath = flow.GetValue<string>(keyPathValueInput);
       var jsonNode = flow.GetValue<JSONNode>(jsonNodeValueInput);

       Debug.Log(string.Format("JSONNodeHandler jsonNode = {0}", jsonNode));

       var jsonNodeValue = jsonNode.GetValueByKeyPath(keyPath);
       if (jsonNodeValue != null) {
           _doubleOutput = jsonNodeValue.AsDouble;
       }
       
       return outputTrigger;
   }
   
}

}