using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Json {

[UnityEngine.Scripting.Preserve]
[UnitTitle("JSON.AsNode")]
[UnitCategory("_Nighday/JSON")]
public class JSONAsNode : Unit {

    [DoNotSerialize]
    private JSONNode _jsonNodeOutput;

    [DoNotSerialize] // Mandatory attribute, to make sure we don’t serialize data that should never be serialized.
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlInput inputTrigger { get; private set; }

    [DoNotSerialize]
    public ValueInput keyPathValueInput;

    [DoNotSerialize]
    public ValueInput jsonNodeValueInput;

    [DoNotSerialize]
    public ValueOutput jsonNodeValueOutput;

    [DoNotSerialize]
    public ValueOutput jsonNodeDictionaryCollectionValueOutput;

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput outputTrigger { get; private set; }

    protected override void Definition() {

        inputTrigger = ControlInput(nameof(inputTrigger), Trigger);
        keyPathValueInput = ValueInput<string>("keyPathInput", "");
        jsonNodeValueInput = ValueInput<JSONNode>("jsonNodeInput", null);
        jsonNodeValueOutput = ValueOutput<JSONNode>("jsonNodeOutput", (flow) => { return _jsonNodeOutput; });
        jsonNodeDictionaryCollectionValueOutput = ValueOutput<IDictionary>("dictionaryCollectionOutput", (flow) => { return (IDictionary)((((JSONObject)_jsonNodeOutput).Linq).ToDictionary(x => x.Key, x => x.Value)); });

        // jsonNodeDictionaryCollectionValueOutput = ValueOutput<IEnumerable<KeyValuePair<string, JSONNode>>>("dictionaryCollectionOutput", (flow) => { return (IEnumerable<KeyValuePair<string, JSONNode>>)(_jsonNodeOutput.Linq); });
        outputTrigger = ControlOutput(nameof(outputTrigger));
        Succession(inputTrigger, outputTrigger);
    }

    //Sending the Event MyCustomEvent with the integer value from the ValueInput port myValueA.
    private ControlOutput Trigger(Flow flow) {
        var jsonNode = flow.GetValue<JSONNode>(jsonNodeValueInput);
        var keyPath = flow.GetValue<string>(keyPathValueInput);

        _jsonNodeOutput = jsonNode.GetValueByKeyPath(keyPath);

        return outputTrigger;
    }

}

}