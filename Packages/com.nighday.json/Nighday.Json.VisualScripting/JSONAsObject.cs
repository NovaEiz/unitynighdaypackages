using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Json {

[UnityEngine.Scripting.Preserve]
[UnitTitle("JSON.AsObject")]
[UnitCategory("_Nighday/JSON")]
public class JSONAsObject : Unit {

    [DoNotSerialize]
    private JSONObject _jsonObject;

    [DoNotSerialize] // Mandatory attribute, to make sure we don’t serialize data that should never be serialized.
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlInput inputTrigger { get; private set; }

    [DoNotSerialize]
    public ValueInput keyPathValueInput;

    [DoNotSerialize]
    public ValueInput jsonNodeValueInput;

    [DoNotSerialize]
    public ValueOutput jsonNodeValueOutput;

    [DoNotSerialize]
    public ValueOutput jsonNodeDictionaryCollectionValueOutput;

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput outputTrigger { get; private set; }

    [DoNotSerialize]
    // [PortLabelHidden]
    public ControlOutput throwExit { get; private set; }

    protected override void Definition() {

        inputTrigger = ControlInput(nameof(inputTrigger), Trigger);
        keyPathValueInput = ValueInput<string>("keyPathInput", "");
        jsonNodeValueInput = ValueInput<JSONNode>("jsonNodeInput", null);
        jsonNodeValueOutput = ValueOutput<JSONNode>("jsonNodeOutput", (flow) => { return _jsonObject; });
        jsonNodeDictionaryCollectionValueOutput = ValueOutput<IDictionary>("dictionaryCollectionOutput", (flow) => { return (IDictionary)((((JSONObject)_jsonObject).Linq).ToDictionary(x => x.Key, x => x.Value)); });

        // jsonNodeDictionaryCollectionValueOutput = ValueOutput<IEnumerable<KeyValuePair<string, JSONNode>>>("dictionaryCollectionOutput", (flow) => { return (IEnumerable<KeyValuePair<string, JSONNode>>)(_jsonNodeOutput.Linq); });
        outputTrigger = ControlOutput(nameof(outputTrigger));
        throwExit = ControlOutput(nameof(throwExit));
        Succession(inputTrigger, outputTrigger);
        Succession(inputTrigger, throwExit);
    }

    //Sending the Event MyCustomEvent with the integer value from the ValueInput port myValueA.
    private ControlOutput Trigger(Flow flow) {
        var jsonNode = flow.GetValue<JSONNode>(jsonNodeValueInput);
        var keyPath = flow.GetValue<string>(keyPathValueInput);

        var jsonObject = jsonNode.GetValueByKeyPath(keyPath);

        if (!jsonObject.IsObject) {
            return throwExit;
        }

        _jsonObject = jsonObject as JSONObject;

        return outputTrigger;
    }

}

}