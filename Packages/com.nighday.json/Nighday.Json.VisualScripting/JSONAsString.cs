using System;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Json {

[UnityEngine.Scripting.Preserve]
[UnitTitle("JSON.AsString")]
[UnitCategory("_Nighday/JSON")]
public class JSONAsString : Unit {

    [DoNotSerialize]
    private string _stringOutput;

    [DoNotSerialize]
    public ValueInput keyPathValueInput;

    [DoNotSerialize]
    public ValueInput jsonNodeValueInput;

    [DoNotSerialize]
    public ValueOutput stringValueOutput;

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput inputTrigger { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput outputTrigger { get; private set; }

    protected override void Definition() {
        inputTrigger = ControlInput(nameof(inputTrigger), Trigger);
        keyPathValueInput = ValueInput<string>("keyPathInput", "");
        jsonNodeValueInput = ValueInput<JSONNode>("jsonNodeInput", new JSONObject());
        stringValueOutput = ValueOutput<string>("stringOutput", (flow) => { return _stringOutput; });
        outputTrigger = ControlOutput(nameof(outputTrigger));
        Succession(inputTrigger, outputTrigger);
    }

    private ControlOutput Trigger(Flow flow) {
        try {
            var keyPath = flow.GetValue<string>(keyPathValueInput);
            var jsonNode = flow.GetValue<JSONNode>(jsonNodeValueInput);
            
            var jsonNodeValue = jsonNode.GetValueByKeyPath(keyPath);
            if (jsonNodeValue == null || jsonNodeValue.IsNull) {
                _stringOutput = null;
            } else {
                _stringOutput = jsonNodeValue.Value;
            }
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }

        return outputTrigger;
    }

}

}