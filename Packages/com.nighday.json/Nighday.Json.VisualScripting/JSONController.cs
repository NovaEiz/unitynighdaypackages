
namespace Nighday.Json {

public class JSONController {

    private JSONNode _jsonNode;

    public JSONNode jsonNode => _jsonNode;

    public void Parse(string json) {
        _jsonNode = JSON.Parse(json);
    }

    public void Set(JSONNode jsonNode_) {
        _jsonNode = jsonNode_;
    }

}

}