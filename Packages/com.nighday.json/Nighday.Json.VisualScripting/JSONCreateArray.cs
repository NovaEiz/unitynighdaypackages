using Unity.VisualScripting;

namespace Nighday.Json {

[UnityEngine.Scripting.Preserve]
[UnitTitle("JSON.Create.Array")]
[UnitCategory("_Nighday/JSON")]//Setting the path to find the unit in the fuzzy finder in Events > My Events.
public class JSONCreateArray : Unit
{
    [DoNotSerialize]// Mandatory attribute, to make sure we don’t serialize data that should never be serialized.
    [PortLabelHidden]// Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlInput inputTrigger { get; private set; }
    [DoNotSerialize]
    public ValueOutput jsonArrayOutput;
    [DoNotSerialize]
    [PortLabelHidden]// Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlOutput outputTrigger { get; private set; }

    protected override void Definition() {
        var value = new JSONObject();

        inputTrigger = ControlInput(nameof(inputTrigger), Trigger);
        jsonArrayOutput = ValueOutput<JSONArray>("jsonArrayOutput", (flow) => {
            return new JSONArray();
        });
        outputTrigger = ControlOutput(nameof(outputTrigger));
        Succession(inputTrigger, outputTrigger);
    }

    //Sending the Event MyCustomEvent with the integer value from the ValueInput port myValueA.
    private ControlOutput Trigger(Flow flow) {
        return outputTrigger;
    }
}

}