using Unity.VisualScripting;

namespace Nighday.Json {

[UnityEngine.Scripting.Preserve]
[UnitTitle("JSON.Create.Node")]
[UnitCategory("_Nighday/JSON")]//Setting the path to find the unit in the fuzzy finder in Events > My Events.
public class JSONCreateNode : Unit
{
    [DoNotSerialize]// Mandatory attribute, to make sure we don’t serialize data that should never be serialized.
    [PortLabelHidden]// Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlInput inputTrigger { get; private set; }
    [DoNotSerialize]
    public ValueOutput jsonNode;
    [DoNotSerialize]
    [PortLabelHidden]// Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlOutput outputTrigger { get; private set; }

    protected override void Definition() {
        inputTrigger = ControlInput(nameof(inputTrigger), Trigger);
        jsonNode = ValueOutput<JSONNode>(nameof(jsonNode), (flow) => {
            return new JSONObject();
        });
        outputTrigger = ControlOutput(nameof(outputTrigger));
        Succession(inputTrigger, outputTrigger);
    }

    //Sending the Event MyCustomEvent with the integer value from the ValueInput port myValueA.
    private ControlOutput Trigger(Flow flow) {
        return outputTrigger;
    }
}

}