using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Json {

[UnityEngine.Scripting.Preserve]
[UnitTitle("JSON.FromTextAsset")]
[UnitCategory("_Nighday/JSON")]
public class JSONFromTextAsset : Unit {

    [DoNotSerialize]
    private JSONNode _jsonNodeOutput;

    [DoNotSerialize]
    public ValueInput keyPathValueInput;

    [DoNotSerialize]
    public ValueInput textAssetValueInput;

    [DoNotSerialize]
    public ValueOutput jsonNodeValueOutput;

    [DoNotSerialize]
    public ValueOutput jsonNodeDictionaryCollectionValueOutput;

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        keyPathValueInput = ValueInput<string>("keyPathInput", "");
        textAssetValueInput = ValueInput<TextAsset>("textAssetInput", null);
        jsonNodeValueOutput = ValueOutput<JSONNode>("jsonNodeOutput", (flow) => { return _jsonNodeOutput; });
        jsonNodeDictionaryCollectionValueOutput = ValueOutput<IDictionary>("dictionaryCollectionOutput", (flow) => { return (IDictionary)((((JSONObject)_jsonNodeOutput).Linq).ToDictionary(x => x.Key, x => x.Value)); });
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        Succession(enter, exit);
    }

    private ControlOutput Trigger(Flow flow) {
        var textAsset = flow.GetValue<TextAsset>(textAssetValueInput);
        JSONNode jsonNode = null;
        try {
            jsonNode = JSON.Parse(textAsset.text);
        } catch (Exception e) {
            Debug.Log("Не правильный json из TextAsset");
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }
        var keyPath = flow.GetValue<string>(keyPathValueInput);

        _jsonNodeOutput = jsonNode.GetValueByKeyPath(keyPath);

        return exit;
    }

}

}