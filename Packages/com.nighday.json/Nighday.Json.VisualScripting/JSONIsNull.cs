using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Json {

[UnityEngine.Scripting.Preserve]
[UnitTitle("JSON.IsNull")]
[UnitCategory("_Nighday/JSON")]
public class JSONIsNull : Unit {

    [DoNotSerialize]
    private bool _result;

    [DoNotSerialize]
    public ValueInput jsonNodeInput;

    [DoNotSerialize]
    public ValueOutput resultOutput;

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput inputTrigger { get; private set; }

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput outputTrigger { get; private set; }

    protected override void Definition() {

        jsonNodeInput = ValueInput<JSONNode>("jsonNode", null);
        resultOutput = ValueOutput<bool>("result", (flow) => { return _result; });

        inputTrigger = ControlInput(nameof(inputTrigger), Trigger);
        outputTrigger = ControlOutput(nameof(outputTrigger));

        Succession(inputTrigger, outputTrigger);
    }

    private ControlOutput Trigger(Flow flow) {
        var jsonNode = flow.GetValue<JSONNode>(jsonNodeInput);

        _result = jsonNode == null || jsonNode.IsNull;

        return outputTrigger;
    }

}

}