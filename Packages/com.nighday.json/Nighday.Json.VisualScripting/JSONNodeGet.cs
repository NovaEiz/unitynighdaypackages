using Unity.VisualScripting;

namespace Nighday.Json {

[UnityEngine.Scripting.Preserve]
[UnitTitle("JSONNode.Get")]
[UnitCategory("_Nighday/JSON")]//Setting the path to find the unit in the fuzzy finder in Events > My Events.
public class JSONNodeGet : Unit
{
    [DoNotSerialize]// Mandatory attribute, to make sure we don’t serialize data that should never be serialized.
    [PortLabelHidden]// Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlInput inputTrigger { get; private set; }
    [DoNotSerialize]
    public ValueOutput jsonNode;
    [DoNotSerialize]
    [PortLabelHidden]// Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlOutput outputTrigger { get; private set; }

    private JSONNode _jsonNode;

    protected override void Definition() {
        var value = new JSONObject();

        inputTrigger = ControlInput(nameof(inputTrigger), Trigger);
        jsonNode = ValueOutput<JSONNode>(nameof(jsonNode), (flow) => {
            return _jsonNode;
        });
        outputTrigger = ControlOutput(nameof(outputTrigger));
        Succession(inputTrigger, outputTrigger);
    }

    //Sending the Event MyCustomEvent with the integer value from the ValueInput port myValueA.
    private ControlOutput Trigger(Flow flow) {
        _jsonNode = new JSONObject();
        _jsonNode.Add("testKey", "testValue");

        var childJsonNode = new JSONObject();

        _jsonNode.Add("childNode", childJsonNode);
        
        var arr = new JSONArray();
        arr.Add("Value1");
        arr.Add("Value2");
        arr.Add("Value3");
        
        childJsonNode.Add("testArray", arr);
        return outputTrigger;
    }
}

}