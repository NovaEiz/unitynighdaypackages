using System;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Json {

[UnityEngine.Scripting.Preserve]
[UnitTitle("JSON.Parse")]
[UnitCategory("_Nighday/JSON")]//Setting the path to find the unit in the fuzzy finder in Events > My Events.
public class JSONParseNode : Unit {

    [DoNotSerialize]
    private JSONNode _jsonNode;
    
    [DoNotSerialize]
    public ValueInput dataValueInput;
    
    [DoNotSerialize]
    public ValueOutput jsonNodeValueOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);
        
        dataValueInput = ValueInput<string>("data");
        jsonNodeValueOutput = ValueOutput<JSONNode>("jsonValue", (flow) => { return _jsonNode; });
        
        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);
    }

    private ControlOutput Trigger(Flow flow) {
        var data = flow.GetValue<string>(dataValueInput);

        try {
            _jsonNode = JSON.Parse(data);
        } catch (Exception e) {
            _jsonNode = new JSONObject();
            Debug.LogException(e);
            throw;
        }
        
        return exit;
    }
}

}