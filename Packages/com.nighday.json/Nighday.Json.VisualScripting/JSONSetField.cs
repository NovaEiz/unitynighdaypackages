using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Json {

[UnityEngine.Scripting.Preserve]
[UnitTitle("JSON.SetField")]
[UnitCategory("_Nighday/JSON")]
public class JSONSetField : Unit {

    [DoNotSerialize]
    private JSONNode _jsonNodeOutput;

    [DoNotSerialize] // Mandatory attribute, to make sure we don’t serialize data that should never be serialized.
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlInput inputTrigger { get; private set; }

    [DoNotSerialize]
    public ValueInput keyPathValueInput;

    [DoNotSerialize]
    public ValueInput isCreateObjectsInPathValueInput;

    [DoNotSerialize]
    public ValueInput jsonNodeValueInput;

    [DoNotSerialize]
    public ValueInput jsonNodeFieldValueInput;

    [DoNotSerialize]
    public ValueOutput operationResultValueOutput;

    [DoNotSerialize]
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlOutput outputTrigger { get; private set; }

    private bool _operationResult;

    protected override void Definition() {

        inputTrigger = ControlInput(nameof(inputTrigger), Trigger);
        keyPathValueInput = ValueInput<string>("keyPathInput", null);
        jsonNodeValueInput = ValueInput<JSONNode>("jsonNodeInput", null);
        jsonNodeFieldValueInput = ValueInput<JSONNode>("jsonNodeFieldInput", null);
        isCreateObjectsInPathValueInput = ValueInput<bool>("isCreateObjectsInPathInput", false);
        operationResultValueOutput = ValueOutput<bool>("resultOutput", (flow) => {
            return _operationResult;
        });
        outputTrigger = ControlOutput(nameof(outputTrigger));
        Succession(inputTrigger, outputTrigger);
    }

    //Sending the Event MyCustomEvent with the integer value from the ValueInput port myValueA.
    private ControlOutput Trigger(Flow flow) {
        var keyPath = flow.GetValue<string>(keyPathValueInput);
        var jsonNode = flow.GetValue<JSONNode>(jsonNodeValueInput);
        var jsonNodeField = flow.GetValue<JSONNode>(jsonNodeFieldValueInput);
        var isCreateObjectsInPathValue = flow.GetValue<bool>(isCreateObjectsInPathValueInput);
        
        _operationResult = jsonNode.SetField(keyPath, jsonNodeField, isCreateObjectsInPathValue);

        return outputTrigger;
    }

}

}