using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Json {

[UnityEngine.Scripting.Preserve]
[UnitTitle("JSON.ToString()")]
[UnitCategory("_Nighday/JSON")]
public class JSONToString : Unit {

    [DoNotSerialize]
    private string _stringOutput;

    [DoNotSerialize]
    public ValueInput jsonNodeValueInput;

    [DoNotSerialize]
    public ValueOutput stringValueOutput;

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput inputTrigger { get; private set; }

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput outputTrigger { get; private set; }

    protected override void Definition() {
        jsonNodeValueInput = ValueInput<JSONNode>("jsonNodeInput", new JSONObject());
        stringValueOutput = ValueOutput<string>("stringOutput", (flow) => { return _stringOutput; });
        
        inputTrigger = ControlInput(nameof(inputTrigger), Trigger);
        outputTrigger = ControlOutput(nameof(outputTrigger));
        Succession(inputTrigger, outputTrigger);
    }

    //Sending the Event MyCustomEvent with the integer value from the ValueInput port myValueA.
    private ControlOutput Trigger(Flow flow) {
        var jsonNode = flow.GetValue<JSONNode>(jsonNodeValueInput);

        _stringOutput = string.Format("{0}", jsonNode);

        return outputTrigger;
    }

}

}