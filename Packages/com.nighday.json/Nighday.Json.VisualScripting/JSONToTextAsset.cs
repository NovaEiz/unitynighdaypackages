using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Nighday.Json {

[UnityEngine.Scripting.Preserve]
[UnitTitle("JSON.ToTextAsset")]
[UnitCategory("_Nighday/JSON")]
public class JSONToTextAsset : Unit {

    [DoNotSerialize]
    public ValueInput jsonNodeInput;

    [DoNotSerialize]
    public ValueInput textAssetInput;

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition() {

        jsonNodeInput = ValueInput<JSONNode>("jsonNode", "");
        textAssetInput = ValueInput<TextAsset>("textAsset", null);

        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        Succession(enter, exit);
    }

    private ControlOutput Trigger(Flow flow) {
        var textAsset = flow.GetValue<TextAsset>(textAssetInput);
        var jsonNode = flow.GetValue<JSONNode>(jsonNodeInput);
        try {
#if UNITY_EDITOR
            var assetPath = AssetDatabase.GetAssetPath(textAsset);
            var text = jsonNode.ToString(4);
            File.WriteAllText(assetPath, text);
            EditorUtility.SetDirty(textAsset);
#endif
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }

        return exit;
    }

}

}