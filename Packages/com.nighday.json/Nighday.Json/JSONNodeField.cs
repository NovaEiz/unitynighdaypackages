using System;
using UnityEngine;

namespace Nighday.Json {

[Serializable]
public class JSONNodeField {
	[SerializeField] private string _data;

	private JSONNode _value;

	public JSONNode Value {
		get {
			if (_value == null) {
				_value = FactValue;
			}
			return _value;
		}
	}

	public JSONNode FactValue {
		get {
			return JSON.Parse(!string.IsNullOrEmpty(_data) ? _data : "{}");
		}
	}

	public void SetValue(JSONNode value) {
		_data = value.ToString();
	}


}
}