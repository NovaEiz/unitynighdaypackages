using System.Linq;

namespace Nighday.Json {
public static class JsonNodeExtension {
    public static T Decode<T>(this JSONNode node) where T : IJsonDecoding, new() {
        var res = new T();
        res.Decode(node.AsObject);
        return res;
    }

    public static T[] DecodeArray<T>(this JSONNode node) where T : IJsonDecoding, new() {
        return !node.IsArray ? new T[0] : node.Children.Select(n => n.Decode<T>()).ToArray();
    }

    public static T DecodeFromJson<T>(this string json) where T : IJsonDecoding, new() {
        return JSON.Parse(json).Decode<T>();
    }

    public static string ToJson(this IJsonEncoding obj) {
        var node = new JSONObject();
        obj.Encode(node);
        return node.ToString();
    }
    
    public static JSONNode GetByKeyPath(this JSONNode jsonNode, string keyPath) {
        if (string.IsNullOrEmpty(keyPath)) {
            return jsonNode;
        }
        if (jsonNode == null) {
            return null;
        }
        var keyPathSplit = keyPath.Split('.').ToList();
        var key = keyPath;
        if (keyPathSplit.Count > 1) {
            key = keyPathSplit[0];
            keyPathSplit.RemoveAt(0);
            keyPath = string.Join(".", keyPathSplit);
            jsonNode = jsonNode[key];
            return jsonNode.GetByKeyPath(keyPath);
        } else {
            jsonNode = jsonNode[key];
            return jsonNode;
        }
    }
}

public interface IJsonDecoding {
    void Decode(JSONObject node);
}

public interface IJsonEncoding {
    void Encode(JSONObject node);
}
}