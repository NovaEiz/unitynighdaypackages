using System;
using UnityEngine;

namespace Nighday.Json {

public static class SimpleJSONExt {

    public static string ConvertToRealKey(string keyFromKeyPath) {
        var left = keyFromKeyPath.IndexOf('[');
        var right = keyFromKeyPath.IndexOf(']');
        if (left > -1) {
            return keyFromKeyPath.Substring(left + 1, right - (left + 1));
        }

        return keyFromKeyPath;
    }

    public static bool AddField(this JSONNode jsonNode,
                                string        keyPath,
                                JSONNode      jsonNodeValue,
                                bool          isCreateObjectsInPath = false) {

        try {

            var jsonNodeParent = jsonNode.GetParentValueByKeyPath(keyPath, out string lastChildKey, isCreateObjectsInPath);
            if (!isCreateObjectsInPath && jsonNodeParent == null) {
                return false;
            }

            lastChildKey = ConvertToRealKey(lastChildKey);
            if (jsonNodeParent.IsArray) {
                var lastChildKeyIndex = Convert.ToInt32(lastChildKey);
                var jsonNodeParentArray = jsonNodeParent.AsArray;

                while (jsonNodeParentArray.Count - 1 < lastChildKeyIndex) {
                    jsonNodeParentArray.Add(null);
                }

                if (jsonNodeParent[lastChildKeyIndex] != null) {
                    return false;
                }

                jsonNodeParent[lastChildKeyIndex] = jsonNodeValue;
            } else {

                if (jsonNodeParent[lastChildKey] != null) {
                    return false;
                }

                jsonNodeParent.Add(lastChildKey, jsonNodeValue);
            }

            return true;

        } catch (Exception e) {
            Debug.Log(e);
            return false;
        }
    }

    public static bool SetField(this JSONNode jsonNode,
                                string        keyPath,
                                JSONNode      jsonNodeValue,
                                bool          isCreateObjectsInPath = false) {
        try {
            var jsonNodeParent = jsonNode.GetParentValueByKeyPath(keyPath, out string lastChildKey, isCreateObjectsInPath);
            if (!isCreateObjectsInPath && jsonNodeParent == null) {
                return false;
            }

            lastChildKey = ConvertToRealKey(lastChildKey);
            if (jsonNodeParent.IsArray) {
                var lastChildKeyIndex = Convert.ToInt32(lastChildKey);
                var jsonNodeParentArray = jsonNodeParent.AsArray;

                while (jsonNodeParentArray.Count - 1 < lastChildKeyIndex) {
                    jsonNodeParentArray.Add(null);
                }

                jsonNodeParent[lastChildKeyIndex] = jsonNodeValue;
            } else {

                if (jsonNodeParent[lastChildKey] == null) {
                    jsonNodeParent.Add(lastChildKey, null);

                }

                jsonNodeParent[lastChildKey] = jsonNodeValue;
            }

            return true;

        } catch (Exception e) {
            Debug.Log(e);
            return false;
        }
    }

    public static JSONNode GetParentValueByKeyPath(this JSONNode jsonNode,
                                                   string        keyPath,
                                                   out string    lastChildKey,
                                                   bool          isCreateObjectsInPath = false) {
        try {

            var keyPathLengthBegin = keyPath.Length;
            var keyPathSplitByDot = keyPath.Split('.');
            var keyPathSplitByDotLastItem = keyPathSplitByDot[keyPathSplitByDot.Length - 1];
            var keyPathSplitByDotLastItemSplitByRightSquareBracket = keyPathSplitByDotLastItem.Split(']');
            var keyPathSplitByDotLastItemSplitByRightSquareBracketLastItem = keyPathSplitByDotLastItemSplitByRightSquareBracket[keyPathSplitByDotLastItemSplitByRightSquareBracket.Length - 1];
            if (string.IsNullOrEmpty(keyPathSplitByDotLastItemSplitByRightSquareBracketLastItem)) {
                var keyPathSplitByDotLastItemSplit = keyPathSplitByDotLastItem.Split('[');

                lastChildKey = "[" + keyPathSplitByDotLastItemSplit[keyPathSplitByDotLastItemSplit.Length - 1];
            } else {
                lastChildKey = keyPathSplitByDotLastItemSplitByRightSquareBracketLastItem;
            }

            var minusChars = 0;
            if (keyPathSplitByDotLastItemSplitByRightSquareBracket.Length == 1 && keyPathSplitByDot.Length > 1) {
                minusChars++;
            }

            keyPath = keyPath.Substring(0, keyPath.Length - (lastChildKey.Length + minusChars));
            if (keyPathLengthBegin == keyPath.Length) {
                return jsonNode;
            }

            if (lastChildKey[0] == '[') {
                keyPath += "[]";
            }

            var jsonNodeValue = GetValueByKeyPath(jsonNode, keyPath, isCreateObjectsInPath);
            return jsonNodeValue;

        } catch (Exception e) {
            Debug.Log(e);
            lastChildKey = null;
            return null;
        }
    }

    public static JSONNode GetValueByKeyPath(this JSONNode jsonNode,
                                             string        keyPath,
                                             bool          isCreateObjectsInPath = false) {
        try {
            if (string.IsNullOrEmpty(keyPath)) {
                return jsonNode;
            }

            var indexOfDot = keyPath.IndexOf(".");
            var indexOfLeftSquareBracket = keyPath.IndexOf("[");

            if (indexOfDot > -1 && ((indexOfLeftSquareBracket == -1) || (indexOfLeftSquareBracket > -1 && indexOfDot < indexOfLeftSquareBracket))) {
                var key = keyPath.Substring(0, indexOfDot);
                keyPath = keyPath.Substring((indexOfDot + 1), keyPath.Length - (indexOfDot + 1));

                JSONNode nextJsonNode = null;
                if (jsonNode.IsArray) {
                    nextJsonNode = jsonNode.AsArray[int.Parse(key)];
                } else {
                    nextJsonNode = jsonNode[key];
                }
                
                if (isCreateObjectsInPath && nextJsonNode == null) {
                    nextJsonNode = new JSONObject();
                    jsonNode.Add(key, nextJsonNode);
                }
                

                return GetValueByKeyPath(nextJsonNode, keyPath);
            }

            if (indexOfLeftSquareBracket > -1) {
                var indexOfRightSquareBracket = keyPath.IndexOf("]");

                if (indexOfLeftSquareBracket == 0 && indexOfRightSquareBracket > 1) {
                    var key = keyPath.Substring(1, indexOfRightSquareBracket - 1);
                    keyPath = keyPath.Substring((indexOfRightSquareBracket + 1), keyPath.Length - (indexOfRightSquareBracket + 1));

                    var jsonArray = jsonNode.AsArray;
                    var nextJsonNode = jsonArray[key];
                    if (isCreateObjectsInPath && nextJsonNode == null) {
                        if (keyPath[0] == '[') {
                            nextJsonNode = new JSONArray();
                        } else {
                            nextJsonNode = new JSONObject();
                        }

                        var intKey = Convert.ToInt32(key);
                        while (jsonArray.Count - 1 < intKey) {
                            jsonArray.Add(null);
                        }

                        jsonArray[key] = nextJsonNode;
                    }

                    return GetValueByKeyPath(nextJsonNode, keyPath);

                } else {
                    var key = keyPath.Substring(0, indexOfLeftSquareBracket);
                    keyPath = keyPath.Substring(indexOfLeftSquareBracket, keyPath.Length - indexOfLeftSquareBracket);

                    if (keyPath.Substring(0, 2) == "[]") {
                        keyPath = keyPath.Substring(2, keyPath.Length - 2);
                    }

                    var nextJsonNode = jsonNode[key];
                    if (isCreateObjectsInPath && nextJsonNode == null) {
                        nextJsonNode = new JSONArray();
                        jsonNode.Add(key, nextJsonNode);
                    }

                    return GetValueByKeyPath(nextJsonNode, keyPath);
                }
            }


            var lastKey = keyPath;
            JSONNode resJsonNode = null;
            if (jsonNode.IsArray) {
                resJsonNode = jsonNode.AsArray[int.Parse(lastKey)];
            } else {
                resJsonNode = jsonNode[lastKey];
            }

            return resJsonNode;

        } catch (Exception e) {
            Debug.Log(e);
            return null;
        }
    }

}

}