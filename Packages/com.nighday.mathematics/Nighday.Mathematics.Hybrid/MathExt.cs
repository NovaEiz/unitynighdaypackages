using Unity.Mathematics;
using UnityEngine;

namespace Nighday.Mathematics {

public static class MathExt {
	
	public static float3 ConvertAngleToDirection(this float angle) {
		var rot = Quaternion.AngleAxis(angle, Vector3.up);
		var direction = (float3)(rot * Vector3.forward);
		direction = math.normalize(direction);

		return direction;
	}
	
	public static float2 ConvertAngleToDirectionXZ(this float angle) {
		var rot = Quaternion.AngleAxis(angle, Vector3.up);
		var direction = (float3)(rot * Vector3.forward);
		direction = math.normalize(direction);

		return new float2(direction.x, direction.z);
	}
	
	public static float ConvertDirectionToAngle(this float2 direction) {
		var angle = ConvertDirectionToAngle((Vector2)direction);

		/*
		
		var dirCamera = math.rotate(unitTrackingCameraLocalToWorld.Rotation, new float3(0, 0, 1));
		dirCamera.y = 0;
		var cameraDirection = new float2(dirCamera.x, dirCamera.z);
		var angleCamera = GetAngleFromVector2(cameraDirection);
		
		angle += angleCamera;
		 */

		return angle;
	}
	
	public static float ConvertDirectionXZToAngle(this float3 direction) {
		direction.y = direction.z;
		var angle = ConvertDirectionToAngle((Vector3)direction);

		/*
		
		var dirCamera = math.rotate(unitTrackingCameraLocalToWorld.Rotation, new float3(0, 0, 1));
		dirCamera.y = 0;
		var cameraDirection = new float2(dirCamera.x, dirCamera.z);
		var angleCamera = GetAngleFromVector2(cameraDirection);
		
		angle += angleCamera;
		 */

		return angle;
	}
	
	public static float ConvertDirectionToAngle(this Vector2 direction) {
		float angle = Vector2.Angle(Vector2.up, direction);
		if (direction.x < 0.0f) {
			angle = 360f - angle;
		}

		return angle;
	}
	
}

}