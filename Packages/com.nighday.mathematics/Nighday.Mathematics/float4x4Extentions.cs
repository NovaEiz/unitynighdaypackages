using Unity.Mathematics;

namespace Nighday.Mathematics {

public static class float4x4Extentions {

	public static float3 GetScale(this float4x4 matrix) => new float3(
		math.length(matrix.c0.xyz),
		math.length(matrix.c1.xyz),
		math.length(matrix.c2.xyz)
	);
	/*
	public static float3 GetEulerAngles(this float4x4 matrix) => new float3(
		math.length(matrix.c0.xyz),
		math.length(matrix.c1.xyz),
		math.length(matrix.c2.xyz)
	);
	*/
	
	public static float convertDirectionXZToAngle(this float3 direction) {
		var dir2 = new float2(direction.x, direction.z);
		var angle = convertDirectionToAngle(dir2);

		/*
		
		var dirCamera = math.rotate(unitTrackingCameraLocalToWorld.Rotation, new float3(0, 0, 1));
		dirCamera.y = 0;
		var cameraDirection = new float2(dirCamera.x, dirCamera.z);
		var angleCamera = GetAngleFromVector2(cameraDirection);
		
		angle += angleCamera;
		 */

		return angle;
	}
	
	public static float convertDirectionToAngle(this float2 direction) {
		float angle = Angle(new float2(0,1), direction);
		if (direction.x < 0.0f) {
			angle = 360f - angle;
		}

		return angle;
	}
	
	public static float Angle(float2 from, float2 to)
	{
		float num = (float) math.sqrt(math.lengthsq(from) * math.lengthsq(to));
		return (double) num < 1.0000000036274937E-15 ? 0.0f : math.acos(math.clamp(math.dot(from, to) / num, -1f, 1f)) * 57.29578f;
	}
	
}

}