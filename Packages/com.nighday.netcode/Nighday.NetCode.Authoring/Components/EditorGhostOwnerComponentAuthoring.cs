using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace Nighday.NetCode.Authoring {

/// <summary>
/// Должен находится в инспекторе ниже чем GhostOwnerComponentAuthoring, чтобы переназначить NetworkId
/// </summary>
public class EditorGhostOwnerComponentAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

	[SerializeField] private int _ownerNetworkId = -1;
	
	public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		dstManager.SetComponentData(entity, new GhostOwnerComponent {
			NetworkId = _ownerNetworkId
		});
	}
}


}