using Unity.Entities;
using Unity.NetCode;

namespace Nighday.NetCode.Command {
public struct SetCommandTargetRequest : IRpcCommand {
	public Entity GhostEntity;
}
}