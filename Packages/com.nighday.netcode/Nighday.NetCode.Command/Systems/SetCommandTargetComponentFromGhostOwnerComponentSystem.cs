using Nighday.Entities.Camera;
using Unity.Entities;
using Unity.NetCode;
using Unity.Transforms;

namespace Nighday.NetCode.Command {

[UpdateInGameWorld(UpdateInGameWorld.TargetWorld.ClientAndServer)]
public class SetCommandTargetComponentFromGhostOwnerComponentSystem : ComponentSystem {

	private struct CommandTargetComponentCorrect : IComponentData {
	}

	protected override void OnCreate() {
		RequireSingletonForUpdate<CameraTarget>();
	}

	protected override void OnUpdate() {
		Entities
			.WithNone<CommandTargetComponentCorrect>()
			.ForEach((Entity entity,
					ref NetworkIdComponent networkIdComponent,
					ref CommandTargetComponent commandTargetComponent
					) => {
						var networkId = networkIdComponent.Value;

						var targetEntity = Entity.Null;
						Entities
							.ForEach((Entity ghostEntity,
									ref GhostOwnerComponent ghostOwnerComponent
									) => {

										if (ghostOwnerComponent.NetworkId == networkId) {

											SetCommandTargetSystem.Send_SetCommandTargetRequest(
												EntityManager, ghostEntity);

											targetEntity = ghostEntity;

											var cameraTargetEntity = GetSingletonEntity<CameraTarget>();

											EntityManager.AddComponentData(entity, new CommandTargetComponentCorrect());

											EntityManager.AddComponentData(cameraTargetEntity, new Parent {
												Value = targetEntity
											});
											EntityManager.AddComponentData(cameraTargetEntity, new LocalToParent());
											EntityManager.AddComponentData(cameraTargetEntity, new Translation());
										}
									});

						commandTargetComponent.targetEntity = targetEntity;
					});
	}

}
}