using Unity.Entities;
using Unity.NetCode;

namespace Nighday.NetCode.Command {

public class SetCommandTargetSystem : ComponentSystem {

	public static void Send_SetCommandTargetRequest(EntityManager EntityManager, Entity ghostEntity) {
		var req = new SetCommandTargetRequest {
			GhostEntity = ghostEntity
		};
		var reqEntity = EntityManager.CreateEntity();
		EntityManager.AddComponentData(reqEntity, req);
		EntityManager.AddComponentData(reqEntity, new SendRpcCommandRequestComponent());
	}

	protected override void OnUpdate() {
		Entities
			.WithNone<SendRpcCommandRequestComponent>()
			.ForEach((Entity entity,
					ref SetCommandTargetRequest setCommandTargetRequest,
					ref ReceiveRpcCommandRequestComponent requestComponent
					) => {
						EntityManager.DestroyEntity(entity);

						var connectionEntity = requestComponent.SourceConnection;
						EntityManager.SetComponentData(connectionEntity, new CommandTargetComponent {
							targetEntity = setCommandTargetRequest.GhostEntity
						});
			});
	}
	
}
}