using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace Nighday.NetCode {

[UnityEngine.Scripting.Preserve]
public class ClientServerBootstrap : Unity.NetCode.ClientServerBootstrap, ICustomBootstrap {

	public static World CreateLobbyClientWorld(World defaultWorld) {
		var world = new World("LobbyClientWorld", WorldFlags.Game);

		world = CreateClientWorld(defaultWorld, "LobbyClientWorld", world);
		
		var s_State = s_LobbyState;
		
		var initializationGroup = world.GetOrCreateSystem<ClientInitializationSystemGroup>();
		var simulationGroup = world.GetOrCreateSystem<ClientSimulationSystemGroup>();
		var presentationGroup = world.GetOrCreateSystem<ClientPresentationSystemGroup>();
		
		//Retrieve all clients systems and create all at once via GetOrCreateSystemsAndLogException.
		var allManagedTypes = new List<Type>(s_State.ClientInitializationSystems.Count +
											s_State.ClientSimulationSystems.Count +
											s_State.ClientPresentationSystems.Count +
											s_State.ClientChildSystems.Count + 3);
		var allUnmanagedTypes = new List<Type>();
		
		AddSystemsToList(s_State.ClientInitializationSystems, allManagedTypes, allUnmanagedTypes);
		AddSystemsToList(s_State.ClientSimulationSystems, allManagedTypes, allUnmanagedTypes);
		AddSystemsToList(s_State.ClientPresentationSystems, allManagedTypes, allUnmanagedTypes);
		foreach (var systemParentType in s_State.ClientChildSystems)
		{
			AddSystemToList(systemParentType.Item1, allManagedTypes, allUnmanagedTypes);
		}
		world.GetOrCreateSystemsAndLogException(allManagedTypes.ToArray());
		
		//Step2: group update binding
		foreach (var systemType in s_State.ClientInitializationSystems)
		{
			// TODO: handle unmanaged systems
			if (!typeof(ComponentSystemBase).IsAssignableFrom(systemType))
				continue;
			var system = world.GetExistingSystem(systemType);
			initializationGroup.AddSystemToUpdateList(system);
		}
		
		foreach (var systemType in s_State.ClientSimulationSystems)
		{
			// TODO: handle unmanaged systems
			if (!typeof(ComponentSystemBase).IsAssignableFrom(systemType))
				continue;
			var system = world.GetExistingSystem(systemType);
			simulationGroup.AddSystemToUpdateList(system);
		}
		foreach (var systemType in s_State.ClientPresentationSystems)
		{
			// TODO: handle unmanaged systems
			if (!typeof(ComponentSystemBase).IsAssignableFrom(systemType))
				continue;
			var system = world.GetExistingSystem(systemType);
			presentationGroup.AddSystemToUpdateList(system);
		}
		foreach (var systemParentType in s_State.ClientChildSystems)
		{
			// TODO: handle unmanaged systems
			if (!typeof(ComponentSystemBase).IsAssignableFrom(systemParentType.Item1))
				continue;
			var system = world.GetExistingSystem(systemParentType.Item1);
			var group = world.GetExistingSystem(systemParentType.Item2) as ComponentSystemGroup;
			group.AddSystemToUpdateList(system);
		}
		
		initializationGroup.SortSystems();
		simulationGroup.SortSystems();
		presentationGroup.SortSystems();

		return world;
	}

	public static World CreateLobbyServerWorld(World defaultWorld) {
		var world = new World("LobbyServerWorld", WorldFlags.Game);

		world = CreateServerWorld(defaultWorld, "LobbyServerWorld", world);
		
		var s_State = s_LobbyState;
		
		var initializationGroup = world.GetOrCreateSystem<ServerInitializationSystemGroup>();
		var simulationGroup = world.GetOrCreateSystem<ServerSimulationSystemGroup>();
		
		//Retrieve all clients systems and create all at once via GetOrCreateSystemsAndLogException.
		var allManagedTypes = new List<Type>(s_State.ServerInitializationSystems.Count +
											s_State.ServerSimulationSystems.Count +
											s_State.ServerChildSystems.Count + 3);
		var allUnmanagedTypes = new List<Type>();
		
		AddSystemsToList(s_State.ServerInitializationSystems, allManagedTypes, allUnmanagedTypes);
		AddSystemsToList(s_State.ServerSimulationSystems, allManagedTypes, allUnmanagedTypes);
		foreach (var systemParentType in s_State.ServerChildSystems)
		{
			AddSystemToList(systemParentType.Item1, allManagedTypes, allUnmanagedTypes);
		}
		world.GetOrCreateSystemsAndLogException(allManagedTypes.ToArray());
		

		//Step2: group update binding
		foreach (var systemType in s_State.ServerInitializationSystems)
		{
			// TODO: handle unmanaged systems
			if (!typeof(ComponentSystemBase).IsAssignableFrom(systemType))
				continue;
			var system = world.GetExistingSystem(systemType);
			initializationGroup.AddSystemToUpdateList(system);
		}
		
		foreach (var systemType in s_State.ServerSimulationSystems)
		{
			// TODO: handle unmanaged systems
			if (!typeof(ComponentSystemBase).IsAssignableFrom(systemType))
				continue;
			var system = world.GetExistingSystem(systemType);
			simulationGroup.AddSystemToUpdateList(system);
		}
		foreach (var systemParentType in s_State.ServerChildSystems)
		{
			// TODO: handle unmanaged systems
			if (!typeof(ComponentSystemBase).IsAssignableFrom(systemParentType.Item1))
				continue;
			var system = world.GetExistingSystem(systemParentType.Item1);
			var group = world.GetExistingSystem(systemParentType.Item2) as ComponentSystemGroup;
			group.AddSystemToUpdateList(system);
		}
		
		initializationGroup.SortSystems();
		simulationGroup.SortSystems();

		return world;
	}

	public static World CreateGameClientWorld(World defaultWorld, string name = "GameClientWorld",
											World worldToUse = null) {
		var world = new World(name, WorldFlags.Game);
		
		world = CreateClientWorld(defaultWorld, name, world);

		var s_State = s_GameState;
		
		var initializationGroup = world.GetOrCreateSystem<ClientInitializationSystemGroup>();
		var simulationGroup = world.GetOrCreateSystem<ClientSimulationSystemGroup>();
		var presentationGroup = world.GetOrCreateSystem<ClientPresentationSystemGroup>();
		
		//Retrieve all clients systems and create all at once via GetOrCreateSystemsAndLogException.
		var allManagedTypes = new List<Type>(s_State.ClientInitializationSystems.Count +
											s_State.ClientSimulationSystems.Count +
											s_State.ClientPresentationSystems.Count +
											s_State.ClientChildSystems.Count + 3);
		var allUnmanagedTypes = new List<Type>();
		
		AddSystemsToList(s_State.ClientInitializationSystems, allManagedTypes, allUnmanagedTypes);
		AddSystemsToList(s_State.ClientSimulationSystems, allManagedTypes, allUnmanagedTypes);
		AddSystemsToList(s_State.ClientPresentationSystems, allManagedTypes, allUnmanagedTypes);
		foreach (var systemParentType in s_State.ClientChildSystems)
		{
			AddSystemToList(systemParentType.Item1, allManagedTypes, allUnmanagedTypes);
		}
		world.GetOrCreateSystemsAndLogException(allManagedTypes.ToArray());
		
		//Step2: group update binding
		foreach (var systemType in s_State.ClientInitializationSystems)
		{
			// TODO: handle unmanaged systems
			if (!typeof(ComponentSystemBase).IsAssignableFrom(systemType))
				continue;
			var system = world.GetExistingSystem(systemType);
			initializationGroup.AddSystemToUpdateList(system);
		}
		
		foreach (var systemType in s_State.ClientSimulationSystems)
		{
			// TODO: handle unmanaged systems
			if (!typeof(ComponentSystemBase).IsAssignableFrom(systemType))
				continue;
			var system = world.GetExistingSystem(systemType);
			simulationGroup.AddSystemToUpdateList(system);
		}
		foreach (var systemType in s_State.ClientPresentationSystems)
		{
			// TODO: handle unmanaged systems
			if (!typeof(ComponentSystemBase).IsAssignableFrom(systemType))
				continue;
			var system = world.GetExistingSystem(systemType);
			presentationGroup.AddSystemToUpdateList(system);
		}
		foreach (var systemParentType in s_State.ClientChildSystems)
		{
			// TODO: handle unmanaged systems
			if (!typeof(ComponentSystemBase).IsAssignableFrom(systemParentType.Item1))
				continue;
			var system = world.GetExistingSystem(systemParentType.Item1);
			var group = world.GetExistingSystem(systemParentType.Item2) as ComponentSystemGroup;
			group.AddSystemToUpdateList(system);
		}
		
		initializationGroup.SortSystems();
		simulationGroup.SortSystems();
		presentationGroup.SortSystems();

		return world;
	}

	public static World CreateGameServerWorld(World defaultWorld, string name = "GameServerWorld",
											World worldToUse = null) {
		var world = new World(name, WorldFlags.Game);
		world = CreateServerWorld(defaultWorld, name, world);

		var s_State = s_GameState;
		
		var initializationGroup = world.GetOrCreateSystem<ServerInitializationSystemGroup>();
		var simulationGroup = world.GetOrCreateSystem<ServerSimulationSystemGroup>();
		
		//Retrieve all clients systems and create all at once via GetOrCreateSystemsAndLogException.
		var allManagedTypes = new List<Type>(s_State.ServerInitializationSystems.Count +
											s_State.ServerSimulationSystems.Count +
											s_State.ServerChildSystems.Count + 3);
		var allUnmanagedTypes = new List<Type>();
		
		AddSystemsToList(s_State.ServerInitializationSystems, allManagedTypes, allUnmanagedTypes);
		AddSystemsToList(s_State.ServerSimulationSystems, allManagedTypes, allUnmanagedTypes);
		foreach (var systemParentType in s_State.ServerChildSystems)
		{
			AddSystemToList(systemParentType.Item1, allManagedTypes, allUnmanagedTypes);
		}
		world.GetOrCreateSystemsAndLogException(allManagedTypes.ToArray());
		

		//Step2: group update binding
		foreach (var systemType in s_State.ServerInitializationSystems)
		{
			// TODO: handle unmanaged systems
			if (!typeof(ComponentSystemBase).IsAssignableFrom(systemType))
				continue;
			var system = world.GetExistingSystem(systemType);
			initializationGroup.AddSystemToUpdateList(system);
		}
		
		foreach (var systemType in s_State.ServerSimulationSystems)
		{
			// TODO: handle unmanaged systems
			if (!typeof(ComponentSystemBase).IsAssignableFrom(systemType))
				continue;
			var system = world.GetExistingSystem(systemType);
			simulationGroup.AddSystemToUpdateList(system);
		}
		foreach (var systemParentType in s_State.ServerChildSystems)
		{
			// TODO: handle unmanaged systems
			if (!typeof(ComponentSystemBase).IsAssignableFrom(systemParentType.Item1))
				continue;
			var system = world.GetExistingSystem(systemParentType.Item1);
			var group = world.GetExistingSystem(systemParentType.Item2) as ComponentSystemGroup;
			group.AddSystemToUpdateList(system);
		}
		
		initializationGroup.SortSystems();
		simulationGroup.SortSystems();

		return world;
	}
	
	private static void AddSystemsToList(List<Type> src, List<Type> allManagedSystems, List<Type> allUnmanagedSystems)
	{
		foreach (var stype in src)
			AddSystemToList(stype, allManagedSystems, allUnmanagedSystems);
	}
	private static void AddSystemToList(Type stype, List<Type> allManagedSystems, List<Type> allUnmanagedSystems)
	{
		if (typeof(ComponentSystemBase).IsAssignableFrom(stype))
			allManagedSystems.Add(stype);
		else if (typeof(ISystemBase).IsAssignableFrom(stype))
			allUnmanagedSystems.Add(stype);
		else
			throw new InvalidOperationException("Bad type");
	}

	internal class LobbyState : State {
	}

	internal class GameState : State {
	}

	protected internal class State {
		public List<Type> ClientInitializationSystems;
		public List<Type> ClientSimulationSystems;
		public List<Type> ClientPresentationSystems;
		public List<Tuple<Type, Type>> ClientChildSystems;
		public List<Type> ServerInitializationSystems;
		public List<Type> ServerSimulationSystems;
		public List<Tuple<Type, Type>> ServerChildSystems;
	}

	internal static LobbyState s_LobbyState;
	internal static GameState s_GameState;

	private static Attribute[] GetSystemAttributes(Type systemType, Type attributeType) {
		Attribute[] attributes;
		var objArr = systemType.GetCustomAttributes(attributeType, true);
		attributes = new Attribute[objArr.Length];
		for (int i = 0; i < objArr.Length; i++) {
			attributes[i] = objArr[i] as Attribute;
		}

		return attributes;
	}

	private static T GetSystemAttribute<T>(Type systemType)
		where T : Attribute {
		//var attribs = TypeManager.GetSystemAttributes(systemType, typeof(T));
		var attribs = GetSystemAttributes(systemType, typeof(T));
		if (attribs.Length != 1)
			return null;
		return attribs[0] as T;
	}


	protected internal static void Game_GenerateSystemLists_(IReadOnlyList<Type> systems, ref State s_State) {
		s_State.ClientInitializationSystems = new List<Type>();
		s_State.ClientSimulationSystems = new List<Type>();
		s_State.ClientPresentationSystems = new List<Type>();
		s_State.ClientChildSystems = new List<Tuple<Type, Type>>();
		s_State.ServerInitializationSystems = new List<Type>();
		s_State.ServerSimulationSystems = new List<Type>();
		s_State.ServerChildSystems = new List<Tuple<Type, Type>>();

		// 

		foreach (var type in systems) {
			var targetWorld = GetSystemAttribute<UpdateInGameWorld>(type);
			if ((targetWorld != null && targetWorld.World == UpdateInGameWorld.TargetWorld.Default) ||
#if !UNITY_DOTSRUNTIME
				type == typeof(ConvertToEntitySystem) ||
#endif
				type == typeof(InitializationSystemGroup) ||
				type == typeof(SimulationSystemGroup) ||
				type == typeof(PresentationSystemGroup)) {
				DefaultWorldSystems.Add(type);
				ExplicitDefaultWorldSystems.Add(type);
				continue;
			}

			if (targetWorld == null) {
				continue;
			}

			var groups = TypeManager.GetSystemAttributes(type, typeof(UpdateInGroupAttribute));
			
			if (groups.Length == 0) {
				groups = new Attribute[] {new UpdateInGroupAttribute(typeof(SimulationSystemGroup))};
			}

			foreach (var grp in groups) {
				var group = grp as UpdateInGroupAttribute;

				if (group.GroupType == typeof(ClientAndServerSimulationSystemGroup) ||
					group.GroupType == typeof(SimulationSystemGroup)) {
					if (group.GroupType == typeof(ClientAndServerSimulationSystemGroup) && targetWorld != null &&
						targetWorld.World == UpdateInGameWorld.TargetWorld.Default)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot use [UpdateInGameWorld(UpdateInGameWorld.TargetWorld.Default)] when using [UpdateInGroup(typeof(ClientAndServerSimulationSystemGroup)] {0}",
														type));
					if (group.GroupType == typeof(SimulationSystemGroup) &&
						(targetWorld.World == UpdateInGameWorld.TargetWorld.Default)) {
						DefaultWorldSystems.Add(type);
						if (targetWorld != null)
							ExplicitDefaultWorldSystems.Add(type);
					}

					if ((targetWorld.World & UpdateInGameWorld.TargetWorld.Server) != 0) {
						s_State.ServerSimulationSystems.Add(type);
					}

					if ((targetWorld.World & UpdateInGameWorld.TargetWorld.Client) != 0) {
						s_State.ClientSimulationSystems.Add(type);
					}
					
				} else if (group.GroupType == typeof(ClientAndServerInitializationSystemGroup) ||
							group.GroupType == typeof(InitializationSystemGroup)) {
					if (group.GroupType == typeof(ClientAndServerInitializationSystemGroup) && targetWorld != null &&
						targetWorld.World == UpdateInGameWorld.TargetWorld.Default)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot use [UpdateInGameWorld(UpdateInGameWorld.TargetWorld.Default)] when using [UpdateInGroup(typeof(ClientAndServerInitializationSystemGroup)] {0}",
														type));
					if (group.GroupType == typeof(InitializationSystemGroup) &&
						(targetWorld.World == UpdateInGameWorld.TargetWorld.Default)) {
						DefaultWorldSystems.Add(type);
						if (targetWorld != null)
							ExplicitDefaultWorldSystems.Add(type);
					}

					if ((targetWorld.World & UpdateInGameWorld.TargetWorld.Server) != 0) {
						s_State.ServerInitializationSystems.Add(type);
					}

					if ((targetWorld.World & UpdateInGameWorld.TargetWorld.Client) != 0) {
						s_State.ClientInitializationSystems.Add(type);
					}
				} else if (group.GroupType == typeof(ServerInitializationSystemGroup)) {
					if (targetWorld != null)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot use [UpdateInGameWorld] when using [UpdateInGroup(typeof(ServerInitializationSystemGroup)] {0}",
														type));
					s_State.ServerInitializationSystems.Add(type);
				} else if (group.GroupType == typeof(ClientInitializationSystemGroup)) {
					if (targetWorld != null)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot use [UpdateInGameWorld] when using [UpdateInGroup(typeof(ClientInitializationSystemGroup)] {0}",
														type));
					s_State.ClientInitializationSystems.Add(type);
				} else if (group.GroupType == typeof(ServerSimulationSystemGroup)) {
					if (targetWorld != null)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot use [UpdateInGameWorld] when using [UpdateInGroup(typeof(ServerSimulationSystemGroup)] {0}",
														type));
					s_State.ServerSimulationSystems.Add(type);
				} else if (group.GroupType == typeof(ClientSimulationSystemGroup)) {
					if (targetWorld != null)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot use [UpdateInGameWorld] when using [UpdateInGroup(typeof(ClientSimulationSystemGroup)] {0}",
														type));
					s_State.ClientSimulationSystems.Add(type);
				} else if (group.GroupType == typeof(ClientPresentationSystemGroup) ||
							group.GroupType == typeof(PresentationSystemGroup)) {
					if (group.GroupType == typeof(ClientPresentationSystemGroup) && targetWorld != null)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot use [UpdateInGameWorld] when using [UpdateInGroup(typeof(ClientPresentationSystemGroup)] {0}",
														type));
					if (targetWorld != null && (targetWorld.World & UpdateInGameWorld.TargetWorld.Server) != 0)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot use presentation systems on the server {0}",
														type));
					if (group.GroupType == typeof(PresentationSystemGroup) &&
						(targetWorld.World == UpdateInGameWorld.TargetWorld.Default)) {
						DefaultWorldSystems.Add(type);
						if (targetWorld != null)
							ExplicitDefaultWorldSystems.Add(type);
					}

					if ((targetWorld.World & UpdateInGameWorld.TargetWorld.Client) != 0) {
						s_State.ClientPresentationSystems.Add(type);
					}
				} else {
					var mask = Game_GetTopLevelWorldMask(group.GroupType);
					if (targetWorld != null && targetWorld.World == UpdateInGameWorld.TargetWorld.Default &&
						(mask & WorldType.DefaultWorld) == 0)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot update in default world when parent is not in the default world {0}",
														type));
					if ((targetWorld != null && (targetWorld.World & UpdateInGameWorld.TargetWorld.Client) != 0) &&
						(mask & WorldType.ClientWorld) == 0)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot update in client world when parent is not in the client world {0}",
														type));
					if ((targetWorld != null && (targetWorld.World & UpdateInGameWorld.TargetWorld.Server) != 0) &&
						(mask & WorldType.ServerWorld) == 0)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot update in server world when parent is not in the server world {0}",
														type));
					if ((mask & WorldType.DefaultWorld) != 0 &&
						(targetWorld.World == UpdateInGameWorld.TargetWorld.Default)) {
						DefaultWorldSystems.Add(type);
						if (targetWorld != null || (mask & WorldType.ExplicitWorld) != 0)
							ExplicitDefaultWorldSystems.Add(type);
					}

					if ((mask & WorldType.ClientWorld) != 0 &&
						((targetWorld.World & UpdateInGameWorld.TargetWorld.Client) != 0)) {
						s_State.ClientChildSystems.Add(new Tuple<Type, Type>(type, group.GroupType));
					}

					if ((mask & WorldType.ServerWorld) != 0 &&
						((targetWorld.World & UpdateInGameWorld.TargetWorld.Server) != 0)) {
						s_State.ServerChildSystems.Add(new Tuple<Type, Type>(type, group.GroupType));
					}
				}
			}
		}
	}

	protected internal static void Lobby_GenerateSystemLists_(IReadOnlyList<Type> systems, ref State s_State) {
		s_State.ClientInitializationSystems = new List<Type>();
		s_State.ClientSimulationSystems = new List<Type>();
		s_State.ClientPresentationSystems = new List<Type>();
		s_State.ClientChildSystems = new List<Tuple<Type, Type>>();
		s_State.ServerInitializationSystems = new List<Type>();
		s_State.ServerSimulationSystems = new List<Type>();
		s_State.ServerChildSystems = new List<Tuple<Type, Type>>();

		// 

		foreach (var type in systems) {
			var targetWorld = GetSystemAttribute<UpdateInLobbyWorld>(type);
			if ((targetWorld != null && targetWorld.World == UpdateInLobbyWorld.TargetWorld.Default) ||
#if !UNITY_DOTSRUNTIME
				type == typeof(ConvertToEntitySystem) ||
#endif
				type == typeof(InitializationSystemGroup) ||
				type == typeof(SimulationSystemGroup) ||
				type == typeof(PresentationSystemGroup)) {
				DefaultWorldSystems.Add(type);
				ExplicitDefaultWorldSystems.Add(type);
				continue;
			}

			if (targetWorld == null) {
				continue;
			}
			
			var groups = TypeManager.GetSystemAttributes(type, typeof(UpdateInGroupAttribute));
			
			if (groups.Length == 0) {
				groups = new Attribute[] {new UpdateInGroupAttribute(typeof(SimulationSystemGroup))};
			}

			foreach (var grp in groups) {
				var group = grp as UpdateInGroupAttribute;
				if (group.GroupType == typeof(ClientAndServerSimulationSystemGroup) ||
					group.GroupType == typeof(SimulationSystemGroup)) {
					if (group.GroupType == typeof(ClientAndServerSimulationSystemGroup) && targetWorld != null &&
						targetWorld.World == UpdateInLobbyWorld.TargetWorld.Default)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot use [UpdateInLobbyWorld(UpdateInLobbyWorld.TargetWorld.Default)] when using [UpdateInGroup(typeof(ClientAndServerSimulationSystemGroup)] {0}",
														type));
					if (group.GroupType == typeof(SimulationSystemGroup) &&
						(targetWorld.World == UpdateInLobbyWorld.TargetWorld.Default)) {
						DefaultWorldSystems.Add(type);
						if (targetWorld != null)
							ExplicitDefaultWorldSystems.Add(type);
					}

					if ((targetWorld.World & UpdateInLobbyWorld.TargetWorld.Server) != 0) {
						s_State.ServerSimulationSystems.Add(type);
					}

					if ((targetWorld.World & UpdateInLobbyWorld.TargetWorld.Client) != 0) {
						s_State.ClientSimulationSystems.Add(type);
					}
				} else if (group.GroupType == typeof(ClientAndServerInitializationSystemGroup) ||
							group.GroupType == typeof(InitializationSystemGroup)) {
					if (group.GroupType == typeof(ClientAndServerInitializationSystemGroup) && targetWorld != null &&
						targetWorld.World == UpdateInLobbyWorld.TargetWorld.Default)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot use [UpdateInLobbyWorld(UpdateInLobbyWorld.TargetWorld.Default)] when using [UpdateInGroup(typeof(ClientAndServerInitializationSystemGroup)] {0}",
														type));
					if (group.GroupType == typeof(InitializationSystemGroup) &&
						(targetWorld.World == UpdateInLobbyWorld.TargetWorld.Default)) {
						DefaultWorldSystems.Add(type);
						if (targetWorld != null)
							ExplicitDefaultWorldSystems.Add(type);
					}

					if ((targetWorld.World & UpdateInLobbyWorld.TargetWorld.Server) != 0) {
						s_State.ServerInitializationSystems.Add(type);
					}

					if ((targetWorld.World & UpdateInLobbyWorld.TargetWorld.Client) != 0) {
						s_State.ClientInitializationSystems.Add(type);
					}
				} else if (group.GroupType == typeof(ServerInitializationSystemGroup)) {
					if (targetWorld != null)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot use [UpdateInLobbyWorld] when using [UpdateInGroup(typeof(ServerInitializationSystemGroup)] {0}",
														type));
					s_State.ServerInitializationSystems.Add(type);
				} else if (group.GroupType == typeof(ClientInitializationSystemGroup)) {
					if (targetWorld != null)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot use [UpdateInLobbyWorld] when using [UpdateInGroup(typeof(ClientInitializationSystemGroup)] {0}",
														type));
					s_State.ClientInitializationSystems.Add(type);
				} else if (group.GroupType == typeof(ServerSimulationSystemGroup)) {
					if (targetWorld != null)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot use [UpdateInLobbyWorld] when using [UpdateInGroup(typeof(ServerSimulationSystemGroup)] {0}",
														type));
					s_State.ServerSimulationSystems.Add(type);
				} else if (group.GroupType == typeof(ClientSimulationSystemGroup)) {
					if (targetWorld != null)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot use [UpdateInLobbyWorld] when using [UpdateInGroup(typeof(ClientSimulationSystemGroup)] {0}",
														type));
					s_State.ClientSimulationSystems.Add(type);
				} else if (group.GroupType == typeof(ClientPresentationSystemGroup) ||
							group.GroupType == typeof(PresentationSystemGroup)) {
					if (group.GroupType == typeof(ClientPresentationSystemGroup) && targetWorld != null)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot use [UpdateInLobbyWorld] when using [UpdateInGroup(typeof(ClientPresentationSystemGroup)] {0}",
														type));
					if (targetWorld != null && (targetWorld.World & UpdateInLobbyWorld.TargetWorld.Server) != 0)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot use presentation systems on the server {0}",
														type));
					if (group.GroupType == typeof(PresentationSystemGroup) &&
						(targetWorld.World == UpdateInLobbyWorld.TargetWorld.Default)) {
						DefaultWorldSystems.Add(type);
						if (targetWorld != null)
							ExplicitDefaultWorldSystems.Add(type);
					}

					if ((targetWorld.World & UpdateInLobbyWorld.TargetWorld.Client) != 0) {
						s_State.ClientPresentationSystems.Add(type);
					}
				} else {
					var mask = Lobby_GetTopLevelWorldMask(group.GroupType);
					if (targetWorld != null && targetWorld.World == UpdateInLobbyWorld.TargetWorld.Default &&
						(mask & WorldType.DefaultWorld) == 0)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot update in default world when parent is not in the default world {0}",
														type));
					if ((targetWorld != null && (targetWorld.World & UpdateInLobbyWorld.TargetWorld.Client) != 0) &&
						(mask & WorldType.ClientWorld) == 0)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot update in client world when parent is not in the client world {0}",
														type));
					if ((targetWorld != null && (targetWorld.World & UpdateInLobbyWorld.TargetWorld.Server) != 0) &&
						(mask & WorldType.ServerWorld) == 0)
						UnityEngine.Debug.LogWarning(String.Format(
														"Cannot update in server world when parent is not in the server world {0}",
														type));
					if ((mask & WorldType.DefaultWorld) != 0 &&
						(targetWorld.World == UpdateInLobbyWorld.TargetWorld.Default)) {
						DefaultWorldSystems.Add(type);
						if (targetWorld != null || (mask & WorldType.ExplicitWorld) != 0)
							ExplicitDefaultWorldSystems.Add(type);
					}

					if ((mask & WorldType.ClientWorld) != 0 &&
						((targetWorld.World & UpdateInLobbyWorld.TargetWorld.Client) != 0)) {
						s_State.ClientChildSystems.Add(new Tuple<Type, Type>(type, group.GroupType));
					}

					if ((mask & WorldType.ServerWorld) != 0 &&
						((targetWorld.World & UpdateInLobbyWorld.TargetWorld.Server) != 0)) {
						s_State.ServerChildSystems.Add(new Tuple<Type, Type>(type, group.GroupType));
					}
				}
			}
		}
	}

	internal static IEnumerable<Type> GetTypesDerivedFrom(Type type) {
#if UNITY_EDITOR
		return UnityEditor.TypeCache.GetTypesDerivedFrom(type);
#else
            var types = new List<Type>();
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (!TypeManager.IsAssemblyReferencingEntities(assembly))
                    continue;

                try
                {
                    var assemblyTypes = assembly.GetTypes();
                    foreach (var t in assemblyTypes)
                    {
                        if (type.IsAssignableFrom(t))
                            types.Add(t);
                    }
                }
                catch (ReflectionTypeLoadException e)
                {
                    foreach (var t in e.Types)
                    {
                        if (t != null && type.IsAssignableFrom(t))
                            types.Add(t);
                    }

                    Debug.LogWarning($"DefaultWorldInitialization failed loading assembly: {(assembly.IsDynamic ? assembly.ToString() : assembly.Location)}");
                }
            }

            return types;
#endif
	}

	private static IReadOnlyList<Type> GetAllSystems(ref List<Type> systems) {
		var filteredSystemTypes = new List<Type>();
		foreach (var systemType in GetTypesDerivedFrom(typeof(ComponentSystemBase))) {
			{
				var targetWorld = GetSystemAttribute<UpdateInLobbyWorld>(systemType);
				if (targetWorld != null) {
					filteredSystemTypes.Add(systemType);
					systems.Remove(systemType);
					continue;
				}
			}
			{
				var targetWorld = GetSystemAttribute<UpdateInGameWorld>(systemType);
				if (targetWorld != null) {
					filteredSystemTypes.Add(systemType);
					systems.Remove(systemType);
					continue;
				}
			}
		}

		return filteredSystemTypes;
	}

	public override bool Initialize(string defaultWorldName) {
		var world = new World(defaultWorldName);
		World.DefaultGameObjectInjectionWorld = world;

		var systems = DefaultWorldInitialization.GetAllSystems(WorldSystemFilterFlags.Default);
		var systemsWithChangingMode = systems.ToList();
		var mySystems = GetAllSystems(ref systemsWithChangingMode);
		systems = systemsWithChangingMode.AsReadOnly();

		s_LobbyState = new LobbyState();
		s_GameState = new GameState();

		var lobbyState = s_LobbyState as State;
		var gameState = s_GameState as State;

		GenerateSystemLists(systems);
		Lobby_GenerateSystemLists_(mySystems, ref lobbyState);
		Game_GenerateSystemLists_(mySystems, ref gameState);

		DefaultWorldInitialization.AddSystemsToRootLevelSystemGroups(world, ExplicitDefaultWorldSystems);

#if !UNITY_DOTSRUNTIME
		ScriptBehaviourUpdateOrder.AddWorldToCurrentPlayerLoop(world);
#endif

#if UNITY_EDITOR
		//if (Resources.Load<HeadServerSettingsAsset>("HeadServerSettingsAsset") == null) {
		PlayType playModeType = RequestedPlayType;

		if (playModeType != PlayType.Server) {
			CreateLobbyClientWorld(world);
		}

		if (playModeType != PlayType.Client) {
			CreateLobbyServerWorld(world);
		}
		//}
#elif !UNITY_EDITOR && UNITY_SERVER && !UNITY_CLIENT
		CreateLobbyServerWorld(world);
#elif !UNITY_EDITOR && UNITY_CLIENT && !UNITY_SERVER
		CreateLobbyClientWorld(world);
#endif


		return true;
	}

	[Flags]
	protected enum WorldType
	{
		NoWorld = 0,
		DefaultWorld = 1,
		ClientWorld = 2,
		ServerWorld = 4,
		ExplicitWorld = 8
	}

	protected static WorldType Game_GetTopLevelWorldMask(Type type) {
		var targetWorld = GetSystemAttribute<UpdateInGameWorld>(type);
		if (targetWorld != null) {
			if (targetWorld.World == UpdateInGameWorld.TargetWorld.Default)
				return WorldType.DefaultWorld | WorldType.ExplicitWorld;
			if (targetWorld.World == UpdateInGameWorld.TargetWorld.Client)
				return WorldType.ClientWorld;
			if (targetWorld.World == UpdateInGameWorld.TargetWorld.Server)
				return WorldType.ServerWorld;
			return WorldType.ClientWorld | WorldType.ServerWorld;
		}

		var groups = TypeManager.GetSystemAttributes(type, typeof(UpdateInGroupAttribute));
		if (groups.Length == 0) {
			if (type == typeof(ClientAndServerSimulationSystemGroup) ||
				type == typeof(ClientAndServerInitializationSystemGroup))
				return WorldType.ClientWorld | WorldType.ServerWorld;
			if (type == typeof(SimulationSystemGroup) || type == typeof(InitializationSystemGroup))
				return WorldType.DefaultWorld | WorldType.ClientWorld | WorldType.ServerWorld;
			if (type == typeof(ServerSimulationSystemGroup) || type == typeof(ServerInitializationSystemGroup))
				return WorldType.ServerWorld;
			if (type == typeof(ClientSimulationSystemGroup) ||
				type == typeof(ClientInitializationSystemGroup) ||
				type == typeof(ClientPresentationSystemGroup))
				return WorldType.ClientWorld;
			if (type == typeof(PresentationSystemGroup))
				return WorldType.DefaultWorld | WorldType.ClientWorld;
			// Empty means the same thing as SimulationSystemGroup
			return WorldType.DefaultWorld | WorldType.ClientWorld | WorldType.ServerWorld;
		}

		WorldType mask = WorldType.NoWorld;
		foreach (var grp in groups) {
			var group = grp as UpdateInGroupAttribute;
			mask |= Game_GetTopLevelWorldMask(group.GroupType);
		}

		return mask;
	}

	protected static WorldType Lobby_GetTopLevelWorldMask(Type type) {
		var targetWorld = GetSystemAttribute<UpdateInLobbyWorld>(type);
		if (targetWorld != null) {
			if (targetWorld.World == UpdateInLobbyWorld.TargetWorld.Default)
				return WorldType.DefaultWorld | WorldType.ExplicitWorld;
			if (targetWorld.World == UpdateInLobbyWorld.TargetWorld.Client)
				return WorldType.ClientWorld;
			if (targetWorld.World == UpdateInLobbyWorld.TargetWorld.Server)
				return WorldType.ServerWorld;
			return WorldType.ClientWorld | WorldType.ServerWorld;
		}

		var groups = TypeManager.GetSystemAttributes(type, typeof(UpdateInGroupAttribute));
		if (groups.Length == 0) {
			if (type == typeof(ClientAndServerSimulationSystemGroup) ||
				type == typeof(ClientAndServerInitializationSystemGroup))
				return WorldType.ClientWorld | WorldType.ServerWorld;
			if (type == typeof(SimulationSystemGroup) || type == typeof(InitializationSystemGroup))
				return WorldType.DefaultWorld | WorldType.ClientWorld | WorldType.ServerWorld;
			if (type == typeof(ServerSimulationSystemGroup) || type == typeof(ServerInitializationSystemGroup))
				return WorldType.ServerWorld;
			if (type == typeof(ClientSimulationSystemGroup) ||
				type == typeof(ClientInitializationSystemGroup) ||
				type == typeof(ClientPresentationSystemGroup))
				return WorldType.ClientWorld;
			if (type == typeof(PresentationSystemGroup))
				return WorldType.DefaultWorld | WorldType.ClientWorld;
			// Empty means the same thing as SimulationSystemGroup
			return WorldType.DefaultWorld | WorldType.ClientWorld | WorldType.ServerWorld;
		}

		WorldType mask = WorldType.NoWorld;
		foreach (var grp in groups) {
			var group = grp as UpdateInGroupAttribute;
			mask |= Lobby_GetTopLevelWorldMask(group.GroupType);
		}

		return mask;
	}

}

}