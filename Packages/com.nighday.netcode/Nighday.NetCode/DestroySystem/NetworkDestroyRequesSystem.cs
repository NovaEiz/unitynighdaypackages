using Unity.Entities;
using Unity.NetCode;
using Unity.Transforms;
using UnityEngine;

namespace Nighday.NetCode {

// [UpdateInWorld(UpdateInWorld.TargetWorld.Server)]
// [UpdateInGroup(typeof(DestroyRequestSystemGroup))]
// public class NetworkDestroyRequestServerSystem : SystemBase {
//
//     protected override void OnUpdate() {
//
//     }
// }

[UpdateInWorld(UpdateInWorld.TargetWorld.Client)]
[UpdateInGroup(typeof(DestroyRequestSystemGroup))]
public class NetworkDestroyRequestClientSystem : SystemBase {

    protected override void OnUpdate() {
        //TODO: тут надо включать эту систему, после подключения клиента к серверу.
        Entities
            .WithAll<DestroyRequest>()
            .WithAll<PredictedGhostComponent>()
            .ForEach((Entity    entity,
                      Transform transform
            ) => {
                GameObject.Destroy(transform.gameObject);
            }).WithStructuralChanges().Run();
		
        // Entities
        //     .WithNone<PredictedGhostComponent>()
        //     .WithAll<DestroyRequest>()
        //     .ForEach((Entity entity
        //     ) => {
        //         Debug.Log("DestroyRequest entity = " + entity);
        //         EntityManager.DestroyEntity(entity);
        //     }).WithStructuralChanges().Run();
		
    }
}

}