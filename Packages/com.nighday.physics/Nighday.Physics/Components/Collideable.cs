﻿using Unity.Entities;

namespace Nighday.Physics {

[GenerateAuthoringComponent]
public struct Collideable : IComponentData {
    public Entity MainEntity;
}
}
