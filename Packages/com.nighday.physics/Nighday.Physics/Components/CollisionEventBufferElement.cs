﻿using Unity.Entities;
using UnityEngine;

namespace Nighday.Physics {

public struct CollisionEventBufferElement : IBufferElementData {
    public Entity WithEntity;
}
}
