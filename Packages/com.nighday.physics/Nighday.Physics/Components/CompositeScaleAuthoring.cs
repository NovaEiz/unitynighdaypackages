using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Nighday.Physics {

public class CompositeScaleAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

    public void Convert(
        Entity                     entity,
        EntityManager              dstManager,
        GameObjectConversionSystem conversionSystem
    ) {
        dstManager.AddComponentData(entity, new CompositeScale {Value = transform.worldToLocalMatrix});
    }

}

}