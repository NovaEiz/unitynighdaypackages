﻿using Unity.Entities;
using Unity.NetCode;

namespace Nighday.Physics {

[GenerateAuthoringComponent]
public struct GhostScale : IComponentData {
	[GhostField(Quantization=100)]
	public float Value;
}

}