﻿using Unity.Entities;
using UnityEngine;

namespace Nighday.Physics {

public struct ProjectileAttackedTargetsForLifeBufferElement : IBufferElementData {
    public Entity TargetEntity;
}

}
