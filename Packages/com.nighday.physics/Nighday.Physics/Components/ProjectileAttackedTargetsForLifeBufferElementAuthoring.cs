﻿using Unity.Entities;
using UnityEngine;

namespace Nighday.Physics {

public class ProjectileAttackedTargetsForLifeBufferElementAuthoring : MonoBehaviour, IConvertGameObjectToEntity {
    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
        dstManager.AddBuffer<ProjectileAttackedTargetsForLifeBufferElement>(entity);
    }
}
}
