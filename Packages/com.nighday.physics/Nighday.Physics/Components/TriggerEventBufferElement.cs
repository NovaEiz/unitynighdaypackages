﻿using Unity.Entities;
using UnityEngine;

namespace Nighday.Physics {

public struct TriggerEventBufferElement : IBufferElementData {
    public Entity WithEntity;
}
}
