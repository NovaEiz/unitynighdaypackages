﻿using Unity.Entities;
using UnityEngine;

namespace Nighday.Physics {

[GenerateAuthoringComponent]
public struct Triggerable : IComponentData {
    public Entity MainEntity;
}
}
