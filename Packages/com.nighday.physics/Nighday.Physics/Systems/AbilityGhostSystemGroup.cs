using Unity.Entities;
using Unity.Physics.Systems;
using Unity.Transforms;

namespace Nighday.Physics {

//[UpdateInGroup(typeof(GhostPredictionSystemGroup))]
//[UpdateAfter(typeof(ExportPhysicsWorld))]

[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
[UpdateAfter(typeof(BuildPhysicsWorld))]
public class AbilityGhostSystemGroup : ComponentSystemGroup {
	
}

}