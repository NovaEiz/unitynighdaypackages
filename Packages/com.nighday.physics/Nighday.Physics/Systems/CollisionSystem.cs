﻿using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.NetCode;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;

namespace Nighday.Physics {

[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
[UpdateAfter(typeof(AbilityGhostSystemGroup))]
[UpdateAfter(typeof(BuildPhysicsWorld))]
public class CollisionSystem : JobComponentSystem
{
	private BuildPhysicsWorld buildPhysicsWorldSystem;
	private StepPhysicsWorld stepPhysicsWorldSystem;
	private EndFramePhysicsSystem endFramePhysicsSystem;

	protected override void OnCreate()
	{
		base.OnCreate();

		buildPhysicsWorldSystem = World.GetOrCreateSystem<BuildPhysicsWorld>();
		stepPhysicsWorldSystem = World.GetOrCreateSystem<StepPhysicsWorld>();
		endFramePhysicsSystem = World.GetOrCreateSystem<EndFramePhysicsSystem>();
	}

	protected override JobHandle OnUpdate(JobHandle inputDeps) {
		Entities
			.ForEach((Entity entity,
					DynamicBuffer<CollisionEventBufferElement> collisionEventBuffer
					) => {
						collisionEventBuffer.Clear();
					}).Run();
		
		var Dependency = new CollideableJob
		{
			CollideableGroup = GetComponentDataFromEntity<Collideable>(),
			CollisionEventBufferGroup = GetBufferFromEntity<CollisionEventBufferElement>()
		}.Schedule(stepPhysicsWorldSystem.Simulation, ref buildPhysicsWorldSystem.PhysicsWorld, inputDeps);
		//endFramePhysicsSystem.AddInputDependency(Dependency);
		Dependency.Complete();
		return Dependency;
	}

	
	[BurstCompile]
	private struct CollideableJob : ICollisionEventsJob {
		public ComponentDataFromEntity<Collideable> CollideableGroup;
		public BufferFromEntity<CollisionEventBufferElement> CollisionEventBufferGroup;

		public void UpdateCollideable(Entity entity, Entity collider, CollisionEvent collisionEvent) {
			if (CollideableGroup.HasComponent(entity)) {
				var collideable = CollideableGroup[entity];
				if (collideable.MainEntity.Equals(Entity.Null)) {
					CollisionEventBufferGroup[entity].Add(new CollisionEventBufferElement{
						WithEntity = collider							
					});
				} else {
					CollisionEventBufferGroup[collideable.MainEntity].Add(new CollisionEventBufferElement{
						WithEntity = collider							
					});
				}
			}
		}

		public void Execute(CollisionEvent collisionEvent) {
			var entityA = collisionEvent.EntityA;
			var entityB = collisionEvent.EntityB;
                
			UpdateCollideable(entityA, entityB, collisionEvent);
			UpdateCollideable(entityB, entityA, collisionEvent);
		}
	}
}


}