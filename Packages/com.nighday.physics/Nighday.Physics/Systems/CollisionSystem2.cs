﻿/*
using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;

namespace Nighday.Physics {

    [UpdateAfter(typeof(EndFramePhysicsSystem))]
    [DisableAutoCreation]
    public class CollisionSystem2 : JobComponentSystem {
        private BuildPhysicsWorld buildPhysicsWorldSystem;
        private StepPhysicsWorld stepPhysicsWorldSystem;

        protected override void OnCreate() {
            base.OnCreate();
            return;

            buildPhysicsWorldSystem = World.GetOrCreateSystem<BuildPhysicsWorld>();
            stepPhysicsWorldSystem = World.GetOrCreateSystem<StepPhysicsWorld>();
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            return inputDeps;
            var inputDeps2 = new CollideableJob {
                CollideableGroup = GetComponentDataFromEntity<Collideable>()
            }.Schedule(stepPhysicsWorldSystem.Simulation, ref buildPhysicsWorldSystem.PhysicsWorld, inputDeps);
            return inputDeps2;
        }

        [BurstCompile]
        private struct CollideableJob : ICollisionEventsJob {
            public ComponentDataFromEntity<Collideable> CollideableGroup;

            public void UpdateCollideable(Entity entity, Entity collider, CollisionEvent collisionEvent) {
                if (CollideableGroup.HasComponent(entity)) {
                    var collideable = CollideableGroup[entity];
                    collideable.CollisionEntity = collider;
                    CollideableGroup[entity] = collideable;
                }
            }

            public void Execute(CollisionEvent collisionEvent) {
                var entityA = collisionEvent.Entities.EntityA;
                var entityB = collisionEvent.Entities.EntityB;
                
                UpdateCollideable(entityA, entityB, collisionEvent);
                UpdateCollideable(entityB, entityA, collisionEvent);
            }
        }
    }
}
*/