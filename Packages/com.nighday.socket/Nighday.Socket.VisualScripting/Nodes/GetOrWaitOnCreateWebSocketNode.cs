using System;
using Nighday.VisualScripting;
using Unity.VisualScripting;

[assembly: UnityEngine.Scripting.AlwaysLinkAssembly]
namespace Nighday.Socket {

[UnityEngine.Scripting.Preserve]
[UnitTitle("WebSocket.GetOrWaitOnCreateWebSocket")]
[UnitCategory("_Nighday/WebSocket")]
public class GetOrWaitOnCreateWebSocketNode : CustomEventUnit<EmptyEventArgs> {

    [DoNotSerialize]
    private object _socket;

    [DoNotSerialize]
    public ValueInput socketKeyValueInput;

    [DoNotSerialize]
    public ValueOutput socketValueOutput;//socket its - WebSocketClient or WebSocketServer

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
#region EventUnit
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow flow, EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;
    public override EventHook GetHook(GraphReference reference) {
        _hook = new EventHook("GetOrWaitWebSocket_" + System.Guid.NewGuid().ToString(), reference.machine);
        return _hook;
    }
#endregion

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);

        socketKeyValueInput = ValueInput<string>("socketKey", null);
        socketValueOutput = ValueOutput<object>("socket",
            (flow) => {
                return _socket;
            });
        
        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);
        
        base.Definition();
    }
    
    private ControlOutput Trigger(Flow flow) {
        var socketKey = flow.GetValue<string>(socketKeyValueInput);

        WebSocketProtocolSubscribes.GetOrWaitOnCreateWebSocket(socketKey, (socketKey_, socket_) => {
            try {
                _socket = socket_;
                EventBus.Trigger(_hook);
            } catch (Exception e) {
                this.SetException(flow.stack, e);
                UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
            }
        });
        
        return exit;
    }

}

}