using System;
using System.Collections;
using System.Collections.Generic;
using Nighday.Json;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Socket {

[UnityEngine.Scripting.Preserve]
[UnitTitle("new WebSocketClient")]
[UnitCategory("_Nighday/WebSocket")]
public class NewWebSocketClientNode : Unit {

    [DoNotSerialize]
    private WebSocketClient _client;

    [DoNotSerialize]
    private WebSocketConnection_ _connection;
    
    [DoNotSerialize] // Mandatory attribute, to make sure we don’t serialize data that should never be serialized.
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    public ValueInput hostValueInput;

    [DoNotSerialize]
    public ValueInput portValueInput;

    [DoNotSerialize]
    public ValueInput socketKeyValueInput;

    [DoNotSerialize]
    public ValueOutput clientValueOutput;

    [DoNotSerialize]
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlOutput exit { get; private set; }
    
    [Serialize]
    [Inspectable, UnitHeaderInspectable("Auto connect")]
    [InspectorToggleLeft]
    public bool autoConnect { get; set; }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);

        hostValueInput = ValueInput<string>("host", "");
        portValueInput = ValueInput<ushort>("port", 0);
        socketKeyValueInput = ValueInput<string>("socketKey", null);
        clientValueOutput = ValueOutput<WebSocketClient>("client", (flow) => { return _client; });
        
        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var host = flow.GetValue<string>(hostValueInput);
        var port = flow.GetValue<ushort>(portValueInput);
        var socketKey = flow.GetValue<string>(socketKeyValueInput);

        var url = "ws://" + host + ":" + port;

        _client = new WebSocketClient(socketKey, url);
        if (autoConnect) {
            _client.Connect();
        }
        
        return exit;
    }

    public override void Dispose() {
        base.Dispose();
        return;

        if (_client != null) {
            _client.Dispose();
        }
    }

    public override void Uninstantiate(GraphReference instance) {
        base.Uninstantiate(instance);
        return;
        if (_client != null) {
            _client.Dispose();
        }
    }

}

}