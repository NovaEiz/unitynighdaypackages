using System;
using System.Collections;
using System.Collections.Generic;
using Nighday.Json;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Socket {

[UnityEngine.Scripting.Preserve]
[UnitTitle("new WebSocketServer")]
[UnitCategory("_Nighday/WebSocket")]
public class NewWebSocketServerNode : Unit {

    [DoNotSerialize]
    private WebSocketServer _server;

    [DoNotSerialize]
    private WebSocketConnection_ _connection;
    
    [DoNotSerialize] // Mandatory attribute, to make sure we don’t serialize data that should never be serialized.
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    public ValueInput portValueInput;

    [DoNotSerialize]
    public ValueInput socketKeyValueInput;

    [DoNotSerialize]
    public ValueOutput serverValueOutput;

    [DoNotSerialize]
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlOutput exit { get; private set; }
    
    [Serialize]
    [Inspectable, UnitHeaderInspectable("Auto start")]
    [InspectorToggleLeft]
    public bool autoStart { get; set; }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);

        portValueInput = ValueInput<ushort>("port", 0);
        socketKeyValueInput = ValueInput<string>("socketKey", null);
        serverValueOutput = ValueOutput<WebSocketServer>("server", (flow) => { return _server; });
        
        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var port = flow.GetValue<ushort>(portValueInput);
        var socketKey = flow.GetValue<string>(socketKeyValueInput);
        
        Debug.Log("port = " + port);

        _server = new WebSocketServer(socketKey, port);
        if (autoStart) {
            _server.Start();
        }
        
        return exit;
    }

    public override void Dispose() {
        base.Dispose();
        return;

        if (_server != null) {
            _server.Dispose();
        }
    }

    public override void Uninstantiate(GraphReference instance) {
        base.Uninstantiate(instance);
        return;

        if (_server != null) {
            _server.Dispose();
        }
    }

}

}