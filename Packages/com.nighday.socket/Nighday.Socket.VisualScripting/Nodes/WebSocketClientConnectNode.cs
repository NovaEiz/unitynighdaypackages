using System;
using System.Collections;
using System.Collections.Generic;
using Nighday.Json;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Socket {

[UnityEngine.Scripting.Preserve]
[UnitTitle("WebSocketClient.Connect")]
[UnitCategory("_Nighday/WebSocket")]
public class WebSocketClientConnectNode : Unit {

    [DoNotSerialize]
    public ValueInput clientValueInput;

    [DoNotSerialize] // Mandatory attribute, to make sure we don’t serialize data that should never be serialized.
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);

        clientValueInput = ValueInput<WebSocketClient>("client", null);
        
        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var client = flow.GetValue<WebSocketClient>(clientValueInput);
        
        client.Connect();
        
        return exit;
    }

}

}