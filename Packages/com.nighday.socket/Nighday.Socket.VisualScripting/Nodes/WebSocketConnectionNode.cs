using System;
using System.Collections;
using System.Collections.Generic;
using Nighday.Json;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Socket {

[UnityEngine.Scripting.Preserve]
[UnitTitle("WebSocket.WebSocketConnection")]
[UnitCategory("_Nighday/WebSocket")]
public class WebSocketConnectionNode : Unit {

    [DoNotSerialize]
    private WebSocketConnection_ _connection;

    [DoNotSerialize] // Mandatory attribute, to make sure we don’t serialize data that should never be serialized.
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    public ValueInput connectionValueInput;

    
    [DoNotSerialize]
    public ValueInput customDataValueInput;

    [DoNotSerialize]
    public ValueOutput connectionValueOutput;

    [DoNotSerialize]
    public ValueOutput customDataValueOutput;

    [DoNotSerialize]
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);

        connectionValueInput = ValueInput<WebSocketConnection_>("connection");
        customDataValueInput = ValueInput<object>("_customData", null);
        connectionValueOutput = ValueOutput<WebSocketConnection_>("_connection", (flow) => { return _connection; });
        customDataValueOutput = ValueOutput<object>("customData", (flow) => { 
            if (_connection == null) {
                return null;
            }

            return _connection.customData;
        });
        
        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var connection = flow.GetValue<WebSocketConnection_>(connectionValueInput);

        if (customDataValueInput.hasValidConnection) {
            var customData = flow.GetValue<object>(customDataValueInput);
            if (customData != null) {
                connection.customData = customData;
            }
        }

        _connection = connection;
        
        return exit;
    }

}

}