using System;
using System.Collections;
using System.Threading.Tasks;
using Nighday.Json;
using Nighday.Socket;
using Nighday.VisualScripting;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Socket {

[UnityEngine.Scripting.Preserve]
[UnitTitle("WebSocketConnection.Send")]
[UnitCategory("_Nighday/WebSocket")]
public class WebSocketConnectionSendNode : CustomEventUnit<EmptyEventArgs> {

    [DoNotSerialize]
    private JSONNode _data;

    [DoNotSerialize]
    private WebSocketConnection_ _connection;

    [DoNotSerialize]
    public ValueInput eventValueInput;

    [DoNotSerialize]
    public ValueInput dataValueInput;
    
    [DoNotSerialize]
    public ValueInput connectionValueInput;

    [DoNotSerialize]
    public ValueOutput dataValueOutput;

    [DoNotSerialize]
    public ValueInput eventKeyResultValueInput;

    [DoNotSerialize]
    public ValueOutput dataResultValueOutput;

    [DoNotSerialize]
    public ValueOutput connectionValueOutput;
    
    [DoNotSerialize]
    private JSONNode _resultJsonNode;

    [DoNotSerialize] // Mandatory attribute, to make sure we don’t serialize data that should never be serialized.
    [PortLabel("Enter")]
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabel("Exit")]
    public ControlOutput exit { get; private set; }
    
    [Serialize]
    [Inspectable, UnitHeaderInspectable("With result")]
    [InspectorToggleLeft]
    public bool withResult { get; set; }

    [Serialize]
    [Inspectable, UnitHeaderInspectable("Unique EventKeyResult")]
    [InspectorToggleLeft]
    public bool uniqueEventKeyResult { get; set; }

    protected override bool register => true;

    protected override bool ShouldTrigger(Flow flow, EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;
    public override EventHook GetHook(GraphReference reference) {
        _hook = new EventHook("WebSocketConnectionSendCompleted_" + Guid.NewGuid().ToString(), reference.machine);
        return _hook;
    }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);
        
        eventValueInput = ValueInput<string>("event", null);
        dataValueInput = ValueInput<JSONNode>("data", null);
        connectionValueInput = ValueInput<WebSocketConnection_>("connection", null);
        
        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);
        
        if (withResult) {
            if (uniqueEventKeyResult) {
                eventKeyResultValueInput = ValueInput<string>("eventKeyResult", "");
            }

            dataResultValueOutput = ValueOutput<JSONNode>("resultJsonNode", (flow) => { return _resultJsonNode; });
            connectionValueOutput = ValueOutput<WebSocketConnection_>("connection_", (flow) => { return _connection; });
            
            base.Definition();
        }

    }

    private ControlOutput Trigger(Flow flow) {
        try {
            var event_ = flow.GetValue<string>(eventValueInput);

            JSONNode data = null;
            if (dataValueInput.hasValidConnection) {
                data = flow.GetValue<JSONNode>(dataValueInput);
            }
            var connection = flow.GetValue<WebSocketConnection_>(connectionValueInput);

            if (withResult) {
                var eventKeyResult = event_ + ".result";
                if (uniqueEventKeyResult) {
                    eventKeyResult = flow.GetValue<string>(eventKeyResultValueInput);
                }
            
                connection.SendWithResult(event_, data,
                    (resultData_) => {
                        Debug.LogFormat("2 Connection receive data: event={0}, data={1}", event_, data);
                        try {
                            _resultJsonNode = resultData_;
                            _connection = connection;
                            UnityEngine.Debug.LogFormat("check _resultJsonNode : = {0}", _resultJsonNode);
                            EventBus.Trigger(_hook);
                        } catch (Exception e) {
                            this.SetException(flow.stack, e);
                            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
                        }
                    }, eventKeyResult);
            } else {
                connection.Send(event_, data);
            }
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }
        
        return exit;
    }

    public override void Dispose() {
        base.Dispose();

    }

    
    public override void Uninstantiate(GraphReference instance) {
        base.Uninstantiate(instance);

    }

    protected override void BeforeUndefine() {
        base.BeforeUndefine();

    }
    
}

}