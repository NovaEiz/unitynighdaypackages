using System;
using System.Collections;
using System.Collections.Generic;
using Nighday.Json;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Socket {

[UnityEngine.Scripting.Preserve]
[UnitTitle("WebSocket.Dispose")]
[UnitCategory("_Nighday/WebSocket")]
public class WebSocketDisposeNode : Unit {

    [DoNotSerialize]
    public ValueInput socketValueInput;

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);

        socketValueInput = ValueInput<object>("socket", 0);
        
        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var socket = flow.GetValue<object>(socketValueInput);

        
        if (socket is WebSocketServer server) {
            server.Dispose();
        } else if (socket is WebSocketClient client) {
            client.Dispose();
        }
        
        return exit;
    }

}

}