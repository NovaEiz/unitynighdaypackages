using System;
using Nighday.VisualScripting;
using Unity.VisualScripting;

namespace Nighday.Socket {

[UnityEngine.Scripting.Preserve]
[UnitTitle("WebSocket.OnConnection")]
[UnitCategory("_Nighday/WebSocket")]
public class WebSocketOnConnectionNode : CustomEventUnit<EmptyEventArgs> {

    [DoNotSerialize]
    private WebSocketConnection_ _connection;

    [DoNotSerialize]
    public ValueInput socketValueInput; //socket its - WebSocketClient or WebSocketServer

    [DoNotSerialize]
    public ValueOutput connectionValueOutput;

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

#region EventUnit

    protected override bool register => true;

    protected override bool ShouldTrigger(Flow           flow,
                                          EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;

    public override EventHook GetHook(GraphReference reference) {
        _hook = new EventHook("WebSocketOnConnection_" + System.Guid.NewGuid()
                                  .ToString(),
            reference.machine);

        return _hook;
    }

#endregion

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);

        socketValueInput = ValueInput<object>("socket", null);
        connectionValueOutput = ValueOutput<WebSocketConnection_>("connection",
            (flow) => { return _connection; });

        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);

        base.Definition();
    }

    private ControlOutput Trigger(Flow flow) {
        var socket = flow.GetValue<object>(socketValueInput);
        
        Action<WebSocketConnection_> action = null;
        action = (connection_) => {
            try {
                _connection = connection_;
                EventBus.Trigger(_hook);
            } catch (Exception e) {
                this.SetException(flow.stack, e);
                UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
            }
        };

        var client = socket as WebSocketClient;
        if (client != null) {
            foreach (var connection in client.ProtocolSubscribes.connections) {
                action(connection);
            }
            client.ProtocolSubscribes.OnConnection(action);
        } else {
            var server = socket as WebSocketServer;
            if (server != null) {
                foreach (var connection in server.ProtocolSubscribes.connections) {
                    action(connection);
                }
                server.ProtocolSubscribes.OnConnection(action);
            }
        }

        return exit;
    }

}

}