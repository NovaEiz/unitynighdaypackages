using System;
using Nighday.Json;
using Nighday.VisualScripting;
using Unity.VisualScripting;

namespace Nighday.Socket {

[UnityEngine.Scripting.Preserve]
[UnitTitle("WebSocket.OnData")]
[UnitCategory("_Nighday/WebSocket")]
public class WebSocketOnDataNode : CustomEventUnit<EmptyEventArgs> {

    [DoNotSerialize]
    private WebSocketConnection_ _connection;

    [DoNotSerialize]
    private string _event;

    [DoNotSerialize]
    private JSONNode _data;

    [DoNotSerialize]
    public ValueInput eventValueInput;
    
    [DoNotSerialize]
    public ValueInput socketValueInput;//socket its - WebSocketClient or WebSocketServer
    
    [DoNotSerialize]
    public ValueOutput connectionValueOutput;
    
    [DoNotSerialize]
    public ValueOutput eventValueOutput;
    
    [DoNotSerialize]
    public ValueOutput dataValueOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
#region EventUnit
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow flow, EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;
    public override EventHook GetHook(GraphReference reference) {
        _hook = new EventHook("WebSocketOnData_" + System.Guid.NewGuid().ToString(), reference.machine);
        return _hook;
    }
#endregion

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);

        eventValueInput = ValueInput<string>("event", null);
        socketValueInput = ValueInput<object>("socket", null);

        connectionValueOutput = ValueOutput<WebSocketConnection_>("connection",
            (flow) => {
                return _connection;
            });
        eventValueOutput = ValueOutput<string>("event",
            (flow) => {
                return _event;
            });
        dataValueOutput = ValueOutput<JSONNode>("data",
            (flow) => {
                return _data;
            });

        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);
        
        base.Definition();
    }
    
    private ControlOutput Trigger(Flow flow) {
        var _event = flow.GetValue<string>(eventValueInput);
        var socket = flow.GetValue<object>(socketValueInput);

        Action<WebSocketConnection_, string, JSONNode> action = null;
        action = (connection_,
                  event_,
                  data_) => {
            try {
                _connection = connection_;
                _event = event_;
                _data = data_;
                EventBus.Trigger(_hook);
            } catch (Exception e) {
                this.SetException(flow.stack, e);
                UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
            }
        };

        var client = socket as WebSocketClient;
        if (client != null) {
            client.ProtocolSubscribes.OnData(_event, action);
        } else {
            var server = socket as WebSocketServer;
            if (server != null) {
                server.ProtocolSubscribes.OnData(_event, action);
            }  
        }
        
        return exit;
    }

}

}