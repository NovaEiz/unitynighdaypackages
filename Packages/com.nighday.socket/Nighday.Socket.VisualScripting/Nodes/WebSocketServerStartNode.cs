using System;
using System.Collections;
using System.Collections.Generic;
using Nighday.Json;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Socket {

[UnityEngine.Scripting.Preserve]
[UnitTitle("WebSocketServer.Start")]
[UnitCategory("_Nighday/WebSocket")]
public class WebSocketServerStartNode : Unit {

    [DoNotSerialize]
    public ValueInput serverValueInput;

    [DoNotSerialize] // Mandatory attribute, to make sure we don’t serialize data that should never be serialized.
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden] // Hiding the port label as we normally hide the label for default Input and Output triggers.
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        enter = ControlInput(nameof(enter), Trigger);

        serverValueInput = ValueInput<WebSocketServer>("server", null);
        
        exit = ControlOutput(nameof(exit));
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var server = flow.GetValue<WebSocketServer>(serverValueInput);
        
        server.Start();
        
        return exit;
    }

}

}