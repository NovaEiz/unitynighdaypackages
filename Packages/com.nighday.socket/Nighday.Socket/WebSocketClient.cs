using System;
using System.Collections.Generic;
using Nighday.Json;
using UnityEngine;

namespace Nighday.Socket {

public class WebSocketClient {
    
    public WebSocketProtocolSubscribes _protocolSubscribes = new WebSocketProtocolSubscribes();

    public WebSocketProtocolSubscribes ProtocolSubscribes => _protocolSubscribes;
    
    //===
    
    public string socketKey { get; private set; }

    private WebSocketSharp.WebSocket _ws;

    private WebSocketConnection_ _connection;

    public WebSocketClient(string socketKey, string url) {
        this.socketKey = socketKey;
        _ws = new WebSocketSharp.WebSocket(url);

        _ws.OnOpen += (o,
                       e) => {
            Debug.Log("MyCheck_: _ws.OnOpen");
            WebSocketMainThread.OnUpdateOneTime += () => {
                _connection = new WebSocketConnection_(this, _ws);
                ProtocolSubscribes.RunOnConnection(_connection);
            };
        };
        _ws.OnClose += (o,
                        e) => {
            Debug.Log("MyCheck_: _ws.OnClose");
            WebSocketMainThread.OnUpdateOneTime += () => {
                ProtocolSubscribes.RunOnDisconnect(_connection);
                Debug.Log("MyCheck_: _ws.OnClose 2");

            };
        };
        _ws.OnError += (o,
                        e) => {
            Debug.LogFormat("MyCheck_: _ws.OnError e = {0}", e);
            
        };

        _ws.OnMessage += (o,
                       e) => {
            Debug.Log("MyCheck_: _ws.OnMessage");

            WebSocketMainThread.OnUpdateOneTime += () => {
                var json = e.Data;
                var jsonNode = JSON.Parse(json);

                var event_ = jsonNode["e"]
                    .Value;
                var data = jsonNode["d"];
                ProtocolSubscribes.RunOnData(_connection, event_, data);
                _connection.RunOnData(event_, data);
            };
        };

        WebSocketProtocolSubscribes.RunOnCreateWebSocket(socketKey, this);
    }

    public void Dispose() {
        if (_ws != null) {
            _ws.Close();
        }
    }

    public void Connect() {
        _ws.Connect();
        
        Debug.Log("MyCheck_: Connecte");

    }
    
    //===
    
}

}