using System;
using System.Collections.Generic;
using Nighday.Json;
using UnityEngine;

namespace Nighday.Socket {

public class WebSocketConnection_ {

    public object customData;

    private WebSocketServer          _server;

    private WebSocketClient _client;
    
    private WebSocketSharp.WebSocket _webSocket;

    public WebSocketConnection_(WebSocketServer server, WebSocketSharp.WebSocket webSocket) {
        _server = server;
        _webSocket = webSocket;
    }

    public WebSocketConnection_(WebSocketClient client, WebSocketSharp.WebSocket webSocket) {
        _client = client;
        _webSocket = webSocket;
    }
    
    public void Send(string   event_,
                     JSONNode data) {
        var jsonNode = new JSONObject();
        jsonNode.Add("e", event_);
        jsonNode.Add("d", data);

        var json = jsonNode.ToString();

        _webSocket.Send(json);
    }
    
    public void SendWithResult(string   event_,
                               JSONNode data,
                               Action<JSONNode> resultCallback,
                               string eventKeyResult = null) {
        if (string.IsNullOrEmpty(eventKeyResult)) {
            eventKeyResult = event_;
        }
        
        var jsonNode = new JSONObject();
        jsonNode.Add("e", event_);
        jsonNode.Add("d", data);

        var json = jsonNode.ToString();

        Action<JSONNode> resultAction = null;
        resultAction = (resultData_) => {
            UnsubscribeOnData(eventKeyResult, resultAction);
            
            resultCallback(resultData_);
        };

        SubscribeOnData(eventKeyResult, resultAction);

        _webSocket.Send(json);
    }
    
    private Dictionary<string, Action<JSONNode>> _onDataSubscribes = new Dictionary<string, Action<JSONNode>>();
    
    private void SubscribeOnData(string event_, Action<JSONNode> callback) {
        if (!_onDataSubscribes.TryGetValue(event_, out var callback_)) {
            _onDataSubscribes.Add(event_, (_) => {});
        }

        _onDataSubscribes[event_] += callback;
    }
    
    private void UnsubscribeOnData(string event_, Action<JSONNode> callback) {
        if (!_onDataSubscribes.TryGetValue(event_, out var callback_)) {
            return;
        }

        _onDataSubscribes[event_] -= callback;
    }
    
    public void RunOnData(string event_, JSONNode data) {
        Debug.LogFormat("Connection receive data: event={0}, data={1}", event_, data);
        if (!_onDataSubscribes.TryGetValue(event_, out var callback_)) {
            return;
        }
        Debug.LogFormat("1 Connection receive data: event={0}, data={1}", event_, data);

        callback_(data);
    }

    
}

}