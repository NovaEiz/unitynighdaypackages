using System;
using UnityEngine;
using UnityEngine.Networking;
using WebSocketSharp;

namespace Nighday.Socket {

public class WebSocketMainThread : MonoBehaviour {

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static void Init() {
        OnUpdateOneTime = null;
        _instance = null;
    }

    private static WebSocketMainThread _instance;

    public static WebSocketMainThread Instance {
        get {
            if (_instance == null) {
                _instance = new GameObject("WebSocketConnection WebSocketMainThread").AddComponent<WebSocketMainThread>();
                DontDestroyOnLoad(_instance.gameObject);
            }
            return _instance;
        }
    }

    public static event Action OnUpdateOneTime;

    public void Update() {
        if (OnUpdateOneTime != null) {
            var action = OnUpdateOneTime;
            OnUpdateOneTime = null;
            action?.Invoke();
        }
    }

}

}