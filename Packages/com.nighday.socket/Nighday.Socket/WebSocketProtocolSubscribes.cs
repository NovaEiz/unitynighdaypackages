using System;
using System.Collections.Generic;
using Nighday.Json;
using UnityEngine;

namespace Nighday.Socket {

public class WebSocketProtocolSubscribes {
    
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
    public static void StaticInitialize() {
        _onCreateSocket = new Dictionary<string, Action<string, object>>();
        _sockets = new Dictionary<string, object>();
    }

    private static Dictionary<string, Action<string, object>> _onCreateSocket;
    private static Dictionary<string, object> _sockets;

    public static void AddSocket(string socketKey,
                                 object socket) {
    }
    
    public static void GetOrWaitOnCreateWebSocket(string socketKey, Action<string, object> callback) {
        if (!_onCreateSocket.TryGetValue(socketKey, out var callback_)) {
            _onCreateSocket.Add(socketKey, (_, __) => {});
        }

        _onCreateSocket[socketKey] += callback;

        if (_sockets.TryGetValue(socketKey, out var socket_)) {
            WebSocketMainThread.OnUpdateOneTime += () => {
                callback(socketKey, socket_);
            };
        }
    }
    public static void RunOnCreateWebSocket(string socketKey, object socket) {
        _sockets.Add(socketKey, socket);

        if (!_onCreateSocket.TryGetValue(socketKey, out var callback_)) {
            return;
        }

        WebSocketMainThread.OnUpdateOneTime += () => {
            callback_(socketKey, socket);
        };
    }
    
    //===
    
    private Action<WebSocketConnection_> _onConnection;

    private List<WebSocketConnection_> _connections = new List<WebSocketConnection_>();
    
    public List<WebSocketConnection_> connections => _connections;

    public void OnConnection(Action<WebSocketConnection_> callback) {
        _onConnection += callback;
    }

    public void RunOnConnection(WebSocketConnection_ connection) {
        WebSocketMainThread.OnUpdateOneTime += () => {
            _connections.Add(connection);
            _onConnection?.Invoke(connection);
        };
    }
    
    private Action<WebSocketConnection_> _onDisconnect;
    public void OnDisconnect(Action<WebSocketConnection_> callback) {
        _onDisconnect += callback;
    }

    public void RunOnDisconnect(WebSocketConnection_ connection) {
        WebSocketMainThread.OnUpdateOneTime += () => {
            _onDisconnect?.Invoke(connection);
        };
    }

    private Dictionary<string, Action<WebSocketConnection_, string, JSONNode>> _onData = new Dictionary<string, Action<WebSocketConnection_, string, JSONNode>>();

    public void OnData(string                                         event_,
                       Action<WebSocketConnection_, string, JSONNode> callback) {
        if (!_onData.TryGetValue(event_, out var callback_)) {
            _onData.Add(event_,
                (_,
                 __,
                 ___) => { });
        }

        _onData[event_] += callback;
    }

    public void RunOnData(WebSocketConnection_ connection,
                          string               event_,
                          JSONNode             data_) {
        if (!_onData.TryGetValue(event_, out var callback_)) {
            return;
        }

        WebSocketMainThread.OnUpdateOneTime += () => {
            callback_?.Invoke(connection, event_, data_);
        };
    }
}

}