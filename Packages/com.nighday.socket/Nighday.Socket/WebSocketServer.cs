using System;
using Nighday.Json;
using UnityEngine;

namespace Nighday.Socket {

public class MainService : WebSocketSharp.Server.WebSocketBehavior {
    public Action<WebSocketConnection_> OnOpenConnection;

    public Action<WebSocketConnection_, string, JSONNode> OnData;

    private WebSocketConnection_ _connection;

    public string socketKey;

    public WebSocketServer server;

    private void ReadMessage(WebSocketConnection_ connection,
                             JSONNode             message) { }

    protected override void OnMessage(WebSocketSharp.MessageEventArgs e) {
        WebSocketMainThread.OnUpdateOneTime += () => {

            // Debug.Log("e.Data = " + e.Data);
            // Debug.Log("e.IsPing = " + e.IsPing);
            // Debug.Log("e.IsText = " + e.IsText);
            // Debug.Log("e.IsBinary = " + e.IsBinary);
            // var msg = e.Data == "BALUS"
            // 	          ? "I've been balused already..."
            // 	          : "I'm not available now.";
            //
            // Send (msg);

            var json = e.Data;
            var jsonNode = JSON.Parse(json);

            var event_ = jsonNode["e"]
                .Value;
            var data = jsonNode["d"];
            server.ProtocolSubscribes.RunOnData(_connection, event_, data);
            
            _connection.RunOnData(event_, data);

            // var jsonNode = JSON.Parse(e.Data);
            // ReadMessage(_webSocketConnection, jsonNode);
            // var event_ = jsonNode["e"]
            // 	.Value;
            // if (_dict.TryGetValue(event_, out var action)) {
            // 	action(_webSocketConnection, new Request {
            // 		webSocket = _websocket
            // 	});
            // }

            Debug.Log("Я получил сообщение, я клиент.");
        };
    }

    protected override void OnOpen() {
        Debug.Log("1 Я создался, я клиент.");

        WebSocketMainThread.OnUpdateOneTime += () => {
            _connection = new WebSocketConnection_(server, _websocket);
            server.ProtocolSubscribes.RunOnConnection(_connection);
            Debug.Log("Я создался, я клиент.");
        };
    }

}

public class WebSocketServer {

    public WebSocketProtocolSubscribes _protocolSubscribes = new WebSocketProtocolSubscribes();

    public WebSocketProtocolSubscribes ProtocolSubscribes => _protocolSubscribes;

    public string socketKey { get; private set; }

    private WebSocketSharp.Server.WebSocketServer _wss;

    private MainService _service;

    //===
    public WebSocketServer(string socketKey,
                           ushort port) {
        this.socketKey = socketKey;
        _wss = new WebSocketSharp.Server.WebSocketServer("ws://127.0.0.1:" + port);

        
        
        _wss.AddWebSocketService<MainService>("/",
            (service_) => {

                //===
                _service = service_;
                _service.socketKey = socketKey;
                _service.server = this;
                
                // new WebSocketServerFramework(this);

                // _service.on("connection",
                //     (connection,
                //      req) => { });
                
                Debug.Log("2 MyCheck_: new WebSocketServer");


                WebSocketProtocolSubscribes.RunOnCreateWebSocket(socketKey, this);
            });
        
        Debug.Log("MyCheck_: new WebSocketServer");

    }

    public void Dispose() {
        if (_wss != null) {
            _wss.Stop();
        }
    }

    public void Start() {
        _wss.Start();
        Debug.Log("MyCheck_: _wss.Start");

    }

    //===

}

}