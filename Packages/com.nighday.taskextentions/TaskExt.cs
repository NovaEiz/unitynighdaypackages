using System.Threading.Tasks;

namespace Nighday.TaskExtentions {
public static class TaskExt {
	public static async Task<(T1, T2)> WhenAll<T1, T2>(Task<T1> task1, Task<T2> task2) {
		return (await task1, await task2);
	}
	public static async Task<(T1, T2, T3)> WhenAll<T1, T2, T3>(Task<T1> task1, Task<T2> task2, Task<T3> task3) {
		return (await task1, await task2, await task3);
	}
	public static void Catch(this Task task) {
        task.ContinueWith((Task fail) => {
                foreach (var x in fail.Exception.InnerExceptions) {
                    Debug.LogException(x);
                }
            },
            TaskContinuationOptions.OnlyOnFaulted);
    }

    public static void Catch<T>(this Task<T> task) {
        task.ContinueWith((Task<T> fail) => {
                              foreach (var x in fail.Exception.InnerExceptions) {
                                  Debug.LogException(x);
                              }
                          },
                          TaskContinuationOptions.OnlyOnFaulted);
    }
    

    public static Task CatchR(this Task task) {
        task.ContinueWith((Task fail) => {
                              foreach (var x in fail.Exception.InnerExceptions) {
                                  Debug.LogException(x);
                              }
                          },
                          TaskContinuationOptions.OnlyOnFaulted);

        return task;
    }
    public static Task<T> CatchR<T>(this Task<T> task) {
        task.ContinueWith((Task<T> fail) => {
                              foreach (var x in fail.Exception.InnerExceptions) {
                                  Debug.LogException(x);
                              }
                          },
                          TaskContinuationOptions.OnlyOnFaulted);

        return task;
    }
    
    public static IEnumerator AsIEnumerator(this Task task) {
        while (!task.IsCompleted) yield return null;

        if (task.IsFaulted) ExceptionDispatchInfo.Capture(task.Exception).Throw();
    }

    public static IEnumerator<T> AsIEnumerator<T>(this Task<T> task) {
        while (!task.IsCompleted) yield return default;

        if (task.IsFaulted) ExceptionDispatchInfo.Capture(task.Exception).Throw();

        yield return task.Result;
    }
    
    public static async Task<(T1, T2)> WhenAll<T1, T2>(Task<T1> a, Task<T2> b) {
        await Task.WhenAll(new Task[] {a, b});
        return (a.Result, b.Result);
    }
    
    public static async Task<(T1, T2, T3)> WhenAll<T1, T2, T3>(Task<T1> a, Task<T2> b, Task<T3> c) {
        await Task.WhenAll(new Task[] {a, b, c});
        return (a.Result, b.Result, c.Result);
    }
    
    public static async Task<(T1, T2, T3, T4)> WhenAll<T1, T2, T3, T4>(Task<T1> a, Task<T2> b, Task<T3> c, Task<T4> d) {
        await Task.WhenAll(new Task[] {a, b, c, d});
        return (a.Result, b.Result, c.Result, d.Result);
    }
}
}