using System;
using Unity.VisualScripting;
using UnityEngine;

[assembly: UnityEngine.Scripting.AlwaysLinkAssembly]
namespace Nighday.UI {

[UnityEngine.Scripting.Preserve]
[UnitTitle("DataUI")]
[UnitCategory("_Nighday/UI/DataUI")]
public class DataUIExpose : Unit {

    [DoNotSerialize]
    private DataUI _dataUI;

    [DoNotSerialize]
    public ValueInput dataUIInput;

    [DoNotSerialize]
    public ValueInput dataInput;

    [DoNotSerialize]
    public ValueOutput dataUIOutput;

    [DoNotSerialize]
    public ValueOutput dataOutput;

    [DoNotSerialize]
    public ValueOutput keyPathOutput;

    [DoNotSerialize]
    public ValueOutput valueByKeyPathOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        dataUIInput = ValueInput<DataUI>("dataUI", null);
        dataInput = ValueInput<object>("data", null);
        
        dataUIOutput = ValueOutput<DataUI>("dataUI_", (flow) => { return _dataUI; });
        dataOutput = ValueOutput<object>("data_",
            (flow) => {
                try {
                    return _dataUI.data;
                } catch (Exception e) {
                    this.SetException(flow.stack, e);
                    UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
                    throw;
                }
            });
        keyPathOutput = ValueOutput<string>("keyPath",
            (flow) => {
                try {
                    return _dataUI.keyPath;
                } catch (Exception e) {
                    this.SetException(flow.stack, e);
                    UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
                    throw;
                }
            });
        valueByKeyPathOutput = ValueOutput<object>("valueByKeyPath",
            (flow) => {
                try {
                    return _dataUI.valueByKeyPath;
                } catch (Exception e) {
                    this.SetException(flow.stack, e);
                    UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
                    throw;
                }
            });
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        try {
            _dataUI = flow.GetValue<DataUI>(dataUIInput);
            
            if (dataInput.hasValidConnection) {
                var data = flow.GetValue<object>(dataInput);
                _dataUI.data = data;
            }
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }
        return exit;
    }

}

}