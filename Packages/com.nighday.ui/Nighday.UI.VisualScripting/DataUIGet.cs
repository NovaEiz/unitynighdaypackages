using System;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.UI {

[UnityEngine.Scripting.Preserve]
[UnitTitle("DataUI.Get")]
[UnitCategory("_Nighday/UI/DataUI")]
public class DataUIGet : Unit {

    [DoNotSerialize]
    public ValueInput targetInput;

    [DoNotSerialize]
    public ValueOutput dataUIOutput;

    protected override void Definition() {
        targetInput = ValueInput<GameObject>("target", null).NullMeansSelf();
        dataUIOutput = ValueOutput<DataUI>("dataUI",
            (flow) => {
                var targetGameObject = flow.GetValue<GameObject>(targetInput);
                return targetGameObject.GetComponent<DataUI>();
            });
    }

}

}