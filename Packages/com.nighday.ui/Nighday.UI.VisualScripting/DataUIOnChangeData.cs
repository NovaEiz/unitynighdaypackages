using System;
using System.Collections;
using System.Collections.Generic;
using Mono.Collections.Generic;
using Nighday.VisualScripting;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace Nighday.UI {

[UnityEngine.Scripting.Preserve]
[UnitTitle("DataUI.OnChangeData")]
[UnitCategory("_Nighday/UI/DataUI")]
public class DataUIOnChangeData : MultipleCustomEventUnit<EmptyEventArgs> {
    
    [DoNotSerialize]
    public DataUI _dataUI;
    
    [DoNotSerialize]
    public ValueInput dataUIInput;
    
    [DoNotSerialize]
    public ValueOutput dataUIOutput;
    
    [DoNotSerialize]
    public ValueOutput dataOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override bool register => true;

    protected override bool ShouldTrigger(Flow           flow,
                                          EmptyEventArgs args) {
        return true;
    }

    [DoNotSerialize]
    [PortLabel("OnChangeData")]
    public ControlOutput onChangeData { get; private set; }

    [DoNotSerialize]
    private EventHook onChangeDataHook;

    public EventHook GetHook_OnChangeData(GraphReference reference) {
        onChangeDataHook = new EventHook("DataUI_OnChangeData_" + Guid.NewGuid()
                                             .ToString(),
            reference.machine);

        return onChangeDataHook;
    }
    
    
    public override void StartListening(GraphStack stack) {
        var data = stack.GetElementData<Data>(this);

        if (data.isListening) {
            return;
        }

        if (register) {
            var reference = stack.ToReference();
            
            //data.RegisterEventPoint(GetHook_OnChangeData(reference), (args) => { Trigger_OnChangeDataItem(reference, args); });
        }

        data.isListening = true;
    }

    public override void StopListening(GraphStack stack) {
        var data = stack.GetElementData<Data>(this);

        if (!data.isListening) {
            return;
        }

        // The coroutine's flow will dispose at the next frame, letting us
        // keep the current flow for clean up operations if needed
        foreach (var activeCoroutine in data.activeCoroutines) {
            activeCoroutine.StopCoroutine(false);
        }

        if (register) {
            //data.UnregisterEventPoints();
        }

        data.isListening = false;
    }

    //===

    protected override void Definition() {
        dataUIInput = ValueInput<DataUI>("dataUI");
        dataOutput = ValueOutput<object>("data",
            (flow) => {
                return _dataUI.data;
            });
        dataUIOutput = ValueOutput<DataUI>("dataUI_",
            (flow) => {
                return _dataUI;
            });
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        onChangeData = ControlOutput(nameof(onChangeData));
        
        Succession(enter, exit);

        base.Definition();
    }

    private ControlOutput Trigger(Flow flow) {
        try {
            var dataUI = flow.GetValue<DataUI>(dataUIInput);
        
            var data = flow.stack.GetElementData<Data>(this);
            var reference = flow.stack.ToReference();
            var onChangeDataItemHook = GetHook_OnChangeData(reference);
            var eventPoint_Complete = data.RegisterEventPoint(onChangeDataItemHook, (args) => { Trigger_OnChangeDataItem(reference, args); });

            Action action = null;
            action = () => {
                try {
                    _dataUI = dataUI;
                    EventBus.Trigger(onChangeDataItemHook);
                } catch (Exception e) {
                    this.SetException(flow.stack, e);
                    UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
                }
            };

            dataUI.OnChangeData += action;
            _dataUI = dataUI;
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }

        return exit;
    }

    public void Trigger_OnChangeDataItem(GraphReference reference,
                                         EmptyEventArgs args) {
        var flow = Flow.New(reference);

        if (!ShouldTrigger(flow, args)) {
            flow.Dispose();
            return;
        }

        AssignArguments(flow, args);

        Run_OnChangeDataItem(flow);
    }
    
    private void Run_OnChangeDataItem(Flow flow) {
        if (flow.enableDebug) {
            var editorData = flow.stack.GetElementDebugData<IUnitDebugData>(this);

            editorData.lastInvokeFrame = EditorTimeBinding.frame;
            editorData.lastInvokeTime = EditorTimeBinding.time;
        }

        try {
            if (coroutine) {
                flow.StartCoroutine(onChangeData,
                    flow.stack.GetElementData<Data>(this)
                        .activeCoroutines);
            } else {
                flow.Run(onChangeData);
            }
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }
    }

    public override void Dispose() {
        base.Dispose();
    }

    public override void Uninstantiate(GraphReference instance) {
        var data = instance.ToStackPooled().GetElementData<Data>(this);
        data.UnregisterEventPoints();
        base.Uninstantiate(instance);
    }

}

}