using Nighday.VisualScripting;
using Unity.VisualScripting;

namespace Nighday.UI {

[UnityEngine.Scripting.Preserve]
[UnitTitle("DataUI.OnSetData")]
[UnitCategory("_Nighday/UI/DataUI")]
public class DataUIOnSetData : CustomEventUnit<object> {
    
    [DoNotSerialize]
    public DataUI _dataUI;

    [DoNotSerialize]
    public ValueOutput dataUIOutput;

    [DoNotSerialize]
    public ValueOutput dataOutput;
    
    //===
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow   flow,
                                          object args) {
        _dataUI = args as DataUI;
        return true;
    }

    private EventHook _hook;

    public override EventHook GetHook(GraphReference reference) {
        if (_hook.Equals(default)) {
            _hook = new EventHook("DataUI_OnSetData",
                reference.machine);
        }

        return _hook;
    }
    //===
    
    protected override void Definition()
    {
        dataUIOutput = ValueOutput<DataUI>("dataUI",
            (flow) => {
                return _dataUI;
            });
        dataOutput = ValueOutput<object>("data",
            (flow) => {
                return _dataUI.data;
            });
        
        base.Definition();
    }
    
}

}