using System;
using System.Collections;
using Nighday.VisualScripting;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Nighday.UI {

[UnityEngine.Scripting.Preserve]
[UnitTitle("DownloadSpriteByUrl")]
[UnitCategory("_Nighday/UI/Sprite")]
public class DownloadSpriteByUrlNode : CustomEventUnit<EmptyEventArgs> {
    
    [DoNotSerialize]
    public Sprite _sprite;

    [DoNotSerialize]
    public ValueInput urlInput;

    [DoNotSerialize]
    public ValueOutput spriteOutput;

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    [Serialize]
    [Inspectable, UnitHeaderInspectable("With write sprite to cache")]
    [InspectorToggleLeft]
    public bool withWriteSpriteToCache { get; set; } = true;
    
    //===
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow           flow,
                                          EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;

    public override EventHook GetHook(GraphReference reference) {
        _hook = new EventHook("DownloadSpriteByUrlEvent_" + Guid.NewGuid().ToString(),
            reference.machine);

        return _hook;
    }
    //===
    
    protected override void Definition() {
        urlInput = ValueInput<string>("url", null);
        spriteOutput = ValueOutput<Sprite>("sprite",
            (flow) => {
                return _sprite;
            });
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        Succession(enter, exit);
        
        base.Definition();
    }
    
    private ControlOutput Trigger(Flow flow) {
        var url = flow.GetValue<string>(urlInput);

        (flow.stack.component as ScriptMachine).StartCoroutine(SpriteExt.DownloadSprite(url,
            (sprite_) => {
                try {
                    _sprite = sprite_;
                    EventBus.Trigger(_hook);
                } catch (Exception e) {
                    this.SetException(flow.stack, e);
                    UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
                }
            }, withWriteSpriteToCache));
        
        return exit;
    }
    
}

}