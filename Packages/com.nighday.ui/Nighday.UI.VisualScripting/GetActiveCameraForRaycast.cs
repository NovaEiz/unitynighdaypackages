using System;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.UI {

[UnityEngine.Scripting.Preserve]
[UnitTitle("_Nighday.UI.GetActiveCameraForRaycast")]
[UnitCategory("_Nighday/UI")]
public class GetActiveCameraForRaycast : Unit {

    [DoNotSerialize]
    public ValueOutput cameraOutput;

    protected override void Definition() {
        cameraOutput = ValueOutput<Camera>("camera",
            (flow) => {
                return ActiveCameraForRaycast.camera;
            });
    }

}

}