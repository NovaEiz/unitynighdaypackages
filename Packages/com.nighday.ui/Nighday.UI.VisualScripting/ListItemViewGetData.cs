using System;
using System.Collections;
using Nighday.UI;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Socket {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ListItemView.GetData")]
[UnitCategory("_Nighday/UI/ListView")]
public class ListItemViewGetData : Unit {

    [DoNotSerialize]
    public ValueInput listItemViewInput;

    [DoNotSerialize]
    public ValueOutput dataOutput;

    protected override void Definition() {
        listItemViewInput = ValueInput<GameObject>("listItemView", null).NullMeansSelf();
        dataOutput = ValueOutput<object>("data",
            (flow) => {
                try {
                    var listItemViewGameObject = flow.GetValue<GameObject>(listItemViewInput);
                    var listItemView = listItemViewGameObject.GetComponent<ListItemView>();
                    return listItemView.data;
                } catch (Exception e) {
                    this.SetException(flow.stack, e);
                    UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
                }

                return null;
            });
    }

}

}