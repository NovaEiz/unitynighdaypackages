using System.Collections;
using Nighday.UI;
using Unity.VisualScripting;

namespace Nighday.Socket {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ListItemView.InitDataComplete")]
[UnitCategory("_Nighday/UI/ListView")]
public class ListItemViewInitDataComplete : Unit {

    [DoNotSerialize]
    public ValueInput listItemViewInput;

    [DoNotSerialize]
    public ValueInput dataInput;

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        listItemViewInput = ValueInput<ListItemView>("listItemView", null).NullMeansSelf();
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var listItemView = flow.GetValue<ListItemView>(listItemViewInput);

        listItemView.InitDataComplete();
        
        return exit;
    }

    public override void Dispose() {
        base.Dispose();
    }

    public override void Uninstantiate(GraphReference instance) {
        base.Uninstantiate(instance);
    }

}

}