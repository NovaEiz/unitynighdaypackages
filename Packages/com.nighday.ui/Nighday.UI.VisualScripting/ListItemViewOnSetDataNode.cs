using System;
using System.Collections;
using Nighday.VisualScripting;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Nighday.UI {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ListItemView.OnSetData")]
[UnitCategory("_Nighday/UI/ListView")]
public class ListItemViewOnSetDataNode : CustomEventUnit<object> {
    
    [DoNotSerialize]
    public object _data;

    [DoNotSerialize]
    public ValueOutput dataOutput;
    
    //===
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow  flow,
                                          object args) {
        _data = args;
        return true;
    }

    public override EventHook GetHook(GraphReference reference) {
        var hook = new EventHook("OnSetData",
            reference.machine);

        return hook;
    }
    //===
    
    protected override void Definition()
    {
        dataOutput = ValueOutput<object>("data",
            (flow) => {
                return _data;
            });
        
        base.Definition();
    }
    
}

}