using System;
using System.Collections;
using Nighday.VisualScripting;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Nighday.UI {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ListItemView.ToggleOnSetActive")]
[UnitCategory("_Nighday/UI/ListView")]
public class ListItemViewToggleOnSetActiveNode : CustomEventUnit<object> {
    
    [DoNotSerialize]
    public bool _value;

    [DoNotSerialize]
    public ValueOutput valueOutput;
    
    //===
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow   flow,
                                          object args) {
        return true;
    }

    public override EventHook GetHook(GraphReference reference) {
        var hook = new EventHook("OnSetActive",
                reference.machine);
        return hook;
    }
    //===
    
    protected override void Definition()
    {
        valueOutput = ValueOutput<bool>("value",
            (flow) => {
                return _value;
            });
        
        base.Definition();
    }
    
}

}