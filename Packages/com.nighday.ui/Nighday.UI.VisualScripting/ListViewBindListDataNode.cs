using System.Collections;
using Nighday.UI;
using Unity.VisualScripting;

namespace Nighday.Socket {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ListView.BindListData")]
[UnitCategory("_Nighday/UI/ListView")]
public class ListViewBindListDataNode : Unit {

    [DoNotSerialize]
    public ValueInput listViewInput;

    [DoNotSerialize]
    public ValueInput dataInput;

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        listViewInput = ValueInput<ListView>("listView", null);
        dataInput = ValueInput<object>("data", null);
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var listView = flow.GetValue<ListView>(listViewInput);
        var listData = flow.GetValue<object>(dataInput);

        listView.Bind(listData);
        
        return exit;
    }

    public override void Dispose() {
        base.Dispose();
    }

    public override void Uninstantiate(GraphReference instance) {
        base.Uninstantiate(instance);
    }

}

}