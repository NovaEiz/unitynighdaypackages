using System.Collections;
using Nighday.UI;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Socket {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ListView.GetData")]
[UnitCategory("_Nighday/UI/ListView")]
public class ListViewGetData : Unit {

    [DoNotSerialize]
    public ValueInput listViewInput;

    [DoNotSerialize]
    public ValueOutput dataOutput;

    [DoNotSerialize]
    public ValueOutput lengthOutput;

    [DoNotSerialize]
    public ValueOutput selectedItemIndexOutput;

    protected override void Definition() {
        listViewInput = ValueInput<ListView>("listView", null);
        dataOutput = ValueOutput<object>("data",
            (flow) => {
                var listView = flow.GetValue<ListView>(listViewInput);
                return listView.dataItems;
            });
        lengthOutput = ValueOutput<int>("length",
            (flow) => {
                var listView = flow.GetValue<ListView>(listViewInput);
                return listView.length;
            });
        selectedItemIndexOutput = ValueOutput<int>("selectedItemIndex",
            (flow) => {
                var listView = flow.GetValue<ListView>(listViewInput);
                return listView.GetSelectedItemIndex();
            });
    }

}

}