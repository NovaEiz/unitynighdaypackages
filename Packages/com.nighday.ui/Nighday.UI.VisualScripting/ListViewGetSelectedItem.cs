using System.Collections;
using Nighday.UI;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Socket {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ListView.GetSelectedItem")]
[UnitCategory("_Nighday/UI/ListView")]
public class ListViewGetSelectedItem : Unit {

    [DoNotSerialize]
    public ValueInput listViewInput;

    [DoNotSerialize]
    public ValueOutput itemDataOutput;

    [DoNotSerialize]
    public ValueOutput itemViewOutput;

    [DoNotSerialize]
    public ValueOutput itemIndexOutput;

    protected override void Definition() {
        listViewInput = ValueInput<ListView>("listView", null);
        itemIndexOutput = ValueOutput<int>("itemIndex",
            (flow) => {
                var listView = flow.GetValue<ListView>(listViewInput);
                return listView.GetSelectedItemIndex();
            });
        itemDataOutput = ValueOutput<object>("itemData",
            (flow) => {
                var listView = flow.GetValue<ListView>(listViewInput);
                return listView.GetSelectedItemData();
            });
        itemViewOutput = ValueOutput<ListItemView>("itemView",
            (flow) => {
                var listView = flow.GetValue<ListView>(listViewInput);
                return listView.GetSelectedItemView();
            });
    }

}

}