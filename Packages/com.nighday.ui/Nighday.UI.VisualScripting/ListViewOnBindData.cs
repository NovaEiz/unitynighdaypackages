using System;
using System.Collections;
using Nighday.VisualScripting;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Nighday.UI {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ListView.OnBindData")]
[UnitCategory("_Nighday/UI/ListView")]
public class ListViewOnBindData : CustomEventUnit<EmptyEventArgs> {
    
    [DoNotSerialize]
    public ListView _listView;
    
    [DoNotSerialize]
    public ValueInput listViewInput;
    
    [DoNotSerialize]
    public ValueOutput listViewOutput;

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    [Serialize]
    [Inspectable, UnitHeaderInspectable("Once call")]
    [InspectorToggleLeft]
    public bool onceCall { get; set; }
    
    //===
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow           flow,
                                          EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;

    public override EventHook GetHook(GraphReference reference) {
        if (_hook.Equals(default)) {
            _hook = new EventHook("ListViewOnBindData_",
                reference.machine);
        }

        return _hook;
    }
    //===
    
    protected override void Definition()
    {
        listViewInput = ValueInput<ListView>("listView", null);

        listViewOutput = ValueOutput<ListView>("listView_",
            (flow) => {
                return _listView;
            });
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        Succession(enter, exit);
        
        base.Definition();
    }
    
    private ControlOutput Trigger(Flow flow) {
        try {
            var listView = flow.GetValue<ListView>(listViewInput);

            Action action = null;
            action = () => {
                try {
                    if (onceCall) {
                        listView.OnBindData -= action;
                    }
                    
                    _listView = listView;
                    EventBus.Trigger(_hook);
                } catch (Exception e) {
                    this.SetException(flow.stack, e);
                    UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
                }
            };

            listView.OnBindData += action;
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }
        
        return exit;
    }
    
}

}