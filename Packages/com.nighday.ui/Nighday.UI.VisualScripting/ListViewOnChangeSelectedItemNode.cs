using System;
using System.Collections;
using Nighday.VisualScripting;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Nighday.UI {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ListView.OnChangeSelectedItem")]
[UnitCategory("_Nighday/UI/ListView")]
public class ListViewOnChangeSelectedItemNode : CustomEventUnit<EmptyEventArgs> {
    
    [DoNotSerialize]
    public ListItemView _itemView;
    
    [DoNotSerialize]
    public object _itemData;
    
    [DoNotSerialize]
    public int _itemIndex;
    
    [DoNotSerialize]
    public ValueInput listViewInput;
    
    [DoNotSerialize]
    public ValueOutput itemViewOutput;
    
    [DoNotSerialize]
    public ValueOutput itemDataOutput;
    
    [DoNotSerialize]
    public ValueOutput itemIndexOutput;

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    //===
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow           flow,
                                          EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;

    public override EventHook GetHook(GraphReference reference) {
        if (_hook.Equals(default)) {
            _hook = new EventHook("ListViewOnChangeSelected",
                reference.machine);
        }

        return _hook;
    }
    //===
    
    protected override void Definition()
    {
        listViewInput = ValueInput<ListView>("listView", null);

        itemIndexOutput = ValueOutput<int>("itemIndex",
            (flow) => {
                return _itemIndex;
            });
        itemDataOutput = ValueOutput<object>("itemData",
            (flow) => {
                return _itemData;
            });
        itemViewOutput = ValueOutput<ListItemView>("itemView",
            (flow) => {
                return _itemView;
            });
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        Succession(enter, exit);
        
        base.Definition();
    }
    
    private ControlOutput Trigger(Flow flow) {
        var listView = flow.GetValue<ListView>(listViewInput);

        Action<ListItemView, object, int> action = null;
        action = (itemView, itemData, itemIndex) => {
            try {
                _itemView = itemView;
                _itemData = itemData;
                _itemIndex = itemIndex;
                EventBus.Trigger(_hook);
            } catch (Exception e) {
                this.SetException(flow.stack, e);
                UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
            }
        };

        listView.OnChangeSelected += action;
        
        return exit;
    }
    
}

}