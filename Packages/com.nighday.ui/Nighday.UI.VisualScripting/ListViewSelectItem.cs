using System.Collections;
using Nighday.UI;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Socket {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ListView.SelectItem")]
[UnitCategory("_Nighday/UI/ListView")]
public class ListViewSelectItem : Unit {

    [DoNotSerialize]
    public ValueInput listViewInput;

    [DoNotSerialize]
    public ValueInput itemIndexInput;
    
    [DoNotSerialize]
    public ControlInput setIndex { get; private set; }
    
    [DoNotSerialize]
    public ControlInput changeIndexToLeft { get; private set; }
    
    [DoNotSerialize]
    public ControlInput changeIndexToRight { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        listViewInput = ValueInput<ListView>("listView", null);
        itemIndexInput = ValueInput<int>("itemIndex", 0);

        setIndex = ControlInput(nameof(setIndex), setIndex_Trigger);
        changeIndexToLeft = ControlInput(nameof(changeIndexToLeft), changeIndexToLeft_Trigger);
        changeIndexToRight = ControlInput(nameof(changeIndexToRight), changeIndexToRight_Trigger);
        exit = ControlOutput(nameof(exit));
        
        Succession(setIndex, exit);
        Succession(changeIndexToLeft, exit);
        Succession(changeIndexToRight, exit);
    }
    
    private ControlOutput setIndex_Trigger(Flow flow) {
        var listView = flow.GetValue<ListView>(listViewInput);
        var itemIndex = flow.GetValue<int>(itemIndexInput);
        
        listView.SelectItem(itemIndex);

        return exit;
    }
    
    private ControlOutput changeIndexToLeft_Trigger(Flow flow) {
        var listView = flow.GetValue<ListView>(listViewInput);
        
        var currentIndex = listView.GetSelectedItemIndex();
        currentIndex--;
        while (currentIndex < 0) {
            currentIndex = listView.length + currentIndex;
        }
        listView.SelectItem(currentIndex);

        return exit;
    }
    
    private ControlOutput changeIndexToRight_Trigger(Flow flow) {
        var listView = flow.GetValue<ListView>(listViewInput);
        
        var currentIndex = listView.GetSelectedItemIndex();
        currentIndex++;
        while (currentIndex >= listView.length) {
            currentIndex = listView.length - currentIndex;
        }
        listView.SelectItem(currentIndex);

        return exit;
    }

}

}