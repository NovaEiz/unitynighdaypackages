using System.Collections;
using Nighday.UI;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.Socket {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ListView.SetContainer")]
[UnitCategory("_Nighday/UI/ListView")]
public class ListViewSetContainerNode : Unit {

    [DoNotSerialize]
    public ValueInput listViewInput;

    [DoNotSerialize]
    public ValueInput containerInput;

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        listViewInput = ValueInput<ListView>("listView", null);
        containerInput = ValueInput<Transform>("container", null);
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        Succession(enter, exit);
    }

    private ControlOutput Trigger(Flow flow) {
        var listView = flow.GetValue<ListView>(listViewInput);
        var container = flow.GetValue<Transform>(containerInput);

        listView.SetContainer(container);

        return exit;
    }

}

}