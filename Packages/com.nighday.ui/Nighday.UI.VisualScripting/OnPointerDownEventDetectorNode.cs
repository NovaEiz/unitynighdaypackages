using System;
using Nighday.VisualScripting;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Nighday.UI {

[UnityEngine.Scripting.Preserve]
[UnitTitle("OnPointerDownEventDetector")]
[UnitCategory("_Nighday/UI")]
public class OnPointerDownEventDetectorNode : MultipleCustomEventUnit<EmptyEventArgs> {
    
    [DoNotSerialize]
    public ValueInput gameObjectInput;
    
    [DoNotSerialize]
    public ValueInput racerInput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    [DoNotSerialize]
    // [PortLabelHidden]
    public ControlOutput onEvent { get; private set; }

    [Serialize]
    [Inspectable, UnitHeaderInspectable("Once call")]
    [InspectorToggleLeft]
    public bool onceCall { get; set; }
    
    //===
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow           flow,
                                          EmptyEventArgs args) {
        return true;
    }
    
    //===
    
    
    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        onEvent = ControlOutput(nameof(onEvent));
        
        gameObjectInput = ValueInput<GameObject>("gameObject", null);
        
        Succession(enter, exit);

        base.Definition();
    }
    
    private ControlOutput Trigger(Flow flow) {
        var gameObject = flow.GetValue<GameObject>(gameObjectInput);

        OnUIEventsDetector onUIEventsDetector = null;
        onUIEventsDetector = gameObject.GetComponent<OnUIEventsDetector>();
        if (onUIEventsDetector == null) {
            onUIEventsDetector = gameObject.AddComponent<OnUIEventsDetector>();
        }
        
        var data = flow.stack.GetElementData<Data>(this);
        var reference = flow.stack.ToReference();
        var hook_onComplete = GetHook_OnEvent(reference);
        var eventPoint_Complete = data.RegisterEventPoint(hook_onComplete, (args) => { Trigger_OnEvent(reference, args); });

        Action<PointerEventData> action = null;
        action = (eventData) => {
            if (onceCall) {
                onUIEventsDetector.OnPointerDownEvent -= action;
            }
            
            try {
                EventBus.Trigger(hook_onComplete);
                data.UnregisterEventPoint(eventPoint_Complete);
            } catch (Exception e) {
                this.SetException(flow.stack, e);
                UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
            }
        };

        onUIEventsDetector.OnPointerDownEvent += action;
        
        Trigger_(flow.stack.AsReference(), new EmptyEventArgs());
        return null;
        // return exit;
    }
    
    private void Run(Flow flow) {
        if (flow.enableDebug) {
            var editorData = flow.stack.GetElementDebugData<IUnitDebugData>(this);

            editorData.lastInvokeFrame = EditorTimeBinding.frame;
            editorData.lastInvokeTime = EditorTimeBinding.time;
        }

        if (coroutine) {
            flow.StartCoroutine(onEvent,
                flow.stack.GetElementData<Data>(this)
                    .activeCoroutines);
        } else {
            flow.Run(onEvent);
        }
    }
    
    //===

    [DoNotSerialize]
    private EventHook _hook_OnEvent;

    public EventHook GetHook_OnEvent(GraphReference reference) {
        _hook_OnEvent = new EventHook("OnPointerDownEventDetectorNode_OnEvent_" + Guid.NewGuid()
                                          .ToString(),
            reference.machine);

        return _hook_OnEvent;
    }
    
    public override void StartListening(GraphStack stack) {
        var data = stack.GetElementData<Data>(this);

        if (data.isListening) {
            return;
        }

        if (register) {
            var reference = stack.ToReference();
            
            data.RegisterEventPoint(GetHook_OnEvent(reference), (args) => { Trigger_OnEvent(reference, args); });
        }

        data.isListening = true;
    }

    public override void StopListening(GraphStack stack) {
        var data = stack.GetElementData<Data>(this);

        if (!data.isListening) {
            return;
        }

        // The coroutine's flow will dispose at the next frame, letting us
        // keep the current flow for clean up operations if needed
        foreach (var activeCoroutine in data.activeCoroutines) {
            activeCoroutine.StopCoroutine(false);
        }

        if (register) {
            data.UnregisterEventPoints();
        }

        data.isListening = false;
    }


    public void Trigger_OnEvent(GraphReference reference,
                                 EmptyEventArgs args) {
        var flow = Flow.New(reference);

        if (!ShouldTrigger(flow, args)) {
            flow.Dispose();
            return;
        }

        AssignArguments(flow, args);

        Run_OnEvent(flow);
    }
    
    private void Run_OnEvent(Flow flow) {
        if (flow.enableDebug) {
            var editorData = flow.stack.GetElementDebugData<IUnitDebugData>(this);

            editorData.lastInvokeFrame = EditorTimeBinding.frame;
            editorData.lastInvokeTime = EditorTimeBinding.time;
        }

        if (coroutine) {
            flow.StartCoroutine(onEvent,
                flow.stack.GetElementData<Data>(this)
                    .activeCoroutines);
        } else {
            flow.Run(onEvent);
        }
    }
    

    public void Trigger_(GraphReference reference,
                                EmptyEventArgs args) {
        var flow = Flow.New(reference);

        if (!ShouldTrigger(flow, args)) {
            flow.Dispose();
            return;
        }

        AssignArguments(flow, args);

        Run_(flow);
    }
    
    private void Run_(Flow flow) {
        if (flow.enableDebug) {
            var editorData = flow.stack.GetElementDebugData<IUnitDebugData>(this);

            editorData.lastInvokeFrame = EditorTimeBinding.frame;
            editorData.lastInvokeTime = EditorTimeBinding.time;
        }

        if (coroutine) {
            flow.StartCoroutine(exit,
                flow.stack.GetElementData<Data>(this)
                    .activeCoroutines);
        } else {
            flow.Run(exit);
        }
    }
    
    //===

    
}

}