using System;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.UI {

public class DataUI : MonoBehaviour {
    
    protected object _data;
    public virtual object data {
        get {
            return _data;
        }
        set {
            _data = value;
            OnChangeData?.Invoke();
            EventBus.Trigger(new EventHook("DataUIOnSetData", gameObject.GetComponent<EventMachine<FlowGraph, ScriptGraphAsset>>()), this as DataUI);
        }
    }
    
    public Action OnChangeData;

    public virtual string keyPath => "";

    public virtual object valueByKeyPath => data;

}

}