using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Nighday.UI {

public class SpriteExt {
    
    private static Dictionary<string, Sprite> _texturesCache;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
    public static void StaticInitialize() {
        _texturesCache = new Dictionary<string, Sprite>();
    }
    
    public static IEnumerator DownloadSprite(string url, Action<Sprite> callback, bool withWriteSpriteToCache = true) {
        if (_texturesCache.TryGetValue(url, out var sprite_)) {
            if (sprite_ == null) {
                while ((_texturesCache.TryGetValue(url, out sprite_) && sprite_ == null)) {
                    yield return null;
                }
            }

            callback(sprite_);
            yield break;
        }

        if (withWriteSpriteToCache) {
            _texturesCache.Add(url, null);
        }

        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(url))
        {
            yield return uwr.SendWebRequest();

            if (uwr.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(uwr.error);
                if (withWriteSpriteToCache) {
                    _texturesCache.Remove(url);
                }
            }
            else
            {
                // Get downloaded asset bundle
                var tex = DownloadHandlerTexture.GetContent(uwr);
				
                Sprite sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100);

                if (withWriteSpriteToCache) {
                    _texturesCache[url] = sprite;
                }

                callback(sprite);
            }
        }
		
    }
    
}

}