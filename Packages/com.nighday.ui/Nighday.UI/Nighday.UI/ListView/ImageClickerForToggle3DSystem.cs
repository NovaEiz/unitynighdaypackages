using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

namespace Nighday.UI {

public interface IImageClickerForToggle3DSystem : IPointerDownHandler, IPointerClickHandler, IPointerUpHandler {
    event Action onPointerDown;
    event Action onPointerClick;
    event Action onPointerUp;
}

public class ImageClickerForToggle3DSystem : MonoBehaviour, IPointerDownHandler, IPointerClickHandler, IPointerUpHandler, IImageClickerForToggle3DSystem {

    public event Action onPointerDown;
    public event Action onPointerClick;
    public event Action onPointerUp;

    public void OnPointerDown(PointerEventData eventData) {
        onPointerDown?.Invoke();
    }

    public void OnPointerClick(PointerEventData eventData) {
        onPointerClick?.Invoke();
    }

    public void OnPointerUp(PointerEventData eventData) {
        onPointerUp?.Invoke();
    }

    private void OnEnable() {
        Toggle3DSystem.RegisterImageClicker(this);
    }

    private void OnDisable() {
        Toggle3DSystem.UnregisterImageClicker(this);
    }

}

}