using System;
using System.Threading.Tasks;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace Nighday.UI {

public class ListItemView : MonoBehaviour {

    private Action onInitDataComplete;
    public void SubscribeOnceInitDataComplete(Action action) {
        onInitDataComplete += action;
    }
    
    public void InitDataComplete() {
        onInitDataComplete?.Invoke();
        onInitDataComplete = null;
    }

    private ListItemViewToggle _toggle;
    public ListItemViewToggle toggle {
        get {
            if (_toggle == null) {
                _toggle = gameObject.GetComponent<ListItemViewToggle>();
                if (_toggle == null) {
                    _toggle = gameObject.AddComponent<ListItemViewToggle>();
                }
            }

            return _toggle;
        }
    }

    private DataUI _dataUI;

    private DataUI dataUI {
        get {
            if (_dataUI == null) {
                _dataUI = GetComponent<DataUI>();
                if (_dataUI == null) {
                    _dataUI = gameObject.AddComponent<DataUI>();
                }
            }

            return _dataUI;
        }
    }
    
    public object data  { get => dataUI.data; private set => dataUI.data = value; }
    public int    index { get;                private set; }

    public ListView listView { get; set; }

    private void Awake() {
        if (toggle != null) {
            toggle.onValueChanged.AddListener((newValue) => {
                SetActive(newValue);
            });
        }
    }

    public void SetData(object data, int index) {
        this.data = data;
        this.index = index;
        
        OnChangeData?.Invoke(this.data);

        Debug.Log("SetData index = " + index + "; gameObject = " + gameObject);
        
        EventBus.Trigger(new EventHook("OnSetData", gameObject.GetComponent<EventMachine<FlowGraph, ScriptGraphAsset>>()), data);
    }

    public void SetActive(bool value) {
        OnSetActive?.Invoke(value);

        EventBus.Trigger(new EventHook("OnSetActive", gameObject.GetComponent<EventMachine<FlowGraph, ScriptGraphAsset>>()), data);
    }

    public event Action<object> OnChangeData;
    public event Action<object> OnSetActive;

}

}