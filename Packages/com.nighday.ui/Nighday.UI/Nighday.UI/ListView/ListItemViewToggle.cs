using System;
using UnityEngine;
using UnityEngine.UI;

namespace Nighday.UI {

public class ListItemViewToggle : MonoBehaviour {

    private Toggle _toggle;

    private Toggle toggle {
        get {
            if (_toggle == null) {
                _toggle = GetComponent<Toggle>();
            }

            return _toggle;
        }
    }

    private Toggle3D _toggle3D;

    private Toggle3D toggle3D {
        get {
            if (_toggle3D == null) {
                _toggle3D = gameObject.GetComponent<Toggle3D>();
                if (_toggle3D == null) {
                    _toggle3D = gameObject.AddComponent<Toggle3D>();
                }
            }

            return _toggle3D;
        }
    }

    private void Awake() {
        if (toggle == null) {
            var toggle3D = this.toggle3D;
        }
    }
    
    public bool isOn {
        get {
            if (toggle != null) {
                return toggle.isOn;
            }
            return toggle3D.isOn;
        }
        set {
            if (toggle != null) {
                toggle.isOn = value;
                return;
            }
            toggle3D.isOn = value;
        }
    }
    
    public ToggleGroup group {
        get {
            if (toggle != null) {
                return toggle.group;
            }
            return toggle3D.group;
        }
        set {
            if (toggle != null) {
                toggle.group = value;
                return;
            }
            toggle3D.isOn = value;
        }
    }
    
    public Toggle.ToggleEvent onValueChanged {
        get {
            if (toggle != null) {
                return toggle.onValueChanged;
            }
            return toggle3D.onValueChanged;
        }
    }

}

}