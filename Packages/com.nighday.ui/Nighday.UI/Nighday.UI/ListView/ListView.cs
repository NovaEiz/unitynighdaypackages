using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Nighday.UI {

public class ListView : MonoBehaviour {

    [SerializeField] private Transform    _itemsContainer;
    [SerializeField] private ListItemView _listItemPrefab;
    [SerializeField] private ToggleGroup  _toggleGroup;

    [Space]
    [SerializeField] private bool _asyncSetDataItems;

    private int _selectedItemIndex;

    public int length => _dataItems.Count;
    
    public int GetSelectedItemIndex() {
        return _selectedItemIndex;
    }

    public object GetSelectedItemData() {
        return _dataItems[_selectedItemIndex];
    }

    public ListItemView GetSelectedItemView() {
        return _viewItems[_selectedItemIndex];
    }

    private IList          _dataItems;
    private ListItemView[] _viewItems;

    public IList dataItems => _dataItems;

    private void Clear() {
        foreach (Transform childTr in _itemsContainer) {
            GameObject.Destroy(childTr.gameObject);
        }
    }

    private bool _firstWasCleared;

    private void FirstClear() {
        if (!_firstWasCleared) {
            _firstWasCleared = true;
            Clear();
        }
    }

    public void SetContainer(Transform container) {
        _itemsContainer = container;
    }

    public virtual void Bind(object data) {
        Bind(data as IList);
    }

    public virtual async void Bind(IList dataItems) {
        FirstClear();
        
        _dataItems = dataItems;
        
        var len = _dataItems.Count;

        var viewItems = new ListItemView[len];

        var currentLen = 0;
        if (_viewItems != null) {
            currentLen = _viewItems.Length;
            var copyLen = currentLen;
            if (currentLen > len) {
                copyLen = len;

                for (var i = len; i < currentLen; i++) {
                    GameObject.Destroy(viewItems[i].gameObject);
                }
            }
            for (var i = 0; i < copyLen; i++) {
                viewItems[i] = _viewItems[i];
            }
        }

        _viewItems = viewItems;

        var count = 0;

        if (len < _selectedItemIndex+1) {
            _selectedItemIndex = 0;
        }
        //for (var i = currentLen; i < len; i++) {
        for (var i = 0; i < len; i++) {
            var index = i;
            var dataItem = _dataItems[index];

            ListItemView listItemView = _viewItems[index];
            if (listItemView == null) {
                listItemView = GameObject.Instantiate(_listItemPrefab, _itemsContainer);
            
                if (listItemView.toggle != null) {
                    listItemView.toggle.isOn = false;
                    listItemView.toggle.group = _toggleGroup;
                    listItemView.toggle.onValueChanged.AddListener((newValue) => {
                        SelectItem(index);
                    });
                }
            
                _viewItems[index] = listItemView;
                listItemView.listView = this;
            }

            listItemView.SetData(dataItem, i);

            if (_asyncSetDataItems) {
                count++;
                listItemView.SubscribeOnceInitDataComplete(() => {
                    count--;
                });
            } else {
                OnBindItem?.Invoke(listItemView, dataItem, index);
            }
        }

        while (count > 0) {
            await Task.Yield();
        }

        if (_asyncSetDataItems) {
            for (var i = 0; i < len; i++) {
                var index = i;
                var dataItem = _dataItems[index];
                var listItemView = _viewItems[index];
                
                OnBindItem?.Invoke(listItemView, dataItem, index);
            }
        }

        if (_selectedItemIndex -1 > len) {
            _selectedItemIndex = len - 1;
        }

        if (len > 0 && _viewItems[_selectedItemIndex].toggle != null) {
            _viewItems[_selectedItemIndex].toggle.isOn = true;
        }
        
        OnBindData?.Invoke();
    }

    public void SelectItem(int index) {
        _selectedItemIndex = index;
        
        OnChangeSelected?.Invoke(_viewItems[_selectedItemIndex], _dataItems[_selectedItemIndex], _selectedItemIndex);
        Debug.Log("OnChangeSelected index = " + index);
    }

    public event Action<ListItemView, object, int> OnBindItem;
    public event Action<ListItemView, object, int> OnChangeSelected;
    public event Action OnBindData;
    
}

}