using System;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Nighday.UI {

public class Toggle3DSystem : MonoBehaviour {

    private static Toggle3DSystem _instance;

    private static Toggle3DSystem instance {
        get {
            if (_instance == null) {
                var obj = new GameObject("Toggle3DSystem");
                _instance = obj.AddComponent<Toggle3DSystem>();
                DontDestroyOnLoad(obj);
            }

            return _instance;
        }
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
    private static void StaticInitialization() {
        _toggles = new List<Toggle3D>();
    }

    private static List<Toggle3D> _toggles;

    public static void RegisterToggle(Toggle3D toggle) {
        _toggles.Add(toggle);
        
        var instance = Toggle3DSystem.instance;
    }

    public static void UnregisterToggle(Toggle3D toggle) {
        _toggles.Remove(toggle);

        if (_toggles.Count == 0) {
            if (_instance != null) {
                GameObject.Destroy(_instance.gameObject);
            }
        }
    }
    
    private static Toggle3D DetectRaycast() {
        var ray = ActiveCameraForRaycast.camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Single.PositiveInfinity)) {
            var task = hit.transform.GetComponent<Toggle3D>();
            if (task != null) {
                return task;
            }
        }

        return null;
    }

    private static void OnPointerClick() {
        var target = DetectRaycast();
        if (target == null) {
            return;
        }
        target.isOn = !target.isOn;
    }
    
    public static void RegisterImageClicker(IImageClickerForToggle3DSystem imageClicker) {
        imageClicker.onPointerClick += OnPointerClick;
    }

    public static void UnregisterImageClicker(IImageClickerForToggle3DSystem imageClicker) {
        imageClicker.onPointerClick -= OnPointerClick;
    }
    
}

public class Toggle3D : Toggle {

    // private bool _isOn;
    // public bool isOn {
    //     get {
    //         return _isOn;
    //     }
    //     set {
    //         _isOn = value;
    //     }
    // }
    //
    // public Toggle.ToggleEvent onValueChanged;
    
}

}