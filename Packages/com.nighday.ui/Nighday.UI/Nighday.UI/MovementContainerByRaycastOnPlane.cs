using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TX5.UI.Map {

public class MovementContainerByRaycastOnPlane : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
	
	[SerializeField] private Camera _camera;
	[SerializeField] private Transform _container;
	[SerializeField] private float _moveSpeed;
	[SerializeField] private LayerMask _layerMask;

	private int layerMask {
		get {
			if (_layerMask.Equals(default)) {
				_layerMask = 1 << LayerMask.NameToLayer("TransparentFX");
			}
			return _layerMask;
		}
	}

	private (bool, Vector3) DetectRaycast() {
		if (_camera == null || _container == null) {
			return (false, default);
		}
		var ray = _camera.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit, Single.PositiveInfinity, layerMask)) {
			return (true, hit.point);
		}

		return (false, default);
	}

	private Vector3 _lastPointPosition;
	private bool _dragging;

	public event Action OnBeginDragEvent;
	public event Action OnDragEvent;
	public event Action OnEndDragEvent;

	public void OnBeginDrag(PointerEventData eventData) {
		var (result, point) = DetectRaycast();
		if (!result) {
			return;
		}
		_dragging = true;

		_lastPointPosition = point;
		
		OnBeginDragEvent?.Invoke();
	}

	public void OnDrag(PointerEventData eventData) {
		if (!_dragging) {
			return;
		}
		
		var delta = eventData.delta;

		var (result, point) = DetectRaycast();

		if (!result) {
			return;
		}

		var currentPosition = point;
		var diff = currentPosition - _lastPointPosition;
		_lastPointPosition = currentPosition;

		// if (_cameraTarget != null) {
		// 	var localPosition = _cameraTarget.localPosition;
		// 	localPosition.x -= diff.x * _moveSpeed;
		// 	localPosition.z -= diff.z * _moveSpeed;
		// 	_cameraTarget.localPosition = localPosition;
		// } else {
			var localPosition = _container.localPosition;
			localPosition.x += diff.x * _moveSpeed;
			localPosition.z += diff.z * _moveSpeed;
			_container.localPosition = localPosition;
		// }
		
		OnDragEvent?.Invoke();
	}

	private Action _lateUpdate;

	private void LateUpdate() {
		if (_lateUpdate != null) {
			_lateUpdate();
			_lateUpdate = null;
		}
	}

	public void OnEndDrag(PointerEventData eventData) {
		OnEndDragEvent?.Invoke();
	}

	public void SetCamera(Camera camera) {
		_camera = camera;
	}

}
}