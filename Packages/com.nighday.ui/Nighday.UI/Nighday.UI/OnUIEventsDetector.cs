using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Nighday.UI {

public class OnUIEventsDetector : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler {

    public event Action<PointerEventData> OnPointerDownEvent;
    public event Action<PointerEventData> OnPointerUpEvent;
    public event Action<PointerEventData> OnPointerClickEvent;

    public void OnPointerDown(PointerEventData eventData) {
        OnPointerDownEvent?.Invoke(eventData);
    }

    public void OnPointerUp(PointerEventData eventData) {
        OnPointerUpEvent?.Invoke(eventData);
    }

    public void OnPointerClick(PointerEventData eventData) {
        OnPointerClickEvent?.Invoke(eventData);
    }

    public void OnPointerEnter(PointerEventData eventData) {
        
    }

    public void OnPointerExit(PointerEventData eventData) {
        
    }
}

}