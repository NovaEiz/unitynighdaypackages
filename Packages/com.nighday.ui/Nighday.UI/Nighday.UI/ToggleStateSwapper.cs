using System;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
[RequireComponent(typeof(Toggle))]
public class ToggleStateSwapper : MonoBehaviour {
    
    [HideInInspector] [SerializeField] private Toggle _targetToggle;
    [SerializeField] private GameObject _on;
    [SerializeField] private GameObject _off;

    private void OnEnable() {
        _targetToggle.toggleTransition = Toggle.ToggleTransition.None;
        _targetToggle.onValueChanged.RemoveListener(OnTargetToggleValueChanged);
        _targetToggle.onValueChanged.AddListener(OnTargetToggleValueChanged);

        OnTargetToggleValueChanged(_targetToggle.isOn);
    }

    private void OnTargetToggleValueChanged(bool newValue) {
        if (newValue) {
            if (_on != null) {
                _on.SetActive(true);
            }
            if (_off != null) {
                _off.SetActive(false);
            }
        }
        else {
            if (_on != null) {
                _on.SetActive(false);
            }
            if (_off != null) {
                _off.SetActive(true);
            }
        }
    }
    
#if UNITY_EDITOR

    public void OnValidate() {
        _targetToggle = GetComponent<Toggle>();
        if (_targetToggle == null) {
            OnTargetToggleValueChanged(_targetToggle.isOn);
        }
    }
    
#endif
    
}