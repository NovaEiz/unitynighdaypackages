using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using UnityEngine.AddressableAssets;

namespace Nighday.UIToolkit {

[CustomFieldEditor(typeof(AssetReference))]
public class AssetReferenceFieldEditor : CustomFieldEditor {

    public override VisualElement CreateInspector() {
        var root = new VisualElement();
        var field = new ObjectField(this.Name);

        var assetReference = (AssetReference) fieldInfo.GetValue(obj);
        field.objectType = typeof(UnityEngine.Object);
        field.value = assetReference.editorAsset;

        field.RegisterValueChangedCallback((changeEvent) => {
            fieldInfo.SetValue(obj, new AssetReference(AssetDatabase.GUIDFromAssetPath(AssetDatabase.GetAssetPath(changeEvent.newValue)).ToString()));
            EditorUtility.SetDirty(target);
            OnChanged?.Invoke();
        });

        root.Add(field);
        return root;
    }

}

}
