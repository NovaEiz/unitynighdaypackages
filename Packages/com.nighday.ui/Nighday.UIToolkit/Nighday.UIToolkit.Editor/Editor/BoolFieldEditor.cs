using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using Nighday.UIToolkit;

namespace Nighday.UIToolkit {

[CustomFieldEditor(typeof(bool))]
public class BoolFieldEditor : CustomFieldEditor {

	public override VisualElement CreateInspector() {
		var root = new VisualElement();
		var field = new Toggle(this.Name);
		field.value = ((bool)fieldInfo.GetValue(obj));

		field.RegisterValueChangedCallback((changeEvent) => {
			fieldInfo.SetValue(obj, changeEvent.newValue);
			EditorUtility.SetDirty((UnityEngine.Object)target);
			OnChanged?.Invoke();
		});
		
		root.Add(field);
		return root;
	}
	
}

}
