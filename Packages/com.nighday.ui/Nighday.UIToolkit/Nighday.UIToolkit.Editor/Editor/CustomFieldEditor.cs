using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.UIElements;

#if UNITY_EDITOR

namespace Nighday.UIToolkit {

public abstract class CustomFieldEditor {//<TValueType> : BaseField<TValueType> {

	public object obj;
	public UnityEngine.Object target;
	public FieldInfo fieldInfo;

	public Action OnChanged;
	
	public abstract VisualElement CreateInspector();

	protected string Name {
		get {
			if (fieldInfo == null) {
				return "";
			}
			var name = UIToolkitEditor.GetVisibleName(fieldInfo.Name);
			return name;
		}
	}

	protected void DrawField(VisualElement root, VisualElement mainRoot, string fieldName, Action onChanged = null) {
		var fieldType = fieldInfo.FieldType;
		var field = fieldType.GetField(fieldName);
		var fieldObj = fieldInfo.GetValue(obj);

		UIToolkitEditor.DrawField(root, mainRoot, field, fieldObj, target, onChanged);

	}

	// protected CustomFieldEditor(string label, VisualElement visualInput) : base(label, visualInput) {
	// 	
	// }

}

}

#endif