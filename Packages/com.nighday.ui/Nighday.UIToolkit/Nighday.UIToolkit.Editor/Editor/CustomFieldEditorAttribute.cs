using System;

#if UNITY_EDITOR

namespace Nighday.UIToolkit {

public class CustomFieldEditorAttribute : Attribute {

	public Type Type;

	public CustomFieldEditorAttribute(Type type) {
		Type = type;
	}
	
}


}

#endif
