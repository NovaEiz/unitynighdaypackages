
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR

// Factory для UIToolkit должен быть в namespace, в котором нет в конце слова .Editor (типа Nighday.UIToolkit.Editor - нельзя)
namespace Nighday.UIToolkit {

using UnityEngine.Scripting;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

public class SimpleListView : VisualElement {

    public Func<VisualElement> makeItem;
    public Action<VisualElement, int> bindItem;
    public Action<VisualElement, int> unbindItem;
    public Func<object> makeEmptySourceItem;

    public event Action OnSizeChanged;

    private ListView _listView;
    
    public IList itemsSource
    {
        get { return _listView.itemsSource; }
        set {
            _listView.itemsSource = value;
            _onSetItemsSource?.Invoke();
        }
    }

    public void Refresh() {
        _listView.Refresh();
    }

    private Action _onSetItemsSource;
    
#region Factory
    
    [Preserve]
    public new class UxmlFactory : UxmlFactory<SimpleListView, UxmlTraits> { }
    
    [Preserve]
    public new class UxmlTraits : VisualElement.UxmlTraits
    {
        UxmlIntAttributeDescription listHeight = new UxmlIntAttributeDescription { name = "listHeight", defaultValue = 100 };
        UxmlIntAttributeDescription itemHeight = new UxmlIntAttributeDescription { name = "itemHeight", defaultValue = 20 };

        
        public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
        {
            get { yield break; }
        }
 
        public override void Init(VisualElement visualElement, IUxmlAttributes bag, CreationContext cc)
        {
            base.Init(visualElement, bag, cc);
            var simpleListView = visualElement as SimpleListView;//Изменить название на лист для редактора
            var hierarchy = visualElement.hierarchy;
            
            simpleListView.Clear();
            
            simpleListView.listHeight = listHeight.GetValueFromBag(bag, cc);
            simpleListView.itemHeight = itemHeight.GetValueFromBag(bag, cc);

            var listCountInputField = new IntegerField(simpleListView.name);
            listCountInputField.name = "Count";
            listCountInputField.style.marginTop = 10;
            hierarchy.Insert(0, listCountInputField);

            // var titleContainer = new VisualElement();
            // titleContainer.name = "Title";
            // titleContainer.style.height = new StyleLength(new Length(simpleListView.titleHeight));
            // hierarchy.Insert(1, titleContainer);
            
            var foldout = new Foldout();
            foldout.text = null;
            foldout.style.marginBottom = 10;
            hierarchy.Insert(1, foldout);

            var listView = new ListView();
            simpleListView._listView = listView;
            listView.itemHeight = simpleListView.itemHeight;
            
            foldout.Add(listView);

            listView.bindItem += (e, i) => {
                simpleListView.bindItem?.Invoke(e, i);
            };
            listView.unbindItem += (e, i) => {
                simpleListView.unbindItem?.Invoke(e, i);
            };
            
            //listView.Refresh();
            // makeItem вызывается снова по всем элементам когда:
            // - список отобразился после того как был скрыт.
            listView.makeItem += () => {
                var newItemView = simpleListView.makeItem();
                return newItemView;
            };

            simpleListView._onSetItemsSource += () => {
                var count = listView.itemsSource.Count;
                listCountInputField.value = count;
                listView.style.height = new StyleLength(new Length(listView.itemHeight * count));
            };
            listCountInputField.RegisterValueChangedCallback((changeEvent) => {
                var newLen = changeEvent.newValue;
                var itemsSource = simpleListView._listView.itemsSource;
                if (itemsSource.Count == newLen) {
                    return;
                }

                while (newLen > itemsSource.Count) {
                    object obj = default;
                    if (simpleListView.makeEmptySourceItem != null) {
                        obj = simpleListView.makeEmptySourceItem();
                    }
                    itemsSource.Add(obj);
                }
                
                while (newLen < itemsSource.Count) {
                    itemsSource.RemoveAt(itemsSource.Count-1);
                }
                
                listView.Refresh();

                simpleListView.OnSizeChanged?.Invoke();
                
            });
            
        }
    }
    
    public int listHeight { get; set; }
    public int itemHeight { get; set; }
    
#endregion

}


}

#endif