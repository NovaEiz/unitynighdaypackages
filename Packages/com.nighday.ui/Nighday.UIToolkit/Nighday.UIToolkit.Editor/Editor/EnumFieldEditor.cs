using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Nighday.UIToolkit {
	
[CustomFieldEditor(typeof(Enum))]
public class EnumFieldEditor : CustomFieldEditor {

    private List<string> GetValuesFromEnum(System.Type enumType) {
        List<string> list = new List<string>();
        System.Type enumUnderlyingType = System.Enum.GetUnderlyingType(enumType);
        System.Array enumValues = System.Enum.GetValues(enumType);
        System.Array enumNames = System.Enum.GetNames(enumType);

        for (int i=0; i < enumValues.Length; i++)
        {
            // Retrieve the value of the ith enum item.
            string name = enumNames.GetValue(i) as string;
            object value = enumValues.GetValue(i);

            // Convert the value to its underlying type (int, byte, long, ...)
            object underlyingValue = System.Convert.ChangeType(value, enumUnderlyingType);
            list.Add(name);
        }

        return list;
    }
    
    private int GetValueByKeyFromEnum(System.Type enumType, string key) {
        System.Type enumUnderlyingType = System.Enum.GetUnderlyingType(enumType);
        System.Array enumValues = System.Enum.GetValues(enumType);
        System.Array enumNames = System.Enum.GetNames(enumType);

        for (int i=0; i < enumValues.Length; i++)
        {
            string name = enumNames.GetValue(i) as string;

            // Retrieve the value of the ith enum item.
            object value = enumValues.GetValue(i);
            if (key == name) {
                // Convert the value to its underlying type (int, byte, long, ...)
                object underlyingValue = System.Convert.ChangeType(value, enumUnderlyingType);
                return (int)underlyingValue;
            }
        }
        return -1;
    }
    private int GetIndexByValueFromEnum(System.Type enumType, int value_) {
        System.Type enumUnderlyingType = System.Enum.GetUnderlyingType(enumType);
        System.Array enumValues = System.Enum.GetValues(enumType);
        System.Array enumNames = System.Enum.GetNames(enumType);

        for (int i=0; i < enumValues.Length; i++)
        {
            string name = enumNames.GetValue(i) as string;

            // Retrieve the value of the ith enum item.
            object value = enumValues.GetValue(i);
            // Convert the value to its underlying type (int, byte, long, ...)
            object underlyingValue = System.Convert.ChangeType(value, enumUnderlyingType);
            if ((int)underlyingValue == value_) {
                return i;
            }
        }
        return -1;
    }
    private int GetIndexByKeyFromEnum(System.Type enumType, string key) {
        System.Type enumUnderlyingType = System.Enum.GetUnderlyingType(enumType);
        System.Array enumValues = System.Enum.GetValues(enumType);
        System.Array enumNames = System.Enum.GetNames(enumType);

        for (int i=0; i < enumValues.Length; i++)
        {
            string name = enumNames.GetValue(i) as string;

            // Retrieve the value of the ith enum item.
            object value = enumValues.GetValue(i);
            // Convert the value to its underlying type (int, byte, long, ...)
            object underlyingValue = System.Convert.ChangeType(value, enumUnderlyingType);
            if (key == name) {
                return i;
            }
        }
        return -1;
    }

    public override VisualElement CreateInspector() {
        var flagsAttribute = UIToolkitEditor.GetAttribute<FlagsEnumFieldAttribute>(fieldInfo);
        var isFlagsField = flagsAttribute != null;

        var root = new VisualElement();

        if (isFlagsField) {
            var field = new MaskField(this.Name, GetValuesFromEnum(fieldInfo.FieldType), (int)(fieldInfo.GetValue(obj)));
            // field.value = (int)fieldInfo.GetValue(obj);

            field.RegisterValueChangedCallback((changeEvent) =>
            {
                fieldInfo.SetValue(obj, (int)changeEvent.newValue);
                EditorUtility.SetDirty((UnityEngine.Object)target);
                OnChanged?.Invoke();
            });
            root.Add(field);

        } else {
            var field = new DropdownField(this.Name, GetValuesFromEnum(fieldInfo.FieldType), GetIndexByValueFromEnum(fieldInfo.FieldType, (int)(fieldInfo.GetValue(obj))));
            // field.value = (int)fieldInfo.GetValue(obj);

            field.RegisterValueChangedCallback((changeEvent) =>
            {
                // fieldInfo.SetValue(obj, GetIndexByValueFromEnum(fieldInfo.FieldType, GetValueByKeyFromEnum(fieldInfo.FieldType, changeEvent.newValue)));
                fieldInfo.SetValue(obj, GetValueByKeyFromEnum(fieldInfo.FieldType, changeEvent.newValue));
                EditorUtility.SetDirty((UnityEngine.Object)target);
                OnChanged?.Invoke();
            });
            root.Add(field);
        }
		
        return root;
    }
	
}

}
