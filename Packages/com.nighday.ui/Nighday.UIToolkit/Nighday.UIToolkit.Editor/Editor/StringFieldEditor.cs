using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using Nighday.UIToolkit;

namespace Nighday.UIToolkit {

[CustomFieldEditor(typeof(string))]
public class StringFieldEditor : CustomFieldEditor {

	public override VisualElement CreateInspector() {
		var root = new VisualElement();
		var field = new TextField(this.Name);
		field.value = ((string)fieldInfo.GetValue(obj));

		// field.RegisterValueChangedCallback((changeEvent) => {
		// 	fieldInfo.SetValue(obj, changeEvent.newValue);
		// 	EditorUtility.SetDirty((UnityEngine.Object)target);
		// 	OnChanged?.Invoke();
		// });

		field.RegisterCallback<FocusOutEvent>((e) => {
			var newValue = field.value;
			fieldInfo.SetValue(obj, newValue);
			EditorUtility.SetDirty((UnityEngine.Object)target);
			OnChanged?.Invoke();
		});

		
		root.Add(field);
		return root;
	}
	
}

}
