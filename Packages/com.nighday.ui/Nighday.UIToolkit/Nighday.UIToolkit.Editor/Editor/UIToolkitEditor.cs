using System.Collections;
using System.Linq;
using UnityEngine.AddressableAssets;

#if UNITY_EDITOR

namespace Nighday.UIToolkit {

using System.Reflection;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public class UIToolkitEditor : Editor {

	protected VisualElement _root;

	public override VisualElement CreateInspectorGUI() {
		_root = new VisualElement();

		Action onChanged = null;
		onChanged = () => {
			_root.Clear();
			DrawInspector(onChanged);
		};
		onChanged();

		return _root;
	}
	
	protected virtual void DrawInspector(Action onChanged) {
		DrawObject(_root, target,
			onChanged: onChanged);
	}

	public static Dictionary<Type, Type> GetTypes() {
		var types = new Dictionary<Type, Type>();
		foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies()) {
			try {
				var assemblyTypes = assembly.GetTypes();
				foreach (var t in assemblyTypes) {
					try {
						object[] attrs = t.GetCustomAttributes(true);
						foreach (object attr in attrs) {
							CustomFieldEditorAttribute customFieldEditorAttribute = attr as CustomFieldEditorAttribute;
							if (customFieldEditorAttribute != null) {
								types.Add(customFieldEditorAttribute.Type, t);
							}
						}
					} catch (Exception e) {
						
					}
				}
			}
			catch (ReflectionTypeLoadException e) {
				Debug.LogError(e);
			}
		}

		return types;
	}

	private static Dictionary<Type, Type> _types;

	private static Dictionary<Type, Type> Types {
		get {
			if (_types == null) {
				_types = GetTypes();
			}

			return _types;
		}
	}

	private static CustomFieldEditor GetCustomFieldEditorBase(object             obj,
	                                                          FieldInfo          field,
	                                                          UnityEngine.Object target,
	                                                          Action             onChanged = null) {
		var type = field != null ? field.FieldType : obj.GetType();
		type = type.BaseType;

		Type ct = null;
		foreach (var t in Types) {
			if (type == t.Key) {
				ct = t.Value;
				break;
			}
		}

		if (ct == null) {
			return null;
		}

		var instance = Activator.CreateInstance(ct) as CustomFieldEditor;
		instance.obj = obj;
		instance.target = target;
		instance.fieldInfo = field;
		instance.OnChanged = onChanged;
		return instance;
	}

	private static CustomFieldEditor GetCustomFieldEditor(object obj, FieldInfo field, UnityEngine.Object target,
	                                                      Action onChanged = null) {
		var type = field != null ? field.FieldType : obj.GetType();

		Type ct = null;
		foreach (var t in Types) {
			if (type == t.Key) {
				ct = t.Value;
				break;
			}
		}

		if (ct == null) {
			return null;
		}

		var instance = Activator.CreateInstance(ct) as CustomFieldEditor;
		instance.obj = obj;
		instance.target = target;
		instance.fieldInfo = field;
		instance.OnChanged = onChanged;
		return instance;
	}

	public static string GetVisibleName(string name_) {
		var name = name_;
		if (name.Substring(0, 1) == "_") {
			name = name.Substring(1, name.Length - 1);
		}

		string fs = name[0].ToString();
		name = fs.ToUpper() + name.Substring(1, name.Length - 1);
		return name;
	}

	private static Attribute[] GetSystemAttributes(Type systemType, Type attributeType) {
		Attribute[] attributes;
		var objArr = systemType.GetCustomAttributes(attributeType, true);
		attributes = new Attribute[objArr.Length];
		for (int i = 0; i < objArr.Length; i++) {
			attributes[i] = objArr[i] as Attribute;
		}

		return attributes;
	}
	private static T GetSystemAttribute<T>(Type systemType)
		where T : Attribute {
		//var attribs = TypeManager.GetSystemAttributes(systemType, typeof(T));
		var attribs = GetSystemAttributes(systemType, typeof(T));
		if (attribs.Length != 1)
			return null;
		return attribs[0] as T;
	}
	
	public static T GetAttribute<T>(MemberInfo sourceMember) where T : Attribute
	{
		object[] attributes = sourceMember.GetCustomAttributes(typeof(T), true);  // Получаем текущий атрибут.

		if (attributes == null || attributes.Length == 0) return default(T); // Если у типа объекта не задан атрибут - возвращаем null.

		return attributes[0] as T;
	}
	
	protected static FieldInfo[] GetHierarchySerializeFields(Type type) {
		FieldInfo[] baseFields = null;
		if (type.BaseType != null) {
			baseFields = GetHierarchySerializeFields(type.BaseType);
		}
		var fields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly)
						.OrderBy(fi => {//OrderByDescending
							return fi.MetadataToken;
						})
						.ToArray();
		if (type.BaseType != null) {
			var list = new List<FieldInfo>();
			list.AddRange(baseFields);
			list.AddRange(fields);

			return list.ToArray();
		} else {
			return fields;
		}
	}

	private static bool HaveBaseType(Type type, Type baseType) {
		if (type == baseType) {
			return true;
		}

		if (type.BaseType == null) {
			return false;
		}

		return HaveBaseType(type.BaseType, baseType);
	}

	public static void DrawField(VisualElement root, VisualElement mainRoot, FieldInfo field, object obj, UnityEngine.Object target, Action onChanged = null) {
			var fieldType = field.FieldType;

			var serializeFieldAttribute = GetAttribute<SerializeField>(field);
			if (field.IsPrivate && serializeFieldAttribute == null) {
				return;
			}
			var hideInInspectorAttribute = GetAttribute<HideInInspector>(field);
			if (hideInInspectorAttribute != null) {
				return;
			}
			
			var spaceAttribute = GetAttribute<SpaceAttribute>(field);
			if (spaceAttribute != null) {
				var spaceField = new VisualElement();
				spaceField.style.height = new StyleLength(spaceAttribute.height);
				root.Add(spaceField);
			}
			var headerAttribute = GetAttribute<HeaderAttribute>(field);
			if (headerAttribute != null) {
				var headerField = new Label(headerAttribute.header);
				headerField.style.height = new StyleLength(30);
				headerField.style.unityTextAlign = new StyleEnum<TextAnchor>(TextAnchor.MiddleCenter);
				root.Add(headerField);
			}
			
			var val = field.GetValue(obj);

			CustomFieldEditor customFieldEditor = GetCustomFieldEditor(obj, field, target, onChanged);
			CustomFieldEditor customFieldEditorBase = GetCustomFieldEditorBase(obj, field, target, onChanged);
			if (customFieldEditor != null) {
				var instanceRoot = customFieldEditor.CreateInspector();
				root.Add(instanceRoot);
			} else if (customFieldEditorBase != null) {
				var instanceRoot = customFieldEditorBase.CreateInspector();
				root.Add(instanceRoot);
			} else {
				if (val is IList list) {
					var len = list.Count;
					
					var listFoldout = new Foldout();
					listFoldout.style.marginTop = 5;
					listFoldout.style.marginBottom = 5;
					root.Add(listFoldout);

					listFoldout.text = GetVisibleName(field.Name);

					var countField = new IntegerField();
					countField.value = len;
					countField.style.right = new StyleLength(0f);
					countField.style.position = new StyleEnum<Position>(Position.Absolute);
					countField.style.width = new StyleLength(50f);
					listFoldout.hierarchy.Add(countField);

					var listItemType = fieldType.GenericTypeArguments.FirstOrDefault();
					if (listItemType == null) {
						listItemType = fieldType.GetElementType();
					}

					var countFieldLastSize = len;

					countField.RegisterCallback<FocusOutEvent>((e) => {
						var currentSize = list.Count;
						var newSize = countFieldLastSize;
						if (newSize > currentSize) {
							if (list.IsFixedSize) {
								var listPrevious = list;
								list = Array.CreateInstance(listItemType, newSize);
								for (var i = 0; i < currentSize; i++) {
									list[i] = listPrevious[i];
								}
								field.SetValue(obj, list);
							} else {
								for (var i = 0; i < (newSize - currentSize); i++) {
									list.Add(Activator.CreateInstance(listItemType));
								}
							}
						} else if (newSize < currentSize) {
							if (list.IsFixedSize) {
								var listPrevious = list;
								list = Array.CreateInstance(listItemType, newSize);
								for (var i = 0; i < newSize; i++) {
									list[i] = listPrevious[i];
								}
								field.SetValue(obj, list);
							} else {
								for (var i = currentSize - 1; i > currentSize - (currentSize - newSize) - 1; i--) {
									list.RemoveAt(i);
								}
							}
						}

						mainRoot.Clear();
						DrawObject(mainRoot, target);
						EditorUtility.SetDirty(target);
					});

					countField.RegisterValueChangedCallback((changeEvent) => { countFieldLastSize = changeEvent.newValue; });

					var listFoldoutContainer = listFoldout.Q("unity-content");

					ColorUtility.TryParseHtmlString("#252525", out Color borderColor);
					ColorUtility.TryParseHtmlString("#414141", out Color backgroundColor);

					listFoldoutContainer.style.backgroundColor = new StyleColor(backgroundColor);

					listFoldoutContainer.style.borderBottomColor = new StyleColor(borderColor);
					listFoldoutContainer.style.borderLeftColor = new StyleColor(borderColor);
					listFoldoutContainer.style.borderRightColor = new StyleColor(borderColor);
					listFoldoutContainer.style.borderTopColor = new StyleColor(borderColor);

					listFoldoutContainer.style.borderBottomWidth = new StyleFloat(1f);
					listFoldoutContainer.style.borderLeftWidth = new StyleFloat(1f);
					listFoldoutContainer.style.borderRightWidth = new StyleFloat(1f);
					listFoldoutContainer.style.borderTopWidth = new StyleFloat(1f);

					listFoldoutContainer.style.marginLeft = new StyleLength(0f);
					if (len > 0) {
						var listItemFields = GetHierarchySerializeFields(listItemType);

						var itemIsAsset = HaveBaseType(listItemType, typeof(UnityEngine.Object));
						var isAssetReference = HaveBaseType(listItemType, typeof(AssetReference));
						
						for (var i = 0; i < len; i++) {
							var index = i;
							var listItem = list[index];
							
							VisualElement listItemContainer = listFoldoutContainer;
							Foldout listItemFoldout = null;
							if (itemIsAsset || isAssetReference) {
							
								var listItemAssetObjectField = new ObjectField();
								listItemAssetObjectField.objectType = listItemType;
								if (isAssetReference) {
									listItemAssetObjectField.objectType = typeof(UnityEngine.Object);
									listItemAssetObjectField.value = (listItem as AssetReference).editorAsset;
								} else {
									listItemAssetObjectField.value = listItem as UnityEngine.Object;
								}
								listItemAssetObjectField.RegisterValueChangedCallback((changeEvent) => {
									listItem = new AssetReference(AssetDatabase.GUIDFromAssetPath(AssetDatabase.GetAssetPath(changeEvent.newValue)).ToString());
									if (isAssetReference) {
										list[index] = listItem;
									} else {
										list[index] = changeEvent.newValue;
									}
									EditorUtility.SetDirty(target);
									onChanged?.Invoke();
								});
								if (listItem == null || isAssetReference) {
									listItemContainer.hierarchy.Add(listItemAssetObjectField);

									continue;
								}
								
								listItemFoldout = new Foldout();
								listFoldoutContainer.Add(listItemFoldout);
								listItemFoldout.style.marginLeft = new StyleLength(15f);
								var listItemFoldoutContainer = listItemFoldout.Q("unity-content");
								listItemFoldoutContainer.hierarchy.Add(listItemAssetObjectField);
							}

							string listItemName = null;

							Action onChanged_ = () => {
								if (listItemFields.Length > 0) {
									var f = listItemFields[0];
									var fValue = f.GetValue(listItem);
									if (fValue is string value) {
										listItemName = value;
									} else if (fValue is UnityEngine.Object valueObj) {
										if (valueObj != null) {
											listItemName = valueObj.name;
										}
									} else if (fValue is int valueInt) {
										listItemName = valueInt.ToString();
									} else if (fValue is uint valueUInt) {
										listItemName = valueUInt.ToString();
									} else if (fValue is long valueLong) {
										listItemName = valueLong.ToString();
									} else if (fValue is ulong valueULong) {
										listItemName = valueULong.ToString();
									} else if (fValue is float valueFloat) {
										listItemName = valueFloat.ToString();
									}
								}

								if (itemIsAsset || isAssetReference) {
									if (listItemName != null && !string.IsNullOrEmpty(listItemName)) {
										listItemFoldout.text = GetVisibleName(listItemName);
									}
								}
							};

							onChanged_();

							Debug.Log("+ list item");
							DrawObject(listItemContainer, listItem, target, mainRoot, onChanged_);
						}
					}
				} else {
					var doubleType = typeof(double);
					if (fieldType == doubleType) { 
					} else if (fieldType.IsClass) {
						switch (fieldType) {
							default:
								var objectField = new ObjectField(GetVisibleName(field.Name));
								objectField.objectType = fieldType;
								if (val != null) {
									objectField.value = (UnityEngine.Object)val;
								}

								root.Add(objectField);

								objectField.RegisterValueChangedCallback((changeEvent) => {
									field.SetValue(obj, changeEvent.newValue);
									EditorUtility.SetDirty(target);
									onChanged?.Invoke();
								});
								break;
						}
					} else {
						Debug.LogError("Не удалось отобразить поле fieldType = " + fieldType + "; field.name = " + field.Name + "; obj = " + obj + "; target = " + target);
					}
				}
			}
	}
	public static void DrawObject(VisualElement root, object obj, UnityEngine.Object target = null,
	                       VisualElement mainRoot = null, Action onChanged = null) {
		if (target == null) {
			target = (UnityEngine.Object)obj;
		}

		if (mainRoot == null) {
			mainRoot = root;
		}

		var type = obj.GetType();
		var fields = GetHierarchySerializeFields(type);

		foreach (var field in fields) {
			DrawField(root, mainRoot, field, obj, target, onChanged);
		}
	}
	             
}

}

#endif