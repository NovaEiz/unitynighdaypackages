using Nighday.UIToolkit;
using System;
using System.Reflection;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public class UIToolkitEditorWindow : EditorWindow
{
	
	protected VisualElement _root;

    public virtual void CreateGUI()
    {

		      //
        // // Each editor window contains a root VisualElement object
        // VisualElement root = rootVisualElement;
        //
        // // VisualElements objects can contain other VisualElement following a tree hierarchy.
        // VisualElement label = new Label("Hello World! From C#");
        // root.Add(label);
        //
        // // Import UXML
        // var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Packages/com.nighday.multiplayer.gamecore.moba/Nighday.GameCore/Nighday.GameCore.Slots.Storages/Nighday.GameCore.Slots.Storages.Editor/UXML/UIToolkitEditorWindow.uxml");
        // VisualElement labelFromUXML = visualTree.Instantiate();
        // root.Add(labelFromUXML);
        //
        // // A stylesheet can be added to a VisualElement.
        // // The style will be applied to the VisualElement and all of its children.
        // var styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Packages/com.nighday.multiplayer.gamecore.moba/Nighday.GameCore/Nighday.GameCore.Slots.Storages/Nighday.GameCore.Slots.Storages.Editor/UXML/UIToolkitEditorWindow.uss");
        // VisualElement labelWithStyle = new Label("Hello World! With Style");
        // labelWithStyle.styleSheets.Add(styleSheet);
        // root.Add(labelWithStyle);
    }
    
    
	public static Dictionary<Type, Type> GetTypes() {
		var types = new Dictionary<Type, Type>();
		foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies()) {
			try {
				var assemblyTypes = assembly.GetTypes();
				foreach (var t in assemblyTypes) {
					object[] attrs = t.GetCustomAttributes(true);
					foreach (object attr in attrs) {
						CustomFieldEditorAttribute customFieldEditorAttribute = attr as CustomFieldEditorAttribute;
						if (customFieldEditorAttribute != null) {
							types.Add(customFieldEditorAttribute.Type, t);
						}
					}
				}
			}
			catch (ReflectionTypeLoadException e) {
				Debug.LogError(e);
			}
		}

		return types;
	}

	private static Dictionary<Type, Type> _types;
	private static Dictionary<Type, Type> Types {
		get {
			if (_types == null) {
				_types = GetTypes();
			}

			return _types;
		}
	}

	private static CustomFieldEditor GetCustomFieldEditor(object obj, FieldInfo field, UnityEngine.Object target) {
		var type = field.FieldType;
		
		Type ct = null;
		foreach (var t in Types) {
			if (type == t.Key) {
				ct = t.Value;
				break;
			}
		}

		if (ct == null) {
			return null;
		}
		
		var instance = Activator.CreateInstance(ct) as CustomFieldEditor;
		instance.obj = obj;
		instance.target = target;
		instance.fieldInfo = field;
		return instance;
	}

	// public static void QWE(VisualElement root, object obj) {
	// 	var type = obj.GetType();
	// 	var fields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
	// 	foreach (var field in fields) {
	// 		// var customFieldEditor = GetCustomFieldEditor(obj, field);
	// 		// if (customFieldEditor != null) {
	// 		// 	var instanceRoot = customFieldEditor.CreateInspector();
	// 		// 	root.Add(instanceRoot);
	// 		// }
	// 	}
	// }
	
}