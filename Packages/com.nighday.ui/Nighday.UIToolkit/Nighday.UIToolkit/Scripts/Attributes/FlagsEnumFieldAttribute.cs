#if UNITY_EDITOR

using System;

namespace Nighday.UIToolkit {

/// <summary><para>Indicates that an enumeration can be treated as a bit field; that is, a set of flags.</para></summary>
[AttributeUsage(AttributeTargets.Field, Inherited = false)]
public class FlagsEnumFieldAttribute : Attribute
{
}

}

#endif