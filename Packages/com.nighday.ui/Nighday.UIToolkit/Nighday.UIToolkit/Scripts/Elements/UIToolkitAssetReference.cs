using UnityEngine;

namespace Nighday.UIToolkit {

#if UNITY_EDITOR
using UnityEditor;
#endif

using System.Threading.Tasks;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;
using UnityEngine.Scripting;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UIElements;

public class UIToolkitAssetReference : VisualElement {

    public T EditorLoadAsset<T>()
        where T : UnityEngine.Object {
#if UNITY_EDITOR
        if (!EditorApplication.isPlaying && !EditorApplication.isPlayingOrWillChangePlaymode) {
            return AssetDatabase.LoadAssetAtPath<T>(path);
        } else {
            return Resources.Load<T>(path);
        }
#else
        return Resources.Load<T>(path);
#endif
    }

    public async Task<T> LoadAssetAsync<T>(string key)
        where T : UnityEngine.Object {
        return await Addressables.LoadAssetAsync<T>(key).Task;
    }
    
#region Factory
    
    [Preserve]
    public new class UxmlFactory : UxmlFactory<UIToolkitAssetReference, UxmlTraits> { }
    
    [Preserve]
    public new class UxmlTraits : VisualElement.UxmlTraits
    {
        UxmlStringAttributeDescription path = new UxmlStringAttributeDescription { name = "path", defaultValue = "" };

        public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
        {
            get { yield break; }
        }
 
        public override void Init(VisualElement visualElement, IUxmlAttributes bag, CreationContext cc)
        {
            base.Init(visualElement, bag, cc);
            var target = visualElement as UIToolkitAssetReference;//Изменить название на лист для редактора
            var hierarchy = visualElement.hierarchy;
            
            target.path = path.GetValueFromBag(bag, cc);
        }
    }
    
    public string path { get; set; }
    
#endregion

}


}

