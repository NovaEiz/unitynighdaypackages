using UnityEngine.UIElements;

namespace Nighday.UIToolkit {

public interface IViewComponentObject {
	VisualElement VisualElement { get; }
}
}