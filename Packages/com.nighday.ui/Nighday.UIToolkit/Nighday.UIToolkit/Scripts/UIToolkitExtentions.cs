// using System.Threading.Tasks;
// using Nighday.TaskExtentions;
// using UnityEngine.AddressableAssets;
// using UnityEngine.UIElements;
//
// namespace Nighday.UIToolkit {
//
// public static class UIToolkitExtentions {
// 	public static Task<(VisualTreeAsset, StyleSheet)> LoadViewWithStyle(string uxmlPath, string ussPath) {
// 		var asyncOperationHandleVisualTreeAsset = Addressables.LoadAssetAsync<VisualTreeAsset>(uxmlPath);
// 		var asyncOperationHandleStyleSheet = Addressables.LoadAssetAsync<StyleSheet>(ussPath);
//
// 		return TaskExt.WhenAll(asyncOperationHandleVisualTreeAsset.Task, asyncOperationHandleStyleSheet.Task);
// 	}
// }
// }