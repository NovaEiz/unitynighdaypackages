// using UnityEngine;
// using UnityEngine.UIElements;
//
// #if UNITY_EDITOR
// using UnityEditor;
// #endif
//
// namespace Nighday.UIToolkit {
//
// [ExecuteInEditMode]
// [RequireComponent(typeof(UIDocument))]
// public class UIToolkit_ElementProgress : MonoBehaviour {
//
// 	[Range(0f, 1f)]
// 	[SerializeField] protected float _progress;
// 	[SerializeField] protected string _elementName;
//
// 	private VisualElement _visualElement;
//
// 	private VisualElement VisualElement {
// 		get {
// 			if (_visualElement == null) {
// 				var document = GetComponent<UIDocument>();
// 				if (document != null) {
// 					var root = document.rootVisualElement;
// 					if (root != null) {
// 						_visualElement = root.Q(_elementName);
// 					}
// 				}
// 			}
//
// 			return _visualElement;
// 		}
// 	}
// 	
// 	private void Awake() {
// 		_visualElement = VisualElement;
// 	}
// 	
// 	public void UpdateView() {
// 		var visualElement = VisualElement;
// 		if (visualElement == null) {
// 			return;
// 		}
// 		visualElement.style.width = new StyleLength(Length.Percent(_progress * 100));
// 	}
//
// 	public void SetProgress(float value) {
// 		_progress = value;
// 	}
// 	
// #if UNITY_EDITOR
//
// 	private void OnValidate() {
// 		if (EditorApplication.isPlayingOrWillChangePlaymode || Application.isPlaying) {
// 			return;
// 		}
// 		UpdateView();
// 	}
//
// #endif
// 	
// }
// }