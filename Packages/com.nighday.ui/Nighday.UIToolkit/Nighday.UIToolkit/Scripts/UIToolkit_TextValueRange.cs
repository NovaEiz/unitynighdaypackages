// using System;
// using UnityEngine;
// using UnityEngine.UIElements;
//
// #if UNITY_EDITOR
// using UnityEditor;
// #endif
//
// namespace Nighday.UIToolkit {
//
// [ExecuteInEditMode]
// [RequireComponent(typeof(UIDocument))]
// public class UIToolkit_TextValueRange : MonoBehaviour {
//
// 	[SerializeField] protected int _healthMin;
// 	[SerializeField] protected int _healthMax;
// 	
// 	[Space]
// 	[Range(0f, 1f)]
// 	[SerializeField] protected float _progress;
// 	[SerializeField] protected string _elementName;
//
// 	private Label _visualElement;
//
// 	private Label VisualElement {
// 		get {
// 			if (_visualElement == null) {
// 				var document = GetComponent<UIDocument>();
// 				if (document != null) {
// 					var root = document.rootVisualElement;
// 					if (root != null) {
// 						_visualElement = root.Q<Label>(_elementName);
// 					}
// 				}
// 			}
// 			
// 			return _visualElement;
// 		}
// 	}
// 	
// 	private void Awake() {
// 		_visualElement = VisualElement;
// 	}
// 	
// 	public void UpdateView() {
// 		var visualElement = VisualElement;
// 		if (visualElement == null) {
// 			return;
// 		}
// 		var currentHealth = (_healthMax - _healthMin) * _progress + _healthMin;
// 		var currentHealthInt = Convert.ToInt32(currentHealth);
// 		visualElement.text = currentHealthInt.ToString();
// 	}
// 	public void SetValueRange(int min, int max) {
// 		_healthMin = min;
// 		_healthMax = max;
// 	}
//
// 	public void SetProgress(float value) {
// 		_progress = value;
// 	}
// 	
// #if UNITY_EDITOR
//
// 	private void OnValidate() {
// 		if (EditorApplication.isPlayingOrWillChangePlaymode || Application.isPlaying) {
// 			return;
// 		}
// 		UpdateView();
// 	}
//
// #endif
// 	
// }
// }