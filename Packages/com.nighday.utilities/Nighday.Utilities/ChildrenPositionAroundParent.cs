using System;
using UnityEngine;

namespace Nighday {

public class ChildrenPositionAroundParent : MonoBehaviour {

    [SerializeField] private float _distance;

    private void OnValidate() {
        var len = transform.childCount;

        var angleStep = 360f / len;
        for (var i = 0; i < len; i++) {
            Transform childTr = transform.GetChild(i);

            var childAngle = angleStep * i;

            var localPosition = Vector3.zero;
            localPosition = Quaternion.Euler(new Vector3(0, childAngle, 0)) * Vector3.forward * -_distance;

            childTr.localPosition = localPosition;
            childTr.localRotation = Quaternion.LookRotation(-childTr.localPosition);
        }
    }

    private void OnTransformChildrenChanged() {
        OnValidate();
    }

}

}