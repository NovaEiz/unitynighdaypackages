using Nighday.VisualScripting;
using UnityEngine;
using Unity.VisualScripting;

namespace Nighday.VisualScripting {

[Descriptor(typeof(InstanceScriptMachine))]
public sealed class FlowMachineDescriptor : MachineDescriptor<InstanceScriptMachine, MachineDescription> {
    public FlowMachineDescriptor(InstanceScriptMachine target) : base(target) { }
}

}