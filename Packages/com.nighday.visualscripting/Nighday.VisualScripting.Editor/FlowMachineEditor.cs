using Nighday.VisualScripting;
using UnityEngine;
using Unity.VisualScripting;

namespace Nighday.VisualScripting {

[Editor(typeof(InstanceScriptMachine))]
public class FlowMachineEditor : MachineEditor {
    public FlowMachineEditor(Metadata metadata) : base(metadata) { }
}

}