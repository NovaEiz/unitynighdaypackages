using Nighday.VisualScripting;
using UnityEngine;
using UnityEditor;
using Unity.VisualScripting;

namespace Nighday.VisualScripting {

[CustomEditor(typeof(InstanceScriptGraphAsset))]
public class InstanceScriptGraphAssetEditor : Editor {

    public override void OnInspectorGUI() {
        this.DrawDefaultInspector();
    }

}

}