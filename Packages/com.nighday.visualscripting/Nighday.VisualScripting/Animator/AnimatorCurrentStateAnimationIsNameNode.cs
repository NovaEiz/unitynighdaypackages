using Unity.VisualScripting;
using UnityEngine;

[assembly: UnityEngine.Scripting.AlwaysLinkAssembly]
namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("Animator.CurrentStateAnimationIsName")]
[UnitCategory("_Nighday/Animator")]
public class AnimatorCurrentStateAnimationIsNameNode : Unit {

    [DoNotSerialize]
    private bool _result;

    [DoNotSerialize]
    public ValueInput animatorValueInput;

    [DoNotSerialize]
    public ValueInput animationNameValueInput;

    [DoNotSerialize]
    public ValueOutput resultValueOutput;

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition() {
        animatorValueInput = ValueInput<object>("animator");
        animationNameValueInput = ValueInput<string>("animationName", null);
        resultValueOutput = ValueOutput<bool>("result", (flow) => { return _result; });

        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        Succession(enter, exit);
    }

    private ControlOutput Trigger(Flow flow) {
        var animatorValue = flow.GetValue<object>(animatorValueInput);
        var animationName = flow.GetValue<string>(animationNameValueInput);

        Animator animator = null;
        if (animatorValue is Animator) {
            animator = animatorValue as Animator;
        } else {
            animator = (animatorValue as GameObject).GetComponent<Animator>();
        }

        _result = animator.GetCurrentAnimatorStateInfo(0)
            .IsName(animationName);

        return exit;
    }

}

}