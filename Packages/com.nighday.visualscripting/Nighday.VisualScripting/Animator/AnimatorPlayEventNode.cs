using System;
using System.Threading.Tasks;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("Animator.Play")]
[UnitCategory("_Nighday/Animator")]
public class AnimatorPlayEventNode : CustomEventUnit<EmptyEventArgs> {
    
    [DoNotSerialize]
    public ValueInput animatorValueInput;

    [DoNotSerialize]
    public ValueInput animationNameValueInput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow           flow,
                                          EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;

    public override EventHook GetHook(GraphReference reference) {
        _hook = new EventHook("AnimatorPlayEvent_" + Guid.NewGuid()
                                  .ToString(),
            reference.machine);

        return _hook;
    }
    //===

    protected override void Definition() {
        animatorValueInput = ValueInput<object>("animator");
        animationNameValueInput = ValueInput<string>("animationName", null);
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        Succession(enter, exit);

        base.Definition();
    }
    
    private ControlOutput Trigger(Flow flow) {
        var animatorValue = flow.GetValue<object>(animatorValueInput);
        var animationName = flow.GetValue<string>(animationNameValueInput);

        Animator animator = null;
        
        if (animatorValue is Animator) {
            animator = animatorValue as Animator;
        } else {
            animator = (animatorValue as GameObject).GetComponent<Animator>();
        }

        if (animator.GetCurrentAnimatorStateInfo(0).IsName(animationName)) {
            return exit;
        }
        animator.Play(animationName);
        Action action = async () => {
            AnimatorStateInfo state = default;
            Debug.Log("state.normalizedTime Step 1 animationName = " + animationName);

            Debug.Log("state.normalizedTime Step 2 state state.normalizedTime = " + state.normalizedTime);
            while (!(state = animator.GetCurrentAnimatorStateInfo(0)).IsName(animationName) && animator.gameObject.activeInHierarchy) {
                Debug.Log("state.normalizedTime wait animationName = " + animationName);

                await Task.Yield();
            }
            while ((state = animator.GetCurrentAnimatorStateInfo(0)).IsName(animationName) && state.normalizedTime < 1f && animator.gameObject.activeInHierarchy) {
                Debug.Log("state.normalizedTime = " + state.normalizedTime + "; animator = " + animator);
                await Task.Yield();
            }
            Debug.Log("state.normalizedTime Step 3");

            // AddressablesMainThread.OnUpdateOneTime += () => {
                try {
                    EventBus.Trigger(_hook);
                } catch (Exception e) {
                    this.SetException(flow.stack, e);
                    UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
                }

                Debug.Log("eeeee 2");
            // };
        };
        action();
        
        return exit;
    }

}

}