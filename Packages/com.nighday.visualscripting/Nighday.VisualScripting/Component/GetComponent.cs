using System;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("System.GetComponent")]
[UnitCategory("_Nighday/System")]
public class GetComponent : Unit {
    
    [DoNotSerialize]
    private Component _component;
    
    [DoNotSerialize]
    public ValueInput targetInput;
    
    [DoNotSerialize]
    public ValueInput componentTypeInput;
    
    [DoNotSerialize]
    public ValueOutput componentOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    protected override void Definition() {
        targetInput = ValueInput<UnityEngine.Object>("target", null);
        componentTypeInput = ValueInput<string>("type");

        componentOutput = ValueOutput<Component>("component",
            (flow) => {
                return _component;
            });

        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        try {
            var target = flow.GetValue<UnityEngine.Object>(targetInput);
            var componentType = flow.GetValue<string>(componentTypeInput);

            _component = target.GetComponent(Type.GetType(componentType));
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }
        
        return exit;
    }


}

}