using System.Collections.Generic;
using System.Collections.ObjectModel;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("Nighday.Switch")]
[UnitCategory("_Nighday/Control")]
public class SwitchNode : Unit {
    
    [DoNotSerialize]
    public ValueInput valueInput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput controlOutputDefault { get; private set; }
    
    [SerializeAs(nameof(inputCount))]
    private int _inputCount = 2;
    
    [DoNotSerialize]
    [Inspectable, InspectorLabel("Args"), UnitHeaderInspectable("Args")]
    public int inputCount
    {
        get => _inputCount;
        set => _inputCount = Mathf.Clamp(value, 1, 100);
    }

    [DoNotSerialize]
    public ReadOnlyCollection<ValueInput> multiValueInputs { get; private set; }

    [DoNotSerialize]
    public ReadOnlyCollection<ControlOutput> multiControlOutputs { get; private set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        
        valueInput = ValueInput<string>("value", null);

        var _multiValueInputs = new List<ValueInput>();
        multiValueInputs = _multiValueInputs.AsReadOnly();
        for (var i = 0; i < inputCount; i++) {
            var valueInput = ValueInput<string>("arg_" + i, null);
            _multiValueInputs.Add(valueInput);
        }

        var _multiControlOutputs = new List<ControlOutput>();
        multiControlOutputs = _multiControlOutputs.AsReadOnly();
        for (var i = 0; i < inputCount; i++) {
            var controlOutput = ControlOutput("case_" + i);
            _multiControlOutputs.Add(controlOutput);
            Succession(enter, controlOutput);
        }
        
        controlOutputDefault = ControlOutput("default");
        Succession(enter, controlOutputDefault);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var value = flow.GetValue<string>(valueInput);
        
        var caseValues = new List<string>();
        for (var i = 0; i < inputCount; i++) {
            var caseValue = flow.GetValue<string>(multiValueInputs[i]);
            caseValues.Add(caseValue);
        }

        var targetIndex = caseValues.IndexOf(value);
        if (targetIndex < 0) {
            return controlOutputDefault;
        }

        return multiControlOutputs[targetIndex];
    }

    
}

}