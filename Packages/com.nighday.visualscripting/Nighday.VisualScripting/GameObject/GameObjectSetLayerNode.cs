using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("GameObject.SetLayer")]
[UnitCategory("_Nighday/GameObject")]
public class GameObjectSetLayerNode : Unit {
    
    [DoNotSerialize]
    public ValueInput gameObjectInput;

    [DoNotSerialize]
    public ValueInput layerInput;

    [DoNotSerialize]
    public ValueInput withChildrenInput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    protected override void Definition() {
        gameObjectInput = ValueInput<GameObject>("gameObject");
        layerInput = ValueInput<int>("layer", 0);
        withChildrenInput = ValueInput<bool>("withChildren", false);
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var gameObject = flow.GetValue<GameObject>(gameObjectInput);
        var layer = flow.GetValue<int>(layerInput);
        var withChildren = flow.GetValue<bool>(withChildrenInput);

        if (withChildren) {
            SetLayerInHierarchy(gameObject, layer);
        } else {
            gameObject.layer = layer;
        }
        
        return exit;
    }

    public static void SetLayerInHierarchy(GameObject go, int layer) {
        go.layer = layer;
        foreach (Transform child in go.transform) {
            SetLayerInHierarchy(child.gameObject, layer);
        }
    }


}

}