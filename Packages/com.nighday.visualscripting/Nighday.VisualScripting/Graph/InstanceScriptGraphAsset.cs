using Nighday.VisualScripting;
using UnityEngine;
using Unity.VisualScripting;

namespace Nighday.VisualScripting {

[TypeIcon(typeof(FlowGraph))]
[CreateAssetMenu(menuName = "Visual Scripting/Instance Script Graph", fileName = "New Instance Script Graph", order = 81)]
public sealed class InstanceScriptGraphAsset : Macro<FlowGraph>
{
    [ContextMenu("Show Data...")]
    protected override void ShowData()
    {
        base.ShowData();
    }

    public override FlowGraph DefaultGraph()
    {
        return FlowGraph.WithInputOutput();
    }
}
}