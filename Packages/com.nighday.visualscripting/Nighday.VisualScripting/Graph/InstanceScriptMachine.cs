using UnityEngine;
using Unity.VisualScripting;

namespace Nighday.VisualScripting {

[AddComponentMenu("Nighday/InstanceScriptMachine")]
[RequireComponent(typeof(Variables))]
[DisableAnnotation]
public class InstanceScriptMachine : EventMachine<FlowGraph, ScriptGraphAsset> {
    
    // public new GraphReference reference => base.reference;

    public override FlowGraph DefaultGraph() {
        return FlowGraph.WithStartUpdate();
    }

    protected override void Awake() {
        ConvertToEmbed();

        base.Awake();
    }

    protected override void OnEnable() {
        if (hasGraph) {
            graph.StartListening(reference);
        }

        base.OnEnable();
    }

    public void ConvertToEmbed() {
        this.nest.source = GraphSource.Embed;
        this.nest.embed = this.nest.macro.graph.CloneViaSerialization();
        this.nest.macro = null;
    }

    protected override void OnInstantiateWhileEnabled() {
        if (hasGraph) {
            graph.StartListening(reference);
        }

        base.OnInstantiateWhileEnabled();
    }

    protected override void OnUninstantiateWhileEnabled() {
        base.OnUninstantiateWhileEnabled();

        if (hasGraph) {
            graph.StopListening(reference);
        }
    }

    protected override void OnDisable() {
        base.OnDisable();

        if (hasGraph) {
            graph.StopListening(reference);
        }
    }

    [ContextMenu("Show Data...")]
    protected override void ShowData() {
        base.ShowData();
    }
}

}