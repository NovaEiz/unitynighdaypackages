using JetBrains.Annotations;

using UnityEngine;
using Unity.VisualScripting;

namespace Nighday.VisualScripting {

    /// <summary>
    /// Set a ScriptGraph to a ScriptMachine
    /// </summary>
    [TypeIcon(typeof(FlowGraph))]
    public sealed class SetScriptGraph : SetGraph<FlowGraph, ScriptGraphAsset, InstanceScriptMachine>
    {
        /// <summary>
        /// The type of object that handles the graph.
        /// </summary>
        [Serialize, Inspectable, UnitHeaderInspectable, UsedImplicitly]
        public ScriptGraphContainerType containerType { get; set; }

        protected override bool isGameObject => containerType == ScriptGraphContainerType.GameObject;
    }
}
