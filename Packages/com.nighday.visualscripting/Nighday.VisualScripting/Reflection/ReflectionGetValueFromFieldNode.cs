using System;
using System.Reflection;
using Unity.VisualScripting;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("Nighday.Reflection.GetValueFromField")]
[UnitCategory("_Nighday/Reflection")]
public class ReflectionGetValueFromFieldNode : Unit {
    
    [DoNotSerialize]
    public object _value;

    [DoNotSerialize]
    public ValueInput objectInput;
    
    [DoNotSerialize]
    public ValueInput fieldNameInput;
    
    [DoNotSerialize]
    public ValueInput valueInput;
    
    [DoNotSerialize]
    public ValueOutput valueOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        objectInput = ValueInput<object>("object", null);
        fieldNameInput = ValueInput<string>("fieldName", null);
        valueOutput = ValueOutput<object>("value_",
            (flow) => {
                return _value;
            });
        
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var obj = flow.GetValue<object>(objectInput);
        var fieldName = flow.GetValue<string>(fieldNameInput);

        try {
            var type = obj.GetType();
            var fieldInfo = type.GetField(fieldName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            _value = fieldInfo.GetValue(obj);
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }
        
        return exit;
    }

    
}

}