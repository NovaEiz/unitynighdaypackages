using System;
using System.Reflection;
using Unity.VisualScripting;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("Nighday.Reflection.SetValueToField")]
[UnitCategory("_Nighday/Reflection")]
public class ReflectionSetValueToFieldNode : Unit {
    
    [DoNotSerialize]
    public object _object;

    [DoNotSerialize]
    public ValueInput objectInput;
    
    [DoNotSerialize]
    public ValueInput fieldNameInput;
    
    [DoNotSerialize]
    public ValueInput valueInput;
    
    [DoNotSerialize]
    public ValueOutput objectOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        objectInput = ValueInput<object>("object", null);
        fieldNameInput = ValueInput<string>("fieldName", null);
        valueInput = ValueInput<object>("value", null);
        objectOutput = ValueOutput<object>("object_",
            (flow) => {
                return _object;
            });
        
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var obj = flow.GetValue<object>(objectInput);
        var fieldName = flow.GetValue<string>(fieldNameInput);
        var value = flow.GetValue<object>(valueInput);

        try {
            var type = obj.GetType();
            var fieldInfo = type.GetField(fieldName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            fieldInfo.SetValue(obj, value);
            _object = obj;
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }
        
        return exit;
    }

    
}

}