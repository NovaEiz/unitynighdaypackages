using Unity.VisualScripting;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("Nighday.System.ConvertToBoolean")]
[UnitCategory("_Nighday/System")]
public class ConvertToBooleanNode : Unit {

    [DoNotSerialize]
    private bool _value;
    
    [DoNotSerialize]
    public ValueInput valueInput;
    
    [DoNotSerialize]
    public ValueOutput valueOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        valueInput = ValueInput<object>("value", null);
        valueOutput = ValueOutput<bool>("value_", (flow) => { return _value; });
        
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var value = flow.GetValue<object>(valueInput);

        if (value != null) {
            if (value is bool booleanValue) {
                _value = booleanValue;
            } else {
                _value = true;
            }
        }

        return exit;
    }

    
}

}