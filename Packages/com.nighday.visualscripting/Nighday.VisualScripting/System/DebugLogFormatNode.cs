using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("Nighday.System.DebugLogFormat")]
[UnitCategory("_Nighday/System")]
public class DebugLogFormatNode : Unit {
    
    [DoNotSerialize]
    public ValueInput formatValueInput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    [SerializeAs(nameof(inputCount))]
    private int _inputCount = 2;
    
    [DoNotSerialize]
    [Inspectable, InspectorLabel("Args"), UnitHeaderInspectable("Args")]
    public int inputCount
    {
        get => _inputCount;
        set => _inputCount = Mathf.Clamp(value, 1, 100);
    }

    [DoNotSerialize]
    public ReadOnlyCollection<ValueInput> multiInputs { get; private set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        formatValueInput = ValueInput<string>("format", null);

        var _multiInputs = new List<ValueInput>();
        multiInputs = _multiInputs.AsReadOnly();
        for (var i = 0; i < inputCount; i++) {
            var valueInput = ValueInput<string>("arg_" + i, null);
            _multiInputs.Add(valueInput);
        }
        
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        try {
            var format = flow.GetValue<string>(formatValueInput);
        
            var args = new List<string>();
            for (var i = 0; i < inputCount; i++) {
                string value = null;
                var valueObj = flow.GetValue<object>(multiInputs[i]);
                if (valueObj is string value_) {
                    value = value_;
                } else {
                    value = "null";
                }
            
                args.Add(value);
            }

            var argsArray = args.ToArray();

            //Этот лог не надо удалять. Не надо!
            Debug.Log(string.Format(format, argsArray), flow.stack.machine.threadSafeGameObject);
        } catch (Exception e) {
            this.SetException(flow.stack, e);
            UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
        }
        
        
        return exit;
    }

    
}

}