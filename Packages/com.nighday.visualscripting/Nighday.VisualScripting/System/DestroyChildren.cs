using System.Collections.Generic;
using System.Collections.ObjectModel;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("Nighday.System.DestroyChildren")]
[UnitCategory("_Nighday/System")]
public class DestroyChildren : Unit {
    
    [DoNotSerialize]
    public ValueInput containerInput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        containerInput = ValueInput<Transform>("container", null);

        
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var container = flow.GetValue<Transform>(containerInput);

        foreach (Transform itemTr in container) {
            GameObject.Destroy(itemTr.gameObject);
        }
        
        return exit;
    }

    
}

}