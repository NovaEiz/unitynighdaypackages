using System;
using Unity.VisualScripting;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("Nighday.System.GenerateGuid")]
[UnitCategory("_Nighday/System")]
public class GenerateGuidNode : Unit {

    [DoNotSerialize]
    public ValueOutput guidOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition()
    {
        guidOutput = ValueOutput<string>("guid", (flow) => { return Guid.NewGuid().ToString(); });
    }

    
}

}