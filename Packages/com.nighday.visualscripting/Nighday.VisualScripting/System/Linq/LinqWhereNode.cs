using System;
using System.Collections;
using System.Collections.Generic;
using Mono.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("Linq.WhereAndSelect")]
[UnitCategory("_Nighday/System/Linq")]
public class LinqWhereNode : MultipleCustomEventUnit<EmptyEventArgs> {
    
    [DoNotSerialize]
    public IList _newList;

    [DoNotSerialize]
    public ValueInput listInput;

    [DoNotSerialize]
    public ValueInput elementWhereResultInput;

    [DoNotSerialize]
    public ValueInput newElementInput;
    
    [DoNotSerialize]
    public ValueOutput currentIndexOutput;
    
    [DoNotSerialize]
    public ValueOutput currentElementOutput;
    
    [DoNotSerialize]
    public ValueOutput newListOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    public ControlInput next { get; private set; }

    [DoNotSerialize]
    private int _listCount;

    [DoNotSerialize]
    private int _currentIndex;

    [DoNotSerialize]
    private object _currentElement;
    
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow           flow,
                                          EmptyEventArgs args) {
        return true;
    }

    [DoNotSerialize]
    [PortLabel("OnElement")]
    public ControlOutput onElement { get; private set; }

    [DoNotSerialize]
    private EventHook _hook_Element;

    public EventHook GetHook_Element(GraphReference reference) {
        _hook_Element = new EventHook("LinqWhere_OnElement_" + Guid.NewGuid()
                                          .ToString(),
            reference.machine);

        return _hook_Element;
    }
    
    [DoNotSerialize]
    [PortLabel("OnEndList")]
    public ControlOutput onEndList { get; private set; }

    [DoNotSerialize]
    private EventHook _hook_EndList;

    public EventHook GetHook_EndList(GraphReference reference) {
        _hook_EndList = new EventHook("LinqWhere_OnEndList_" + Guid.NewGuid()
                                          .ToString(),
            reference.machine);

        return _hook_EndList;
    }
    
    public override void StartListening(GraphStack stack) {
        var data = stack.GetElementData<Data>(this);

        if (data.isListening) {
            return;
        }

        if (register) {
            var reference = stack.ToReference();
            
            data.RegisterEventPoint(GetHook_Element(reference), (args) => { TriggerOnElement(reference, args); });
            data.RegisterEventPoint(GetHook_EndList(reference), (args) => { TriggerOnEndList(reference, args); });
        }

        data.isListening = true;
    }

    public override void StopListening(GraphStack stack) {
        var data = stack.GetElementData<Data>(this);

        if (!data.isListening) {
            return;
        }

        // The coroutine's flow will dispose at the next frame, letting us
        // keep the current flow for clean up operations if needed
        foreach (var activeCoroutine in data.activeCoroutines) {
            activeCoroutine.StopCoroutine(false);
        }

        if (register) {
            data.UnregisterEventPoints();
        }

        data.isListening = false;
    }

    //===

    protected override void Definition() {

        enter = ControlInput(nameof(enter), Trigger);

        next = ControlInput(nameof(next), TriggerNext);

        onElement = ControlOutput(nameof(onElement));
        onEndList = ControlOutput(nameof(onEndList));

        listInput = ValueInput<IList>("list", null);
        elementWhereResultInput = ValueInput<bool>("elementWhereResult", false);
        newElementInput = ValueInput<object>("newElement", null);
        
        currentElementOutput = ValueOutput<object>("element",
            (flow) => {
                return _currentElement;
            });

        currentIndexOutput = ValueOutput<int>("index",
            (flow) => {
                return _currentIndex;
            });

        newListOutput = ValueOutput<IList>("newList",
            (flow) => {
                return _newList;
            });
        
        Succession(next, onElement);
        Succession(next, onEndList);

        base.Definition();
    }

    private ControlOutput Trigger(Flow flow) {
        var list = flow.GetValue<IList>(listInput);
        
        _currentIndex = -1;
        _currentElement = null;
        _listCount = list.Count;
        _newList = new List<object>();

        if (list.Count == 0) {
            return onEndList;
        }

        return Next(list);
    }

    private ControlOutput Next(IList list) {
        if (_currentIndex == _listCount-1) {
            // EventBus.Trigger(_hook_EndList);
            return onEndList;
        }
        _currentIndex++;
        _currentElement = list[_currentIndex];
        
        return onElement;
     }

    private ControlOutput TriggerNext(Flow flow) {
        var list = flow.GetValue<IList>(listInput);
        var elementWhereResult = flow.GetValue<bool>(elementWhereResultInput);

        if (elementWhereResult) {
            var newElement = flow.GetValue<object>(newElementInput);
            _newList.Add(newElement);
        }

        return Next(list);
    }

    public void TriggerOnElement(GraphReference reference,
                                 EmptyEventArgs args) {
        var flow = Flow.New(reference);

        if (!ShouldTrigger(flow, args)) {
            flow.Dispose();
            return;
        }

        AssignArguments(flow, args);

        RunOnElement(flow);
    }
    
    private void RunOnElement(Flow flow) {
        if (flow.enableDebug) {
            var editorData = flow.stack.GetElementDebugData<IUnitDebugData>(this);

            editorData.lastInvokeFrame = EditorTimeBinding.frame;
            editorData.lastInvokeTime = EditorTimeBinding.time;
        }

        if (coroutine) {
            flow.StartCoroutine(onElement,
                flow.stack.GetElementData<Data>(this)
                    .activeCoroutines);
        } else {
            flow.Run(onElement);
        }
    }

    public void TriggerOnEndList(GraphReference reference,
                                 EmptyEventArgs args) {
        var flow = Flow.New(reference);

        if (!ShouldTrigger(flow, args)) {
            flow.Dispose();
            return;
        }

        AssignArguments(flow, args);

        RunOnEndList(flow);
    }
    
    private void RunOnEndList(Flow flow) {
        if (flow.enableDebug) {
            var editorData = flow.stack.GetElementDebugData<IUnitDebugData>(this);

            editorData.lastInvokeFrame = EditorTimeBinding.frame;
            editorData.lastInvokeTime = EditorTimeBinding.time;
        }

        if (coroutine) {
            flow.StartCoroutine(onEndList,
                flow.stack.GetElementData<Data>(this)
                    .activeCoroutines);
        } else {
            flow.Run(onEndList);
        }
    }

}

}