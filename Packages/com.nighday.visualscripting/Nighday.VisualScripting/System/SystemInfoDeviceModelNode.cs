using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("SystemInfo.DeviceModel")]
[UnitCategory("_Nighday/System")]
public class SystemInfoDeviceModelNode : Unit {

    [DoNotSerialize]
    public ValueOutput deviceModelOutput;
    
    protected override void Definition()
    {
        deviceModelOutput = ValueOutput<string>("deviceModel", (flow) => { return SystemInfo.deviceModel; });
    }

    
}

}