using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("Nighday.System.Transform.FindInHierarchy")]
[UnitCategory("_Nighday/System")]
public class TransformFindInHierarchy : Unit {

    [DoNotSerialize]
    private Transform _child;
    
    [PortLabelHidden]
    [DoNotSerialize]
    public ValueInput targetInput;
    
    [DoNotSerialize]
    public ValueInput nameInput;
    
    [PortLabelHidden]
    [DoNotSerialize]
    public ValueOutput childOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        targetInput = ValueInput<Transform>("target", null).NullMeansSelf();
        nameInput = ValueInput<string>("name", null);
        childOutput = ValueOutput<Transform>("child", (flow) => { return _child; });
        
        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var target = flow.GetValue<Transform>(targetInput);
        var name = flow.GetValue<string>(nameInput);

        var transform = target;
        _child = FindInHierarchy(transform, name);

        return exit;
    }

    private Transform FindInHierarchy(Transform transform, string name) {
        foreach (Transform childTr in transform) {
            if (childTr.name == name) {
                return childTr;
            }

            var child = FindInHierarchy(childTr, name);
            if (child != null) {
                return child;
            }
        }

        return null;
    }

    
}

}