using System;
using System.Collections.Generic;
using Mono.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("WaitTasks")]
[UnitCategory("_Nighday/System")]
public class WaitTasksNode : CustomEventUnit<EmptyEventArgs> {

    [DoNotSerialize]
    private int _completedTasksCount;
    
    //===
    [SerializeAs(nameof(outputCount))]
    private int _outputCount = 2;

    [DoNotSerialize]
    [Inspectable, InspectorLabel("Tasks"), UnitHeaderInspectable("Tasks")]
    public int outputCount
    {
        get => _outputCount;
        set => _outputCount = Mathf.Clamp(value, 1, 100);
    }

    [DoNotSerialize]
    public System.Collections.ObjectModel.ReadOnlyCollection<ControlInput> multiInputs { get; private set; }
    //===
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow           flow,
                                          EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;

    public override EventHook GetHook(GraphReference reference) {
        _hook = new EventHook("WaitTasksEvent_" + Guid.NewGuid()
                                  .ToString(),
            reference.machine);

        return _hook;
    }
    //===

    protected override void Definition() {

        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));
        
        var _multiInputs = new List<ControlInput>();

        multiInputs = _multiInputs.AsReadOnly();

        for (var i = 0; i < outputCount; i++)
        {
            var output = ControlInput(i.ToString(), TaskTrigger);
            _multiInputs.Add(output);
        }
        
        Succession(enter, exit);

        base.Definition();
    }
    
    private ControlOutput Trigger(Flow flow) {
        _completedTasksCount = 0;
        
        return exit;
    }
    
    private ControlOutput TaskTrigger(Flow flow) {
        _completedTasksCount++;
        if (_completedTasksCount == _outputCount) {
            try {
                EventBus.Trigger(_hook);
            } catch (Exception e) {
                this.SetException(flow.stack, e);
                UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
            }
        }
        
        return exit;
    }


}

}