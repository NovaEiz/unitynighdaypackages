using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("Button.Click")]
[UnitCategory("_Nighday/UI")]
public class ButtonClickNode : Unit {
    
    [DoNotSerialize]
    public ValueInput buttonValueInput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    protected override void Definition() {
        buttonValueInput = ValueInput<object>("button");
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var buttonObj = flow.GetValue<object>(buttonValueInput);
        
        Button button = null;
        if (buttonObj is Button) {
            button = buttonObj as Button;
        } else {
            var buttonGameObject = buttonObj as GameObject;
            button = buttonGameObject.GetComponent<Button>();
        }
        
        if (button != null) {
            button.onClick.Invoke();
        }
        
        return exit;
    }


}

}