using System;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("Button.OnClick")]
[UnitCategory("_Nighday/UI")]
public class ButtonOnClickEventNode : CustomEventUnit<EmptyEventArgs> {
    
    [DoNotSerialize]
    public object _button;

    [DoNotSerialize]
    public ValueInput buttonValueInput;

    [DoNotSerialize]
    public ValueOutput buttonValueOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow           flow,
                                          EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;

    public override EventHook GetHook(GraphReference reference) {
        _hook = new EventHook("ButtonOnClick_" + Guid.NewGuid()
                                  .ToString(),
            reference.machine);

        return _hook;
    }
    //===

    protected override void Definition() {
        buttonValueInput = ValueInput<object>("button");
        buttonValueOutput = ValueOutput<object>("button",
            (flow) => {
                return _button;
            });
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        Succession(enter, exit);

        base.Definition();
    }
    
    private ControlOutput Trigger(Flow flow) {
        var buttonObj = flow.GetValue<object>(buttonValueInput);

        Button button = null;
        if (buttonObj is Button) {
            button = buttonObj as Button;
        } else {
            var buttonGameObject = buttonObj as GameObject;
            button = buttonGameObject.GetComponent<Button>();
        }
        
        if (button != null) {
            button.onClick.AddListener(() => {
                _button = button;
                try {
                    EventBus.Trigger(_hook);
                } catch (Exception e) {
                    this.SetException(flow.stack, e);
                    UnityEngine.Debug.LogException(e, flow.stack.machine.threadSafeGameObject);
                }
            });
        }
        
        return exit;
    }


}

}