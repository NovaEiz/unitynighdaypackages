using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("Button.SetInteractable")]
[UnitCategory("_Nighday/UI")]
public class ButtonSetInteractableNode : Unit {
    
    [DoNotSerialize]
    public ValueInput buttonValueInput;

    [DoNotSerialize]
    public ValueInput interactableValueInput;
    
    [DoNotSerialize]
    public ValueOutput buttonValueOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    protected override void Definition() {
        buttonValueInput = ValueInput<object>("button");
        interactableValueInput = ValueInput<bool>("interactable", false);
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var buttonObj = flow.GetValue<object>(buttonValueInput);
        var interactable = flow.GetValue<bool>(interactableValueInput);
        
        Button button = null;
        if (buttonObj is Button) {
            button = buttonObj as Button;
        } else {
            var buttonGameObject = buttonObj as GameObject;
            button = buttonGameObject.GetComponent<Button>();
        }
        
        if (button != null) {
            button.interactable = interactable;
        }
        
        return exit;
    }


}

}