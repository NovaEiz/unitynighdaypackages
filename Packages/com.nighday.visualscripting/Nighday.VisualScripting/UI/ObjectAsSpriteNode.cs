using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("ObjectAsSprite")]
[UnitCategory("_Nighday/UI")]
public class ObjectAsSpriteNode : Unit {
    
    [DoNotSerialize]
    private Sprite _sprite;
    
    [DoNotSerialize]
    public ValueInput objectInput;
    
    [DoNotSerialize]
    public ValueOutput spriteOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    protected override void Definition() {
        objectInput = ValueInput<object>("object");
        spriteOutput = ValueOutput<Sprite>("sprite",
            (flow) => {
                return _sprite;
            });
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var obj = flow.GetValue<object>(objectInput);

        _sprite = obj as Sprite;
        
        return exit;
    }


}

}