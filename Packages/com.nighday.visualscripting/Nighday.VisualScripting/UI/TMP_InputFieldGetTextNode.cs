using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("TMP_InputField.GetText")]
[UnitCategory("_Nighday/UI")]
public class TMP_InputFieldGetTextNode : Unit {
    
    [DoNotSerialize]
    private string _text;
    
    [DoNotSerialize]
    public ValueInput inputFieldInput;
    
    [DoNotSerialize]
    public ValueOutput textOutput;
    
    protected override void Definition() {
        inputFieldInput = ValueInput<object>("inputField");
        textOutput = ValueOutput<string>("text",
            (flow) => {
                var obj = flow.GetValue<object>(inputFieldInput);

                TMP_InputField inputField = null;
                if (obj is Button) {
                    inputField = obj as TMP_InputField;
                } else {
                    var gameObject = obj as GameObject;
                    inputField = gameObject.GetComponent<TMP_InputField>();
                }
        
                if (inputField != null) {
                    _text = inputField.text;
                }

                return _text;
            });
    }

}

}