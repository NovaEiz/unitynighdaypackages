using TMPro;
using Unity.VisualScripting;
using UnityEngine;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("TMP_Text.SetText")]
[UnitCategory("_Nighday/UI")]
public class TMP_TextSetText : Unit {
    
    [DoNotSerialize]
    public ValueInput tmp_TextInput;
    
    [DoNotSerialize]
    public ValueInput textInput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    protected override void Definition() {
        tmp_TextInput = ValueInput<object>("tmp_Text");
        textInput = ValueInput<string>("text", null);
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        Succession(enter, exit);
    }
    
    private ControlOutput Trigger(Flow flow) {
        var tmp_TextObj = flow.GetValue<object>(tmp_TextInput);
        var text = flow.GetValue<string>(textInput);
        
        var tmp_Text = tmp_TextObj as TMP_Text;
        if (tmp_Text == null) {
            Transform tmp_TextTransform = null;
            var tmp_TextGameObject = tmp_TextObj as GameObject;
            if (tmp_TextGameObject != null) {
                tmp_TextTransform = tmp_TextGameObject.transform;
            } else {
                tmp_TextTransform = tmp_TextObj as Transform;
            }

            if (tmp_TextTransform != null) {
                tmp_Text = tmp_TextTransform.GetComponent<TMP_Text>();
            }
        }

        tmp_Text.SetText(text);
        
        return exit;
    }


}

}