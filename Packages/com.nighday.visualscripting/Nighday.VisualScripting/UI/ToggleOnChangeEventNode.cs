using System;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace Nighday.VisualScripting {

[UnityEngine.Scripting.Preserve]
[UnitTitle("Toggle.OnChange")]
[UnitCategory("_Nighday/UI")]
public class ToggleOnChangeEventNode : CustomEventUnit<EmptyEventArgs> {
    
    [DoNotSerialize]
    public object _toggle;
    
    [DoNotSerialize]
    public bool _value;
    
    [DoNotSerialize]
    public ValueInput toggleValueInput;

    [DoNotSerialize]
    public ValueOutput toggleValueOutput;

    [DoNotSerialize]
    public ValueOutput valueOutput;
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }
    
    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }
    
    protected override bool register => true;

    protected override bool ShouldTrigger(Flow           flow,
                                          EmptyEventArgs args) {
        return true;
    }

    private EventHook _hook;

    public override EventHook GetHook(GraphReference reference) {
        _hook = new EventHook("ToggleOnChange_" + Guid.NewGuid()
                                  .ToString(),
            reference.machine);

        return _hook;
    }
    //===

    protected override void Definition() {
        toggleValueInput = ValueInput<object>("toggle");
        toggleValueOutput = ValueOutput<object>("toggle",
            (flow) => {
                return _toggle;
            });
        valueOutput = ValueOutput<bool>("value",
            (flow) => {
                return _value;
            });
        
        enter = ControlInput(nameof(enter), Trigger);
        exit = ControlOutput(nameof(exit));

        Succession(enter, exit);

        base.Definition();
    }
    
    private ControlOutput Trigger(Flow flow) {
        var toggleObj = flow.GetValue<object>(toggleValueInput);

        Toggle toggle = null;
        if (toggleObj is Toggle) {
            toggle = toggle as Toggle;
        } else {
            var toggleGameObject = toggleObj as GameObject;
            toggle = toggleGameObject.GetComponent<Toggle>();
        }
        
        if (toggle != null) {
            toggle.onValueChanged.AddListener((newValue) => {
                _value = newValue;
                _toggle = toggle;
                EventBus.Trigger(_hook);
            });
        }
        
        return exit;
    }


}

}